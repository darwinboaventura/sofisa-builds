/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginFormComponent } from './login-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { InputModule, ButtonModule, CheckboxModule } from 'sofisa-ui';
export class LoginFormModule {
}
LoginFormModule.decorators = [
    { type: NgModule, args: [{
                declarations: [LoginFormComponent],
                imports: [
                    CommonModule,
                    InputModule,
                    ButtonModule,
                    CheckboxModule,
                    ReactiveFormsModule
                ],
                exports: [LoginFormComponent]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4tZm9ybS5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9zb2Zpc2EtbG9naW4vIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9sb2dpbi1mb3JtL2xvZ2luLWZvcm0ubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsUUFBUSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ3ZDLE9BQU8sRUFBQyxZQUFZLEVBQUMsTUFBTSxpQkFBaUIsQ0FBQztBQUM3QyxPQUFPLEVBQUMsa0JBQWtCLEVBQUMsTUFBTSx3QkFBd0IsQ0FBQztBQUMxRCxPQUFPLEVBQUMsbUJBQW1CLEVBQUMsTUFBTSxnQkFBZ0IsQ0FBQztBQUNuRCxPQUFPLEVBQUMsV0FBVyxFQUFFLFlBQVksRUFBRSxjQUFjLEVBQUMsTUFBTSxXQUFXLENBQUM7QUFjcEUsTUFBTSxPQUFPLGVBQWU7OztZQVozQixRQUFRLFNBQUM7Z0JBQ1QsWUFBWSxFQUFFLENBQUMsa0JBQWtCLENBQUM7Z0JBQ2xDLE9BQU8sRUFBRTtvQkFDUixZQUFZO29CQUNaLFdBQVc7b0JBQ1gsWUFBWTtvQkFDWixjQUFjO29CQUNkLG1CQUFtQjtpQkFDbkI7Z0JBQ0QsT0FBTyxFQUFFLENBQUMsa0JBQWtCLENBQUM7YUFDN0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge05nTW9kdWxlfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7Q29tbW9uTW9kdWxlfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHtMb2dpbkZvcm1Db21wb25lbnR9IGZyb20gJy4vbG9naW4tZm9ybS5jb21wb25lbnQnO1xuaW1wb3J0IHtSZWFjdGl2ZUZvcm1zTW9kdWxlfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQge0lucHV0TW9kdWxlLCBCdXR0b25Nb2R1bGUsIENoZWNrYm94TW9kdWxlfSBmcm9tICdzb2Zpc2EtdWknO1xuXG5ATmdNb2R1bGUoe1xuXHRkZWNsYXJhdGlvbnM6IFtMb2dpbkZvcm1Db21wb25lbnRdLFxuXHRpbXBvcnRzOiBbXG5cdFx0Q29tbW9uTW9kdWxlLFxuXHRcdElucHV0TW9kdWxlLFxuXHRcdEJ1dHRvbk1vZHVsZSxcblx0XHRDaGVja2JveE1vZHVsZSxcblx0XHRSZWFjdGl2ZUZvcm1zTW9kdWxlXG5cdF0sXG5cdGV4cG9ydHM6IFtMb2dpbkZvcm1Db21wb25lbnRdXG59KVxuXG5leHBvcnQgY2xhc3MgTG9naW5Gb3JtTW9kdWxlIHsgfVxuIl19