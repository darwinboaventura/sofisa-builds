(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/common'), require('@angular/core'), require('@angular/forms'), require('sofisa-ui')) :
    typeof define === 'function' && define.amd ? define('sofisa-login', ['exports', '@angular/common', '@angular/core', '@angular/forms', 'sofisa-ui'], factory) :
    (factory((global['sofisa-login'] = {}),global.ng.common,global.ng.core,global.ng.forms,global.sofisaUi));
}(this, (function (exports,common,core,forms,sofisaUi) { 'use strict';

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var LoginFormComponent = /** @class */ (function () {
        function LoginFormComponent(fb) {
            this.fb = fb;
            this.wasSubmited = new core.EventEmitter();
            this.state = {
                form: {
                    formGroup: this.fb.group({
                        username: [null, forms.Validators.required],
                        password: [null, forms.Validators.required]
                    }),
                    validations: {
                        username: {
                            type: '',
                            message: ''
                        },
                        password: {
                            type: '',
                            message: ''
                        }
                    },
                    wasSubmitted: false
                }
            };
        }
        /**
         * @param {?} value
         * @param {?} name
         * @return {?}
         */
        LoginFormComponent.prototype.updateFieldValue = /**
         * @param {?} value
         * @param {?} name
         * @return {?}
         */
            function (value, name) {
                if (value) {
                    this.state.form.formGroup.get(name).setValue(value);
                    this.state.form.validations[name].type = '';
                    this.state.form.validations[name].message = '';
                }
                else if (this.state.form.wasSubmitted) {
                    this.state.form.validations[name].type = 'error';
                    this.state.form.validations[name].message = 'Campo obrigatório';
                }
            };
        /**
         * @param {?} e
         * @return {?}
         */
        LoginFormComponent.prototype.handleForm = /**
         * @param {?} e
         * @return {?}
         */
            function (e) {
                this.state.form.wasSubmitted = true;
                if (this.state.form.formGroup.valid) {
                    this.wasSubmited.emit(this.state.form.formGroup.value);
                }
            };
        LoginFormComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'so-login-form',
                        template: "<form [formGroup]=\"state.form.formGroup\">\n\t<img src=\"assets/images/sofisa-logo.png\" alt=\"\" class=\"so-pg-login--form--logo\">\n\t\n\t<so-input label=\"Usu\u00E1rio\" placeholder=\"Digite seu usu\u00E1rio\" [inputConfig]=\" { validation: { type: state.form.validations.username.type, message: state.form.validations.username.message} }\" (hasChanged)=\"updateFieldValue($event, 'username')\"></so-input>\n\t\n\t<so-input label=\"Senha\" placeholder=\"Digite sua senha\" inputType=\"password\" [inputConfig]=\" { validation: { type: state.form.validations.password.type, message: state.form.validations.password.message} }\" (hasChanged)=\"updateFieldValue($event, 'password')\"></so-input>\n\t\n\t<so-checkbox label=\"Lembrar usu\u00E1rio\" [checked]=\"true\"></so-checkbox>\n\t\n\t<div class=\"clearfix\"></div>\n\t\n\t<so-button type=\"primary\" text=\"Entrar\" (hasClicked)=\"handleForm($event)\"></so-button>\n</form>",
                        styles: [""]
                    }] }
        ];
        /** @nocollapse */
        LoginFormComponent.ctorParameters = function () {
            return [
                { type: forms.FormBuilder }
            ];
        };
        LoginFormComponent.propDecorators = {
            wasSubmited: [{ type: core.Output }]
        };
        return LoginFormComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var LoginFormModule = /** @class */ (function () {
        function LoginFormModule() {
        }
        LoginFormModule.decorators = [
            { type: core.NgModule, args: [{
                        declarations: [LoginFormComponent],
                        imports: [
                            common.CommonModule,
                            sofisaUi.InputModule,
                            sofisaUi.ButtonModule,
                            sofisaUi.CheckboxModule,
                            forms.ReactiveFormsModule
                        ],
                        exports: [LoginFormComponent]
                    },] }
        ];
        return LoginFormModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    exports.LoginFormModule = LoginFormModule;
    exports.ɵa = LoginFormComponent;

    Object.defineProperty(exports, '__esModule', { value: true });

})));

//# sourceMappingURL=sofisa-login.umd.js.map