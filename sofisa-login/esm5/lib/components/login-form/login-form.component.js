/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, EventEmitter, Output } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
var LoginFormComponent = /** @class */ (function () {
    function LoginFormComponent(fb) {
        this.fb = fb;
        this.wasSubmited = new EventEmitter();
        this.state = {
            form: {
                formGroup: this.fb.group({
                    username: [null, Validators.required],
                    password: [null, Validators.required]
                }),
                validations: {
                    username: {
                        type: '',
                        message: ''
                    },
                    password: {
                        type: '',
                        message: ''
                    }
                },
                wasSubmitted: false
            }
        };
    }
    /**
     * @param {?} value
     * @param {?} name
     * @return {?}
     */
    LoginFormComponent.prototype.updateFieldValue = /**
     * @param {?} value
     * @param {?} name
     * @return {?}
     */
    function (value, name) {
        if (value) {
            this.state.form.formGroup.get(name).setValue(value);
            this.state.form.validations[name].type = '';
            this.state.form.validations[name].message = '';
        }
        else if (this.state.form.wasSubmitted) {
            this.state.form.validations[name].type = 'error';
            this.state.form.validations[name].message = 'Campo obrigatório';
        }
    };
    /**
     * @param {?} e
     * @return {?}
     */
    LoginFormComponent.prototype.handleForm = /**
     * @param {?} e
     * @return {?}
     */
    function (e) {
        this.state.form.wasSubmitted = true;
        if (this.state.form.formGroup.valid) {
            this.wasSubmited.emit(this.state.form.formGroup.value);
        }
    };
    LoginFormComponent.decorators = [
        { type: Component, args: [{
                    selector: 'so-login-form',
                    template: "<form [formGroup]=\"state.form.formGroup\">\n\t<img src=\"assets/images/sofisa-logo.png\" alt=\"\" class=\"so-pg-login--form--logo\">\n\t\n\t<so-input label=\"Usu\u00E1rio\" placeholder=\"Digite seu usu\u00E1rio\" [inputConfig]=\" { validation: { type: state.form.validations.username.type, message: state.form.validations.username.message} }\" (hasChanged)=\"updateFieldValue($event, 'username')\"></so-input>\n\t\n\t<so-input label=\"Senha\" placeholder=\"Digite sua senha\" inputType=\"password\" [inputConfig]=\" { validation: { type: state.form.validations.password.type, message: state.form.validations.password.message} }\" (hasChanged)=\"updateFieldValue($event, 'password')\"></so-input>\n\t\n\t<so-checkbox label=\"Lembrar usu\u00E1rio\" [checked]=\"true\"></so-checkbox>\n\t\n\t<div class=\"clearfix\"></div>\n\t\n\t<so-button type=\"primary\" text=\"Entrar\" (hasClicked)=\"handleForm($event)\"></so-button>\n</form>",
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    LoginFormComponent.ctorParameters = function () { return [
        { type: FormBuilder }
    ]; };
    LoginFormComponent.propDecorators = {
        wasSubmited: [{ type: Output }]
    };
    return LoginFormComponent;
}());
export { LoginFormComponent };
if (false) {
    /** @type {?} */
    LoginFormComponent.prototype.wasSubmited;
    /** @type {?} */
    LoginFormComponent.prototype.state;
    /** @type {?} */
    LoginFormComponent.prototype.fb;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4tZm9ybS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9zb2Zpc2EtbG9naW4vIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9sb2dpbi1mb3JtL2xvZ2luLWZvcm0uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsU0FBUyxFQUFFLFlBQVksRUFBRSxNQUFNLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDOUQsT0FBTyxFQUFFLFdBQVcsRUFBRSxVQUFVLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUd6RDtJQTZCQyw0QkFBbUIsRUFBZTtRQUFmLE9BQUUsR0FBRixFQUFFLENBQWE7UUF0QnhCLGdCQUFXLEdBQXNCLElBQUksWUFBWSxFQUFFLENBQUM7UUFFOUQsVUFBSyxHQUFRO1lBQ1osSUFBSSxFQUFFO2dCQUNMLFNBQVMsRUFBRSxJQUFJLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztvQkFDeEIsUUFBUSxFQUFFLENBQUMsSUFBSSxFQUFFLFVBQVUsQ0FBQyxRQUFRLENBQUM7b0JBQ3JDLFFBQVEsRUFBRSxDQUFDLElBQUksRUFBRSxVQUFVLENBQUMsUUFBUSxDQUFDO2lCQUNyQyxDQUFDO2dCQUNGLFdBQVcsRUFBRTtvQkFDWixRQUFRLEVBQUU7d0JBQ1QsSUFBSSxFQUFFLEVBQUU7d0JBQ1IsT0FBTyxFQUFFLEVBQUU7cUJBQ1g7b0JBQ0QsUUFBUSxFQUFFO3dCQUNULElBQUksRUFBRSxFQUFFO3dCQUNSLE9BQU8sRUFBRSxFQUFFO3FCQUNYO2lCQUNEO2dCQUNELFlBQVksRUFBRSxLQUFLO2FBQ25CO1NBQ0QsQ0FBQztJQUVtQyxDQUFDOzs7Ozs7SUFFdEMsNkNBQWdCOzs7OztJQUFoQixVQUFpQixLQUFVLEVBQUUsSUFBUztRQUNyQyxJQUFJLEtBQUssRUFBRTtZQUNWLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBRXBELElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDO1lBQzVDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFDO1NBQy9DO2FBQU0sSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUU7WUFDeEMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksR0FBRyxPQUFPLENBQUM7WUFDakQsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDLE9BQU8sR0FBRyxtQkFBbUIsQ0FBQztTQUNoRTtJQUNGLENBQUM7Ozs7O0lBRUQsdUNBQVU7Ozs7SUFBVixVQUFXLENBQU07UUFDaEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztRQUVwQyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUU7WUFDcEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3ZEO0lBQ0YsQ0FBQzs7Z0JBakRELFNBQVMsU0FBQztvQkFDVixRQUFRLEVBQUUsZUFBZTtvQkFDekIsNDZCQUEwQzs7aUJBRTFDOzs7O2dCQVBRLFdBQVc7Ozs4QkFVbEIsTUFBTTs7SUEyQ1IseUJBQUM7Q0FBQSxBQWxERCxJQWtEQztTQTVDWSxrQkFBa0I7OztJQUM5Qix5Q0FBOEQ7O0lBRTlELG1DQWtCRTs7SUFFVSxnQ0FBc0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgRXZlbnRFbWl0dGVyLCBPdXRwdXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRm9ybUJ1aWxkZXIsIFZhbGlkYXRvcnMgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQgeyBBdXRoU2VydmljZSB9IGZyb20gJ3NvZmlzYS1jb3JlJztcblxuQENvbXBvbmVudCh7XG5cdHNlbGVjdG9yOiAnc28tbG9naW4tZm9ybScsXG5cdHRlbXBsYXRlVXJsOiAnLi9sb2dpbi1mb3JtLmNvbXBvbmVudC5odG1sJyxcblx0c3R5bGVVcmxzOiBbJy4vbG9naW4tZm9ybS5jb21wb25lbnQuc2NzcyddXG59KVxuXG5leHBvcnQgY2xhc3MgTG9naW5Gb3JtQ29tcG9uZW50IHtcblx0QE91dHB1dCgpIHdhc1N1Ym1pdGVkOiBFdmVudEVtaXR0ZXI8YW55PiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcblx0XG5cdHN0YXRlOiBhbnkgPSB7XG5cdFx0Zm9ybToge1xuXHRcdFx0Zm9ybUdyb3VwOiB0aGlzLmZiLmdyb3VwKHtcblx0XHRcdFx0dXNlcm5hbWU6IFtudWxsLCBWYWxpZGF0b3JzLnJlcXVpcmVkXSxcblx0XHRcdFx0cGFzc3dvcmQ6IFtudWxsLCBWYWxpZGF0b3JzLnJlcXVpcmVkXVxuXHRcdFx0fSksXG5cdFx0XHR2YWxpZGF0aW9uczoge1xuXHRcdFx0XHR1c2VybmFtZToge1xuXHRcdFx0XHRcdHR5cGU6ICcnLFxuXHRcdFx0XHRcdG1lc3NhZ2U6ICcnXG5cdFx0XHRcdH0sXG5cdFx0XHRcdHBhc3N3b3JkOiB7XG5cdFx0XHRcdFx0dHlwZTogJycsXG5cdFx0XHRcdFx0bWVzc2FnZTogJydcblx0XHRcdFx0fVxuXHRcdFx0fSxcblx0XHRcdHdhc1N1Ym1pdHRlZDogZmFsc2Vcblx0XHR9XG5cdH07XG5cdFxuXHRjb25zdHJ1Y3RvcihwdWJsaWMgZmI6IEZvcm1CdWlsZGVyKSB7fVxuXHRcblx0dXBkYXRlRmllbGRWYWx1ZSh2YWx1ZTogYW55LCBuYW1lOiBhbnkpIHtcblx0XHRpZiAodmFsdWUpIHtcblx0XHRcdHRoaXMuc3RhdGUuZm9ybS5mb3JtR3JvdXAuZ2V0KG5hbWUpLnNldFZhbHVlKHZhbHVlKTtcblx0XHRcdFxuXHRcdFx0dGhpcy5zdGF0ZS5mb3JtLnZhbGlkYXRpb25zW25hbWVdLnR5cGUgPSAnJztcblx0XHRcdHRoaXMuc3RhdGUuZm9ybS52YWxpZGF0aW9uc1tuYW1lXS5tZXNzYWdlID0gJyc7XG5cdFx0fSBlbHNlIGlmICh0aGlzLnN0YXRlLmZvcm0ud2FzU3VibWl0dGVkKSB7XG5cdFx0XHR0aGlzLnN0YXRlLmZvcm0udmFsaWRhdGlvbnNbbmFtZV0udHlwZSA9ICdlcnJvcic7XG5cdFx0XHR0aGlzLnN0YXRlLmZvcm0udmFsaWRhdGlvbnNbbmFtZV0ubWVzc2FnZSA9ICdDYW1wbyBvYnJpZ2F0w7NyaW8nO1xuXHRcdH1cblx0fVxuXHRcblx0aGFuZGxlRm9ybShlOiBhbnkpIHtcblx0XHR0aGlzLnN0YXRlLmZvcm0ud2FzU3VibWl0dGVkID0gdHJ1ZTtcblx0XHRcblx0XHRpZiAodGhpcy5zdGF0ZS5mb3JtLmZvcm1Hcm91cC52YWxpZCkge1xuXHRcdFx0dGhpcy53YXNTdWJtaXRlZC5lbWl0KHRoaXMuc3RhdGUuZm9ybS5mb3JtR3JvdXAudmFsdWUpO1xuXHRcdH1cblx0fVxufVxuIl19