/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginFormComponent } from './login-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { InputModule, ButtonModule, CheckboxModule } from 'sofisa-ui';
var LoginFormModule = /** @class */ (function () {
    function LoginFormModule() {
    }
    LoginFormModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [LoginFormComponent],
                    imports: [
                        CommonModule,
                        InputModule,
                        ButtonModule,
                        CheckboxModule,
                        ReactiveFormsModule
                    ],
                    exports: [LoginFormComponent]
                },] }
    ];
    return LoginFormModule;
}());
export { LoginFormModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4tZm9ybS5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9zb2Zpc2EtbG9naW4vIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9sb2dpbi1mb3JtL2xvZ2luLWZvcm0ubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsUUFBUSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ3ZDLE9BQU8sRUFBQyxZQUFZLEVBQUMsTUFBTSxpQkFBaUIsQ0FBQztBQUM3QyxPQUFPLEVBQUMsa0JBQWtCLEVBQUMsTUFBTSx3QkFBd0IsQ0FBQztBQUMxRCxPQUFPLEVBQUMsbUJBQW1CLEVBQUMsTUFBTSxnQkFBZ0IsQ0FBQztBQUNuRCxPQUFPLEVBQUMsV0FBVyxFQUFFLFlBQVksRUFBRSxjQUFjLEVBQUMsTUFBTSxXQUFXLENBQUM7QUFFcEU7SUFBQTtJQVkrQixDQUFDOztnQkFaL0IsUUFBUSxTQUFDO29CQUNULFlBQVksRUFBRSxDQUFDLGtCQUFrQixDQUFDO29CQUNsQyxPQUFPLEVBQUU7d0JBQ1IsWUFBWTt3QkFDWixXQUFXO3dCQUNYLFlBQVk7d0JBQ1osY0FBYzt3QkFDZCxtQkFBbUI7cUJBQ25CO29CQUNELE9BQU8sRUFBRSxDQUFDLGtCQUFrQixDQUFDO2lCQUM3Qjs7SUFFOEIsc0JBQUM7Q0FBQSxBQVpoQyxJQVlnQztTQUFuQixlQUFlIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtOZ01vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0NvbW1vbk1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7TG9naW5Gb3JtQ29tcG9uZW50fSBmcm9tICcuL2xvZ2luLWZvcm0uY29tcG9uZW50JztcbmltcG9ydCB7UmVhY3RpdmVGb3Jtc01vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuaW1wb3J0IHtJbnB1dE1vZHVsZSwgQnV0dG9uTW9kdWxlLCBDaGVja2JveE1vZHVsZX0gZnJvbSAnc29maXNhLXVpJztcblxuQE5nTW9kdWxlKHtcblx0ZGVjbGFyYXRpb25zOiBbTG9naW5Gb3JtQ29tcG9uZW50XSxcblx0aW1wb3J0czogW1xuXHRcdENvbW1vbk1vZHVsZSxcblx0XHRJbnB1dE1vZHVsZSxcblx0XHRCdXR0b25Nb2R1bGUsXG5cdFx0Q2hlY2tib3hNb2R1bGUsXG5cdFx0UmVhY3RpdmVGb3Jtc01vZHVsZVxuXHRdLFxuXHRleHBvcnRzOiBbTG9naW5Gb3JtQ29tcG9uZW50XVxufSlcblxuZXhwb3J0IGNsYXNzIExvZ2luRm9ybU1vZHVsZSB7IH1cbiJdfQ==