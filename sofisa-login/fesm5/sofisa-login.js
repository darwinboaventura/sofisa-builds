import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Output, NgModule } from '@angular/core';
import { FormBuilder, Validators, ReactiveFormsModule } from '@angular/forms';
import { InputModule, ButtonModule, CheckboxModule } from 'sofisa-ui';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var LoginFormComponent = /** @class */ (function () {
    function LoginFormComponent(fb) {
        this.fb = fb;
        this.wasSubmited = new EventEmitter();
        this.state = {
            form: {
                formGroup: this.fb.group({
                    username: [null, Validators.required],
                    password: [null, Validators.required]
                }),
                validations: {
                    username: {
                        type: '',
                        message: ''
                    },
                    password: {
                        type: '',
                        message: ''
                    }
                },
                wasSubmitted: false
            }
        };
    }
    /**
     * @param {?} value
     * @param {?} name
     * @return {?}
     */
    LoginFormComponent.prototype.updateFieldValue = /**
     * @param {?} value
     * @param {?} name
     * @return {?}
     */
    function (value, name) {
        if (value) {
            this.state.form.formGroup.get(name).setValue(value);
            this.state.form.validations[name].type = '';
            this.state.form.validations[name].message = '';
        }
        else if (this.state.form.wasSubmitted) {
            this.state.form.validations[name].type = 'error';
            this.state.form.validations[name].message = 'Campo obrigatório';
        }
    };
    /**
     * @param {?} e
     * @return {?}
     */
    LoginFormComponent.prototype.handleForm = /**
     * @param {?} e
     * @return {?}
     */
    function (e) {
        this.state.form.wasSubmitted = true;
        if (this.state.form.formGroup.valid) {
            this.wasSubmited.emit(this.state.form.formGroup.value);
        }
    };
    LoginFormComponent.decorators = [
        { type: Component, args: [{
                    selector: 'so-login-form',
                    template: "<form [formGroup]=\"state.form.formGroup\">\n\t<img src=\"assets/images/sofisa-logo.png\" alt=\"\" class=\"so-pg-login--form--logo\">\n\t\n\t<so-input label=\"Usu\u00E1rio\" placeholder=\"Digite seu usu\u00E1rio\" [inputConfig]=\" { validation: { type: state.form.validations.username.type, message: state.form.validations.username.message} }\" (hasChanged)=\"updateFieldValue($event, 'username')\"></so-input>\n\t\n\t<so-input label=\"Senha\" placeholder=\"Digite sua senha\" inputType=\"password\" [inputConfig]=\" { validation: { type: state.form.validations.password.type, message: state.form.validations.password.message} }\" (hasChanged)=\"updateFieldValue($event, 'password')\"></so-input>\n\t\n\t<so-checkbox label=\"Lembrar usu\u00E1rio\" [checked]=\"true\"></so-checkbox>\n\t\n\t<div class=\"clearfix\"></div>\n\t\n\t<so-button type=\"primary\" text=\"Entrar\" (hasClicked)=\"handleForm($event)\"></so-button>\n</form>",
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    LoginFormComponent.ctorParameters = function () { return [
        { type: FormBuilder }
    ]; };
    LoginFormComponent.propDecorators = {
        wasSubmited: [{ type: Output }]
    };
    return LoginFormComponent;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var LoginFormModule = /** @class */ (function () {
    function LoginFormModule() {
    }
    LoginFormModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [LoginFormComponent],
                    imports: [
                        CommonModule,
                        InputModule,
                        ButtonModule,
                        CheckboxModule,
                        ReactiveFormsModule
                    ],
                    exports: [LoginFormComponent]
                },] }
    ];
    return LoginFormModule;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { LoginFormModule, LoginFormComponent as ɵa };

//# sourceMappingURL=sofisa-login.js.map