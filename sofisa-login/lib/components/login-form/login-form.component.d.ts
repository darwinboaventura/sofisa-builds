import { EventEmitter } from '@angular/core';
import { FormBuilder } from '@angular/forms';
export declare class LoginFormComponent {
    fb: FormBuilder;
    wasSubmited: EventEmitter<any>;
    state: any;
    constructor(fb: FormBuilder);
    updateFieldValue(value: any, name: any): void;
    handleForm(e: any): void;
}
