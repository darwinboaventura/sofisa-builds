export declare function handleError(err: any, loader: {
    page: string;
    name: string;
}): void;
