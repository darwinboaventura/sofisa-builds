import { Action } from '@ngrx/store';
export declare enum ActionTypes {
    CreateTask = "[TasklistComponent] CreateTask",
    SetAllTasks = "[TasklistComponent] SetAllTasks",
    SetValidationOnTaskForm = "[NovoComponent] SetValidationOnTaskForm",
    UpdateActionsOnTaskForm = "[NovoComponent] UpdateActionsOnTaskForm",
    SetTaskBeingEdited = "[EditComponent] SetTaskBeingEdited",
    SetUserOnTaskBeingEdited = "[EditComponent] SetUserOnTaskBeingEdited"
}
export declare class CreateTask implements Action {
    payload: {
        task: any;
    };
    readonly type = ActionTypes.CreateTask;
    constructor(payload: {
        task: any;
    });
}
export declare class SetAllTasks implements Action {
    payload: {
        tasks: any;
    };
    readonly type = ActionTypes.SetAllTasks;
    constructor(payload: {
        tasks: any;
    });
}
export declare class SetValidationOnTaskForm implements Action {
    payload: {
        page: string;
        fieldName: string;
        validationType: string;
        validationMessage: string;
    };
    readonly type = ActionTypes.SetValidationOnTaskForm;
    constructor(payload: {
        page: string;
        fieldName: string;
        validationType: string;
        validationMessage: string;
    });
}
export declare class SetUserOnTaskBeingEdited implements Action {
    payload: {
        user: any;
    };
    readonly type = ActionTypes.SetUserOnTaskBeingEdited;
    constructor(payload: {
        user: any;
    });
}
export declare class UpdateActionsOnTaskForm implements Action {
    payload: {
        page: string;
        actionName: string;
        actionValue: any;
    };
    readonly type = ActionTypes.UpdateActionsOnTaskForm;
    constructor(payload: {
        page: string;
        actionName: string;
        actionValue: any;
    });
}
export declare class SetTaskBeingEdited implements Action {
    payload: {
        fieldName: any;
        fieldValue: any;
    };
    readonly type = ActionTypes.SetTaskBeingEdited;
    constructor(payload: {
        fieldName: any;
        fieldValue: any;
    });
}
