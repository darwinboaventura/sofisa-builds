export declare const defaultState: {
    'tasklist': {
        'tasks': any[];
    };
    'tasklist/novo': {
        'form': {
            'validations': {
                'cliente': {
                    'type': string;
                    'message': string;
                };
                'cnpj': {
                    'type': string;
                    'message': string;
                };
                'dataVisita': {
                    'type': string;
                    'message': string;
                };
            };
            'actions': {
                'submitted': boolean;
            };
        };
    };
    'tasklist/editar': {
        'task': {
            'id': any;
            'name': string;
            'type': string;
            'claimed': boolean;
            'claimedAuthor': string;
        };
        'user': {
            'id': any;
        };
        'form': {
            'validations': {
                'cliente': {
                    'type': string;
                    'message': string;
                };
                'cnpj': {
                    'type': string;
                    'message': string;
                };
                'dataVisita': {
                    'type': string;
                    'message': string;
                };
            };
            'actions': {
                'submitted': boolean;
            };
        };
    };
};
export declare function taskReducer(state: {
    'tasklist': {
        'tasks': any[];
    };
    'tasklist/novo': {
        'form': {
            'validations': {
                'cliente': {
                    'type': string;
                    'message': string;
                };
                'cnpj': {
                    'type': string;
                    'message': string;
                };
                'dataVisita': {
                    'type': string;
                    'message': string;
                };
            };
            'actions': {
                'submitted': boolean;
            };
        };
    };
    'tasklist/editar': {
        'task': {
            'id': any;
            'name': string;
            'type': string;
            'claimed': boolean;
            'claimedAuthor': string;
        };
        'user': {
            'id': any;
        };
        'form': {
            'validations': {
                'cliente': {
                    'type': string;
                    'message': string;
                };
                'cnpj': {
                    'type': string;
                    'message': string;
                };
                'dataVisita': {
                    'type': string;
                    'message': string;
                };
            };
            'actions': {
                'submitted': boolean;
            };
        };
    };
}, action: any): {
    'tasklist': {
        'tasks': any[];
    };
    'tasklist/novo': {
        'form': {
            'validations': {
                'cliente': {
                    'type': string;
                    'message': string;
                };
                'cnpj': {
                    'type': string;
                    'message': string;
                };
                'dataVisita': {
                    'type': string;
                    'message': string;
                };
            };
            'actions': {
                'submitted': boolean;
            };
        };
    };
    'tasklist/editar': {
        'task': {
            'id': any;
            'name': string;
            'type': string;
            'claimed': boolean;
            'claimedAuthor': string;
        };
        'user': {
            'id': any;
        };
        'form': {
            'validations': {
                'cliente': {
                    'type': string;
                    'message': string;
                };
                'cnpj': {
                    'type': string;
                    'message': string;
                };
                'dataVisita': {
                    'type': string;
                    'message': string;
                };
            };
            'actions': {
                'submitted': boolean;
            };
        };
    };
};
