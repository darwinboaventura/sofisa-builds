import { OAuthService } from 'angular-oauth2-oidc';
import { LoaderService } from 'sofisa-loader';
import { NotificationService } from 'sofisa-notification';
export declare class AuthService {
    private oauthService;
    loaderService: LoaderService;
    store: any;
    notificationService: NotificationService;
    apiUri: String;
    constructor(oauthService: OAuthService, loaderService: LoaderService, store: any, notificationService: NotificationService);
    init(): void;
    isTokenValid(): boolean;
    getAccessToken(): string;
    doLogin(user: {
        username: string;
        password: any;
        string: any;
    }): void;
    doLogout(): void;
}
