import { Router } from '@angular/router';
import { AuthService } from '../../services/auth/auth.service';
export declare class AsideComponent {
    authService: AuthService;
    router: Router;
    constructor(authService: AuthService, router: Router);
    makeLogout(): void;
}
