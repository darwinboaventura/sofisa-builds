/**
 * Generated bundle index. Do not edit.
 */
export * from './public_api';
export { AsideComponent as ɵc } from './lib/components/aside/aside.component';
export { HeaderComponent as ɵb } from './lib/components/header/header.component';
export { MainComponent as ɵa } from './lib/components/main/main.component';
