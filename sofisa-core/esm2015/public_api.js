/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*
 * Public API Surface of sofisa-core
 */
// Componentes
export { MainModule } from './lib/components/main/main.module';
export { HeaderModule } from './lib/components/header/header.module';
export { AsideModule } from './lib/components/aside/aside.module';
// Services
export { AuthService } from './lib/services/auth/auth.service';
// Interceptors
export { AuthInterceptor } from './lib/inteceptors/auth/auth';
// Guards
export { AuthGuard } from './lib/guards/auth/auth.guard';
// Utils
export { formatDateToInput, formatDateToApiFormat } from './lib/utils/Dates.util';
export { handleFieldChange } from './lib/utils/Forms.util';
export { handleError } from './lib/utils/Error.util';
// Actions
export { ActionTypes, CreateTask, SetAllTasks, SetValidationOnTaskForm, SetUserOnTaskBeingEdited, UpdateActionsOnTaskForm, SetTaskBeingEdited } from './lib/ngrx/actions/tasks/tasks.actions';
// Reducers
export { taskReducer, defaultState } from './lib/ngrx/reducers/task/task.reducer';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHVibGljX2FwaS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3NvZmlzYS1jb3JlLyIsInNvdXJjZXMiOlsicHVibGljX2FwaS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUtBLDJCQUFjLG1DQUFtQyxDQUFDO0FBQ2xELDZCQUFjLHVDQUF1QyxDQUFDO0FBQ3RELDRCQUFjLHFDQUFxQyxDQUFDOztBQUdwRCw0QkFBYyxrQ0FBa0MsQ0FBQzs7QUFHakQsZ0NBQWMsNkJBQTZCLENBQUM7O0FBRzVDLDBCQUFjLDhCQUE4QixDQUFDOztBQUc3Qyx5REFBYyx3QkFBd0IsQ0FBQztBQUN2QyxrQ0FBYyx3QkFBd0IsQ0FBQztBQUN2Qyw0QkFBYyx3QkFBd0IsQ0FBQzs7QUFHdkMscUpBQWMsd0NBQXdDLENBQUM7O0FBR3ZELDBDQUFjLHVDQUF1QyxDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiLypcbiAqIFB1YmxpYyBBUEkgU3VyZmFjZSBvZiBzb2Zpc2EtY29yZVxuICovXG5cbi8vIENvbXBvbmVudGVzXG5leHBvcnQgKiBmcm9tICcuL2xpYi9jb21wb25lbnRzL21haW4vbWFpbi5tb2R1bGUnO1xuZXhwb3J0ICogZnJvbSAnLi9saWIvY29tcG9uZW50cy9oZWFkZXIvaGVhZGVyLm1vZHVsZSc7XG5leHBvcnQgKiBmcm9tICcuL2xpYi9jb21wb25lbnRzL2FzaWRlL2FzaWRlLm1vZHVsZSc7XG5cbi8vIFNlcnZpY2VzXG5leHBvcnQgKiBmcm9tICcuL2xpYi9zZXJ2aWNlcy9hdXRoL2F1dGguc2VydmljZSc7XG5cbi8vIEludGVyY2VwdG9yc1xuZXhwb3J0ICogZnJvbSAnLi9saWIvaW50ZWNlcHRvcnMvYXV0aC9hdXRoJztcblxuLy8gR3VhcmRzXG5leHBvcnQgKiBmcm9tICcuL2xpYi9ndWFyZHMvYXV0aC9hdXRoLmd1YXJkJztcblxuLy8gVXRpbHNcbmV4cG9ydCAqIGZyb20gJy4vbGliL3V0aWxzL0RhdGVzLnV0aWwnO1xuZXhwb3J0ICogZnJvbSAnLi9saWIvdXRpbHMvRm9ybXMudXRpbCc7XG5leHBvcnQgKiBmcm9tICcuL2xpYi91dGlscy9FcnJvci51dGlsJztcblxuLy8gQWN0aW9uc1xuZXhwb3J0ICogZnJvbSAnLi9saWIvbmdyeC9hY3Rpb25zL3Rhc2tzL3Rhc2tzLmFjdGlvbnMnO1xuXG4vLyBSZWR1Y2Vyc1xuZXhwb3J0ICogZnJvbSAnLi9saWIvbmdyeC9yZWR1Y2Vycy90YXNrL3Rhc2sucmVkdWNlcic7XG4iXX0=