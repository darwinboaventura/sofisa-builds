/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { ActionTypes } from '../../actions/tasks/tasks.actions';
/** @type {?} */
export const defaultState = {
    'tasklist': {
        'tasks': []
    },
    'tasklist/novo': {
        'form': {
            'validations': {
                'cliente': {
                    'type': '',
                    'message': ''
                },
                'cnpj': {
                    'type': '',
                    'message': ''
                },
                'dataVisita': {
                    'type': '',
                    'message': ''
                }
            },
            'actions': {
                'submitted': false
            }
        }
    },
    'tasklist/editar': {
        'task': {
            'id': null,
            'name': '',
            'type': '',
            'claimed': false,
            'claimedAuthor': ''
        },
        'user': {
            'id': null
        },
        'form': {
            'validations': {
                'cliente': {
                    'type': '',
                    'message': ''
                },
                'cnpj': {
                    'type': '',
                    'message': ''
                },
                'dataVisita': {
                    'type': '',
                    'message': ''
                }
            },
            'actions': {
                'submitted': false
            }
        }
    }
};
/**
 * @param {?=} state
 * @param {?=} action
 * @return {?}
 */
export function taskReducer(state = defaultState, action) {
    switch (action.type) {
        case ActionTypes.CreateTask:
            /** @type {?} */
            const tasks = state.tasklist.tasks;
            tasks.push(action.payload.task);
            return Object.assign({}, state, {
                tasklist: Object.assign({}, state.tasklist, { tasks: tasks })
            });
        case ActionTypes.SetAllTasks:
            return Object.assign({}, state, {
                tasklist: Object.assign({}, state.tasklist, { tasks: action.payload.tasks })
            });
        case ActionTypes.SetValidationOnTaskForm:
            return Object.assign({}, state, {
                [action.payload.page]: {
                    task: state[action.payload.page].task,
                    user: state[action.payload.page].user,
                    form: {
                        actions: state[action.payload.page].form.actions,
                        validations: Object.assign({}, state[action.payload.page].form.validations, { [action.payload.fieldName]: {
                                type: action.payload.validationType,
                                message: action.payload.validationMessage
                            } })
                    }
                }
            });
        case ActionTypes.SetUserOnTaskBeingEdited:
            return Object.assign({}, state, {
                'tasklist/editar': Object.assign({}, state['tasklist/editar'], { user: action.payload.user })
            });
        case ActionTypes.UpdateActionsOnTaskForm:
            return Object.assign({}, state, {
                [action.payload.page]: Object.assign({}, state[action.payload.page], { form: Object.assign({}, state[action.payload.page].form, { actions: Object.assign({}, state[action.payload.page].form.actions, { [action.payload.actionName]: action.payload.actionValue }) }) })
            });
        case ActionTypes.SetTaskBeingEdited:
            return Object.assign({}, state, {
                'tasklist/editar': Object.assign({}, state['tasklist/editar'], { task: Object.assign({}, state['tasklist/editar'].task, { [action.payload.fieldName]: action.payload.fieldValue }) })
            });
        default:
            return state;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFzay5yZWR1Y2VyLmpzIiwic291cmNlUm9vdCI6Im5nOi8vc29maXNhLWNvcmUvIiwic291cmNlcyI6WyJsaWIvbmdyeC9yZWR1Y2Vycy90YXNrL3Rhc2sucmVkdWNlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFDLFdBQVcsRUFBQyxNQUFNLG1DQUFtQyxDQUFDOztBQUU5RCxNQUFNLE9BQU8sWUFBWSxHQUFHO0lBQzNCLFVBQVUsRUFBRTtRQUNYLE9BQU8sRUFBRSxFQUFFO0tBQ1g7SUFDRCxlQUFlLEVBQUU7UUFDaEIsTUFBTSxFQUFFO1lBQ1AsYUFBYSxFQUFFO2dCQUNkLFNBQVMsRUFBRTtvQkFDVixNQUFNLEVBQUUsRUFBRTtvQkFDVixTQUFTLEVBQUUsRUFBRTtpQkFDYjtnQkFDRCxNQUFNLEVBQUU7b0JBQ1AsTUFBTSxFQUFFLEVBQUU7b0JBQ1YsU0FBUyxFQUFFLEVBQUU7aUJBQ2I7Z0JBQ0QsWUFBWSxFQUFFO29CQUNiLE1BQU0sRUFBRSxFQUFFO29CQUNWLFNBQVMsRUFBRSxFQUFFO2lCQUNiO2FBQ0Q7WUFDRCxTQUFTLEVBQUU7Z0JBQ1YsV0FBVyxFQUFFLEtBQUs7YUFDbEI7U0FDRDtLQUNEO0lBQ0QsaUJBQWlCLEVBQUU7UUFDbEIsTUFBTSxFQUFFO1lBQ1AsSUFBSSxFQUFFLElBQUk7WUFDVixNQUFNLEVBQUUsRUFBRTtZQUNWLE1BQU0sRUFBRSxFQUFFO1lBQ1YsU0FBUyxFQUFFLEtBQUs7WUFDaEIsZUFBZSxFQUFFLEVBQUU7U0FDbkI7UUFDRCxNQUFNLEVBQUU7WUFDUCxJQUFJLEVBQUUsSUFBSTtTQUNWO1FBQ0QsTUFBTSxFQUFFO1lBQ1AsYUFBYSxFQUFFO2dCQUNkLFNBQVMsRUFBRTtvQkFDVixNQUFNLEVBQUUsRUFBRTtvQkFDVixTQUFTLEVBQUUsRUFBRTtpQkFDYjtnQkFDRCxNQUFNLEVBQUU7b0JBQ1AsTUFBTSxFQUFFLEVBQUU7b0JBQ1YsU0FBUyxFQUFFLEVBQUU7aUJBQ2I7Z0JBQ0QsWUFBWSxFQUFFO29CQUNiLE1BQU0sRUFBRSxFQUFFO29CQUNWLFNBQVMsRUFBRSxFQUFFO2lCQUNiO2FBQ0Q7WUFDRCxTQUFTLEVBQUU7Z0JBQ1YsV0FBVyxFQUFFLEtBQUs7YUFDbEI7U0FDRDtLQUNEO0NBQ0Q7Ozs7OztBQUVELE1BQU0sVUFBVSxXQUFXLENBQUMsS0FBSyxHQUFHLFlBQVksRUFBRSxNQUFXO0lBQzVELFFBQVEsTUFBTSxDQUFDLElBQUksRUFBRTtRQUNwQixLQUFLLFdBQVcsQ0FBQyxVQUFVOztrQkFDcEIsS0FBSyxHQUFHLEtBQUssQ0FBQyxRQUFRLENBQUMsS0FBSztZQUNsQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7WUFFaEMsT0FBTyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxLQUFLLEVBQUU7Z0JBQy9CLFFBQVEsb0JBQ0osS0FBSyxDQUFDLFFBQVEsSUFDakIsS0FBSyxFQUFFLEtBQUssR0FDWjthQUNELENBQUMsQ0FBQztRQUNKLEtBQUssV0FBVyxDQUFDLFdBQVc7WUFDM0IsT0FBTyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxLQUFLLEVBQUU7Z0JBQy9CLFFBQVEsb0JBQ0osS0FBSyxDQUFDLFFBQVEsSUFDakIsS0FBSyxFQUFFLE1BQU0sQ0FBQyxPQUFPLENBQUMsS0FBSyxHQUMzQjthQUNELENBQUMsQ0FBQztRQUNKLEtBQUssV0FBVyxDQUFDLHVCQUF1QjtZQUN2QyxPQUFPLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLEtBQUssRUFBRTtnQkFDL0IsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFO29CQUN0QixJQUFJLEVBQUUsS0FBSyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSTtvQkFDckMsSUFBSSxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUk7b0JBQ3JDLElBQUksRUFBRTt3QkFDTCxPQUFPLEVBQUUsS0FBSyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU87d0JBQ2hELFdBQVcsb0JBQ1AsS0FBSyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsSUFDOUMsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxFQUFFO2dDQUMzQixJQUFJLEVBQUUsTUFBTSxDQUFDLE9BQU8sQ0FBQyxjQUFjO2dDQUNuQyxPQUFPLEVBQUUsTUFBTSxDQUFDLE9BQU8sQ0FBQyxpQkFBaUI7NkJBQ3pDLEdBQ0Q7cUJBQ0Q7aUJBQ0Q7YUFDRCxDQUFDLENBQUM7UUFDSixLQUFLLFdBQVcsQ0FBQyx3QkFBd0I7WUFDeEMsT0FBTyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxLQUFLLEVBQUU7Z0JBQy9CLGlCQUFpQixvQkFDYixLQUFLLENBQUMsaUJBQWlCLENBQUMsSUFDM0IsSUFBSSxFQUFFLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSSxHQUN6QjthQUNELENBQUMsQ0FBQztRQUNKLEtBQUssV0FBVyxDQUFDLHVCQUF1QjtZQUN2QyxPQUFPLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLEtBQUssRUFBRTtnQkFDL0IsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxvQkFDakIsS0FBSyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQzdCLElBQUksb0JBQ0EsS0FBSyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxJQUNsQyxPQUFPLG9CQUNILEtBQUssQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLElBQzFDLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsRUFBRSxNQUFNLENBQUMsT0FBTyxDQUFDLFdBQVcsU0FHekQ7YUFDRCxDQUFDLENBQUM7UUFDSixLQUFLLFdBQVcsQ0FBQyxrQkFBa0I7WUFDbEMsT0FBTyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxLQUFLLEVBQUU7Z0JBQy9CLGlCQUFpQixvQkFDYixLQUFLLENBQUMsaUJBQWlCLENBQUMsSUFDM0IsSUFBSSxvQkFDQSxLQUFLLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxJQUFJLElBQ2hDLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsRUFBRSxNQUFNLENBQUMsT0FBTyxDQUFDLFVBQVUsTUFFdEQ7YUFDRCxDQUFDLENBQUM7UUFDSjtZQUNDLE9BQU8sS0FBSyxDQUFDO0tBQ2Q7QUFDRixDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtBY3Rpb25UeXBlc30gZnJvbSAnLi4vLi4vYWN0aW9ucy90YXNrcy90YXNrcy5hY3Rpb25zJztcblxuZXhwb3J0IGNvbnN0IGRlZmF1bHRTdGF0ZSA9IHtcblx0J3Rhc2tsaXN0Jzoge1xuXHRcdCd0YXNrcyc6IFtdXG5cdH0sXG5cdCd0YXNrbGlzdC9ub3ZvJzoge1xuXHRcdCdmb3JtJzoge1xuXHRcdFx0J3ZhbGlkYXRpb25zJzoge1xuXHRcdFx0XHQnY2xpZW50ZSc6IHtcblx0XHRcdFx0XHQndHlwZSc6ICcnLFxuXHRcdFx0XHRcdCdtZXNzYWdlJzogJydcblx0XHRcdFx0fSxcblx0XHRcdFx0J2NucGonOiB7XG5cdFx0XHRcdFx0J3R5cGUnOiAnJyxcblx0XHRcdFx0XHQnbWVzc2FnZSc6ICcnXG5cdFx0XHRcdH0sXG5cdFx0XHRcdCdkYXRhVmlzaXRhJzoge1xuXHRcdFx0XHRcdCd0eXBlJzogJycsXG5cdFx0XHRcdFx0J21lc3NhZ2UnOiAnJ1xuXHRcdFx0XHR9XG5cdFx0XHR9LFxuXHRcdFx0J2FjdGlvbnMnOiB7XG5cdFx0XHRcdCdzdWJtaXR0ZWQnOiBmYWxzZVxuXHRcdFx0fVxuXHRcdH1cblx0fSxcblx0J3Rhc2tsaXN0L2VkaXRhcic6IHtcblx0XHQndGFzayc6IHtcblx0XHRcdCdpZCc6IG51bGwsXG5cdFx0XHQnbmFtZSc6ICcnLFxuXHRcdFx0J3R5cGUnOiAnJyxcblx0XHRcdCdjbGFpbWVkJzogZmFsc2UsXG5cdFx0XHQnY2xhaW1lZEF1dGhvcic6ICcnXG5cdFx0fSxcblx0XHQndXNlcic6IHtcblx0XHRcdCdpZCc6IG51bGxcblx0XHR9LFxuXHRcdCdmb3JtJzoge1xuXHRcdFx0J3ZhbGlkYXRpb25zJzoge1xuXHRcdFx0XHQnY2xpZW50ZSc6IHtcblx0XHRcdFx0XHQndHlwZSc6ICcnLFxuXHRcdFx0XHRcdCdtZXNzYWdlJzogJydcblx0XHRcdFx0fSxcblx0XHRcdFx0J2NucGonOiB7XG5cdFx0XHRcdFx0J3R5cGUnOiAnJyxcblx0XHRcdFx0XHQnbWVzc2FnZSc6ICcnXG5cdFx0XHRcdH0sXG5cdFx0XHRcdCdkYXRhVmlzaXRhJzoge1xuXHRcdFx0XHRcdCd0eXBlJzogJycsXG5cdFx0XHRcdFx0J21lc3NhZ2UnOiAnJ1xuXHRcdFx0XHR9XG5cdFx0XHR9LFxuXHRcdFx0J2FjdGlvbnMnOiB7XG5cdFx0XHRcdCdzdWJtaXR0ZWQnOiBmYWxzZVxuXHRcdFx0fVxuXHRcdH1cblx0fVxufTtcblxuZXhwb3J0IGZ1bmN0aW9uIHRhc2tSZWR1Y2VyKHN0YXRlID0gZGVmYXVsdFN0YXRlLCBhY3Rpb246IGFueSkge1xuXHRzd2l0Y2ggKGFjdGlvbi50eXBlKSB7XG5cdFx0Y2FzZSBBY3Rpb25UeXBlcy5DcmVhdGVUYXNrOlxuXHRcdFx0Y29uc3QgdGFza3MgPSBzdGF0ZS50YXNrbGlzdC50YXNrcztcblx0XHRcdHRhc2tzLnB1c2goYWN0aW9uLnBheWxvYWQudGFzayk7XG5cdFx0XHRcblx0XHRcdHJldHVybiBPYmplY3QuYXNzaWduKHt9LCBzdGF0ZSwge1xuXHRcdFx0XHR0YXNrbGlzdDoge1xuXHRcdFx0XHRcdC4uLnN0YXRlLnRhc2tsaXN0LFxuXHRcdFx0XHRcdHRhc2tzOiB0YXNrc1xuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHRjYXNlIEFjdGlvblR5cGVzLlNldEFsbFRhc2tzOlxuXHRcdFx0cmV0dXJuIE9iamVjdC5hc3NpZ24oe30sIHN0YXRlLCB7XG5cdFx0XHRcdHRhc2tsaXN0OiB7XG5cdFx0XHRcdFx0Li4uc3RhdGUudGFza2xpc3QsXG5cdFx0XHRcdFx0dGFza3M6IGFjdGlvbi5wYXlsb2FkLnRhc2tzXG5cdFx0XHRcdH1cblx0XHRcdH0pO1xuXHRcdGNhc2UgQWN0aW9uVHlwZXMuU2V0VmFsaWRhdGlvbk9uVGFza0Zvcm06XG5cdFx0XHRyZXR1cm4gT2JqZWN0LmFzc2lnbih7fSwgc3RhdGUsIHtcblx0XHRcdFx0W2FjdGlvbi5wYXlsb2FkLnBhZ2VdOiB7XG5cdFx0XHRcdFx0dGFzazogc3RhdGVbYWN0aW9uLnBheWxvYWQucGFnZV0udGFzayxcblx0XHRcdFx0XHR1c2VyOiBzdGF0ZVthY3Rpb24ucGF5bG9hZC5wYWdlXS51c2VyLFxuXHRcdFx0XHRcdGZvcm06IHtcblx0XHRcdFx0XHRcdGFjdGlvbnM6IHN0YXRlW2FjdGlvbi5wYXlsb2FkLnBhZ2VdLmZvcm0uYWN0aW9ucyxcblx0XHRcdFx0XHRcdHZhbGlkYXRpb25zOiB7XG5cdFx0XHRcdFx0XHRcdC4uLnN0YXRlW2FjdGlvbi5wYXlsb2FkLnBhZ2VdLmZvcm0udmFsaWRhdGlvbnMsXG5cdFx0XHRcdFx0XHRcdFthY3Rpb24ucGF5bG9hZC5maWVsZE5hbWVdOiB7XG5cdFx0XHRcdFx0XHRcdFx0dHlwZTogYWN0aW9uLnBheWxvYWQudmFsaWRhdGlvblR5cGUsXG5cdFx0XHRcdFx0XHRcdFx0bWVzc2FnZTogYWN0aW9uLnBheWxvYWQudmFsaWRhdGlvbk1lc3NhZ2Vcblx0XHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdH1cblx0XHRcdFx0fVxuXHRcdFx0fSk7XG5cdFx0Y2FzZSBBY3Rpb25UeXBlcy5TZXRVc2VyT25UYXNrQmVpbmdFZGl0ZWQ6XG5cdFx0XHRyZXR1cm4gT2JqZWN0LmFzc2lnbih7fSwgc3RhdGUsIHtcblx0XHRcdFx0J3Rhc2tsaXN0L2VkaXRhcic6IHtcblx0XHRcdFx0XHQuLi5zdGF0ZVsndGFza2xpc3QvZWRpdGFyJ10sXG5cdFx0XHRcdFx0dXNlcjogYWN0aW9uLnBheWxvYWQudXNlclxuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHRjYXNlIEFjdGlvblR5cGVzLlVwZGF0ZUFjdGlvbnNPblRhc2tGb3JtOlxuXHRcdFx0cmV0dXJuIE9iamVjdC5hc3NpZ24oe30sIHN0YXRlLCB7XG5cdFx0XHRcdFthY3Rpb24ucGF5bG9hZC5wYWdlXToge1xuXHRcdFx0XHRcdC4uLnN0YXRlW2FjdGlvbi5wYXlsb2FkLnBhZ2VdLFxuXHRcdFx0XHRcdGZvcm06IHtcblx0XHRcdFx0XHRcdC4uLnN0YXRlW2FjdGlvbi5wYXlsb2FkLnBhZ2VdLmZvcm0sXG5cdFx0XHRcdFx0XHRhY3Rpb25zOiB7XG5cdFx0XHRcdFx0XHRcdC4uLnN0YXRlW2FjdGlvbi5wYXlsb2FkLnBhZ2VdLmZvcm0uYWN0aW9ucyxcblx0XHRcdFx0XHRcdFx0W2FjdGlvbi5wYXlsb2FkLmFjdGlvbk5hbWVdOiBhY3Rpb24ucGF5bG9hZC5hY3Rpb25WYWx1ZVxuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdH1cblx0XHRcdFx0fVxuXHRcdFx0fSk7XG5cdFx0Y2FzZSBBY3Rpb25UeXBlcy5TZXRUYXNrQmVpbmdFZGl0ZWQ6XG5cdFx0XHRyZXR1cm4gT2JqZWN0LmFzc2lnbih7fSwgc3RhdGUsIHtcblx0XHRcdFx0J3Rhc2tsaXN0L2VkaXRhcic6IHtcblx0XHRcdFx0XHQuLi5zdGF0ZVsndGFza2xpc3QvZWRpdGFyJ10sXG5cdFx0XHRcdFx0dGFzazoge1xuXHRcdFx0XHRcdFx0Li4uc3RhdGVbJ3Rhc2tsaXN0L2VkaXRhciddLnRhc2ssXG5cdFx0XHRcdFx0XHRbYWN0aW9uLnBheWxvYWQuZmllbGROYW1lXTogYWN0aW9uLnBheWxvYWQuZmllbGRWYWx1ZVxuXHRcdFx0XHRcdH1cblx0XHRcdFx0fVxuXHRcdFx0fSk7XG5cdFx0ZGVmYXVsdDpcblx0XHRcdHJldHVybiBzdGF0ZTtcblx0fVxufVxuXG5cbiJdfQ==