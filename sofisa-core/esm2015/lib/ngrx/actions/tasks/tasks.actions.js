/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
const ActionTypes = {
    CreateTask: '[TasklistComponent] CreateTask',
    SetAllTasks: '[TasklistComponent] SetAllTasks',
    SetValidationOnTaskForm: '[NovoComponent] SetValidationOnTaskForm',
    UpdateActionsOnTaskForm: '[NovoComponent] UpdateActionsOnTaskForm',
    SetTaskBeingEdited: '[EditComponent] SetTaskBeingEdited',
    SetUserOnTaskBeingEdited: '[EditComponent] SetUserOnTaskBeingEdited',
};
export { ActionTypes };
export class CreateTask {
    /**
     * @param {?} payload
     */
    constructor(payload) {
        this.payload = payload;
        this.type = ActionTypes.CreateTask;
    }
}
if (false) {
    /** @type {?} */
    CreateTask.prototype.type;
    /** @type {?} */
    CreateTask.prototype.payload;
}
export class SetAllTasks {
    /**
     * @param {?} payload
     */
    constructor(payload) {
        this.payload = payload;
        this.type = ActionTypes.SetAllTasks;
    }
}
if (false) {
    /** @type {?} */
    SetAllTasks.prototype.type;
    /** @type {?} */
    SetAllTasks.prototype.payload;
}
export class SetValidationOnTaskForm {
    /**
     * @param {?} payload
     */
    constructor(payload) {
        this.payload = payload;
        this.type = ActionTypes.SetValidationOnTaskForm;
    }
}
if (false) {
    /** @type {?} */
    SetValidationOnTaskForm.prototype.type;
    /** @type {?} */
    SetValidationOnTaskForm.prototype.payload;
}
export class SetUserOnTaskBeingEdited {
    /**
     * @param {?} payload
     */
    constructor(payload) {
        this.payload = payload;
        this.type = ActionTypes.SetUserOnTaskBeingEdited;
    }
}
if (false) {
    /** @type {?} */
    SetUserOnTaskBeingEdited.prototype.type;
    /** @type {?} */
    SetUserOnTaskBeingEdited.prototype.payload;
}
export class UpdateActionsOnTaskForm {
    /**
     * @param {?} payload
     */
    constructor(payload) {
        this.payload = payload;
        this.type = ActionTypes.UpdateActionsOnTaskForm;
    }
}
if (false) {
    /** @type {?} */
    UpdateActionsOnTaskForm.prototype.type;
    /** @type {?} */
    UpdateActionsOnTaskForm.prototype.payload;
}
export class SetTaskBeingEdited {
    /**
     * @param {?} payload
     */
    constructor(payload) {
        this.payload = payload;
        this.type = ActionTypes.SetTaskBeingEdited;
    }
}
if (false) {
    /** @type {?} */
    SetTaskBeingEdited.prototype.type;
    /** @type {?} */
    SetTaskBeingEdited.prototype.payload;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFza3MuYWN0aW9ucy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3NvZmlzYS1jb3JlLyIsInNvdXJjZXMiOlsibGliL25ncngvYWN0aW9ucy90YXNrcy90YXNrcy5hY3Rpb25zLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztJQUdDLFlBQWEsZ0NBQWdDO0lBQzdDLGFBQWMsaUNBQWlDO0lBQy9DLHlCQUEwQix5Q0FBeUM7SUFDbkUseUJBQTBCLHlDQUF5QztJQUNuRSxvQkFBcUIsb0NBQW9DO0lBQ3pELDBCQUEyQiwwQ0FBMEM7OztBQUd0RSxNQUFNLE9BQU8sVUFBVTs7OztJQUd0QixZQUFtQixPQUFzQjtRQUF0QixZQUFPLEdBQVAsT0FBTyxDQUFlO1FBRmhDLFNBQUksR0FBRyxXQUFXLENBQUMsVUFBVSxDQUFDO0lBRUssQ0FBQztDQUM3Qzs7O0lBSEEsMEJBQXVDOztJQUUzQiw2QkFBNkI7O0FBRzFDLE1BQU0sT0FBTyxXQUFXOzs7O0lBR3ZCLFlBQW1CLE9BQXVCO1FBQXZCLFlBQU8sR0FBUCxPQUFPLENBQWdCO1FBRmpDLFNBQUksR0FBRyxXQUFXLENBQUMsV0FBVyxDQUFDO0lBRUssQ0FBQztDQUM5Qzs7O0lBSEEsMkJBQXdDOztJQUU1Qiw4QkFBOEI7O0FBRzNDLE1BQU0sT0FBTyx1QkFBdUI7Ozs7SUFHbkMsWUFBbUIsT0FBZ0c7UUFBaEcsWUFBTyxHQUFQLE9BQU8sQ0FBeUY7UUFGMUcsU0FBSSxHQUFHLFdBQVcsQ0FBQyx1QkFBdUIsQ0FBQztJQUVrRSxDQUFDO0NBQ3ZIOzs7SUFIQSx1Q0FBb0Q7O0lBRXhDLDBDQUF1Rzs7QUFHcEgsTUFBTSxPQUFPLHdCQUF3Qjs7OztJQUdwQyxZQUFtQixPQUFzQjtRQUF0QixZQUFPLEdBQVAsT0FBTyxDQUFlO1FBRmhDLFNBQUksR0FBRyxXQUFXLENBQUMsd0JBQXdCLENBQUM7SUFFVCxDQUFDO0NBQzdDOzs7SUFIQSx3Q0FBcUQ7O0lBRXpDLDJDQUE2Qjs7QUFHMUMsTUFBTSxPQUFPLHVCQUF1Qjs7OztJQUduQyxZQUFtQixPQUFnRTtRQUFoRSxZQUFPLEdBQVAsT0FBTyxDQUF5RDtRQUYxRSxTQUFJLEdBQUcsV0FBVyxDQUFDLHVCQUF1QixDQUFDO0lBRWtDLENBQUM7Q0FDdkY7OztJQUhBLHVDQUFvRDs7SUFFeEMsMENBQXVFOztBQUdwRixNQUFNLE9BQU8sa0JBQWtCOzs7O0lBRzlCLFlBQW1CLE9BQTRDO1FBQTVDLFlBQU8sR0FBUCxPQUFPLENBQXFDO1FBRnRELFNBQUksR0FBRyxXQUFXLENBQUMsa0JBQWtCLENBQUM7SUFFbUIsQ0FBQztDQUNuRTs7O0lBSEEsa0NBQStDOztJQUVuQyxxQ0FBbUQiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0FjdGlvbn0gZnJvbSAnQG5ncngvc3RvcmUnO1xuXG5leHBvcnQgZW51bSBBY3Rpb25UeXBlcyB7XG5cdENyZWF0ZVRhc2sgPSAnW1Rhc2tsaXN0Q29tcG9uZW50XSBDcmVhdGVUYXNrJyxcblx0U2V0QWxsVGFza3MgPSAnW1Rhc2tsaXN0Q29tcG9uZW50XSBTZXRBbGxUYXNrcycsXG5cdFNldFZhbGlkYXRpb25PblRhc2tGb3JtID0gJ1tOb3ZvQ29tcG9uZW50XSBTZXRWYWxpZGF0aW9uT25UYXNrRm9ybScsXG5cdFVwZGF0ZUFjdGlvbnNPblRhc2tGb3JtID0gJ1tOb3ZvQ29tcG9uZW50XSBVcGRhdGVBY3Rpb25zT25UYXNrRm9ybScsXG5cdFNldFRhc2tCZWluZ0VkaXRlZCA9ICdbRWRpdENvbXBvbmVudF0gU2V0VGFza0JlaW5nRWRpdGVkJyxcblx0U2V0VXNlck9uVGFza0JlaW5nRWRpdGVkID0gJ1tFZGl0Q29tcG9uZW50XSBTZXRVc2VyT25UYXNrQmVpbmdFZGl0ZWQnLFxufVxuXG5leHBvcnQgY2xhc3MgQ3JlYXRlVGFzayBpbXBsZW1lbnRzIEFjdGlvbiB7XG5cdHJlYWRvbmx5IHR5cGUgPSBBY3Rpb25UeXBlcy5DcmVhdGVUYXNrO1xuXHRcblx0Y29uc3RydWN0b3IocHVibGljIHBheWxvYWQ6IHsgdGFzazogYW55IH0pIHt9XG59XG5cbmV4cG9ydCBjbGFzcyBTZXRBbGxUYXNrcyBpbXBsZW1lbnRzIEFjdGlvbiB7XG5cdHJlYWRvbmx5IHR5cGUgPSBBY3Rpb25UeXBlcy5TZXRBbGxUYXNrcztcblx0XG5cdGNvbnN0cnVjdG9yKHB1YmxpYyBwYXlsb2FkOiB7IHRhc2tzOiBhbnkgfSkge31cbn1cblxuZXhwb3J0IGNsYXNzIFNldFZhbGlkYXRpb25PblRhc2tGb3JtIGltcGxlbWVudHMgQWN0aW9uIHtcblx0cmVhZG9ubHkgdHlwZSA9IEFjdGlvblR5cGVzLlNldFZhbGlkYXRpb25PblRhc2tGb3JtO1xuXHRcblx0Y29uc3RydWN0b3IocHVibGljIHBheWxvYWQ6IHsgcGFnZTogc3RyaW5nLCBmaWVsZE5hbWU6IHN0cmluZywgIHZhbGlkYXRpb25UeXBlOiBzdHJpbmcsIHZhbGlkYXRpb25NZXNzYWdlOiBzdHJpbmcgfSkge31cbn1cblxuZXhwb3J0IGNsYXNzIFNldFVzZXJPblRhc2tCZWluZ0VkaXRlZCBpbXBsZW1lbnRzIEFjdGlvbiB7XG5cdHJlYWRvbmx5IHR5cGUgPSBBY3Rpb25UeXBlcy5TZXRVc2VyT25UYXNrQmVpbmdFZGl0ZWQ7XG5cdFxuXHRjb25zdHJ1Y3RvcihwdWJsaWMgcGF5bG9hZDogeyB1c2VyOiBhbnkgfSkge31cbn1cblxuZXhwb3J0IGNsYXNzIFVwZGF0ZUFjdGlvbnNPblRhc2tGb3JtIGltcGxlbWVudHMgQWN0aW9uIHtcblx0cmVhZG9ubHkgdHlwZSA9IEFjdGlvblR5cGVzLlVwZGF0ZUFjdGlvbnNPblRhc2tGb3JtO1xuXHRcblx0Y29uc3RydWN0b3IocHVibGljIHBheWxvYWQ6IHsgcGFnZTogc3RyaW5nLCBhY3Rpb25OYW1lOiBzdHJpbmcsICBhY3Rpb25WYWx1ZTogYW55IH0pIHt9XG59XG5cbmV4cG9ydCBjbGFzcyBTZXRUYXNrQmVpbmdFZGl0ZWQgaW1wbGVtZW50cyBBY3Rpb24ge1xuXHRyZWFkb25seSB0eXBlID0gQWN0aW9uVHlwZXMuU2V0VGFza0JlaW5nRWRpdGVkO1xuXHRcblx0Y29uc3RydWN0b3IocHVibGljIHBheWxvYWQ6IHsgZmllbGROYW1lOiBhbnksIGZpZWxkVmFsdWU6IGFueSB9KSB7fVxufVxuXG4iXX0=