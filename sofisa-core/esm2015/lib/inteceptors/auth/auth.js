/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Inject } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';
import { HttpHeaders } from '@angular/common/http';
export class AuthInterceptor {
    /**
     * @param {?} authService
     */
    constructor(authService) {
        this.authService = authService;
    }
    /**
     * @param {?} req
     * @param {?} next
     * @return {?}
     */
    intercept(req, next) {
        if (req.url.includes('token')) {
            /** @type {?} */
            const headers = new HttpHeaders({
                Authorization: 'Basic ' + btoa(`trusted-app:secret`)
            });
            /** @type {?} */
            const uri = req.clone({ headers });
            return next.handle(uri);
        }
        else {
            /** @type {?} */
            const headers = new HttpHeaders({
                Authorization: 'Bearer ' + this.authService.getAccessToken()
            });
            /** @type {?} */
            const uri = req.clone({ headers });
            return next.handle(uri);
        }
        return next.handle(req);
    }
}
/** @nocollapse */
AuthInterceptor.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [AuthService,] }] }
];
if (false) {
    /** @type {?} */
    AuthInterceptor.prototype.authService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3NvZmlzYS1jb3JlLyIsInNvdXJjZXMiOlsibGliL2ludGVjZXB0b3JzL2F1dGgvYXV0aC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFDLE1BQU0sRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUVyQyxPQUFPLEVBQUMsV0FBVyxFQUFDLE1BQU0sa0NBQWtDLENBQUM7QUFDN0QsT0FBTyxFQUFtRCxXQUFXLEVBQUMsTUFBTSxzQkFBc0IsQ0FBQztBQUVuRyxNQUFNLE9BQU8sZUFBZTs7OztJQUMzQixZQUF3QyxXQUFXO1FBQVgsZ0JBQVcsR0FBWCxXQUFXLENBQUE7SUFBRyxDQUFDOzs7Ozs7SUFFdkQsU0FBUyxDQUFDLEdBQXFCLEVBQUUsSUFBaUI7UUFDakQsSUFBSSxHQUFHLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsRUFBRTs7a0JBQ3hCLE9BQU8sR0FBRyxJQUFJLFdBQVcsQ0FBQztnQkFDL0IsYUFBYSxFQUFFLFFBQVEsR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUM7YUFDcEQsQ0FBQzs7a0JBRUksR0FBRyxHQUFHLEdBQUcsQ0FBQyxLQUFLLENBQUMsRUFBQyxPQUFPLEVBQUMsQ0FBQztZQUVoQyxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDeEI7YUFBTTs7a0JBQ0EsT0FBTyxHQUFHLElBQUksV0FBVyxDQUFDO2dCQUMvQixhQUFhLEVBQUUsU0FBUyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxFQUFFO2FBQzVELENBQUM7O2tCQUVJLEdBQUcsR0FBRyxHQUFHLENBQUMsS0FBSyxDQUFDLEVBQUMsT0FBTyxFQUFDLENBQUM7WUFFaEMsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1NBQ3hCO1FBRUQsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ3pCLENBQUM7Ozs7NENBdEJZLE1BQU0sU0FBQyxXQUFXOzs7O0lBQW5CLHNDQUF1QyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7SW5qZWN0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7T2JzZXJ2YWJsZX0gZnJvbSAncnhqcyc7XG5pbXBvcnQge0F1dGhTZXJ2aWNlfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9hdXRoL2F1dGguc2VydmljZSc7XG5pbXBvcnQge0h0dHBJbnRlcmNlcHRvcixIdHRwRXZlbnQsSHR0cEhhbmRsZXIsSHR0cFJlcXVlc3QsSHR0cEhlYWRlcnN9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcblxuZXhwb3J0IGNsYXNzIEF1dGhJbnRlcmNlcHRvciBpbXBsZW1lbnRzIEh0dHBJbnRlcmNlcHRvciB7XG5cdGNvbnN0cnVjdG9yKEBJbmplY3QoQXV0aFNlcnZpY2UpIHB1YmxpYyBhdXRoU2VydmljZSkge31cblx0XG5cdGludGVyY2VwdChyZXE6IEh0dHBSZXF1ZXN0PGFueT4sIG5leHQ6IEh0dHBIYW5kbGVyKTogT2JzZXJ2YWJsZTxIdHRwRXZlbnQ8YW55Pj4ge1xuXHRcdGlmIChyZXEudXJsLmluY2x1ZGVzKCd0b2tlbicpKSB7XG5cdFx0XHRjb25zdCBoZWFkZXJzID0gbmV3IEh0dHBIZWFkZXJzKHtcblx0XHRcdFx0QXV0aG9yaXphdGlvbjogJ0Jhc2ljICcgKyBidG9hKGB0cnVzdGVkLWFwcDpzZWNyZXRgKVxuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdGNvbnN0IHVyaSA9IHJlcS5jbG9uZSh7aGVhZGVyc30pO1xuXHRcdFx0XG5cdFx0XHRyZXR1cm4gbmV4dC5oYW5kbGUodXJpKTtcblx0XHR9IGVsc2Uge1xuXHRcdFx0Y29uc3QgaGVhZGVycyA9IG5ldyBIdHRwSGVhZGVycyh7XG5cdFx0XHRcdEF1dGhvcml6YXRpb246ICdCZWFyZXIgJyArIHRoaXMuYXV0aFNlcnZpY2UuZ2V0QWNjZXNzVG9rZW4oKVxuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdGNvbnN0IHVyaSA9IHJlcS5jbG9uZSh7aGVhZGVyc30pO1xuXHRcdFx0XG5cdFx0XHRyZXR1cm4gbmV4dC5oYW5kbGUodXJpKTtcblx0XHR9XG5cdFx0XG5cdFx0cmV0dXJuIG5leHQuaGFuZGxlKHJlcSk7XG5cdH1cbn0iXX0=