/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Store } from '@ngrx/store';
import { OAuthService } from 'angular-oauth2-oidc';
import { LoaderService } from 'sofisa-loader';
import { Injectable, Inject } from '@angular/core';
import { AddLoaderItem, RemoveLoaderItem } from 'sofisa-loader';
import { NotificationService } from 'sofisa-notification';
import * as i0 from "@angular/core";
import * as i1 from "angular-oauth2-oidc";
import * as i2 from "sofisa-loader";
import * as i3 from "@ngrx/store";
import * as i4 from "sofisa-notification";
export class AuthService {
    /**
     * @param {?} oauthService
     * @param {?} loaderService
     * @param {?} store
     * @param {?} notificationService
     */
    constructor(oauthService, loaderService, store, notificationService) {
        this.oauthService = oauthService;
        this.loaderService = loaderService;
        this.store = store;
        this.notificationService = notificationService;
        // TODO: Mover para arquivos de configurações
        this.apiUri = '//sofisa-lab-gateway.eastus2.cloudapp.azure.com';
    }
    /**
     * @return {?}
     */
    init() {
        this.oauthService.configure({
            tokenEndpoint: `${this.apiUri}api/oauth-api/oauth/token`,
            redirectUri: window.location.origin,
            scope: '',
            showDebugInformation: true,
            oidc: true,
            requireHttps: false,
            clientId: 'trusted-app'
        });
    }
    /**
     * @return {?}
     */
    isTokenValid() {
        return this.oauthService.hasValidAccessToken();
    }
    /**
     * @return {?}
     */
    getAccessToken() {
        return this.oauthService.getAccessToken();
    }
    /**
     * @param {?} user
     * @return {?}
     */
    doLogin(user) {
        this.store.dispatch(new AddLoaderItem({
            page: 'login',
            name: 'login'
        }));
        this.oauthService.fetchTokenUsingPasswordFlow(user.username, user.password)
            .then((response) => {
            window.location.href = this.oauthService.redirectUri;
        })
            .catch((err) => {
            this.notificationService.addMessage({
                type: 'error',
                message: 'Login ou senha incorreto! Tente novamente'
            });
        })
            .finally(() => {
            this.store.dispatch(new RemoveLoaderItem({
                page: 'login',
                name: 'login'
            }));
        });
    }
    /**
     * @return {?}
     */
    doLogout() {
        this.oauthService.logOut(true);
    }
}
AuthService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
AuthService.ctorParameters = () => [
    { type: OAuthService },
    { type: LoaderService },
    { type: undefined, decorators: [{ type: Inject, args: [Store,] }] },
    { type: NotificationService }
];
/** @nocollapse */ AuthService.ngInjectableDef = i0.defineInjectable({ factory: function AuthService_Factory() { return new AuthService(i0.inject(i1.OAuthService), i0.inject(i2.LoaderService), i0.inject(i3.Store), i0.inject(i4.NotificationService)); }, token: AuthService, providedIn: "root" });
if (false) {
    /** @type {?} */
    AuthService.prototype.apiUri;
    /**
     * @type {?}
     * @private
     */
    AuthService.prototype.oauthService;
    /** @type {?} */
    AuthService.prototype.loaderService;
    /** @type {?} */
    AuthService.prototype.store;
    /** @type {?} */
    AuthService.prototype.notificationService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vc29maXNhLWNvcmUvIiwic291cmNlcyI6WyJsaWIvc2VydmljZXMvYXV0aC9hdXRoLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBQyxLQUFLLEVBQUMsTUFBTSxhQUFhLENBQUM7QUFFbEMsT0FBTyxFQUFDLFlBQVksRUFBQyxNQUFNLHFCQUFxQixDQUFDO0FBQ2pELE9BQU8sRUFBQyxhQUFhLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDNUMsT0FBTyxFQUFDLFVBQVUsRUFBRSxNQUFNLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDakQsT0FBTyxFQUFDLGFBQWEsRUFBRSxnQkFBZ0IsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUM5RCxPQUFPLEVBQUMsbUJBQW1CLEVBQUMsTUFBTSxxQkFBcUIsQ0FBQzs7Ozs7O0FBTXhELE1BQU0sT0FBTyxXQUFXOzs7Ozs7O0lBSXZCLFlBQW9CLFlBQTBCLEVBQVMsYUFBNEIsRUFBd0IsS0FBSyxFQUFTLG1CQUF3QztRQUE3SSxpQkFBWSxHQUFaLFlBQVksQ0FBYztRQUFTLGtCQUFhLEdBQWIsYUFBYSxDQUFlO1FBQXdCLFVBQUssR0FBTCxLQUFLLENBQUE7UUFBUyx3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCOztRQUZqSyxXQUFNLEdBQVcsaURBQWlELENBQUM7SUFFaUcsQ0FBQzs7OztJQUVySyxJQUFJO1FBQ0gsSUFBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUM7WUFDM0IsYUFBYSxFQUFFLEdBQUcsSUFBSSxDQUFDLE1BQU0sMkJBQTJCO1lBQ3hELFdBQVcsRUFBRSxNQUFNLENBQUMsUUFBUSxDQUFDLE1BQU07WUFDbkMsS0FBSyxFQUFFLEVBQUU7WUFDVCxvQkFBb0IsRUFBRSxJQUFJO1lBQzFCLElBQUksRUFBRSxJQUFJO1lBQ1YsWUFBWSxFQUFFLEtBQUs7WUFDbkIsUUFBUSxFQUFFLGFBQWE7U0FDdkIsQ0FBQyxDQUFDO0lBQ0osQ0FBQzs7OztJQUVELFlBQVk7UUFDWCxPQUFPLElBQUksQ0FBQyxZQUFZLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztJQUNoRCxDQUFDOzs7O0lBRUQsY0FBYztRQUNiLE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQyxjQUFjLEVBQUUsQ0FBQztJQUMzQyxDQUFDOzs7OztJQUVELE9BQU8sQ0FBQyxJQUE0QztRQUNuRCxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxJQUFJLGFBQWEsQ0FBQztZQUNyQyxJQUFJLEVBQUUsT0FBTztZQUNiLElBQUksRUFBRSxPQUFPO1NBQ2IsQ0FBQyxDQUFDLENBQUM7UUFFSixJQUFJLENBQUMsWUFBWSxDQUFDLDJCQUEyQixDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQzthQUMxRSxJQUFJLENBQUMsQ0FBQyxRQUFhLEVBQUUsRUFBRTtZQUN2QixNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQztRQUN0RCxDQUFDLENBQUM7YUFDRCxLQUFLLENBQUMsQ0FBQyxHQUFHLEVBQUUsRUFBRTtZQUNkLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxVQUFVLENBQUM7Z0JBQ25DLElBQUksRUFBRSxPQUFPO2dCQUNiLE9BQU8sRUFBRSwyQ0FBMkM7YUFDcEQsQ0FBQyxDQUFDO1FBQ0osQ0FBQyxDQUFDO2FBQ0QsT0FBTyxDQUFDLEdBQUcsRUFBRTtZQUNiLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksZ0JBQWdCLENBQUM7Z0JBQ3hDLElBQUksRUFBRSxPQUFPO2dCQUNiLElBQUksRUFBRSxPQUFPO2FBQ2IsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNKLENBQUM7Ozs7SUFFRCxRQUFRO1FBQ1AsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDaEMsQ0FBQzs7O1lBeERELFVBQVUsU0FBQztnQkFDWCxVQUFVLEVBQUUsTUFBTTthQUNsQjs7OztZQVJPLFlBQVk7WUFDWixhQUFhOzRDQWFrRSxNQUFNLFNBQUMsS0FBSztZQVYzRixtQkFBbUI7Ozs7O0lBUTFCLDZCQUFtRTs7Ozs7SUFFdkQsbUNBQWtDOztJQUFFLG9DQUFtQzs7SUFBRSw0QkFBMkI7O0lBQUUsMENBQStDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtTdG9yZX0gZnJvbSAnQG5ncngvc3RvcmUnO1xuaW1wb3J0IHtoYW5kbGVFcnJvcn0gZnJvbSAnLi4vLi4vdXRpbHMvRXJyb3IudXRpbCc7XG5pbXBvcnQge09BdXRoU2VydmljZX0gZnJvbSAnYW5ndWxhci1vYXV0aDItb2lkYyc7XG5pbXBvcnQge0xvYWRlclNlcnZpY2V9IGZyb20gJ3NvZmlzYS1sb2FkZXInO1xuaW1wb3J0IHtJbmplY3RhYmxlLCBJbmplY3R9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtBZGRMb2FkZXJJdGVtLCBSZW1vdmVMb2FkZXJJdGVtfSBmcm9tICdzb2Zpc2EtbG9hZGVyJztcbmltcG9ydCB7Tm90aWZpY2F0aW9uU2VydmljZX0gZnJvbSAnc29maXNhLW5vdGlmaWNhdGlvbic7XG5cbkBJbmplY3RhYmxlKHtcblx0cHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuXG5leHBvcnQgY2xhc3MgQXV0aFNlcnZpY2Uge1xuXHQvLyBUT0RPOiBNb3ZlciBwYXJhIGFycXVpdm9zIGRlIGNvbmZpZ3VyYcOnw7Vlc1xuXHRhcGlVcmk6IFN0cmluZyA9ICcvL3NvZmlzYS1sYWItZ2F0ZXdheS5lYXN0dXMyLmNsb3VkYXBwLmF6dXJlLmNvbSc7XG5cblx0Y29uc3RydWN0b3IocHJpdmF0ZSBvYXV0aFNlcnZpY2U6IE9BdXRoU2VydmljZSwgcHVibGljIGxvYWRlclNlcnZpY2U6IExvYWRlclNlcnZpY2UsIEBJbmplY3QoU3RvcmUpIHB1YmxpYyBzdG9yZSwgcHVibGljIG5vdGlmaWNhdGlvblNlcnZpY2U6IE5vdGlmaWNhdGlvblNlcnZpY2UpIHt9XG5cdFxuXHRpbml0KCkge1xuXHRcdHRoaXMub2F1dGhTZXJ2aWNlLmNvbmZpZ3VyZSh7XG5cdFx0XHR0b2tlbkVuZHBvaW50OiBgJHt0aGlzLmFwaVVyaX1hcGkvb2F1dGgtYXBpL29hdXRoL3Rva2VuYCxcblx0XHRcdHJlZGlyZWN0VXJpOiB3aW5kb3cubG9jYXRpb24ub3JpZ2luLFxuXHRcdFx0c2NvcGU6ICcnLFxuXHRcdFx0c2hvd0RlYnVnSW5mb3JtYXRpb246IHRydWUsXG5cdFx0XHRvaWRjOiB0cnVlLFxuXHRcdFx0cmVxdWlyZUh0dHBzOiBmYWxzZSxcblx0XHRcdGNsaWVudElkOiAndHJ1c3RlZC1hcHAnXG5cdFx0fSk7XG5cdH1cblx0XG5cdGlzVG9rZW5WYWxpZCgpIHtcblx0XHRyZXR1cm4gdGhpcy5vYXV0aFNlcnZpY2UuaGFzVmFsaWRBY2Nlc3NUb2tlbigpO1xuXHR9XG5cdFxuXHRnZXRBY2Nlc3NUb2tlbigpIHtcblx0XHRyZXR1cm4gdGhpcy5vYXV0aFNlcnZpY2UuZ2V0QWNjZXNzVG9rZW4oKTtcblx0fVxuXHRcblx0ZG9Mb2dpbih1c2VyOiB7IHVzZXJuYW1lOiBzdHJpbmcsIHBhc3N3b3JkOyBzdHJpbmcgfSkge1xuXHRcdHRoaXMuc3RvcmUuZGlzcGF0Y2gobmV3IEFkZExvYWRlckl0ZW0oe1xuXHRcdFx0cGFnZTogJ2xvZ2luJyxcblx0XHRcdG5hbWU6ICdsb2dpbidcblx0XHR9KSk7XG5cdFx0XG5cdFx0dGhpcy5vYXV0aFNlcnZpY2UuZmV0Y2hUb2tlblVzaW5nUGFzc3dvcmRGbG93KHVzZXIudXNlcm5hbWUsIHVzZXIucGFzc3dvcmQpXG5cdFx0LnRoZW4oKHJlc3BvbnNlOiBhbnkpID0+IHtcblx0XHRcdHdpbmRvdy5sb2NhdGlvbi5ocmVmID0gdGhpcy5vYXV0aFNlcnZpY2UucmVkaXJlY3RVcmk7XG5cdFx0fSlcblx0XHQuY2F0Y2goKGVycikgPT4ge1x0XHRcdFxuXHRcdFx0dGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmFkZE1lc3NhZ2Uoe1xuXHRcdFx0XHR0eXBlOiAnZXJyb3InLFxuXHRcdFx0XHRtZXNzYWdlOiAnTG9naW4gb3Ugc2VuaGEgaW5jb3JyZXRvISBUZW50ZSBub3ZhbWVudGUnXG5cdFx0XHR9KTtcblx0XHR9KVxuXHRcdC5maW5hbGx5KCgpID0+IHtcblx0XHRcdHRoaXMuc3RvcmUuZGlzcGF0Y2gobmV3IFJlbW92ZUxvYWRlckl0ZW0oe1xuXHRcdFx0XHRwYWdlOiAnbG9naW4nLFxuXHRcdFx0XHRuYW1lOiAnbG9naW4nXG5cdFx0XHR9KSk7XG5cdFx0fSk7XG5cdH1cblx0XG5cdGRvTG9nb3V0KCkge1xuXHRcdHRoaXMub2F1dGhTZXJ2aWNlLmxvZ091dCh0cnVlKTtcblx0fVxufVxuIl19