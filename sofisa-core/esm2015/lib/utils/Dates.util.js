/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @param {?} value
 * @param {?=} type
 * @return {?}
 */
export function formatDateToInput(value, type = 1) {
    /** @type {?} */
    const separator = value.includes('-') ? '-' : '/';
    /** @type {?} */
    let pieces;
    /** @type {?} */
    let formated;
    if (type === 1) {
        pieces = value.split(separator);
        formated = `${pieces[0]}-${pieces[1]}-${pieces[2]}`;
    }
    else if (type === 2) {
        pieces = value.split(separator);
        formated = `${pieces[2]}-${pieces[1]}-${pieces[0]}`;
    }
    if (formated && formated.length === 10) {
        formated = formated.substring(0, 10);
    }
    return formated;
}
/**
 * @param {?} value
 * @return {?}
 */
export function formatDateToApiFormat(value) {
    /** @type {?} */
    const separator = value.includes('-') ? '-' : '/';
    /** @type {?} */
    const breakedDate = value.split(separator);
    /** @type {?} */
    const data = `${breakedDate[2]}/${breakedDate[1]}/${breakedDate[0]}`;
    return data;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRGF0ZXMudXRpbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3NvZmlzYS1jb3JlLyIsInNvdXJjZXMiOlsibGliL3V0aWxzL0RhdGVzLnV0aWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBQUEsTUFBTSxVQUFVLGlCQUFpQixDQUFDLEtBQVUsRUFBRSxPQUFlLENBQUM7O1VBQ3ZELFNBQVMsR0FBRyxLQUFLLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEdBQUc7O1FBRTdDLE1BQU07O1FBQ04sUUFBUTtJQUVaLElBQUksSUFBSSxLQUFLLENBQUMsRUFBRTtRQUNmLE1BQU0sR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ2hDLFFBQVEsR0FBRyxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxNQUFNLENBQUMsQ0FBQyxDQUFDLElBQUksTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7S0FDcEQ7U0FBTSxJQUFJLElBQUksS0FBSyxDQUFDLEVBQUU7UUFDdEIsTUFBTSxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDaEMsUUFBUSxHQUFHLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztLQUNwRDtJQUVELElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxNQUFNLEtBQUssRUFBRSxFQUFFO1FBQ3ZDLFFBQVEsR0FBRyxRQUFRLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQztLQUNyQztJQUVELE9BQU8sUUFBUSxDQUFDO0FBQ2pCLENBQUM7Ozs7O0FBRUQsTUFBTSxVQUFVLHFCQUFxQixDQUFDLEtBQWE7O1VBQzVDLFNBQVMsR0FBRyxLQUFLLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEdBQUc7O1VBQzNDLFdBQVcsR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQzs7VUFDcEMsSUFBSSxHQUFHLEdBQUcsV0FBVyxDQUFDLENBQUMsQ0FBQyxJQUFJLFdBQVcsQ0FBQyxDQUFDLENBQUMsSUFBSSxXQUFXLENBQUMsQ0FBQyxDQUFDLEVBQUU7SUFFcEUsT0FBTyxJQUFJLENBQUM7QUFDYixDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGZ1bmN0aW9uIGZvcm1hdERhdGVUb0lucHV0KHZhbHVlOiBhbnksIHR5cGU6IG51bWJlciA9IDEpIHtcblx0Y29uc3Qgc2VwYXJhdG9yID0gdmFsdWUuaW5jbHVkZXMoJy0nKSA/ICctJyA6ICcvJztcblx0XG5cdGxldCBwaWVjZXM7XG5cdGxldCBmb3JtYXRlZDtcblx0XG5cdGlmICh0eXBlID09PSAxKSB7XG5cdFx0cGllY2VzID0gdmFsdWUuc3BsaXQoc2VwYXJhdG9yKTtcblx0XHRmb3JtYXRlZCA9IGAke3BpZWNlc1swXX0tJHtwaWVjZXNbMV19LSR7cGllY2VzWzJdfWA7XG5cdH0gZWxzZSBpZiAodHlwZSA9PT0gMikge1xuXHRcdHBpZWNlcyA9IHZhbHVlLnNwbGl0KHNlcGFyYXRvcik7XG5cdFx0Zm9ybWF0ZWQgPSBgJHtwaWVjZXNbMl19LSR7cGllY2VzWzFdfS0ke3BpZWNlc1swXX1gO1xuXHR9XG5cdFxuXHRpZiAoZm9ybWF0ZWQgJiYgZm9ybWF0ZWQubGVuZ3RoID09PSAxMCkge1xuXHRcdGZvcm1hdGVkID0gZm9ybWF0ZWQuc3Vic3RyaW5nKDAsIDEwKTtcblx0fVxuXHRcblx0cmV0dXJuIGZvcm1hdGVkO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZm9ybWF0RGF0ZVRvQXBpRm9ybWF0KHZhbHVlOiBzdHJpbmcpIHtcblx0Y29uc3Qgc2VwYXJhdG9yID0gdmFsdWUuaW5jbHVkZXMoJy0nKSA/ICctJyA6ICcvJztcblx0Y29uc3QgYnJlYWtlZERhdGUgPSB2YWx1ZS5zcGxpdChzZXBhcmF0b3IpO1xuXHRjb25zdCBkYXRhID0gYCR7YnJlYWtlZERhdGVbMl19LyR7YnJlYWtlZERhdGVbMV19LyR7YnJlYWtlZERhdGVbMF19YDtcblx0XG5cdHJldHVybiBkYXRhO1xufVxuIl19