/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
export class HeaderComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() { }
}
HeaderComponent.decorators = [
    { type: Component, args: [{
                selector: 'so-header',
                template: "<header class=\"so-header clearfix\">\n\t<div class=\"so-header--logo clearfix\">\n\t\t<a href=\"#\">\n\t\t\t<img src=\"assets/images/sofisa-logo.png\" alt=\"Sofisa\">\n\t\t</a>\n\t</div>\n\t\n\t<div class=\"so-header--profile clearfix\">\t</div>\n</header>",
                styles: [".so-header{width:100%;height:88px;border-bottom:1px solid #e5e5e5;position:fixed;top:0;left:0;z-index:1000;background:#fff}.so-header--logo{max-width:120px;position:absolute;top:50%;left:25px;-webkit-transform:translateY(-50%);transform:translateY(-50%)}.so-header--logo img{max-width:100%}.so-header--profile{width:32px;height:32px;border-radius:50%;overflow:hidden;border:1px solid #e5e5e5;position:absolute;top:50%;right:25px;-webkit-transform:translateY(-50%);transform:translateY(-50%)}.so-header--profile img{max-width:100%;position:absolute;top:50%;left:50%;-webkit-transform:translate(-50%,-50%);transform:translate(-50%,-50%)}"]
            }] }
];
/** @nocollapse */
HeaderComponent.ctorParameters = () => [];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGVhZGVyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3NvZmlzYS1jb3JlLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvaGVhZGVyL2hlYWRlci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQVMsTUFBTSxlQUFlLENBQUM7QUFRaEQsTUFBTSxPQUFPLGVBQWU7SUFDM0IsZ0JBQWUsQ0FBQzs7OztJQUVoQixRQUFRLEtBQUksQ0FBQzs7O1lBVGIsU0FBUyxTQUFDO2dCQUNWLFFBQVEsRUFBRSxXQUFXO2dCQUNyQiw2UUFBc0M7O2FBRXRDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIE9uSW5pdH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuXHRzZWxlY3RvcjogJ3NvLWhlYWRlcicsXG5cdHRlbXBsYXRlVXJsOiAnLi9oZWFkZXIuY29tcG9uZW50Lmh0bWwnLFxuXHRzdHlsZVVybHM6IFsnLi9oZWFkZXIuY29tcG9uZW50LnNjc3MnXVxufSlcblxuZXhwb3J0IGNsYXNzIEhlYWRlckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cdGNvbnN0cnVjdG9yKCkge31cblx0XG5cdG5nT25Jbml0KCkge31cbn1cbiJdfQ==