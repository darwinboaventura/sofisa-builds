/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Router } from '@angular/router';
import { Component } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';
export class AsideComponent {
    /**
     * @param {?} authService
     * @param {?} router
     */
    constructor(authService, router) {
        this.authService = authService;
        this.router = router;
    }
    /**
     * @return {?}
     */
    makeLogout() {
        this.authService.doLogout();
        this.router.navigateByUrl('login');
    }
}
AsideComponent.decorators = [
    { type: Component, args: [{
                selector: 'so-aside',
                template: "<aside class=\"so-aside clearfix\">\n\t<nav class=\"so-nav clearfix\">\n\t\t<ul class=\"so-nav--menu clearfix\">\n\t\t\t<li class=\"so-nav--menu--item clearfix\" routerLink=\"dashboard\" routerLinkActive=\"is-active\">\n\t\t\t\t<a routerLink=\"dashboard\" class=\"so-nav--menu--item--anchor clearfix\">\n\t\t\t\t\t<span class=\"so-nav--menu--item--anchor--icon\">\n\t\t\t\t\t\t<i class=\"fas fa-home\"></i>\n\t\t\t\t\t</span>\n\t\t\t\t\t\n\t\t\t\t\t<span class=\"so-nav--menu--item--anchor--name\">In\u00EDcio</span>\n\t\t\t\t</a>\n\t\t\t</li>\n\t\t\t\n\t\t\t<li class=\"so-nav--menu--item clearfix\" routerLink=\"tasklist\" routerLinkActive=\"is-active\">\n\t\t\t\t<a routerLink=\"tasklist\" class=\"so-nav--menu--item--anchor clearfix\">\n\t\t\t\t\t<span class=\"so-nav--menu--item--anchor--icon\">\n\t\t\t\t\t\t<i class=\"far fa-file\"></i>\n\t\t\t\t\t</span>\n\t\t\t\t\t\n\t\t\t\t\t<span class=\"so-nav--menu--item--anchor--name\">Tasklist</span>\n\t\t\t\t</a>\n\t\t\t</li>\n\t\t\t\n\t\t\t<li class=\"so-nav--menu--item clearfix\" routerLink=\"admin\" routerLinkActive=\"is-active\">\n\t\t\t\t<a routerLink=\"admin\" class=\"so-nav--menu--item--anchor clearfix\">\n\t\t\t\t\t<span class=\"so-nav--menu--item--anchor--icon\">\n\t\t\t\t\t\t<i class=\"far fa-user\"></i>\n\t\t\t\t\t</span>\n\t\t\t\t\t\n\t\t\t\t\t<span class=\"so-nav--menu--item--anchor--name\">Admin</span>\n\t\t\t\t</a>\n\t\t\t</li>\n\t\t</ul>\n\t</nav>\n\t\n\t<div class=\"so-aside--logout clearfix\" (click)=\"makeLogout()\">\n\t\t<span class=\"so-aside--logout--icon\">\n\t\t\t<i class=\"fas fa-sign-out-alt\"></i>\n\t\t</span>\n\t\t\n\t\t<span class=\"so-aside--logout--name\">Sair</span>\n\t</div>\n</aside>",
                styles: [".so-aside{width:250px;height:100vh;position:fixed;top:0;left:0;padding-top:88px;background:#fff}.so-aside--logout{position:absolute;bottom:32px;left:24px}.so-aside--logout--icon,.so-aside--logout--name{color:#bfc0c0;display:block;cursor:pointer}.so-aside--logout--name{font-family:Roboto;font-weight:400;font-size:16px;line-height:24px;position:absolute;left:41px;top:50%;-webkit-transform:translateY(-50%);transform:translateY(-50%)}.so-aside--logout--icon i{color:inherit}.so-nav--menu{list-style:none;padding:0;margin:0}.so-nav--menu--item--anchor{padding:16px 24px;display:block;text-decoration:none}.so-nav--menu--item--anchor--icon,.so-nav--menu--item--anchor--name{color:#bfc0c0;float:left;display:block}.so-nav--menu--item--anchor--icon{width:20px;height:20px;margin-right:21px}.so-nav--menu--item--anchor--icon i{color:inherit}.so-nav--menu--item--anchor--name{font-family:Roboto;font-weight:400;font-size:16px;line-height:24px}.so-nav--menu--item.is-active .so-nav--menu--item--anchor,.so-nav--menu--item:hover .so-nav--menu--item--anchor{background-color:#e5e5e5}.so-nav--menu--item.is-active .so-nav--menu--item--anchor--name,.so-nav--menu--item:hover .so-nav--menu--item--anchor--name{font-weight:500;color:#8a8a8a}"]
            }] }
];
/** @nocollapse */
AsideComponent.ctorParameters = () => [
    { type: AuthService },
    { type: Router }
];
if (false) {
    /** @type {?} */
    AsideComponent.prototype.authService;
    /** @type {?} */
    AsideComponent.prototype.router;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXNpZGUuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vc29maXNhLWNvcmUvIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9hc2lkZS9hc2lkZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBQyxNQUFNLEVBQUMsTUFBTSxpQkFBaUIsQ0FBQztBQUN2QyxPQUFPLEVBQUMsU0FBUyxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ3hDLE9BQU8sRUFBQyxXQUFXLEVBQUMsTUFBTSxrQ0FBa0MsQ0FBQztBQVE3RCxNQUFNLE9BQU8sY0FBYzs7Ozs7SUFDMUIsWUFBbUIsV0FBd0IsRUFBUyxNQUFjO1FBQS9DLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQVMsV0FBTSxHQUFOLE1BQU0sQ0FBUTtJQUFHLENBQUM7Ozs7SUFFdEUsVUFBVTtRQUNULElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxFQUFFLENBQUM7UUFFNUIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDcEMsQ0FBQzs7O1lBYkQsU0FBUyxTQUFDO2dCQUNWLFFBQVEsRUFBRSxVQUFVO2dCQUNwQiw4cERBQXFDOzthQUVyQzs7OztZQU5PLFdBQVc7WUFGWCxNQUFNOzs7O0lBV0QscUNBQStCOztJQUFFLGdDQUFxQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Um91dGVyfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuaW1wb3J0IHtDb21wb25lbnR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtBdXRoU2VydmljZX0gZnJvbSAnLi4vLi4vc2VydmljZXMvYXV0aC9hdXRoLnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcblx0c2VsZWN0b3I6ICdzby1hc2lkZScsXG5cdHRlbXBsYXRlVXJsOiAnLi9hc2lkZS5jb21wb25lbnQuaHRtbCcsXG5cdHN0eWxlVXJsczogWycuL2FzaWRlLmNvbXBvbmVudC5zY3NzJ11cbn0pXG5cbmV4cG9ydCBjbGFzcyBBc2lkZUNvbXBvbmVudCB7XG5cdGNvbnN0cnVjdG9yKHB1YmxpYyBhdXRoU2VydmljZTogQXV0aFNlcnZpY2UsIHB1YmxpYyByb3V0ZXI6IFJvdXRlcikge31cblx0XG5cdG1ha2VMb2dvdXQoKSB7XG5cdFx0dGhpcy5hdXRoU2VydmljZS5kb0xvZ291dCgpO1xuXHRcdFxuXHRcdHRoaXMucm91dGVyLm5hdmlnYXRlQnlVcmwoJ2xvZ2luJyk7XG5cdH1cbn1cbiJdfQ==