/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
export class MainComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() { }
}
MainComponent.decorators = [
    { type: Component, args: [{
                selector: 'so-main',
                template: "<main class=\"so-main clearfix\">\n\t<ng-content></ng-content>\n</main>",
                styles: [".so-main{width:100%;min-height:100vh;padding-top:121px;padding-left:274px;padding-right:24px}"]
            }] }
];
/** @nocollapse */
MainComponent.ctorParameters = () => [];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFpbi5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9zb2Zpc2EtY29yZS8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL21haW4vbWFpbi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQVMsTUFBTSxlQUFlLENBQUM7QUFRaEQsTUFBTSxPQUFPLGFBQWE7SUFDekIsZ0JBQWdCLENBQUM7Ozs7SUFFakIsUUFBUSxLQUFJLENBQUM7OztZQVRiLFNBQVMsU0FBQztnQkFDVixRQUFRLEVBQUUsU0FBUztnQkFDbkIsbUZBQW9DOzthQUVwQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBPbkluaXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcblx0c2VsZWN0b3I6ICdzby1tYWluJyxcblx0dGVtcGxhdGVVcmw6ICcuL21haW4uY29tcG9uZW50Lmh0bWwnLFxuXHRzdHlsZVVybHM6IFsnLi9tYWluLmNvbXBvbmVudC5zY3NzJ11cbn0pXG5cbmV4cG9ydCBjbGFzcyBNYWluQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblx0Y29uc3RydWN0b3IoKSB7IH1cblx0XG5cdG5nT25Jbml0KCkge31cbn1cbiJdfQ==