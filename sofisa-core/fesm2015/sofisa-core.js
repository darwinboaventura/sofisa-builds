import { CommonModule } from '@angular/common';
import { Store } from '@ngrx/store';
import { OAuthService } from 'angular-oauth2-oidc';
import { LoaderService, AddLoaderItem, RemoveLoaderItem } from 'sofisa-loader';
import { NotificationService } from 'sofisa-notification';
import { Router, RouterModule } from '@angular/router';
import { Component, NgModule, Injectable, Inject, defineInjectable, inject } from '@angular/core';
import { HttpHeaders, HttpErrorResponse } from '@angular/common/http';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class MainComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() { }
}
MainComponent.decorators = [
    { type: Component, args: [{
                selector: 'so-main',
                template: "<main class=\"so-main clearfix\">\n\t<ng-content></ng-content>\n</main>",
                styles: [".so-main{width:100%;min-height:100vh;padding-top:121px;padding-left:274px;padding-right:24px}"]
            }] }
];
/** @nocollapse */
MainComponent.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class MainModule {
}
MainModule.decorators = [
    { type: NgModule, args: [{
                declarations: [MainComponent],
                imports: [
                    CommonModule
                ],
                exports: [MainComponent]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class HeaderComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() { }
}
HeaderComponent.decorators = [
    { type: Component, args: [{
                selector: 'so-header',
                template: "<header class=\"so-header clearfix\">\n\t<div class=\"so-header--logo clearfix\">\n\t\t<a href=\"#\">\n\t\t\t<img src=\"assets/images/sofisa-logo.png\" alt=\"Sofisa\">\n\t\t</a>\n\t</div>\n\t\n\t<div class=\"so-header--profile clearfix\">\t</div>\n</header>",
                styles: [".so-header{width:100%;height:88px;border-bottom:1px solid #e5e5e5;position:fixed;top:0;left:0;z-index:1000;background:#fff}.so-header--logo{max-width:120px;position:absolute;top:50%;left:25px;-webkit-transform:translateY(-50%);transform:translateY(-50%)}.so-header--logo img{max-width:100%}.so-header--profile{width:32px;height:32px;border-radius:50%;overflow:hidden;border:1px solid #e5e5e5;position:absolute;top:50%;right:25px;-webkit-transform:translateY(-50%);transform:translateY(-50%)}.so-header--profile img{max-width:100%;position:absolute;top:50%;left:50%;-webkit-transform:translate(-50%,-50%);transform:translate(-50%,-50%)}"]
            }] }
];
/** @nocollapse */
HeaderComponent.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class HeaderModule {
}
HeaderModule.decorators = [
    { type: NgModule, args: [{
                declarations: [HeaderComponent],
                imports: [
                    CommonModule
                ],
                exports: [HeaderComponent]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class AuthService {
    /**
     * @param {?} oauthService
     * @param {?} loaderService
     * @param {?} store
     * @param {?} notificationService
     */
    constructor(oauthService, loaderService, store, notificationService) {
        this.oauthService = oauthService;
        this.loaderService = loaderService;
        this.store = store;
        this.notificationService = notificationService;
        // TODO: Mover para arquivos de configurações
        this.apiUri = '//sofisa-lab-gateway.eastus2.cloudapp.azure.com';
    }
    /**
     * @return {?}
     */
    init() {
        this.oauthService.configure({
            tokenEndpoint: `${this.apiUri}api/oauth-api/oauth/token`,
            redirectUri: window.location.origin,
            scope: '',
            showDebugInformation: true,
            oidc: true,
            requireHttps: false,
            clientId: 'trusted-app'
        });
    }
    /**
     * @return {?}
     */
    isTokenValid() {
        return this.oauthService.hasValidAccessToken();
    }
    /**
     * @return {?}
     */
    getAccessToken() {
        return this.oauthService.getAccessToken();
    }
    /**
     * @param {?} user
     * @return {?}
     */
    doLogin(user) {
        this.store.dispatch(new AddLoaderItem({
            page: 'login',
            name: 'login'
        }));
        this.oauthService.fetchTokenUsingPasswordFlow(user.username, user.password)
            .then((response) => {
            window.location.href = this.oauthService.redirectUri;
        })
            .catch((err) => {
            this.notificationService.addMessage({
                type: 'error',
                message: 'Login ou senha incorreto! Tente novamente'
            });
        })
            .finally(() => {
            this.store.dispatch(new RemoveLoaderItem({
                page: 'login',
                name: 'login'
            }));
        });
    }
    /**
     * @return {?}
     */
    doLogout() {
        this.oauthService.logOut(true);
    }
}
AuthService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
AuthService.ctorParameters = () => [
    { type: OAuthService },
    { type: LoaderService },
    { type: undefined, decorators: [{ type: Inject, args: [Store,] }] },
    { type: NotificationService }
];
/** @nocollapse */ AuthService.ngInjectableDef = defineInjectable({ factory: function AuthService_Factory() { return new AuthService(inject(OAuthService), inject(LoaderService), inject(Store), inject(NotificationService)); }, token: AuthService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class AsideComponent {
    /**
     * @param {?} authService
     * @param {?} router
     */
    constructor(authService, router) {
        this.authService = authService;
        this.router = router;
    }
    /**
     * @return {?}
     */
    makeLogout() {
        this.authService.doLogout();
        this.router.navigateByUrl('login');
    }
}
AsideComponent.decorators = [
    { type: Component, args: [{
                selector: 'so-aside',
                template: "<aside class=\"so-aside clearfix\">\n\t<nav class=\"so-nav clearfix\">\n\t\t<ul class=\"so-nav--menu clearfix\">\n\t\t\t<li class=\"so-nav--menu--item clearfix\" routerLink=\"dashboard\" routerLinkActive=\"is-active\">\n\t\t\t\t<a routerLink=\"dashboard\" class=\"so-nav--menu--item--anchor clearfix\">\n\t\t\t\t\t<span class=\"so-nav--menu--item--anchor--icon\">\n\t\t\t\t\t\t<i class=\"fas fa-home\"></i>\n\t\t\t\t\t</span>\n\t\t\t\t\t\n\t\t\t\t\t<span class=\"so-nav--menu--item--anchor--name\">In\u00EDcio</span>\n\t\t\t\t</a>\n\t\t\t</li>\n\t\t\t\n\t\t\t<li class=\"so-nav--menu--item clearfix\" routerLink=\"tasklist\" routerLinkActive=\"is-active\">\n\t\t\t\t<a routerLink=\"tasklist\" class=\"so-nav--menu--item--anchor clearfix\">\n\t\t\t\t\t<span class=\"so-nav--menu--item--anchor--icon\">\n\t\t\t\t\t\t<i class=\"far fa-file\"></i>\n\t\t\t\t\t</span>\n\t\t\t\t\t\n\t\t\t\t\t<span class=\"so-nav--menu--item--anchor--name\">Tasklist</span>\n\t\t\t\t</a>\n\t\t\t</li>\n\t\t\t\n\t\t\t<li class=\"so-nav--menu--item clearfix\" routerLink=\"admin\" routerLinkActive=\"is-active\">\n\t\t\t\t<a routerLink=\"admin\" class=\"so-nav--menu--item--anchor clearfix\">\n\t\t\t\t\t<span class=\"so-nav--menu--item--anchor--icon\">\n\t\t\t\t\t\t<i class=\"far fa-user\"></i>\n\t\t\t\t\t</span>\n\t\t\t\t\t\n\t\t\t\t\t<span class=\"so-nav--menu--item--anchor--name\">Admin</span>\n\t\t\t\t</a>\n\t\t\t</li>\n\t\t</ul>\n\t</nav>\n\t\n\t<div class=\"so-aside--logout clearfix\" (click)=\"makeLogout()\">\n\t\t<span class=\"so-aside--logout--icon\">\n\t\t\t<i class=\"fas fa-sign-out-alt\"></i>\n\t\t</span>\n\t\t\n\t\t<span class=\"so-aside--logout--name\">Sair</span>\n\t</div>\n</aside>",
                styles: [".so-aside{width:250px;height:100vh;position:fixed;top:0;left:0;padding-top:88px;background:#fff}.so-aside--logout{position:absolute;bottom:32px;left:24px}.so-aside--logout--icon,.so-aside--logout--name{color:#bfc0c0;display:block;cursor:pointer}.so-aside--logout--name{font-family:Roboto;font-weight:400;font-size:16px;line-height:24px;position:absolute;left:41px;top:50%;-webkit-transform:translateY(-50%);transform:translateY(-50%)}.so-aside--logout--icon i{color:inherit}.so-nav--menu{list-style:none;padding:0;margin:0}.so-nav--menu--item--anchor{padding:16px 24px;display:block;text-decoration:none}.so-nav--menu--item--anchor--icon,.so-nav--menu--item--anchor--name{color:#bfc0c0;float:left;display:block}.so-nav--menu--item--anchor--icon{width:20px;height:20px;margin-right:21px}.so-nav--menu--item--anchor--icon i{color:inherit}.so-nav--menu--item--anchor--name{font-family:Roboto;font-weight:400;font-size:16px;line-height:24px}.so-nav--menu--item.is-active .so-nav--menu--item--anchor,.so-nav--menu--item:hover .so-nav--menu--item--anchor{background-color:#e5e5e5}.so-nav--menu--item.is-active .so-nav--menu--item--anchor--name,.so-nav--menu--item:hover .so-nav--menu--item--anchor--name{font-weight:500;color:#8a8a8a}"]
            }] }
];
/** @nocollapse */
AsideComponent.ctorParameters = () => [
    { type: AuthService },
    { type: Router }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class AsideModule {
}
AsideModule.decorators = [
    { type: NgModule, args: [{
                declarations: [AsideComponent],
                imports: [
                    CommonModule,
                    RouterModule
                ],
                exports: [AsideComponent]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class AuthInterceptor {
    /**
     * @param {?} authService
     */
    constructor(authService) {
        this.authService = authService;
    }
    /**
     * @param {?} req
     * @param {?} next
     * @return {?}
     */
    intercept(req, next) {
        if (req.url.includes('token')) {
            /** @type {?} */
            const headers = new HttpHeaders({
                Authorization: 'Basic ' + btoa(`trusted-app:secret`)
            });
            /** @type {?} */
            const uri = req.clone({ headers });
            return next.handle(uri);
        }
        else {
            /** @type {?} */
            const headers = new HttpHeaders({
                Authorization: 'Bearer ' + this.authService.getAccessToken()
            });
            /** @type {?} */
            const uri = req.clone({ headers });
            return next.handle(uri);
        }
        return next.handle(req);
    }
}
/** @nocollapse */
AuthInterceptor.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [AuthService,] }] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class AuthGuard {
    /**
     * @param {?} authService
     * @param {?} router
     */
    constructor(authService, router) {
        this.authService = authService;
        this.router = router;
    }
    /**
     * @param {?} next
     * @param {?} state
     * @return {?}
     */
    canActivate(next, state) {
        /** @type {?} */
        const isValid = this.authService.isTokenValid();
        if (!isValid) {
            this.router.navigateByUrl('login');
        }
        return isValid;
    }
}
AuthGuard.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
AuthGuard.ctorParameters = () => [
    { type: AuthService },
    { type: Router }
];
/** @nocollapse */ AuthGuard.ngInjectableDef = defineInjectable({ factory: function AuthGuard_Factory() { return new AuthGuard(inject(AuthService), inject(Router)); }, token: AuthGuard, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @param {?} value
 * @param {?=} type
 * @return {?}
 */
function formatDateToInput(value, type = 1) {
    /** @type {?} */
    const separator = value.includes('-') ? '-' : '/';
    /** @type {?} */
    let pieces;
    /** @type {?} */
    let formated;
    if (type === 1) {
        pieces = value.split(separator);
        formated = `${pieces[0]}-${pieces[1]}-${pieces[2]}`;
    }
    else if (type === 2) {
        pieces = value.split(separator);
        formated = `${pieces[2]}-${pieces[1]}-${pieces[0]}`;
    }
    if (formated && formated.length === 10) {
        formated = formated.substring(0, 10);
    }
    return formated;
}
/**
 * @param {?} value
 * @return {?}
 */
function formatDateToApiFormat(value) {
    /** @type {?} */
    const separator = value.includes('-') ? '-' : '/';
    /** @type {?} */
    const breakedDate = value.split(separator);
    /** @type {?} */
    const data = `${breakedDate[2]}/${breakedDate[1]}/${breakedDate[0]}`;
    return data;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
const ActionTypes = {
    CreateTask: '[TasklistComponent] CreateTask',
    SetAllTasks: '[TasklistComponent] SetAllTasks',
    SetValidationOnTaskForm: '[NovoComponent] SetValidationOnTaskForm',
    UpdateActionsOnTaskForm: '[NovoComponent] UpdateActionsOnTaskForm',
    SetTaskBeingEdited: '[EditComponent] SetTaskBeingEdited',
    SetUserOnTaskBeingEdited: '[EditComponent] SetUserOnTaskBeingEdited',
};
class CreateTask {
    /**
     * @param {?} payload
     */
    constructor(payload) {
        this.payload = payload;
        this.type = ActionTypes.CreateTask;
    }
}
class SetAllTasks {
    /**
     * @param {?} payload
     */
    constructor(payload) {
        this.payload = payload;
        this.type = ActionTypes.SetAllTasks;
    }
}
class SetValidationOnTaskForm {
    /**
     * @param {?} payload
     */
    constructor(payload) {
        this.payload = payload;
        this.type = ActionTypes.SetValidationOnTaskForm;
    }
}
class SetUserOnTaskBeingEdited {
    /**
     * @param {?} payload
     */
    constructor(payload) {
        this.payload = payload;
        this.type = ActionTypes.SetUserOnTaskBeingEdited;
    }
}
class UpdateActionsOnTaskForm {
    /**
     * @param {?} payload
     */
    constructor(payload) {
        this.payload = payload;
        this.type = ActionTypes.UpdateActionsOnTaskForm;
    }
}
class SetTaskBeingEdited {
    /**
     * @param {?} payload
     */
    constructor(payload) {
        this.payload = payload;
        this.type = ActionTypes.SetTaskBeingEdited;
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @param {?} formGroup
 * @param {?} store
 * @param {?} page
 * @param {?} value
 * @param {?} name
 * @return {?}
 */
function handleFieldChange(formGroup, store, page, value, name) {
    if (value) {
        formGroup.get(`variables.${name}.value`).setValue(value);
        store.dispatch(new SetValidationOnTaskForm({
            page: page,
            fieldName: name,
            validationType: '',
            validationMessage: ''
        }));
    }
    else if (this.props.form.actions.submitted) {
        store.dispatch(new SetValidationOnTaskForm({
            page: page,
            fieldName: name,
            validationType: 'error',
            validationMessage: 'Campo obrigatório'
        }));
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @param {?} err
 * @param {?} loader
 * @return {?}
 */
function handleError(err, loader) {
    console.log('Error:', err);
    this.loaderService.removeAnItemToLoader(loader);
    if (err instanceof HttpErrorResponse) {
        this.notificationService.addMessage({
            type: 'error',
            message: err.error.message || err.error.error_description
        });
    }
    else {
        this.notificationService.addMessage({
            type: 'error',
            message: err.message
        });
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const defaultState = {
    'tasklist': {
        'tasks': []
    },
    'tasklist/novo': {
        'form': {
            'validations': {
                'cliente': {
                    'type': '',
                    'message': ''
                },
                'cnpj': {
                    'type': '',
                    'message': ''
                },
                'dataVisita': {
                    'type': '',
                    'message': ''
                }
            },
            'actions': {
                'submitted': false
            }
        }
    },
    'tasklist/editar': {
        'task': {
            'id': null,
            'name': '',
            'type': '',
            'claimed': false,
            'claimedAuthor': ''
        },
        'user': {
            'id': null
        },
        'form': {
            'validations': {
                'cliente': {
                    'type': '',
                    'message': ''
                },
                'cnpj': {
                    'type': '',
                    'message': ''
                },
                'dataVisita': {
                    'type': '',
                    'message': ''
                }
            },
            'actions': {
                'submitted': false
            }
        }
    }
};
/**
 * @param {?=} state
 * @param {?=} action
 * @return {?}
 */
function taskReducer(state = defaultState, action) {
    switch (action.type) {
        case ActionTypes.CreateTask:
            /** @type {?} */
            const tasks = state.tasklist.tasks;
            tasks.push(action.payload.task);
            return Object.assign({}, state, {
                tasklist: Object.assign({}, state.tasklist, { tasks: tasks })
            });
        case ActionTypes.SetAllTasks:
            return Object.assign({}, state, {
                tasklist: Object.assign({}, state.tasklist, { tasks: action.payload.tasks })
            });
        case ActionTypes.SetValidationOnTaskForm:
            return Object.assign({}, state, {
                [action.payload.page]: {
                    task: state[action.payload.page].task,
                    user: state[action.payload.page].user,
                    form: {
                        actions: state[action.payload.page].form.actions,
                        validations: Object.assign({}, state[action.payload.page].form.validations, { [action.payload.fieldName]: {
                                type: action.payload.validationType,
                                message: action.payload.validationMessage
                            } })
                    }
                }
            });
        case ActionTypes.SetUserOnTaskBeingEdited:
            return Object.assign({}, state, {
                'tasklist/editar': Object.assign({}, state['tasklist/editar'], { user: action.payload.user })
            });
        case ActionTypes.UpdateActionsOnTaskForm:
            return Object.assign({}, state, {
                [action.payload.page]: Object.assign({}, state[action.payload.page], { form: Object.assign({}, state[action.payload.page].form, { actions: Object.assign({}, state[action.payload.page].form.actions, { [action.payload.actionName]: action.payload.actionValue }) }) })
            });
        case ActionTypes.SetTaskBeingEdited:
            return Object.assign({}, state, {
                'tasklist/editar': Object.assign({}, state['tasklist/editar'], { task: Object.assign({}, state['tasklist/editar'].task, { [action.payload.fieldName]: action.payload.fieldValue }) })
            });
        default:
            return state;
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { MainModule, HeaderModule, AsideModule, AuthService, AuthInterceptor, AuthGuard, formatDateToInput, formatDateToApiFormat, handleFieldChange, handleError, ActionTypes, CreateTask, SetAllTasks, SetValidationOnTaskForm, SetUserOnTaskBeingEdited, UpdateActionsOnTaskForm, SetTaskBeingEdited, taskReducer, defaultState, AsideComponent as ɵc, HeaderComponent as ɵb, MainComponent as ɵa };

//# sourceMappingURL=sofisa-core.js.map