/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { ActionTypes } from '../../actions/tasks/tasks.actions';
/** @type {?} */
export var defaultState = {
    'tasklist': {
        'tasks': []
    },
    'tasklist/novo': {
        'form': {
            'validations': {
                'cliente': {
                    'type': '',
                    'message': ''
                },
                'cnpj': {
                    'type': '',
                    'message': ''
                },
                'dataVisita': {
                    'type': '',
                    'message': ''
                }
            },
            'actions': {
                'submitted': false
            }
        }
    },
    'tasklist/editar': {
        'task': {
            'id': null,
            'name': '',
            'type': '',
            'claimed': false,
            'claimedAuthor': ''
        },
        'user': {
            'id': null
        },
        'form': {
            'validations': {
                'cliente': {
                    'type': '',
                    'message': ''
                },
                'cnpj': {
                    'type': '',
                    'message': ''
                },
                'dataVisita': {
                    'type': '',
                    'message': ''
                }
            },
            'actions': {
                'submitted': false
            }
        }
    }
};
/**
 * @param {?=} state
 * @param {?=} action
 * @return {?}
 */
export function taskReducer(state, action) {
    if (state === void 0) { state = defaultState; }
    var _a, _b, _c, _d, _e;
    switch (action.type) {
        case ActionTypes.CreateTask:
            /** @type {?} */
            var tasks = state.tasklist.tasks;
            tasks.push(action.payload.task);
            return Object.assign({}, state, {
                tasklist: tslib_1.__assign({}, state.tasklist, { tasks: tasks })
            });
        case ActionTypes.SetAllTasks:
            return Object.assign({}, state, {
                tasklist: tslib_1.__assign({}, state.tasklist, { tasks: action.payload.tasks })
            });
        case ActionTypes.SetValidationOnTaskForm:
            return Object.assign({}, state, (_a = {},
                _a[action.payload.page] = {
                    task: state[action.payload.page].task,
                    user: state[action.payload.page].user,
                    form: {
                        actions: state[action.payload.page].form.actions,
                        validations: tslib_1.__assign({}, state[action.payload.page].form.validations, (_b = {}, _b[action.payload.fieldName] = {
                            type: action.payload.validationType,
                            message: action.payload.validationMessage
                        }, _b))
                    }
                },
                _a));
        case ActionTypes.SetUserOnTaskBeingEdited:
            return Object.assign({}, state, {
                'tasklist/editar': tslib_1.__assign({}, state['tasklist/editar'], { user: action.payload.user })
            });
        case ActionTypes.UpdateActionsOnTaskForm:
            return Object.assign({}, state, (_c = {},
                _c[action.payload.page] = tslib_1.__assign({}, state[action.payload.page], { form: tslib_1.__assign({}, state[action.payload.page].form, { actions: tslib_1.__assign({}, state[action.payload.page].form.actions, (_d = {}, _d[action.payload.actionName] = action.payload.actionValue, _d)) }) }),
                _c));
        case ActionTypes.SetTaskBeingEdited:
            return Object.assign({}, state, {
                'tasklist/editar': tslib_1.__assign({}, state['tasklist/editar'], { task: tslib_1.__assign({}, state['tasklist/editar'].task, (_e = {}, _e[action.payload.fieldName] = action.payload.fieldValue, _e)) })
            });
        default:
            return state;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFzay5yZWR1Y2VyLmpzIiwic291cmNlUm9vdCI6Im5nOi8vc29maXNhLWNvcmUvIiwic291cmNlcyI6WyJsaWIvbmdyeC9yZWR1Y2Vycy90YXNrL3Rhc2sucmVkdWNlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBQyxXQUFXLEVBQUMsTUFBTSxtQ0FBbUMsQ0FBQzs7QUFFOUQsTUFBTSxLQUFPLFlBQVksR0FBRztJQUMzQixVQUFVLEVBQUU7UUFDWCxPQUFPLEVBQUUsRUFBRTtLQUNYO0lBQ0QsZUFBZSxFQUFFO1FBQ2hCLE1BQU0sRUFBRTtZQUNQLGFBQWEsRUFBRTtnQkFDZCxTQUFTLEVBQUU7b0JBQ1YsTUFBTSxFQUFFLEVBQUU7b0JBQ1YsU0FBUyxFQUFFLEVBQUU7aUJBQ2I7Z0JBQ0QsTUFBTSxFQUFFO29CQUNQLE1BQU0sRUFBRSxFQUFFO29CQUNWLFNBQVMsRUFBRSxFQUFFO2lCQUNiO2dCQUNELFlBQVksRUFBRTtvQkFDYixNQUFNLEVBQUUsRUFBRTtvQkFDVixTQUFTLEVBQUUsRUFBRTtpQkFDYjthQUNEO1lBQ0QsU0FBUyxFQUFFO2dCQUNWLFdBQVcsRUFBRSxLQUFLO2FBQ2xCO1NBQ0Q7S0FDRDtJQUNELGlCQUFpQixFQUFFO1FBQ2xCLE1BQU0sRUFBRTtZQUNQLElBQUksRUFBRSxJQUFJO1lBQ1YsTUFBTSxFQUFFLEVBQUU7WUFDVixNQUFNLEVBQUUsRUFBRTtZQUNWLFNBQVMsRUFBRSxLQUFLO1lBQ2hCLGVBQWUsRUFBRSxFQUFFO1NBQ25CO1FBQ0QsTUFBTSxFQUFFO1lBQ1AsSUFBSSxFQUFFLElBQUk7U0FDVjtRQUNELE1BQU0sRUFBRTtZQUNQLGFBQWEsRUFBRTtnQkFDZCxTQUFTLEVBQUU7b0JBQ1YsTUFBTSxFQUFFLEVBQUU7b0JBQ1YsU0FBUyxFQUFFLEVBQUU7aUJBQ2I7Z0JBQ0QsTUFBTSxFQUFFO29CQUNQLE1BQU0sRUFBRSxFQUFFO29CQUNWLFNBQVMsRUFBRSxFQUFFO2lCQUNiO2dCQUNELFlBQVksRUFBRTtvQkFDYixNQUFNLEVBQUUsRUFBRTtvQkFDVixTQUFTLEVBQUUsRUFBRTtpQkFDYjthQUNEO1lBQ0QsU0FBUyxFQUFFO2dCQUNWLFdBQVcsRUFBRSxLQUFLO2FBQ2xCO1NBQ0Q7S0FDRDtDQUNEOzs7Ozs7QUFFRCxNQUFNLFVBQVUsV0FBVyxDQUFDLEtBQW9CLEVBQUUsTUFBVztJQUFqQyxzQkFBQSxFQUFBLG9CQUFvQjs7SUFDL0MsUUFBUSxNQUFNLENBQUMsSUFBSSxFQUFFO1FBQ3BCLEtBQUssV0FBVyxDQUFDLFVBQVU7O2dCQUNwQixLQUFLLEdBQUcsS0FBSyxDQUFDLFFBQVEsQ0FBQyxLQUFLO1lBQ2xDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUVoQyxPQUFPLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLEtBQUssRUFBRTtnQkFDL0IsUUFBUSx1QkFDSixLQUFLLENBQUMsUUFBUSxJQUNqQixLQUFLLEVBQUUsS0FBSyxHQUNaO2FBQ0QsQ0FBQyxDQUFDO1FBQ0osS0FBSyxXQUFXLENBQUMsV0FBVztZQUMzQixPQUFPLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLEtBQUssRUFBRTtnQkFDL0IsUUFBUSx1QkFDSixLQUFLLENBQUMsUUFBUSxJQUNqQixLQUFLLEVBQUUsTUFBTSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEdBQzNCO2FBQ0QsQ0FBQyxDQUFDO1FBQ0osS0FBSyxXQUFXLENBQUMsdUJBQXVCO1lBQ3ZDLE9BQU8sTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsS0FBSztnQkFDN0IsR0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksSUFBRztvQkFDdEIsSUFBSSxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUk7b0JBQ3JDLElBQUksRUFBRSxLQUFLLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJO29CQUNyQyxJQUFJLEVBQUU7d0JBQ0wsT0FBTyxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPO3dCQUNoRCxXQUFXLHVCQUNQLEtBQUssQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLGVBQzdDLE1BQU0sQ0FBQyxPQUFPLENBQUMsU0FBUyxJQUFHOzRCQUMzQixJQUFJLEVBQUUsTUFBTSxDQUFDLE9BQU8sQ0FBQyxjQUFjOzRCQUNuQyxPQUFPLEVBQUUsTUFBTSxDQUFDLE9BQU8sQ0FBQyxpQkFBaUI7eUJBQ3pDLE1BQ0Q7cUJBQ0Q7aUJBQ0Q7b0JBQ0EsQ0FBQztRQUNKLEtBQUssV0FBVyxDQUFDLHdCQUF3QjtZQUN4QyxPQUFPLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLEtBQUssRUFBRTtnQkFDL0IsaUJBQWlCLHVCQUNiLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxJQUMzQixJQUFJLEVBQUUsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEdBQ3pCO2FBQ0QsQ0FBQyxDQUFDO1FBQ0osS0FBSyxXQUFXLENBQUMsdUJBQXVCO1lBQ3ZDLE9BQU8sTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsS0FBSztnQkFDN0IsR0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUkseUJBQ2hCLEtBQUssQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUM3QixJQUFJLHVCQUNBLEtBQUssQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksSUFDbEMsT0FBTyx1QkFDSCxLQUFLLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxlQUN6QyxNQUFNLENBQUMsT0FBTyxDQUFDLFVBQVUsSUFBRyxNQUFNLENBQUMsT0FBTyxDQUFDLFdBQVcsWUFHekQ7b0JBQ0EsQ0FBQztRQUNKLEtBQUssV0FBVyxDQUFDLGtCQUFrQjtZQUNsQyxPQUFPLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLEtBQUssRUFBRTtnQkFDL0IsaUJBQWlCLHVCQUNiLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxJQUMzQixJQUFJLHVCQUNBLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLElBQUksZUFDL0IsTUFBTSxDQUFDLE9BQU8sQ0FBQyxTQUFTLElBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQyxVQUFVLFNBRXREO2FBQ0QsQ0FBQyxDQUFDO1FBQ0o7WUFDQyxPQUFPLEtBQUssQ0FBQztLQUNkO0FBQ0YsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7QWN0aW9uVHlwZXN9IGZyb20gJy4uLy4uL2FjdGlvbnMvdGFza3MvdGFza3MuYWN0aW9ucyc7XG5cbmV4cG9ydCBjb25zdCBkZWZhdWx0U3RhdGUgPSB7XG5cdCd0YXNrbGlzdCc6IHtcblx0XHQndGFza3MnOiBbXVxuXHR9LFxuXHQndGFza2xpc3Qvbm92byc6IHtcblx0XHQnZm9ybSc6IHtcblx0XHRcdCd2YWxpZGF0aW9ucyc6IHtcblx0XHRcdFx0J2NsaWVudGUnOiB7XG5cdFx0XHRcdFx0J3R5cGUnOiAnJyxcblx0XHRcdFx0XHQnbWVzc2FnZSc6ICcnXG5cdFx0XHRcdH0sXG5cdFx0XHRcdCdjbnBqJzoge1xuXHRcdFx0XHRcdCd0eXBlJzogJycsXG5cdFx0XHRcdFx0J21lc3NhZ2UnOiAnJ1xuXHRcdFx0XHR9LFxuXHRcdFx0XHQnZGF0YVZpc2l0YSc6IHtcblx0XHRcdFx0XHQndHlwZSc6ICcnLFxuXHRcdFx0XHRcdCdtZXNzYWdlJzogJydcblx0XHRcdFx0fVxuXHRcdFx0fSxcblx0XHRcdCdhY3Rpb25zJzoge1xuXHRcdFx0XHQnc3VibWl0dGVkJzogZmFsc2Vcblx0XHRcdH1cblx0XHR9XG5cdH0sXG5cdCd0YXNrbGlzdC9lZGl0YXInOiB7XG5cdFx0J3Rhc2snOiB7XG5cdFx0XHQnaWQnOiBudWxsLFxuXHRcdFx0J25hbWUnOiAnJyxcblx0XHRcdCd0eXBlJzogJycsXG5cdFx0XHQnY2xhaW1lZCc6IGZhbHNlLFxuXHRcdFx0J2NsYWltZWRBdXRob3InOiAnJ1xuXHRcdH0sXG5cdFx0J3VzZXInOiB7XG5cdFx0XHQnaWQnOiBudWxsXG5cdFx0fSxcblx0XHQnZm9ybSc6IHtcblx0XHRcdCd2YWxpZGF0aW9ucyc6IHtcblx0XHRcdFx0J2NsaWVudGUnOiB7XG5cdFx0XHRcdFx0J3R5cGUnOiAnJyxcblx0XHRcdFx0XHQnbWVzc2FnZSc6ICcnXG5cdFx0XHRcdH0sXG5cdFx0XHRcdCdjbnBqJzoge1xuXHRcdFx0XHRcdCd0eXBlJzogJycsXG5cdFx0XHRcdFx0J21lc3NhZ2UnOiAnJ1xuXHRcdFx0XHR9LFxuXHRcdFx0XHQnZGF0YVZpc2l0YSc6IHtcblx0XHRcdFx0XHQndHlwZSc6ICcnLFxuXHRcdFx0XHRcdCdtZXNzYWdlJzogJydcblx0XHRcdFx0fVxuXHRcdFx0fSxcblx0XHRcdCdhY3Rpb25zJzoge1xuXHRcdFx0XHQnc3VibWl0dGVkJzogZmFsc2Vcblx0XHRcdH1cblx0XHR9XG5cdH1cbn07XG5cbmV4cG9ydCBmdW5jdGlvbiB0YXNrUmVkdWNlcihzdGF0ZSA9IGRlZmF1bHRTdGF0ZSwgYWN0aW9uOiBhbnkpIHtcblx0c3dpdGNoIChhY3Rpb24udHlwZSkge1xuXHRcdGNhc2UgQWN0aW9uVHlwZXMuQ3JlYXRlVGFzazpcblx0XHRcdGNvbnN0IHRhc2tzID0gc3RhdGUudGFza2xpc3QudGFza3M7XG5cdFx0XHR0YXNrcy5wdXNoKGFjdGlvbi5wYXlsb2FkLnRhc2spO1xuXHRcdFx0XG5cdFx0XHRyZXR1cm4gT2JqZWN0LmFzc2lnbih7fSwgc3RhdGUsIHtcblx0XHRcdFx0dGFza2xpc3Q6IHtcblx0XHRcdFx0XHQuLi5zdGF0ZS50YXNrbGlzdCxcblx0XHRcdFx0XHR0YXNrczogdGFza3Ncblx0XHRcdFx0fVxuXHRcdFx0fSk7XG5cdFx0Y2FzZSBBY3Rpb25UeXBlcy5TZXRBbGxUYXNrczpcblx0XHRcdHJldHVybiBPYmplY3QuYXNzaWduKHt9LCBzdGF0ZSwge1xuXHRcdFx0XHR0YXNrbGlzdDoge1xuXHRcdFx0XHRcdC4uLnN0YXRlLnRhc2tsaXN0LFxuXHRcdFx0XHRcdHRhc2tzOiBhY3Rpb24ucGF5bG9hZC50YXNrc1xuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHRjYXNlIEFjdGlvblR5cGVzLlNldFZhbGlkYXRpb25PblRhc2tGb3JtOlxuXHRcdFx0cmV0dXJuIE9iamVjdC5hc3NpZ24oe30sIHN0YXRlLCB7XG5cdFx0XHRcdFthY3Rpb24ucGF5bG9hZC5wYWdlXToge1xuXHRcdFx0XHRcdHRhc2s6IHN0YXRlW2FjdGlvbi5wYXlsb2FkLnBhZ2VdLnRhc2ssXG5cdFx0XHRcdFx0dXNlcjogc3RhdGVbYWN0aW9uLnBheWxvYWQucGFnZV0udXNlcixcblx0XHRcdFx0XHRmb3JtOiB7XG5cdFx0XHRcdFx0XHRhY3Rpb25zOiBzdGF0ZVthY3Rpb24ucGF5bG9hZC5wYWdlXS5mb3JtLmFjdGlvbnMsXG5cdFx0XHRcdFx0XHR2YWxpZGF0aW9uczoge1xuXHRcdFx0XHRcdFx0XHQuLi5zdGF0ZVthY3Rpb24ucGF5bG9hZC5wYWdlXS5mb3JtLnZhbGlkYXRpb25zLFxuXHRcdFx0XHRcdFx0XHRbYWN0aW9uLnBheWxvYWQuZmllbGROYW1lXToge1xuXHRcdFx0XHRcdFx0XHRcdHR5cGU6IGFjdGlvbi5wYXlsb2FkLnZhbGlkYXRpb25UeXBlLFxuXHRcdFx0XHRcdFx0XHRcdG1lc3NhZ2U6IGFjdGlvbi5wYXlsb2FkLnZhbGlkYXRpb25NZXNzYWdlXG5cdFx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHR9XG5cdFx0XHRcdH1cblx0XHRcdH0pO1xuXHRcdGNhc2UgQWN0aW9uVHlwZXMuU2V0VXNlck9uVGFza0JlaW5nRWRpdGVkOlxuXHRcdFx0cmV0dXJuIE9iamVjdC5hc3NpZ24oe30sIHN0YXRlLCB7XG5cdFx0XHRcdCd0YXNrbGlzdC9lZGl0YXInOiB7XG5cdFx0XHRcdFx0Li4uc3RhdGVbJ3Rhc2tsaXN0L2VkaXRhciddLFxuXHRcdFx0XHRcdHVzZXI6IGFjdGlvbi5wYXlsb2FkLnVzZXJcblx0XHRcdFx0fVxuXHRcdFx0fSk7XG5cdFx0Y2FzZSBBY3Rpb25UeXBlcy5VcGRhdGVBY3Rpb25zT25UYXNrRm9ybTpcblx0XHRcdHJldHVybiBPYmplY3QuYXNzaWduKHt9LCBzdGF0ZSwge1xuXHRcdFx0XHRbYWN0aW9uLnBheWxvYWQucGFnZV06IHtcblx0XHRcdFx0XHQuLi5zdGF0ZVthY3Rpb24ucGF5bG9hZC5wYWdlXSxcblx0XHRcdFx0XHRmb3JtOiB7XG5cdFx0XHRcdFx0XHQuLi5zdGF0ZVthY3Rpb24ucGF5bG9hZC5wYWdlXS5mb3JtLFxuXHRcdFx0XHRcdFx0YWN0aW9uczoge1xuXHRcdFx0XHRcdFx0XHQuLi5zdGF0ZVthY3Rpb24ucGF5bG9hZC5wYWdlXS5mb3JtLmFjdGlvbnMsXG5cdFx0XHRcdFx0XHRcdFthY3Rpb24ucGF5bG9hZC5hY3Rpb25OYW1lXTogYWN0aW9uLnBheWxvYWQuYWN0aW9uVmFsdWVcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHR9XG5cdFx0XHRcdH1cblx0XHRcdH0pO1xuXHRcdGNhc2UgQWN0aW9uVHlwZXMuU2V0VGFza0JlaW5nRWRpdGVkOlxuXHRcdFx0cmV0dXJuIE9iamVjdC5hc3NpZ24oe30sIHN0YXRlLCB7XG5cdFx0XHRcdCd0YXNrbGlzdC9lZGl0YXInOiB7XG5cdFx0XHRcdFx0Li4uc3RhdGVbJ3Rhc2tsaXN0L2VkaXRhciddLFxuXHRcdFx0XHRcdHRhc2s6IHtcblx0XHRcdFx0XHRcdC4uLnN0YXRlWyd0YXNrbGlzdC9lZGl0YXInXS50YXNrLFxuXHRcdFx0XHRcdFx0W2FjdGlvbi5wYXlsb2FkLmZpZWxkTmFtZV06IGFjdGlvbi5wYXlsb2FkLmZpZWxkVmFsdWVcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH1cblx0XHRcdH0pO1xuXHRcdGRlZmF1bHQ6XG5cdFx0XHRyZXR1cm4gc3RhdGU7XG5cdH1cbn1cblxuXG4iXX0=