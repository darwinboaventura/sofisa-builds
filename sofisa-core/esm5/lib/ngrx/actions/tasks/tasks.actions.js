/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
var ActionTypes = {
    CreateTask: '[TasklistComponent] CreateTask',
    SetAllTasks: '[TasklistComponent] SetAllTasks',
    SetValidationOnTaskForm: '[NovoComponent] SetValidationOnTaskForm',
    UpdateActionsOnTaskForm: '[NovoComponent] UpdateActionsOnTaskForm',
    SetTaskBeingEdited: '[EditComponent] SetTaskBeingEdited',
    SetUserOnTaskBeingEdited: '[EditComponent] SetUserOnTaskBeingEdited',
};
export { ActionTypes };
var CreateTask = /** @class */ (function () {
    function CreateTask(payload) {
        this.payload = payload;
        this.type = ActionTypes.CreateTask;
    }
    return CreateTask;
}());
export { CreateTask };
if (false) {
    /** @type {?} */
    CreateTask.prototype.type;
    /** @type {?} */
    CreateTask.prototype.payload;
}
var SetAllTasks = /** @class */ (function () {
    function SetAllTasks(payload) {
        this.payload = payload;
        this.type = ActionTypes.SetAllTasks;
    }
    return SetAllTasks;
}());
export { SetAllTasks };
if (false) {
    /** @type {?} */
    SetAllTasks.prototype.type;
    /** @type {?} */
    SetAllTasks.prototype.payload;
}
var SetValidationOnTaskForm = /** @class */ (function () {
    function SetValidationOnTaskForm(payload) {
        this.payload = payload;
        this.type = ActionTypes.SetValidationOnTaskForm;
    }
    return SetValidationOnTaskForm;
}());
export { SetValidationOnTaskForm };
if (false) {
    /** @type {?} */
    SetValidationOnTaskForm.prototype.type;
    /** @type {?} */
    SetValidationOnTaskForm.prototype.payload;
}
var SetUserOnTaskBeingEdited = /** @class */ (function () {
    function SetUserOnTaskBeingEdited(payload) {
        this.payload = payload;
        this.type = ActionTypes.SetUserOnTaskBeingEdited;
    }
    return SetUserOnTaskBeingEdited;
}());
export { SetUserOnTaskBeingEdited };
if (false) {
    /** @type {?} */
    SetUserOnTaskBeingEdited.prototype.type;
    /** @type {?} */
    SetUserOnTaskBeingEdited.prototype.payload;
}
var UpdateActionsOnTaskForm = /** @class */ (function () {
    function UpdateActionsOnTaskForm(payload) {
        this.payload = payload;
        this.type = ActionTypes.UpdateActionsOnTaskForm;
    }
    return UpdateActionsOnTaskForm;
}());
export { UpdateActionsOnTaskForm };
if (false) {
    /** @type {?} */
    UpdateActionsOnTaskForm.prototype.type;
    /** @type {?} */
    UpdateActionsOnTaskForm.prototype.payload;
}
var SetTaskBeingEdited = /** @class */ (function () {
    function SetTaskBeingEdited(payload) {
        this.payload = payload;
        this.type = ActionTypes.SetTaskBeingEdited;
    }
    return SetTaskBeingEdited;
}());
export { SetTaskBeingEdited };
if (false) {
    /** @type {?} */
    SetTaskBeingEdited.prototype.type;
    /** @type {?} */
    SetTaskBeingEdited.prototype.payload;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFza3MuYWN0aW9ucy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3NvZmlzYS1jb3JlLyIsInNvdXJjZXMiOlsibGliL25ncngvYWN0aW9ucy90YXNrcy90YXNrcy5hY3Rpb25zLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztJQUdDLFlBQWEsZ0NBQWdDO0lBQzdDLGFBQWMsaUNBQWlDO0lBQy9DLHlCQUEwQix5Q0FBeUM7SUFDbkUseUJBQTBCLHlDQUF5QztJQUNuRSxvQkFBcUIsb0NBQW9DO0lBQ3pELDBCQUEyQiwwQ0FBMEM7OztBQUd0RTtJQUdDLG9CQUFtQixPQUFzQjtRQUF0QixZQUFPLEdBQVAsT0FBTyxDQUFlO1FBRmhDLFNBQUksR0FBRyxXQUFXLENBQUMsVUFBVSxDQUFDO0lBRUssQ0FBQztJQUM5QyxpQkFBQztBQUFELENBQUMsQUFKRCxJQUlDOzs7O0lBSEEsMEJBQXVDOztJQUUzQiw2QkFBNkI7O0FBRzFDO0lBR0MscUJBQW1CLE9BQXVCO1FBQXZCLFlBQU8sR0FBUCxPQUFPLENBQWdCO1FBRmpDLFNBQUksR0FBRyxXQUFXLENBQUMsV0FBVyxDQUFDO0lBRUssQ0FBQztJQUMvQyxrQkFBQztBQUFELENBQUMsQUFKRCxJQUlDOzs7O0lBSEEsMkJBQXdDOztJQUU1Qiw4QkFBOEI7O0FBRzNDO0lBR0MsaUNBQW1CLE9BQWdHO1FBQWhHLFlBQU8sR0FBUCxPQUFPLENBQXlGO1FBRjFHLFNBQUksR0FBRyxXQUFXLENBQUMsdUJBQXVCLENBQUM7SUFFa0UsQ0FBQztJQUN4SCw4QkFBQztBQUFELENBQUMsQUFKRCxJQUlDOzs7O0lBSEEsdUNBQW9EOztJQUV4QywwQ0FBdUc7O0FBR3BIO0lBR0Msa0NBQW1CLE9BQXNCO1FBQXRCLFlBQU8sR0FBUCxPQUFPLENBQWU7UUFGaEMsU0FBSSxHQUFHLFdBQVcsQ0FBQyx3QkFBd0IsQ0FBQztJQUVULENBQUM7SUFDOUMsK0JBQUM7QUFBRCxDQUFDLEFBSkQsSUFJQzs7OztJQUhBLHdDQUFxRDs7SUFFekMsMkNBQTZCOztBQUcxQztJQUdDLGlDQUFtQixPQUFnRTtRQUFoRSxZQUFPLEdBQVAsT0FBTyxDQUF5RDtRQUYxRSxTQUFJLEdBQUcsV0FBVyxDQUFDLHVCQUF1QixDQUFDO0lBRWtDLENBQUM7SUFDeEYsOEJBQUM7QUFBRCxDQUFDLEFBSkQsSUFJQzs7OztJQUhBLHVDQUFvRDs7SUFFeEMsMENBQXVFOztBQUdwRjtJQUdDLDRCQUFtQixPQUE0QztRQUE1QyxZQUFPLEdBQVAsT0FBTyxDQUFxQztRQUZ0RCxTQUFJLEdBQUcsV0FBVyxDQUFDLGtCQUFrQixDQUFDO0lBRW1CLENBQUM7SUFDcEUseUJBQUM7QUFBRCxDQUFDLEFBSkQsSUFJQzs7OztJQUhBLGtDQUErQzs7SUFFbkMscUNBQW1EIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtBY3Rpb259IGZyb20gJ0BuZ3J4L3N0b3JlJztcblxuZXhwb3J0IGVudW0gQWN0aW9uVHlwZXMge1xuXHRDcmVhdGVUYXNrID0gJ1tUYXNrbGlzdENvbXBvbmVudF0gQ3JlYXRlVGFzaycsXG5cdFNldEFsbFRhc2tzID0gJ1tUYXNrbGlzdENvbXBvbmVudF0gU2V0QWxsVGFza3MnLFxuXHRTZXRWYWxpZGF0aW9uT25UYXNrRm9ybSA9ICdbTm92b0NvbXBvbmVudF0gU2V0VmFsaWRhdGlvbk9uVGFza0Zvcm0nLFxuXHRVcGRhdGVBY3Rpb25zT25UYXNrRm9ybSA9ICdbTm92b0NvbXBvbmVudF0gVXBkYXRlQWN0aW9uc09uVGFza0Zvcm0nLFxuXHRTZXRUYXNrQmVpbmdFZGl0ZWQgPSAnW0VkaXRDb21wb25lbnRdIFNldFRhc2tCZWluZ0VkaXRlZCcsXG5cdFNldFVzZXJPblRhc2tCZWluZ0VkaXRlZCA9ICdbRWRpdENvbXBvbmVudF0gU2V0VXNlck9uVGFza0JlaW5nRWRpdGVkJyxcbn1cblxuZXhwb3J0IGNsYXNzIENyZWF0ZVRhc2sgaW1wbGVtZW50cyBBY3Rpb24ge1xuXHRyZWFkb25seSB0eXBlID0gQWN0aW9uVHlwZXMuQ3JlYXRlVGFzaztcblx0XG5cdGNvbnN0cnVjdG9yKHB1YmxpYyBwYXlsb2FkOiB7IHRhc2s6IGFueSB9KSB7fVxufVxuXG5leHBvcnQgY2xhc3MgU2V0QWxsVGFza3MgaW1wbGVtZW50cyBBY3Rpb24ge1xuXHRyZWFkb25seSB0eXBlID0gQWN0aW9uVHlwZXMuU2V0QWxsVGFza3M7XG5cdFxuXHRjb25zdHJ1Y3RvcihwdWJsaWMgcGF5bG9hZDogeyB0YXNrczogYW55IH0pIHt9XG59XG5cbmV4cG9ydCBjbGFzcyBTZXRWYWxpZGF0aW9uT25UYXNrRm9ybSBpbXBsZW1lbnRzIEFjdGlvbiB7XG5cdHJlYWRvbmx5IHR5cGUgPSBBY3Rpb25UeXBlcy5TZXRWYWxpZGF0aW9uT25UYXNrRm9ybTtcblx0XG5cdGNvbnN0cnVjdG9yKHB1YmxpYyBwYXlsb2FkOiB7IHBhZ2U6IHN0cmluZywgZmllbGROYW1lOiBzdHJpbmcsICB2YWxpZGF0aW9uVHlwZTogc3RyaW5nLCB2YWxpZGF0aW9uTWVzc2FnZTogc3RyaW5nIH0pIHt9XG59XG5cbmV4cG9ydCBjbGFzcyBTZXRVc2VyT25UYXNrQmVpbmdFZGl0ZWQgaW1wbGVtZW50cyBBY3Rpb24ge1xuXHRyZWFkb25seSB0eXBlID0gQWN0aW9uVHlwZXMuU2V0VXNlck9uVGFza0JlaW5nRWRpdGVkO1xuXHRcblx0Y29uc3RydWN0b3IocHVibGljIHBheWxvYWQ6IHsgdXNlcjogYW55IH0pIHt9XG59XG5cbmV4cG9ydCBjbGFzcyBVcGRhdGVBY3Rpb25zT25UYXNrRm9ybSBpbXBsZW1lbnRzIEFjdGlvbiB7XG5cdHJlYWRvbmx5IHR5cGUgPSBBY3Rpb25UeXBlcy5VcGRhdGVBY3Rpb25zT25UYXNrRm9ybTtcblx0XG5cdGNvbnN0cnVjdG9yKHB1YmxpYyBwYXlsb2FkOiB7IHBhZ2U6IHN0cmluZywgYWN0aW9uTmFtZTogc3RyaW5nLCAgYWN0aW9uVmFsdWU6IGFueSB9KSB7fVxufVxuXG5leHBvcnQgY2xhc3MgU2V0VGFza0JlaW5nRWRpdGVkIGltcGxlbWVudHMgQWN0aW9uIHtcblx0cmVhZG9ubHkgdHlwZSA9IEFjdGlvblR5cGVzLlNldFRhc2tCZWluZ0VkaXRlZDtcblx0XG5cdGNvbnN0cnVjdG9yKHB1YmxpYyBwYXlsb2FkOiB7IGZpZWxkTmFtZTogYW55LCBmaWVsZFZhbHVlOiBhbnkgfSkge31cbn1cblxuIl19