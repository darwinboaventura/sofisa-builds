/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
var MainComponent = /** @class */ (function () {
    function MainComponent() {
    }
    /**
     * @return {?}
     */
    MainComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () { };
    MainComponent.decorators = [
        { type: Component, args: [{
                    selector: 'so-main',
                    template: "<main class=\"so-main clearfix\">\n\t<ng-content></ng-content>\n</main>",
                    styles: [".so-main{width:100%;min-height:100vh;padding-top:121px;padding-left:274px;padding-right:24px}"]
                }] }
    ];
    /** @nocollapse */
    MainComponent.ctorParameters = function () { return []; };
    return MainComponent;
}());
export { MainComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFpbi5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9zb2Zpc2EtY29yZS8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL21haW4vbWFpbi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQVMsTUFBTSxlQUFlLENBQUM7QUFFaEQ7SUFPQztJQUFnQixDQUFDOzs7O0lBRWpCLGdDQUFROzs7SUFBUixjQUFZLENBQUM7O2dCQVRiLFNBQVMsU0FBQztvQkFDVixRQUFRLEVBQUUsU0FBUztvQkFDbkIsbUZBQW9DOztpQkFFcEM7Ozs7SUFNRCxvQkFBQztDQUFBLEFBVkQsSUFVQztTQUpZLGFBQWEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgT25Jbml0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQENvbXBvbmVudCh7XG5cdHNlbGVjdG9yOiAnc28tbWFpbicsXG5cdHRlbXBsYXRlVXJsOiAnLi9tYWluLmNvbXBvbmVudC5odG1sJyxcblx0c3R5bGVVcmxzOiBbJy4vbWFpbi5jb21wb25lbnQuc2NzcyddXG59KVxuXG5leHBvcnQgY2xhc3MgTWFpbkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cdGNvbnN0cnVjdG9yKCkgeyB9XG5cdFxuXHRuZ09uSW5pdCgpIHt9XG59XG4iXX0=