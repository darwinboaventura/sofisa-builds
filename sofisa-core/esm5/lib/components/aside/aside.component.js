/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Router } from '@angular/router';
import { Component } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';
var AsideComponent = /** @class */ (function () {
    function AsideComponent(authService, router) {
        this.authService = authService;
        this.router = router;
    }
    /**
     * @return {?}
     */
    AsideComponent.prototype.makeLogout = /**
     * @return {?}
     */
    function () {
        this.authService.doLogout();
        this.router.navigateByUrl('login');
    };
    AsideComponent.decorators = [
        { type: Component, args: [{
                    selector: 'so-aside',
                    template: "<aside class=\"so-aside clearfix\">\n\t<nav class=\"so-nav clearfix\">\n\t\t<ul class=\"so-nav--menu clearfix\">\n\t\t\t<li class=\"so-nav--menu--item clearfix\" routerLink=\"dashboard\" routerLinkActive=\"is-active\">\n\t\t\t\t<a routerLink=\"dashboard\" class=\"so-nav--menu--item--anchor clearfix\">\n\t\t\t\t\t<span class=\"so-nav--menu--item--anchor--icon\">\n\t\t\t\t\t\t<i class=\"fas fa-home\"></i>\n\t\t\t\t\t</span>\n\t\t\t\t\t\n\t\t\t\t\t<span class=\"so-nav--menu--item--anchor--name\">In\u00EDcio</span>\n\t\t\t\t</a>\n\t\t\t</li>\n\t\t\t\n\t\t\t<li class=\"so-nav--menu--item clearfix\" routerLink=\"tasklist\" routerLinkActive=\"is-active\">\n\t\t\t\t<a routerLink=\"tasklist\" class=\"so-nav--menu--item--anchor clearfix\">\n\t\t\t\t\t<span class=\"so-nav--menu--item--anchor--icon\">\n\t\t\t\t\t\t<i class=\"far fa-file\"></i>\n\t\t\t\t\t</span>\n\t\t\t\t\t\n\t\t\t\t\t<span class=\"so-nav--menu--item--anchor--name\">Tasklist</span>\n\t\t\t\t</a>\n\t\t\t</li>\n\t\t\t\n\t\t\t<li class=\"so-nav--menu--item clearfix\" routerLink=\"admin\" routerLinkActive=\"is-active\">\n\t\t\t\t<a routerLink=\"admin\" class=\"so-nav--menu--item--anchor clearfix\">\n\t\t\t\t\t<span class=\"so-nav--menu--item--anchor--icon\">\n\t\t\t\t\t\t<i class=\"far fa-user\"></i>\n\t\t\t\t\t</span>\n\t\t\t\t\t\n\t\t\t\t\t<span class=\"so-nav--menu--item--anchor--name\">Admin</span>\n\t\t\t\t</a>\n\t\t\t</li>\n\t\t</ul>\n\t</nav>\n\t\n\t<div class=\"so-aside--logout clearfix\" (click)=\"makeLogout()\">\n\t\t<span class=\"so-aside--logout--icon\">\n\t\t\t<i class=\"fas fa-sign-out-alt\"></i>\n\t\t</span>\n\t\t\n\t\t<span class=\"so-aside--logout--name\">Sair</span>\n\t</div>\n</aside>",
                    styles: [".so-aside{width:250px;height:100vh;position:fixed;top:0;left:0;padding-top:88px;background:#fff}.so-aside--logout{position:absolute;bottom:32px;left:24px}.so-aside--logout--icon,.so-aside--logout--name{color:#bfc0c0;display:block;cursor:pointer}.so-aside--logout--name{font-family:Roboto;font-weight:400;font-size:16px;line-height:24px;position:absolute;left:41px;top:50%;-webkit-transform:translateY(-50%);transform:translateY(-50%)}.so-aside--logout--icon i{color:inherit}.so-nav--menu{list-style:none;padding:0;margin:0}.so-nav--menu--item--anchor{padding:16px 24px;display:block;text-decoration:none}.so-nav--menu--item--anchor--icon,.so-nav--menu--item--anchor--name{color:#bfc0c0;float:left;display:block}.so-nav--menu--item--anchor--icon{width:20px;height:20px;margin-right:21px}.so-nav--menu--item--anchor--icon i{color:inherit}.so-nav--menu--item--anchor--name{font-family:Roboto;font-weight:400;font-size:16px;line-height:24px}.so-nav--menu--item.is-active .so-nav--menu--item--anchor,.so-nav--menu--item:hover .so-nav--menu--item--anchor{background-color:#e5e5e5}.so-nav--menu--item.is-active .so-nav--menu--item--anchor--name,.so-nav--menu--item:hover .so-nav--menu--item--anchor--name{font-weight:500;color:#8a8a8a}"]
                }] }
    ];
    /** @nocollapse */
    AsideComponent.ctorParameters = function () { return [
        { type: AuthService },
        { type: Router }
    ]; };
    return AsideComponent;
}());
export { AsideComponent };
if (false) {
    /** @type {?} */
    AsideComponent.prototype.authService;
    /** @type {?} */
    AsideComponent.prototype.router;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXNpZGUuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vc29maXNhLWNvcmUvIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9hc2lkZS9hc2lkZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBQyxNQUFNLEVBQUMsTUFBTSxpQkFBaUIsQ0FBQztBQUN2QyxPQUFPLEVBQUMsU0FBUyxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ3hDLE9BQU8sRUFBQyxXQUFXLEVBQUMsTUFBTSxrQ0FBa0MsQ0FBQztBQUU3RDtJQU9DLHdCQUFtQixXQUF3QixFQUFTLE1BQWM7UUFBL0MsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFBUyxXQUFNLEdBQU4sTUFBTSxDQUFRO0lBQUcsQ0FBQzs7OztJQUV0RSxtQ0FBVTs7O0lBQVY7UUFDQyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBRTVCLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQ3BDLENBQUM7O2dCQWJELFNBQVMsU0FBQztvQkFDVixRQUFRLEVBQUUsVUFBVTtvQkFDcEIsOHBEQUFxQzs7aUJBRXJDOzs7O2dCQU5PLFdBQVc7Z0JBRlgsTUFBTTs7SUFrQmQscUJBQUM7Q0FBQSxBQWRELElBY0M7U0FSWSxjQUFjOzs7SUFDZCxxQ0FBK0I7O0lBQUUsZ0NBQXFCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtSb3V0ZXJ9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5pbXBvcnQge0NvbXBvbmVudH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0F1dGhTZXJ2aWNlfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9hdXRoL2F1dGguc2VydmljZSc7XG5cbkBDb21wb25lbnQoe1xuXHRzZWxlY3RvcjogJ3NvLWFzaWRlJyxcblx0dGVtcGxhdGVVcmw6ICcuL2FzaWRlLmNvbXBvbmVudC5odG1sJyxcblx0c3R5bGVVcmxzOiBbJy4vYXNpZGUuY29tcG9uZW50LnNjc3MnXVxufSlcblxuZXhwb3J0IGNsYXNzIEFzaWRlQ29tcG9uZW50IHtcblx0Y29uc3RydWN0b3IocHVibGljIGF1dGhTZXJ2aWNlOiBBdXRoU2VydmljZSwgcHVibGljIHJvdXRlcjogUm91dGVyKSB7fVxuXHRcblx0bWFrZUxvZ291dCgpIHtcblx0XHR0aGlzLmF1dGhTZXJ2aWNlLmRvTG9nb3V0KCk7XG5cdFx0XG5cdFx0dGhpcy5yb3V0ZXIubmF2aWdhdGVCeVVybCgnbG9naW4nKTtcblx0fVxufVxuIl19