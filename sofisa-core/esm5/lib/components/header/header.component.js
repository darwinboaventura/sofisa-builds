/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
var HeaderComponent = /** @class */ (function () {
    function HeaderComponent() {
    }
    /**
     * @return {?}
     */
    HeaderComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () { };
    HeaderComponent.decorators = [
        { type: Component, args: [{
                    selector: 'so-header',
                    template: "<header class=\"so-header clearfix\">\n\t<div class=\"so-header--logo clearfix\">\n\t\t<a href=\"#\">\n\t\t\t<img src=\"assets/images/sofisa-logo.png\" alt=\"Sofisa\">\n\t\t</a>\n\t</div>\n\t\n\t<div class=\"so-header--profile clearfix\">\t</div>\n</header>",
                    styles: [".so-header{width:100%;height:88px;border-bottom:1px solid #e5e5e5;position:fixed;top:0;left:0;z-index:1000;background:#fff}.so-header--logo{max-width:120px;position:absolute;top:50%;left:25px;-webkit-transform:translateY(-50%);transform:translateY(-50%)}.so-header--logo img{max-width:100%}.so-header--profile{width:32px;height:32px;border-radius:50%;overflow:hidden;border:1px solid #e5e5e5;position:absolute;top:50%;right:25px;-webkit-transform:translateY(-50%);transform:translateY(-50%)}.so-header--profile img{max-width:100%;position:absolute;top:50%;left:50%;-webkit-transform:translate(-50%,-50%);transform:translate(-50%,-50%)}"]
                }] }
    ];
    /** @nocollapse */
    HeaderComponent.ctorParameters = function () { return []; };
    return HeaderComponent;
}());
export { HeaderComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGVhZGVyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3NvZmlzYS1jb3JlLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvaGVhZGVyL2hlYWRlci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQVMsTUFBTSxlQUFlLENBQUM7QUFFaEQ7SUFPQztJQUFlLENBQUM7Ozs7SUFFaEIsa0NBQVE7OztJQUFSLGNBQVksQ0FBQzs7Z0JBVGIsU0FBUyxTQUFDO29CQUNWLFFBQVEsRUFBRSxXQUFXO29CQUNyQiw2UUFBc0M7O2lCQUV0Qzs7OztJQU1ELHNCQUFDO0NBQUEsQUFWRCxJQVVDO1NBSlksZUFBZSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBPbkluaXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcblx0c2VsZWN0b3I6ICdzby1oZWFkZXInLFxuXHR0ZW1wbGF0ZVVybDogJy4vaGVhZGVyLmNvbXBvbmVudC5odG1sJyxcblx0c3R5bGVVcmxzOiBbJy4vaGVhZGVyLmNvbXBvbmVudC5zY3NzJ11cbn0pXG5cbmV4cG9ydCBjbGFzcyBIZWFkZXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXHRjb25zdHJ1Y3RvcigpIHt9XG5cdFxuXHRuZ09uSW5pdCgpIHt9XG59XG4iXX0=