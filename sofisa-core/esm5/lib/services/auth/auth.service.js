/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Store } from '@ngrx/store';
import { OAuthService } from 'angular-oauth2-oidc';
import { LoaderService } from 'sofisa-loader';
import { Injectable, Inject } from '@angular/core';
import { AddLoaderItem, RemoveLoaderItem } from 'sofisa-loader';
import { NotificationService } from 'sofisa-notification';
import * as i0 from "@angular/core";
import * as i1 from "angular-oauth2-oidc";
import * as i2 from "sofisa-loader";
import * as i3 from "@ngrx/store";
import * as i4 from "sofisa-notification";
var AuthService = /** @class */ (function () {
    function AuthService(oauthService, loaderService, store, notificationService) {
        this.oauthService = oauthService;
        this.loaderService = loaderService;
        this.store = store;
        this.notificationService = notificationService;
        // TODO: Mover para arquivos de configurações
        this.apiUri = '//sofisa-lab-gateway.eastus2.cloudapp.azure.com';
    }
    /**
     * @return {?}
     */
    AuthService.prototype.init = /**
     * @return {?}
     */
    function () {
        this.oauthService.configure({
            tokenEndpoint: this.apiUri + "api/oauth-api/oauth/token",
            redirectUri: window.location.origin,
            scope: '',
            showDebugInformation: true,
            oidc: true,
            requireHttps: false,
            clientId: 'trusted-app'
        });
    };
    /**
     * @return {?}
     */
    AuthService.prototype.isTokenValid = /**
     * @return {?}
     */
    function () {
        return this.oauthService.hasValidAccessToken();
    };
    /**
     * @return {?}
     */
    AuthService.prototype.getAccessToken = /**
     * @return {?}
     */
    function () {
        return this.oauthService.getAccessToken();
    };
    /**
     * @param {?} user
     * @return {?}
     */
    AuthService.prototype.doLogin = /**
     * @param {?} user
     * @return {?}
     */
    function (user) {
        var _this = this;
        this.store.dispatch(new AddLoaderItem({
            page: 'login',
            name: 'login'
        }));
        this.oauthService.fetchTokenUsingPasswordFlow(user.username, user.password)
            .then(function (response) {
            window.location.href = _this.oauthService.redirectUri;
        })
            .catch(function (err) {
            _this.notificationService.addMessage({
                type: 'error',
                message: 'Login ou senha incorreto! Tente novamente'
            });
        })
            .finally(function () {
            _this.store.dispatch(new RemoveLoaderItem({
                page: 'login',
                name: 'login'
            }));
        });
    };
    /**
     * @return {?}
     */
    AuthService.prototype.doLogout = /**
     * @return {?}
     */
    function () {
        this.oauthService.logOut(true);
    };
    AuthService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    AuthService.ctorParameters = function () { return [
        { type: OAuthService },
        { type: LoaderService },
        { type: undefined, decorators: [{ type: Inject, args: [Store,] }] },
        { type: NotificationService }
    ]; };
    /** @nocollapse */ AuthService.ngInjectableDef = i0.defineInjectable({ factory: function AuthService_Factory() { return new AuthService(i0.inject(i1.OAuthService), i0.inject(i2.LoaderService), i0.inject(i3.Store), i0.inject(i4.NotificationService)); }, token: AuthService, providedIn: "root" });
    return AuthService;
}());
export { AuthService };
if (false) {
    /** @type {?} */
    AuthService.prototype.apiUri;
    /**
     * @type {?}
     * @private
     */
    AuthService.prototype.oauthService;
    /** @type {?} */
    AuthService.prototype.loaderService;
    /** @type {?} */
    AuthService.prototype.store;
    /** @type {?} */
    AuthService.prototype.notificationService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vc29maXNhLWNvcmUvIiwic291cmNlcyI6WyJsaWIvc2VydmljZXMvYXV0aC9hdXRoLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBQyxLQUFLLEVBQUMsTUFBTSxhQUFhLENBQUM7QUFFbEMsT0FBTyxFQUFDLFlBQVksRUFBQyxNQUFNLHFCQUFxQixDQUFDO0FBQ2pELE9BQU8sRUFBQyxhQUFhLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDNUMsT0FBTyxFQUFDLFVBQVUsRUFBRSxNQUFNLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDakQsT0FBTyxFQUFDLGFBQWEsRUFBRSxnQkFBZ0IsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUM5RCxPQUFPLEVBQUMsbUJBQW1CLEVBQUMsTUFBTSxxQkFBcUIsQ0FBQzs7Ozs7O0FBRXhEO0lBUUMscUJBQW9CLFlBQTBCLEVBQVMsYUFBNEIsRUFBd0IsS0FBSyxFQUFTLG1CQUF3QztRQUE3SSxpQkFBWSxHQUFaLFlBQVksQ0FBYztRQUFTLGtCQUFhLEdBQWIsYUFBYSxDQUFlO1FBQXdCLFVBQUssR0FBTCxLQUFLLENBQUE7UUFBUyx3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCOztRQUZqSyxXQUFNLEdBQVcsaURBQWlELENBQUM7SUFFaUcsQ0FBQzs7OztJQUVySywwQkFBSTs7O0lBQUo7UUFDQyxJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQztZQUMzQixhQUFhLEVBQUssSUFBSSxDQUFDLE1BQU0sOEJBQTJCO1lBQ3hELFdBQVcsRUFBRSxNQUFNLENBQUMsUUFBUSxDQUFDLE1BQU07WUFDbkMsS0FBSyxFQUFFLEVBQUU7WUFDVCxvQkFBb0IsRUFBRSxJQUFJO1lBQzFCLElBQUksRUFBRSxJQUFJO1lBQ1YsWUFBWSxFQUFFLEtBQUs7WUFDbkIsUUFBUSxFQUFFLGFBQWE7U0FDdkIsQ0FBQyxDQUFDO0lBQ0osQ0FBQzs7OztJQUVELGtDQUFZOzs7SUFBWjtRQUNDLE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO0lBQ2hELENBQUM7Ozs7SUFFRCxvQ0FBYzs7O0lBQWQ7UUFDQyxPQUFPLElBQUksQ0FBQyxZQUFZLENBQUMsY0FBYyxFQUFFLENBQUM7SUFDM0MsQ0FBQzs7Ozs7SUFFRCw2QkFBTzs7OztJQUFQLFVBQVEsSUFBNEM7UUFBcEQsaUJBc0JDO1FBckJBLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksYUFBYSxDQUFDO1lBQ3JDLElBQUksRUFBRSxPQUFPO1lBQ2IsSUFBSSxFQUFFLE9BQU87U0FDYixDQUFDLENBQUMsQ0FBQztRQUVKLElBQUksQ0FBQyxZQUFZLENBQUMsMkJBQTJCLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDO2FBQzFFLElBQUksQ0FBQyxVQUFDLFFBQWE7WUFDbkIsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEdBQUcsS0FBSSxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUM7UUFDdEQsQ0FBQyxDQUFDO2FBQ0QsS0FBSyxDQUFDLFVBQUMsR0FBRztZQUNWLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxVQUFVLENBQUM7Z0JBQ25DLElBQUksRUFBRSxPQUFPO2dCQUNiLE9BQU8sRUFBRSwyQ0FBMkM7YUFDcEQsQ0FBQyxDQUFDO1FBQ0osQ0FBQyxDQUFDO2FBQ0QsT0FBTyxDQUFDO1lBQ1IsS0FBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsSUFBSSxnQkFBZ0IsQ0FBQztnQkFDeEMsSUFBSSxFQUFFLE9BQU87Z0JBQ2IsSUFBSSxFQUFFLE9BQU87YUFDYixDQUFDLENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ0osQ0FBQzs7OztJQUVELDhCQUFROzs7SUFBUjtRQUNDLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ2hDLENBQUM7O2dCQXhERCxVQUFVLFNBQUM7b0JBQ1gsVUFBVSxFQUFFLE1BQU07aUJBQ2xCOzs7O2dCQVJPLFlBQVk7Z0JBQ1osYUFBYTtnREFha0UsTUFBTSxTQUFDLEtBQUs7Z0JBVjNGLG1CQUFtQjs7O3NCQU4zQjtDQWlFQyxBQXpERCxJQXlEQztTQXJEWSxXQUFXOzs7SUFFdkIsNkJBQW1FOzs7OztJQUV2RCxtQ0FBa0M7O0lBQUUsb0NBQW1DOztJQUFFLDRCQUEyQjs7SUFBRSwwQ0FBK0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1N0b3JlfSBmcm9tICdAbmdyeC9zdG9yZSc7XG5pbXBvcnQge2hhbmRsZUVycm9yfSBmcm9tICcuLi8uLi91dGlscy9FcnJvci51dGlsJztcbmltcG9ydCB7T0F1dGhTZXJ2aWNlfSBmcm9tICdhbmd1bGFyLW9hdXRoMi1vaWRjJztcbmltcG9ydCB7TG9hZGVyU2VydmljZX0gZnJvbSAnc29maXNhLWxvYWRlcic7XG5pbXBvcnQge0luamVjdGFibGUsIEluamVjdH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0FkZExvYWRlckl0ZW0sIFJlbW92ZUxvYWRlckl0ZW19IGZyb20gJ3NvZmlzYS1sb2FkZXInO1xuaW1wb3J0IHtOb3RpZmljYXRpb25TZXJ2aWNlfSBmcm9tICdzb2Zpc2Etbm90aWZpY2F0aW9uJztcblxuQEluamVjdGFibGUoe1xuXHRwcm92aWRlZEluOiAncm9vdCdcbn0pXG5cbmV4cG9ydCBjbGFzcyBBdXRoU2VydmljZSB7XG5cdC8vIFRPRE86IE1vdmVyIHBhcmEgYXJxdWl2b3MgZGUgY29uZmlndXJhw6fDtWVzXG5cdGFwaVVyaTogU3RyaW5nID0gJy8vc29maXNhLWxhYi1nYXRld2F5LmVhc3R1czIuY2xvdWRhcHAuYXp1cmUuY29tJztcblxuXHRjb25zdHJ1Y3Rvcihwcml2YXRlIG9hdXRoU2VydmljZTogT0F1dGhTZXJ2aWNlLCBwdWJsaWMgbG9hZGVyU2VydmljZTogTG9hZGVyU2VydmljZSwgQEluamVjdChTdG9yZSkgcHVibGljIHN0b3JlLCBwdWJsaWMgbm90aWZpY2F0aW9uU2VydmljZTogTm90aWZpY2F0aW9uU2VydmljZSkge31cblx0XG5cdGluaXQoKSB7XG5cdFx0dGhpcy5vYXV0aFNlcnZpY2UuY29uZmlndXJlKHtcblx0XHRcdHRva2VuRW5kcG9pbnQ6IGAke3RoaXMuYXBpVXJpfWFwaS9vYXV0aC1hcGkvb2F1dGgvdG9rZW5gLFxuXHRcdFx0cmVkaXJlY3RVcmk6IHdpbmRvdy5sb2NhdGlvbi5vcmlnaW4sXG5cdFx0XHRzY29wZTogJycsXG5cdFx0XHRzaG93RGVidWdJbmZvcm1hdGlvbjogdHJ1ZSxcblx0XHRcdG9pZGM6IHRydWUsXG5cdFx0XHRyZXF1aXJlSHR0cHM6IGZhbHNlLFxuXHRcdFx0Y2xpZW50SWQ6ICd0cnVzdGVkLWFwcCdcblx0XHR9KTtcblx0fVxuXHRcblx0aXNUb2tlblZhbGlkKCkge1xuXHRcdHJldHVybiB0aGlzLm9hdXRoU2VydmljZS5oYXNWYWxpZEFjY2Vzc1Rva2VuKCk7XG5cdH1cblx0XG5cdGdldEFjY2Vzc1Rva2VuKCkge1xuXHRcdHJldHVybiB0aGlzLm9hdXRoU2VydmljZS5nZXRBY2Nlc3NUb2tlbigpO1xuXHR9XG5cdFxuXHRkb0xvZ2luKHVzZXI6IHsgdXNlcm5hbWU6IHN0cmluZywgcGFzc3dvcmQ7IHN0cmluZyB9KSB7XG5cdFx0dGhpcy5zdG9yZS5kaXNwYXRjaChuZXcgQWRkTG9hZGVySXRlbSh7XG5cdFx0XHRwYWdlOiAnbG9naW4nLFxuXHRcdFx0bmFtZTogJ2xvZ2luJ1xuXHRcdH0pKTtcblx0XHRcblx0XHR0aGlzLm9hdXRoU2VydmljZS5mZXRjaFRva2VuVXNpbmdQYXNzd29yZEZsb3codXNlci51c2VybmFtZSwgdXNlci5wYXNzd29yZClcblx0XHQudGhlbigocmVzcG9uc2U6IGFueSkgPT4ge1xuXHRcdFx0d2luZG93LmxvY2F0aW9uLmhyZWYgPSB0aGlzLm9hdXRoU2VydmljZS5yZWRpcmVjdFVyaTtcblx0XHR9KVxuXHRcdC5jYXRjaCgoZXJyKSA9PiB7XHRcdFx0XG5cdFx0XHR0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuYWRkTWVzc2FnZSh7XG5cdFx0XHRcdHR5cGU6ICdlcnJvcicsXG5cdFx0XHRcdG1lc3NhZ2U6ICdMb2dpbiBvdSBzZW5oYSBpbmNvcnJldG8hIFRlbnRlIG5vdmFtZW50ZSdcblx0XHRcdH0pO1xuXHRcdH0pXG5cdFx0LmZpbmFsbHkoKCkgPT4ge1xuXHRcdFx0dGhpcy5zdG9yZS5kaXNwYXRjaChuZXcgUmVtb3ZlTG9hZGVySXRlbSh7XG5cdFx0XHRcdHBhZ2U6ICdsb2dpbicsXG5cdFx0XHRcdG5hbWU6ICdsb2dpbidcblx0XHRcdH0pKTtcblx0XHR9KTtcblx0fVxuXHRcblx0ZG9Mb2dvdXQoKSB7XG5cdFx0dGhpcy5vYXV0aFNlcnZpY2UubG9nT3V0KHRydWUpO1xuXHR9XG59XG4iXX0=