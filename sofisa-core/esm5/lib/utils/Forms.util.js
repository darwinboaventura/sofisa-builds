/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { SetValidationOnTaskForm } from '../ngrx/actions/tasks/tasks.actions';
/**
 * @param {?} formGroup
 * @param {?} store
 * @param {?} page
 * @param {?} value
 * @param {?} name
 * @return {?}
 */
export function handleFieldChange(formGroup, store, page, value, name) {
    if (value) {
        formGroup.get("variables." + name + ".value").setValue(value);
        store.dispatch(new SetValidationOnTaskForm({
            page: page,
            fieldName: name,
            validationType: '',
            validationMessage: ''
        }));
    }
    else if (this.props.form.actions.submitted) {
        store.dispatch(new SetValidationOnTaskForm({
            page: page,
            fieldName: name,
            validationType: 'error',
            validationMessage: 'Campo obrigatório'
        }));
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRm9ybXMudXRpbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3NvZmlzYS1jb3JlLyIsInNvdXJjZXMiOlsibGliL3V0aWxzL0Zvcm1zLnV0aWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUNBLE9BQU8sRUFBQyx1QkFBdUIsRUFBQyxNQUFNLHFDQUFxQyxDQUFDOzs7Ozs7Ozs7QUFFNUUsTUFBTSxVQUFVLGlCQUFpQixDQUFDLFNBQW9CLEVBQUUsS0FBVSxFQUFFLElBQVMsRUFBRSxLQUFVLEVBQUUsSUFBUztJQUNuRyxJQUFJLEtBQUssRUFBRTtRQUNWLFNBQVMsQ0FBQyxHQUFHLENBQUMsZUFBYSxJQUFJLFdBQVEsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUV6RCxLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksdUJBQXVCLENBQUM7WUFDMUMsSUFBSSxFQUFFLElBQUk7WUFDVixTQUFTLEVBQUUsSUFBSTtZQUNmLGNBQWMsRUFBRSxFQUFFO1lBQ2xCLGlCQUFpQixFQUFFLEVBQUU7U0FDckIsQ0FBQyxDQUFDLENBQUM7S0FDSjtTQUFNLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsRUFBRTtRQUM3QyxLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksdUJBQXVCLENBQUM7WUFDMUMsSUFBSSxFQUFFLElBQUk7WUFDVixTQUFTLEVBQUUsSUFBSTtZQUNmLGNBQWMsRUFBRSxPQUFPO1lBQ3ZCLGlCQUFpQixFQUFFLG1CQUFtQjtTQUN0QyxDQUFDLENBQUMsQ0FBQztLQUNKO0FBQ0YsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Rm9ybUdyb3VwfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQge1NldFZhbGlkYXRpb25PblRhc2tGb3JtfSBmcm9tICcuLi9uZ3J4L2FjdGlvbnMvdGFza3MvdGFza3MuYWN0aW9ucyc7XG5cbmV4cG9ydCBmdW5jdGlvbiBoYW5kbGVGaWVsZENoYW5nZShmb3JtR3JvdXA6IEZvcm1Hcm91cCwgc3RvcmU6IGFueSwgcGFnZTogYW55LCB2YWx1ZTogYW55LCBuYW1lOiBhbnkpIHtcblx0aWYgKHZhbHVlKSB7XG5cdFx0Zm9ybUdyb3VwLmdldChgdmFyaWFibGVzLiR7bmFtZX0udmFsdWVgKS5zZXRWYWx1ZSh2YWx1ZSk7XG5cdFx0XG5cdFx0c3RvcmUuZGlzcGF0Y2gobmV3IFNldFZhbGlkYXRpb25PblRhc2tGb3JtKHtcblx0XHRcdHBhZ2U6IHBhZ2UsXG5cdFx0XHRmaWVsZE5hbWU6IG5hbWUsXG5cdFx0XHR2YWxpZGF0aW9uVHlwZTogJycsXG5cdFx0XHR2YWxpZGF0aW9uTWVzc2FnZTogJydcblx0XHR9KSk7XG5cdH0gZWxzZSBpZiAodGhpcy5wcm9wcy5mb3JtLmFjdGlvbnMuc3VibWl0dGVkKSB7XG5cdFx0c3RvcmUuZGlzcGF0Y2gobmV3IFNldFZhbGlkYXRpb25PblRhc2tGb3JtKHtcblx0XHRcdHBhZ2U6IHBhZ2UsXG5cdFx0XHRmaWVsZE5hbWU6IG5hbWUsXG5cdFx0XHR2YWxpZGF0aW9uVHlwZTogJ2Vycm9yJyxcblx0XHRcdHZhbGlkYXRpb25NZXNzYWdlOiAnQ2FtcG8gb2JyaWdhdMOzcmlvJ1xuXHRcdH0pKTtcblx0fVxufVxuIl19