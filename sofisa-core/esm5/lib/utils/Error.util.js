/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { HttpErrorResponse } from '@angular/common/http';
/**
 * @param {?} err
 * @param {?} loader
 * @return {?}
 */
export function handleError(err, loader) {
    console.log('Error:', err);
    this.loaderService.removeAnItemToLoader(loader);
    if (err instanceof HttpErrorResponse) {
        this.notificationService.addMessage({
            type: 'error',
            message: err.error.message || err.error.error_description
        });
    }
    else {
        this.notificationService.addMessage({
            type: 'error',
            message: err.message
        });
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRXJyb3IudXRpbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3NvZmlzYS1jb3JlLyIsInNvdXJjZXMiOlsibGliL3V0aWxzL0Vycm9yLnV0aWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBQyxpQkFBaUIsRUFBQyxNQUFNLHNCQUFzQixDQUFDOzs7Ozs7QUFFdkQsTUFBTSxVQUFVLFdBQVcsQ0FBQyxHQUFRLEVBQUUsTUFBcUM7SUFDMUUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUUsR0FBRyxDQUFDLENBQUM7SUFFM0IsSUFBSSxDQUFDLGFBQWEsQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUVoRCxJQUFJLEdBQUcsWUFBWSxpQkFBaUIsRUFBRTtRQUNyQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsVUFBVSxDQUFDO1lBQ25DLElBQUksRUFBRSxPQUFPO1lBQ2IsT0FBTyxFQUFFLEdBQUcsQ0FBQyxLQUFLLENBQUMsT0FBTyxJQUFJLEdBQUcsQ0FBQyxLQUFLLENBQUMsaUJBQWlCO1NBQ3pELENBQUMsQ0FBQztLQUNIO1NBQU07UUFDTixJQUFJLENBQUMsbUJBQW1CLENBQUMsVUFBVSxDQUFDO1lBQ25DLElBQUksRUFBRSxPQUFPO1lBQ2IsT0FBTyxFQUFFLEdBQUcsQ0FBQyxPQUFPO1NBQ3BCLENBQUMsQ0FBQztLQUNIO0FBQ0YsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7SHR0cEVycm9yUmVzcG9uc2V9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcblxuZXhwb3J0IGZ1bmN0aW9uIGhhbmRsZUVycm9yKGVycjogYW55LCBsb2FkZXI6IHsgcGFnZTogc3RyaW5nLCBuYW1lOiBzdHJpbmd9KSB7XG5cdGNvbnNvbGUubG9nKCdFcnJvcjonLCBlcnIpO1xuXHRcblx0dGhpcy5sb2FkZXJTZXJ2aWNlLnJlbW92ZUFuSXRlbVRvTG9hZGVyKGxvYWRlcik7XG5cdFxuXHRpZiAoZXJyIGluc3RhbmNlb2YgSHR0cEVycm9yUmVzcG9uc2UpIHtcblx0XHR0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuYWRkTWVzc2FnZSh7XG5cdFx0XHR0eXBlOiAnZXJyb3InLFxuXHRcdFx0bWVzc2FnZTogZXJyLmVycm9yLm1lc3NhZ2UgfHwgZXJyLmVycm9yLmVycm9yX2Rlc2NyaXB0aW9uXG5cdFx0fSk7XG5cdH0gZWxzZSB7XG5cdFx0dGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmFkZE1lc3NhZ2Uoe1xuXHRcdFx0dHlwZTogJ2Vycm9yJyxcblx0XHRcdG1lc3NhZ2U6IGVyci5tZXNzYWdlXG5cdFx0fSk7XG5cdH1cbn1cblxuIl19