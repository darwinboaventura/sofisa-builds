/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @param {?} value
 * @param {?=} type
 * @return {?}
 */
export function formatDateToInput(value, type) {
    if (type === void 0) { type = 1; }
    /** @type {?} */
    var separator = value.includes('-') ? '-' : '/';
    /** @type {?} */
    var pieces;
    /** @type {?} */
    var formated;
    if (type === 1) {
        pieces = value.split(separator);
        formated = pieces[0] + "-" + pieces[1] + "-" + pieces[2];
    }
    else if (type === 2) {
        pieces = value.split(separator);
        formated = pieces[2] + "-" + pieces[1] + "-" + pieces[0];
    }
    if (formated && formated.length === 10) {
        formated = formated.substring(0, 10);
    }
    return formated;
}
/**
 * @param {?} value
 * @return {?}
 */
export function formatDateToApiFormat(value) {
    /** @type {?} */
    var separator = value.includes('-') ? '-' : '/';
    /** @type {?} */
    var breakedDate = value.split(separator);
    /** @type {?} */
    var data = breakedDate[2] + "/" + breakedDate[1] + "/" + breakedDate[0];
    return data;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRGF0ZXMudXRpbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3NvZmlzYS1jb3JlLyIsInNvdXJjZXMiOlsibGliL3V0aWxzL0RhdGVzLnV0aWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBQUEsTUFBTSxVQUFVLGlCQUFpQixDQUFDLEtBQVUsRUFBRSxJQUFnQjtJQUFoQixxQkFBQSxFQUFBLFFBQWdCOztRQUN2RCxTQUFTLEdBQUcsS0FBSyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxHQUFHOztRQUU3QyxNQUFNOztRQUNOLFFBQVE7SUFFWixJQUFJLElBQUksS0FBSyxDQUFDLEVBQUU7UUFDZixNQUFNLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUNoQyxRQUFRLEdBQU0sTUFBTSxDQUFDLENBQUMsQ0FBQyxTQUFJLE1BQU0sQ0FBQyxDQUFDLENBQUMsU0FBSSxNQUFNLENBQUMsQ0FBQyxDQUFHLENBQUM7S0FDcEQ7U0FBTSxJQUFJLElBQUksS0FBSyxDQUFDLEVBQUU7UUFDdEIsTUFBTSxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDaEMsUUFBUSxHQUFNLE1BQU0sQ0FBQyxDQUFDLENBQUMsU0FBSSxNQUFNLENBQUMsQ0FBQyxDQUFDLFNBQUksTUFBTSxDQUFDLENBQUMsQ0FBRyxDQUFDO0tBQ3BEO0lBRUQsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLE1BQU0sS0FBSyxFQUFFLEVBQUU7UUFDdkMsUUFBUSxHQUFHLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO0tBQ3JDO0lBRUQsT0FBTyxRQUFRLENBQUM7QUFDakIsQ0FBQzs7Ozs7QUFFRCxNQUFNLFVBQVUscUJBQXFCLENBQUMsS0FBYTs7UUFDNUMsU0FBUyxHQUFHLEtBQUssQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsR0FBRzs7UUFDM0MsV0FBVyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDOztRQUNwQyxJQUFJLEdBQU0sV0FBVyxDQUFDLENBQUMsQ0FBQyxTQUFJLFdBQVcsQ0FBQyxDQUFDLENBQUMsU0FBSSxXQUFXLENBQUMsQ0FBQyxDQUFHO0lBRXBFLE9BQU8sSUFBSSxDQUFDO0FBQ2IsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBmdW5jdGlvbiBmb3JtYXREYXRlVG9JbnB1dCh2YWx1ZTogYW55LCB0eXBlOiBudW1iZXIgPSAxKSB7XG5cdGNvbnN0IHNlcGFyYXRvciA9IHZhbHVlLmluY2x1ZGVzKCctJykgPyAnLScgOiAnLyc7XG5cdFxuXHRsZXQgcGllY2VzO1xuXHRsZXQgZm9ybWF0ZWQ7XG5cdFxuXHRpZiAodHlwZSA9PT0gMSkge1xuXHRcdHBpZWNlcyA9IHZhbHVlLnNwbGl0KHNlcGFyYXRvcik7XG5cdFx0Zm9ybWF0ZWQgPSBgJHtwaWVjZXNbMF19LSR7cGllY2VzWzFdfS0ke3BpZWNlc1syXX1gO1xuXHR9IGVsc2UgaWYgKHR5cGUgPT09IDIpIHtcblx0XHRwaWVjZXMgPSB2YWx1ZS5zcGxpdChzZXBhcmF0b3IpO1xuXHRcdGZvcm1hdGVkID0gYCR7cGllY2VzWzJdfS0ke3BpZWNlc1sxXX0tJHtwaWVjZXNbMF19YDtcblx0fVxuXHRcblx0aWYgKGZvcm1hdGVkICYmIGZvcm1hdGVkLmxlbmd0aCA9PT0gMTApIHtcblx0XHRmb3JtYXRlZCA9IGZvcm1hdGVkLnN1YnN0cmluZygwLCAxMCk7XG5cdH1cblx0XG5cdHJldHVybiBmb3JtYXRlZDtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGZvcm1hdERhdGVUb0FwaUZvcm1hdCh2YWx1ZTogc3RyaW5nKSB7XG5cdGNvbnN0IHNlcGFyYXRvciA9IHZhbHVlLmluY2x1ZGVzKCctJykgPyAnLScgOiAnLyc7XG5cdGNvbnN0IGJyZWFrZWREYXRlID0gdmFsdWUuc3BsaXQoc2VwYXJhdG9yKTtcblx0Y29uc3QgZGF0YSA9IGAke2JyZWFrZWREYXRlWzJdfS8ke2JyZWFrZWREYXRlWzFdfS8ke2JyZWFrZWREYXRlWzBdfWA7XG5cdFxuXHRyZXR1cm4gZGF0YTtcbn1cbiJdfQ==