/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Inject } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';
import { HttpHeaders } from '@angular/common/http';
var AuthInterceptor = /** @class */ (function () {
    function AuthInterceptor(authService) {
        this.authService = authService;
    }
    /**
     * @param {?} req
     * @param {?} next
     * @return {?}
     */
    AuthInterceptor.prototype.intercept = /**
     * @param {?} req
     * @param {?} next
     * @return {?}
     */
    function (req, next) {
        if (req.url.includes('token')) {
            /** @type {?} */
            var headers = new HttpHeaders({
                Authorization: 'Basic ' + btoa("trusted-app:secret")
            });
            /** @type {?} */
            var uri = req.clone({ headers: headers });
            return next.handle(uri);
        }
        else {
            /** @type {?} */
            var headers = new HttpHeaders({
                Authorization: 'Bearer ' + this.authService.getAccessToken()
            });
            /** @type {?} */
            var uri = req.clone({ headers: headers });
            return next.handle(uri);
        }
        return next.handle(req);
    };
    /** @nocollapse */
    AuthInterceptor.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [AuthService,] }] }
    ]; };
    return AuthInterceptor;
}());
export { AuthInterceptor };
if (false) {
    /** @type {?} */
    AuthInterceptor.prototype.authService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3NvZmlzYS1jb3JlLyIsInNvdXJjZXMiOlsibGliL2ludGVjZXB0b3JzL2F1dGgvYXV0aC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFDLE1BQU0sRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUVyQyxPQUFPLEVBQUMsV0FBVyxFQUFDLE1BQU0sa0NBQWtDLENBQUM7QUFDN0QsT0FBTyxFQUFtRCxXQUFXLEVBQUMsTUFBTSxzQkFBc0IsQ0FBQztBQUVuRztJQUNDLHlCQUF3QyxXQUFXO1FBQVgsZ0JBQVcsR0FBWCxXQUFXLENBQUE7SUFBRyxDQUFDOzs7Ozs7SUFFdkQsbUNBQVM7Ozs7O0lBQVQsVUFBVSxHQUFxQixFQUFFLElBQWlCO1FBQ2pELElBQUksR0FBRyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLEVBQUU7O2dCQUN4QixPQUFPLEdBQUcsSUFBSSxXQUFXLENBQUM7Z0JBQy9CLGFBQWEsRUFBRSxRQUFRLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixDQUFDO2FBQ3BELENBQUM7O2dCQUVJLEdBQUcsR0FBRyxHQUFHLENBQUMsS0FBSyxDQUFDLEVBQUMsT0FBTyxTQUFBLEVBQUMsQ0FBQztZQUVoQyxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDeEI7YUFBTTs7Z0JBQ0EsT0FBTyxHQUFHLElBQUksV0FBVyxDQUFDO2dCQUMvQixhQUFhLEVBQUUsU0FBUyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxFQUFFO2FBQzVELENBQUM7O2dCQUVJLEdBQUcsR0FBRyxHQUFHLENBQUMsS0FBSyxDQUFDLEVBQUMsT0FBTyxTQUFBLEVBQUMsQ0FBQztZQUVoQyxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDeEI7UUFFRCxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDekIsQ0FBQzs7O2dEQXRCWSxNQUFNLFNBQUMsV0FBVzs7SUF1QmhDLHNCQUFDO0NBQUEsQUF4QkQsSUF3QkM7U0F4QlksZUFBZTs7O0lBQ2Ysc0NBQXVDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtJbmplY3R9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtPYnNlcnZhYmxlfSBmcm9tICdyeGpzJztcbmltcG9ydCB7QXV0aFNlcnZpY2V9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2F1dGgvYXV0aC5zZXJ2aWNlJztcbmltcG9ydCB7SHR0cEludGVyY2VwdG9yLEh0dHBFdmVudCxIdHRwSGFuZGxlcixIdHRwUmVxdWVzdCxIdHRwSGVhZGVyc30gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuXG5leHBvcnQgY2xhc3MgQXV0aEludGVyY2VwdG9yIGltcGxlbWVudHMgSHR0cEludGVyY2VwdG9yIHtcblx0Y29uc3RydWN0b3IoQEluamVjdChBdXRoU2VydmljZSkgcHVibGljIGF1dGhTZXJ2aWNlKSB7fVxuXHRcblx0aW50ZXJjZXB0KHJlcTogSHR0cFJlcXVlc3Q8YW55PiwgbmV4dDogSHR0cEhhbmRsZXIpOiBPYnNlcnZhYmxlPEh0dHBFdmVudDxhbnk+PiB7XG5cdFx0aWYgKHJlcS51cmwuaW5jbHVkZXMoJ3Rva2VuJykpIHtcblx0XHRcdGNvbnN0IGhlYWRlcnMgPSBuZXcgSHR0cEhlYWRlcnMoe1xuXHRcdFx0XHRBdXRob3JpemF0aW9uOiAnQmFzaWMgJyArIGJ0b2EoYHRydXN0ZWQtYXBwOnNlY3JldGApXG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0Y29uc3QgdXJpID0gcmVxLmNsb25lKHtoZWFkZXJzfSk7XG5cdFx0XHRcblx0XHRcdHJldHVybiBuZXh0LmhhbmRsZSh1cmkpO1xuXHRcdH0gZWxzZSB7XG5cdFx0XHRjb25zdCBoZWFkZXJzID0gbmV3IEh0dHBIZWFkZXJzKHtcblx0XHRcdFx0QXV0aG9yaXphdGlvbjogJ0JlYXJlciAnICsgdGhpcy5hdXRoU2VydmljZS5nZXRBY2Nlc3NUb2tlbigpXG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0Y29uc3QgdXJpID0gcmVxLmNsb25lKHtoZWFkZXJzfSk7XG5cdFx0XHRcblx0XHRcdHJldHVybiBuZXh0LmhhbmRsZSh1cmkpO1xuXHRcdH1cblx0XHRcblx0XHRyZXR1cm4gbmV4dC5oYW5kbGUocmVxKTtcblx0fVxufSJdfQ==