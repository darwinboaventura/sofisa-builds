import { Action } from '@ngrx/store';
export declare enum LoaderTypes {
    AddLoaderItem = "[AppComponent] AddLoaderItem",
    RemoveLoaderItem = "[AppComponent] RemoveLoaderItem"
}
export declare class AddLoaderItem implements Action {
    payload: {
        page: string;
        name: string;
    };
    readonly type = LoaderTypes.AddLoaderItem;
    constructor(payload: {
        page: string;
        name: string;
    });
}
export declare class RemoveLoaderItem implements Action {
    payload: {
        page: string;
        name: string;
    };
    readonly type = LoaderTypes.RemoveLoaderItem;
    constructor(payload: {
        page: string;
        name: string;
    });
}
