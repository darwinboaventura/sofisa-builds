import { Observable } from 'rxjs';
export declare class LoaderService {
    store: any;
    store$: Observable<any>;
    isActive: boolean;
    constructor(store: any);
    addAnItemToLoader(payload: {
        page: string;
        name: string;
    }): void;
    removeAnItemToLoader(payload: {
        page: string;
        name: string;
    }): void;
}
