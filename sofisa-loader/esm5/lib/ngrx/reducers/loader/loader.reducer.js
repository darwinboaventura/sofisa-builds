/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { LoaderTypes } from '../../actions/loader/loader.actions';
/** @type {?} */
export var loaderDefaultState = [];
/**
 * @param {?=} state
 * @param {?=} action
 * @return {?}
 */
export function loaderReducer(state, action) {
    if (state === void 0) { state = loaderDefaultState; }
    /** @type {?} */
    var loaders;
    /** @type {?} */
    var indexOnArray;
    switch (action.type) {
        case LoaderTypes.AddLoaderItem:
            loaders = state.slice(0);
            indexOnArray = loaders.findIndex(function (element) {
                return element.page === action.payload.page && element.name === action.payload.name;
            });
            if (indexOnArray === -1) {
                loaders.push(action.payload);
            }
            return loaders;
        case LoaderTypes.RemoveLoaderItem:
            loaders = state.slice(0);
            indexOnArray = loaders.findIndex(function (element) {
                return element.page === action.payload.page && element.name === action.payload.name;
            });
            loaders.splice(indexOnArray, 1);
            return loaders;
        default:
            return state;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9hZGVyLnJlZHVjZXIuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9zb2Zpc2EtbG9hZGVyLyIsInNvdXJjZXMiOlsibGliL25ncngvcmVkdWNlcnMvbG9hZGVyL2xvYWRlci5yZWR1Y2VyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsV0FBVyxFQUFDLE1BQU0scUNBQXFDLENBQUM7O0FBRWhFLE1BQU0sS0FBTyxrQkFBa0IsR0FBRyxFQUFFOzs7Ozs7QUFFcEMsTUFBTSxVQUFVLGFBQWEsQ0FBQyxLQUEwQixFQUFFLE1BQVc7SUFBdkMsc0JBQUEsRUFBQSwwQkFBMEI7O1FBQ25ELE9BQU87O1FBQ1AsWUFBWTtJQUVoQixRQUFRLE1BQU0sQ0FBQyxJQUFJLEVBQUU7UUFDcEIsS0FBSyxXQUFXLENBQUMsYUFBYTtZQUM3QixPQUFPLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN6QixZQUFZLEdBQUcsT0FBTyxDQUFDLFNBQVMsQ0FBQyxVQUFDLE9BQU87Z0JBQ3hDLE9BQU8sT0FBTyxDQUFDLElBQUksS0FBSyxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksSUFBSSxPQUFPLENBQUMsSUFBSSxLQUFLLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDO1lBQ3JGLENBQUMsQ0FBQyxDQUFDO1lBRUgsSUFBSSxZQUFZLEtBQUssQ0FBQyxDQUFDLEVBQUU7Z0JBQ3hCLE9BQU8sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2FBQzdCO1lBRUQsT0FBTyxPQUFPLENBQUM7UUFDaEIsS0FBSyxXQUFXLENBQUMsZ0JBQWdCO1lBQ2hDLE9BQU8sR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3pCLFlBQVksR0FBRyxPQUFPLENBQUMsU0FBUyxDQUFDLFVBQUMsT0FBTztnQkFDeEMsT0FBTyxPQUFPLENBQUMsSUFBSSxLQUFLLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSSxJQUFJLE9BQU8sQ0FBQyxJQUFJLEtBQUssTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUM7WUFDckYsQ0FBQyxDQUFDLENBQUM7WUFFSCxPQUFPLENBQUMsTUFBTSxDQUFDLFlBQVksRUFBRSxDQUFDLENBQUMsQ0FBQztZQUVoQyxPQUFPLE9BQU8sQ0FBQztRQUNoQjtZQUNDLE9BQU8sS0FBSyxDQUFDO0tBQ2Q7QUFDRixDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtMb2FkZXJUeXBlc30gZnJvbSAnLi4vLi4vYWN0aW9ucy9sb2FkZXIvbG9hZGVyLmFjdGlvbnMnO1xyXG5cclxuZXhwb3J0IGNvbnN0IGxvYWRlckRlZmF1bHRTdGF0ZSA9IFtdO1xyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIGxvYWRlclJlZHVjZXIoc3RhdGUgPSBsb2FkZXJEZWZhdWx0U3RhdGUsIGFjdGlvbjogYW55KSB7XHJcblx0bGV0IGxvYWRlcnM7XHJcblx0bGV0IGluZGV4T25BcnJheTtcclxuXHJcblx0c3dpdGNoIChhY3Rpb24udHlwZSkge1xyXG5cdFx0Y2FzZSBMb2FkZXJUeXBlcy5BZGRMb2FkZXJJdGVtOlxyXG5cdFx0XHRsb2FkZXJzID0gc3RhdGUuc2xpY2UoMCk7XHJcblx0XHRcdGluZGV4T25BcnJheSA9IGxvYWRlcnMuZmluZEluZGV4KChlbGVtZW50KSA9PiB7XHJcblx0XHRcdFx0cmV0dXJuIGVsZW1lbnQucGFnZSA9PT0gYWN0aW9uLnBheWxvYWQucGFnZSAmJiBlbGVtZW50Lm5hbWUgPT09IGFjdGlvbi5wYXlsb2FkLm5hbWU7XHJcblx0XHRcdH0pO1xyXG5cclxuXHRcdFx0aWYgKGluZGV4T25BcnJheSA9PT0gLTEpIHtcclxuXHRcdFx0XHRsb2FkZXJzLnB1c2goYWN0aW9uLnBheWxvYWQpO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRyZXR1cm4gbG9hZGVycztcclxuXHRcdGNhc2UgTG9hZGVyVHlwZXMuUmVtb3ZlTG9hZGVySXRlbTpcclxuXHRcdFx0bG9hZGVycyA9IHN0YXRlLnNsaWNlKDApO1xyXG5cdFx0XHRpbmRleE9uQXJyYXkgPSBsb2FkZXJzLmZpbmRJbmRleCgoZWxlbWVudCkgPT4ge1xyXG5cdFx0XHRcdHJldHVybiBlbGVtZW50LnBhZ2UgPT09IGFjdGlvbi5wYXlsb2FkLnBhZ2UgJiYgZWxlbWVudC5uYW1lID09PSBhY3Rpb24ucGF5bG9hZC5uYW1lO1xyXG5cdFx0XHR9KTtcclxuXHJcblx0XHRcdGxvYWRlcnMuc3BsaWNlKGluZGV4T25BcnJheSwgMSk7XHJcblxyXG5cdFx0XHRyZXR1cm4gbG9hZGVycztcclxuXHRcdGRlZmF1bHQ6XHJcblx0XHRcdHJldHVybiBzdGF0ZTtcclxuXHR9XHJcbn1cclxuXHJcblxyXG4iXX0=