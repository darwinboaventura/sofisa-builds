/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
var LoaderTypes = {
    AddLoaderItem: '[AppComponent] AddLoaderItem',
    RemoveLoaderItem: '[AppComponent] RemoveLoaderItem',
};
export { LoaderTypes };
var AddLoaderItem = /** @class */ (function () {
    function AddLoaderItem(payload) {
        this.payload = payload;
        this.type = LoaderTypes.AddLoaderItem;
    }
    return AddLoaderItem;
}());
export { AddLoaderItem };
if (false) {
    /** @type {?} */
    AddLoaderItem.prototype.type;
    /** @type {?} */
    AddLoaderItem.prototype.payload;
}
var RemoveLoaderItem = /** @class */ (function () {
    function RemoveLoaderItem(payload) {
        this.payload = payload;
        this.type = LoaderTypes.RemoveLoaderItem;
    }
    return RemoveLoaderItem;
}());
export { RemoveLoaderItem };
if (false) {
    /** @type {?} */
    RemoveLoaderItem.prototype.type;
    /** @type {?} */
    RemoveLoaderItem.prototype.payload;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9hZGVyLmFjdGlvbnMuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9zb2Zpc2EtbG9hZGVyLyIsInNvdXJjZXMiOlsibGliL25ncngvYWN0aW9ucy9sb2FkZXIvbG9hZGVyLmFjdGlvbnMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0lBR0MsZUFBZ0IsOEJBQThCO0lBQzlDLGtCQUFtQixpQ0FBaUM7OztBQUdyRDtJQUdDLHVCQUFtQixPQUF1QztRQUF2QyxZQUFPLEdBQVAsT0FBTyxDQUFnQztRQUZqRCxTQUFJLEdBQUcsV0FBVyxDQUFDLGFBQWEsQ0FBQztJQUVtQixDQUFDO0lBQy9ELG9CQUFDO0FBQUQsQ0FBQyxBQUpELElBSUM7Ozs7SUFIQSw2QkFBMEM7O0lBRTlCLGdDQUE4Qzs7QUFHM0Q7SUFHQywwQkFBbUIsT0FBdUM7UUFBdkMsWUFBTyxHQUFQLE9BQU8sQ0FBZ0M7UUFGakQsU0FBSSxHQUFHLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQztJQUVnQixDQUFDO0lBQy9ELHVCQUFDO0FBQUQsQ0FBQyxBQUpELElBSUM7Ozs7SUFIQSxnQ0FBNkM7O0lBRWpDLG1DQUE4QyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7QWN0aW9ufSBmcm9tICdAbmdyeC9zdG9yZSc7XHJcblxyXG5leHBvcnQgZW51bSBMb2FkZXJUeXBlcyB7XHJcblx0QWRkTG9hZGVySXRlbSA9ICdbQXBwQ29tcG9uZW50XSBBZGRMb2FkZXJJdGVtJyxcclxuXHRSZW1vdmVMb2FkZXJJdGVtID0gJ1tBcHBDb21wb25lbnRdIFJlbW92ZUxvYWRlckl0ZW0nXHJcbn1cclxuXHJcbmV4cG9ydCBjbGFzcyBBZGRMb2FkZXJJdGVtIGltcGxlbWVudHMgQWN0aW9uIHtcclxuXHRyZWFkb25seSB0eXBlID0gTG9hZGVyVHlwZXMuQWRkTG9hZGVySXRlbTtcclxuXHRcclxuXHRjb25zdHJ1Y3RvcihwdWJsaWMgcGF5bG9hZDogeyBwYWdlOiBzdHJpbmcsIG5hbWU6IHN0cmluZyB9KSB7fVxyXG59XHJcblxyXG5leHBvcnQgY2xhc3MgUmVtb3ZlTG9hZGVySXRlbSBpbXBsZW1lbnRzIEFjdGlvbiB7XHJcblx0cmVhZG9ubHkgdHlwZSA9IExvYWRlclR5cGVzLlJlbW92ZUxvYWRlckl0ZW07XHJcblx0XHJcblx0Y29uc3RydWN0b3IocHVibGljIHBheWxvYWQ6IHsgcGFnZTogc3RyaW5nLCBuYW1lOiBzdHJpbmcgfSkge31cclxufVxyXG5cclxuIl19