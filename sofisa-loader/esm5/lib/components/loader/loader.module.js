/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoaderComponent } from './loader.component';
var LoaderModule = /** @class */ (function () {
    function LoaderModule() {
    }
    LoaderModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [LoaderComponent],
                    imports: [
                        CommonModule
                    ],
                    exports: [LoaderComponent]
                },] }
    ];
    return LoaderModule;
}());
export { LoaderModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9hZGVyLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3NvZmlzYS1sb2FkZXIvIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9sb2FkZXIvbG9hZGVyLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFDLFFBQVEsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUN2QyxPQUFPLEVBQUMsWUFBWSxFQUFDLE1BQU0saUJBQWlCLENBQUM7QUFDN0MsT0FBTyxFQUFDLGVBQWUsRUFBQyxNQUFNLG9CQUFvQixDQUFDO0FBRW5EO0lBQUE7SUFRMkIsQ0FBQzs7Z0JBUjNCLFFBQVEsU0FBQztvQkFDVCxZQUFZLEVBQUUsQ0FBQyxlQUFlLENBQUM7b0JBQy9CLE9BQU8sRUFBRTt3QkFDUixZQUFZO3FCQUNaO29CQUNELE9BQU8sRUFBRSxDQUFDLGVBQWUsQ0FBQztpQkFDMUI7O0lBRTBCLG1CQUFDO0NBQUEsQUFSNUIsSUFRNEI7U0FBZixZQUFZIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtOZ01vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0NvbW1vbk1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7TG9hZGVyQ29tcG9uZW50fSBmcm9tICcuL2xvYWRlci5jb21wb25lbnQnO1xuXG5ATmdNb2R1bGUoe1xuXHRkZWNsYXJhdGlvbnM6IFtMb2FkZXJDb21wb25lbnRdLFxuXHRpbXBvcnRzOiBbXG5cdFx0Q29tbW9uTW9kdWxlXG5cdF0sXG5cdGV4cG9ydHM6IFtMb2FkZXJDb21wb25lbnRdXG59KVxuXG5leHBvcnQgY2xhc3MgTG9hZGVyTW9kdWxlIHt9XG4iXX0=