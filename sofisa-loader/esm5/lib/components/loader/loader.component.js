/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
var LoaderComponent = /** @class */ (function () {
    function LoaderComponent() {
        this.show = false;
    }
    LoaderComponent.decorators = [
        { type: Component, args: [{
                    selector: 'so-loader',
                    template: "<div class=\"so-loader clearfix\" [hidden]=\"!show\">\n\t<div class=\"so-loader--content clearfix\">\n\t\t<img src=\"assets/images/comet-spinner.gif\" />\n\t\t\n\t\t<span>Carregando...</span>\n\t</div>\n</div>",
                    styles: [".so-loader{width:100%;height:100%;position:fixed;top:0;left:0;z-index:9000;background:rgba(255,255,255,.8)}.so-loader--content{display:table;position:absolute;top:50%;left:50%;-webkit-transform:translate(-50%,-50%);transform:translate(-50%,-50%);text-align:center}.so-loader span{font-family:Roboto;font-weight:500;color:#333;text-transform:uppercase;text-align:center;display:block;margin-top:20px}"]
                }] }
    ];
    LoaderComponent.propDecorators = {
        show: [{ type: Input }]
    };
    return LoaderComponent;
}());
export { LoaderComponent };
if (false) {
    /** @type {?} */
    LoaderComponent.prototype.show;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9hZGVyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3NvZmlzYS1sb2FkZXIvIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9sb2FkZXIvbG9hZGVyLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFDLFNBQVMsRUFBRSxLQUFLLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFFL0M7SUFBQTtRQU9VLFNBQUksR0FBWSxLQUFLLENBQUM7SUFDaEMsQ0FBQzs7Z0JBUkEsU0FBUyxTQUFDO29CQUNWLFFBQVEsRUFBRSxXQUFXO29CQUNyQiw2TkFBc0M7O2lCQUV0Qzs7O3VCQUdDLEtBQUs7O0lBQ1Asc0JBQUM7Q0FBQSxBQVJELElBUUM7U0FGWSxlQUFlOzs7SUFDM0IsK0JBQStCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIElucHV0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQENvbXBvbmVudCh7XG5cdHNlbGVjdG9yOiAnc28tbG9hZGVyJyxcblx0dGVtcGxhdGVVcmw6ICcuL2xvYWRlci5jb21wb25lbnQuaHRtbCcsXG5cdHN0eWxlVXJsczogWycuL2xvYWRlci5jb21wb25lbnQuc2NzcyddXG59KVxuXG5leHBvcnQgY2xhc3MgTG9hZGVyQ29tcG9uZW50IHtcblx0QElucHV0KCkgc2hvdzogQm9vbGVhbiA9IGZhbHNlO1xufVxuIl19