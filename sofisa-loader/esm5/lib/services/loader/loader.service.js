/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable, Inject } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { AddLoaderItem, RemoveLoaderItem } from '../../ngrx/actions/loader/loader.actions';
import * as i0 from "@angular/core";
import * as i1 from "@ngrx/store";
var LoaderService = /** @class */ (function () {
    function LoaderService(store) {
        var _this = this;
        this.store = store;
        this.store$ = store.pipe(select('loader'));
        this.store$.subscribe(function (state) {
            _this.isActive = Boolean(state.length);
        });
    }
    /**
     * @param {?} payload
     * @return {?}
     */
    LoaderService.prototype.addAnItemToLoader = /**
     * @param {?} payload
     * @return {?}
     */
    function (payload) {
        this.store.dispatch(new AddLoaderItem(payload));
    };
    /**
     * @param {?} payload
     * @return {?}
     */
    LoaderService.prototype.removeAnItemToLoader = /**
     * @param {?} payload
     * @return {?}
     */
    function (payload) {
        this.store.dispatch(new RemoveLoaderItem(payload));
    };
    LoaderService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    LoaderService.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [Store,] }] }
    ]; };
    /** @nocollapse */ LoaderService.ngInjectableDef = i0.defineInjectable({ factory: function LoaderService_Factory() { return new LoaderService(i0.inject(i1.Store)); }, token: LoaderService, providedIn: "root" });
    return LoaderService;
}());
export { LoaderService };
if (false) {
    /** @type {?} */
    LoaderService.prototype.store$;
    /** @type {?} */
    LoaderService.prototype.isActive;
    /** @type {?} */
    LoaderService.prototype.store;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9hZGVyLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9zb2Zpc2EtbG9hZGVyLyIsInNvdXJjZXMiOlsibGliL3NlcnZpY2VzL2xvYWRlci9sb2FkZXIuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDbkQsT0FBTyxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsTUFBTSxhQUFhLENBQUM7QUFFNUMsT0FBTyxFQUFDLGFBQWEsRUFBRSxnQkFBZ0IsRUFBQyxNQUFNLDBDQUEwQyxDQUFDOzs7QUFFekY7SUFRQyx1QkFBa0MsS0FBSztRQUF2QyxpQkFNQztRQU5pQyxVQUFLLEdBQUwsS0FBSyxDQUFBO1FBQ3RDLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztRQUUzQyxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxVQUFDLEtBQVU7WUFDaEMsS0FBSSxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3ZDLENBQUMsQ0FBQyxDQUFDO0lBQ0osQ0FBQzs7Ozs7SUFFRCx5Q0FBaUI7Ozs7SUFBakIsVUFBa0IsT0FBdUM7UUFDeEQsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsSUFBSSxhQUFhLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztJQUNqRCxDQUFDOzs7OztJQUVELDRDQUFvQjs7OztJQUFwQixVQUFxQixPQUF1QztRQUMzRCxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxJQUFJLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7SUFDcEQsQ0FBQzs7Z0JBdEJELFVBQVUsU0FBQztvQkFDWCxVQUFVLEVBQUUsTUFBTTtpQkFDbEI7Ozs7Z0RBTWEsTUFBTSxTQUFDLEtBQUs7Ozt3QkFiMUI7Q0E0QkMsQUF2QkQsSUF1QkM7U0FuQlksYUFBYTs7O0lBQ3pCLCtCQUErQjs7SUFDL0IsaUNBQXlCOztJQUViLDhCQUEyQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUsIEluamVjdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgU3RvcmUsIHNlbGVjdCB9IGZyb20gJ0BuZ3J4L3N0b3JlJztcbmltcG9ydCB7T2JzZXJ2YWJsZX0gZnJvbSAncnhqcyc7XG5pbXBvcnQge0FkZExvYWRlckl0ZW0sIFJlbW92ZUxvYWRlckl0ZW19IGZyb20gJy4uLy4uL25ncngvYWN0aW9ucy9sb2FkZXIvbG9hZGVyLmFjdGlvbnMnO1xuXG5ASW5qZWN0YWJsZSh7XG5cdHByb3ZpZGVkSW46ICdyb290J1xufSlcblxuZXhwb3J0IGNsYXNzIExvYWRlclNlcnZpY2Uge1xuXHRwdWJsaWMgc3RvcmUkOiBPYnNlcnZhYmxlPGFueT47XG5cdHB1YmxpYyBpc0FjdGl2ZTogYm9vbGVhbjtcblx0XG5cdGNvbnN0cnVjdG9yKEBJbmplY3QoU3RvcmUpIHB1YmxpYyBzdG9yZSkge1xuXHRcdHRoaXMuc3RvcmUkID0gc3RvcmUucGlwZShzZWxlY3QoJ2xvYWRlcicpKTtcblxuXHRcdHRoaXMuc3RvcmUkLnN1YnNjcmliZSgoc3RhdGU6IGFueSkgPT4ge1xuXHRcdFx0dGhpcy5pc0FjdGl2ZSA9IEJvb2xlYW4oc3RhdGUubGVuZ3RoKTtcblx0XHR9KTtcblx0fVxuXG5cdGFkZEFuSXRlbVRvTG9hZGVyKHBheWxvYWQ6IHsgcGFnZTogc3RyaW5nLCBuYW1lOiBzdHJpbmcgfSkge1xuXHRcdHRoaXMuc3RvcmUuZGlzcGF0Y2gobmV3IEFkZExvYWRlckl0ZW0ocGF5bG9hZCkpO1xuXHR9XG5cdFxuXHRyZW1vdmVBbkl0ZW1Ub0xvYWRlcihwYXlsb2FkOiB7IHBhZ2U6IHN0cmluZywgbmFtZTogc3RyaW5nIH0pIHtcblx0XHR0aGlzLnN0b3JlLmRpc3BhdGNoKG5ldyBSZW1vdmVMb2FkZXJJdGVtKHBheWxvYWQpKTtcblx0fVxufVxuIl19