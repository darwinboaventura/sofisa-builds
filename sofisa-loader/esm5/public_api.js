/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
// Componentes
export { LoaderModule } from './lib/components/loader/loader.module';
// Services
export { LoaderService } from './lib/services/loader/loader.service';
// Actions
export { LoaderTypes, AddLoaderItem, RemoveLoaderItem } from './lib/ngrx/actions/loader/loader.actions';
// Reducers
export { loaderReducer, loaderDefaultState } from './lib/ngrx/reducers/loader/loader.reducer';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHVibGljX2FwaS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3NvZmlzYS1sb2FkZXIvIiwic291cmNlcyI6WyJwdWJsaWNfYXBpLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQ0EsNkJBQWMsdUNBQXVDLENBQUM7O0FBR3RELDhCQUFjLHNDQUFzQyxDQUFDOztBQUdyRCw2REFBYywwQ0FBMEMsQ0FBQzs7QUFHekQsa0RBQWMsMkNBQTJDLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBDb21wb25lbnRlc1xuZXhwb3J0ICogZnJvbSAnLi9saWIvY29tcG9uZW50cy9sb2FkZXIvbG9hZGVyLm1vZHVsZSc7XG5cbi8vIFNlcnZpY2VzXG5leHBvcnQgKiBmcm9tICcuL2xpYi9zZXJ2aWNlcy9sb2FkZXIvbG9hZGVyLnNlcnZpY2UnO1xuXG4vLyBBY3Rpb25zXG5leHBvcnQgKiBmcm9tICcuL2xpYi9uZ3J4L2FjdGlvbnMvbG9hZGVyL2xvYWRlci5hY3Rpb25zJztcblxuLy8gUmVkdWNlcnNcbmV4cG9ydCAqIGZyb20gJy4vbGliL25ncngvcmVkdWNlcnMvbG9hZGVyL2xvYWRlci5yZWR1Y2VyJzsiXX0=