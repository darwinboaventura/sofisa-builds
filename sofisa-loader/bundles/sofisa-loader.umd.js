(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/common'), require('@angular/core'), require('@ngrx/store')) :
    typeof define === 'function' && define.amd ? define('sofisa-loader', ['exports', '@angular/common', '@angular/core', '@ngrx/store'], factory) :
    (factory((global['sofisa-loader'] = {}),global.ng.common,global.ng.core,global.i1));
}(this, (function (exports,common,i0,i1) { 'use strict';

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var LoaderComponent = /** @class */ (function () {
        function LoaderComponent() {
            this.show = false;
        }
        LoaderComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'so-loader',
                        template: "<div class=\"so-loader clearfix\" [hidden]=\"!show\">\n\t<div class=\"so-loader--content clearfix\">\n\t\t<img src=\"assets/images/comet-spinner.gif\" />\n\t\t\n\t\t<span>Carregando...</span>\n\t</div>\n</div>",
                        styles: [".so-loader{width:100%;height:100%;position:fixed;top:0;left:0;z-index:9000;background:rgba(255,255,255,.8)}.so-loader--content{display:table;position:absolute;top:50%;left:50%;-webkit-transform:translate(-50%,-50%);transform:translate(-50%,-50%);text-align:center}.so-loader span{font-family:Roboto;font-weight:500;color:#333;text-transform:uppercase;text-align:center;display:block;margin-top:20px}"]
                    }] }
        ];
        LoaderComponent.propDecorators = {
            show: [{ type: i0.Input }]
        };
        return LoaderComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var LoaderModule = /** @class */ (function () {
        function LoaderModule() {
        }
        LoaderModule.decorators = [
            { type: i0.NgModule, args: [{
                        declarations: [LoaderComponent],
                        imports: [
                            common.CommonModule
                        ],
                        exports: [LoaderComponent]
                    },] }
        ];
        return LoaderModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @enum {string} */
    var LoaderTypes = {
        AddLoaderItem: '[AppComponent] AddLoaderItem',
        RemoveLoaderItem: '[AppComponent] RemoveLoaderItem',
    };
    var AddLoaderItem = /** @class */ (function () {
        function AddLoaderItem(payload) {
            this.payload = payload;
            this.type = LoaderTypes.AddLoaderItem;
        }
        return AddLoaderItem;
    }());
    var RemoveLoaderItem = /** @class */ (function () {
        function RemoveLoaderItem(payload) {
            this.payload = payload;
            this.type = LoaderTypes.RemoveLoaderItem;
        }
        return RemoveLoaderItem;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var LoaderService = /** @class */ (function () {
        function LoaderService(store) {
            var _this = this;
            this.store = store;
            this.store$ = store.pipe(i1.select('loader'));
            this.store$.subscribe(function (state) {
                _this.isActive = Boolean(state.length);
            });
        }
        /**
         * @param {?} payload
         * @return {?}
         */
        LoaderService.prototype.addAnItemToLoader = /**
         * @param {?} payload
         * @return {?}
         */
            function (payload) {
                this.store.dispatch(new AddLoaderItem(payload));
            };
        /**
         * @param {?} payload
         * @return {?}
         */
        LoaderService.prototype.removeAnItemToLoader = /**
         * @param {?} payload
         * @return {?}
         */
            function (payload) {
                this.store.dispatch(new RemoveLoaderItem(payload));
            };
        LoaderService.decorators = [
            { type: i0.Injectable, args: [{
                        providedIn: 'root'
                    },] }
        ];
        /** @nocollapse */
        LoaderService.ctorParameters = function () {
            return [
                { type: undefined, decorators: [{ type: i0.Inject, args: [i1.Store,] }] }
            ];
        };
        /** @nocollapse */ LoaderService.ngInjectableDef = i0.defineInjectable({ factory: function LoaderService_Factory() { return new LoaderService(i0.inject(i1.Store)); }, token: LoaderService, providedIn: "root" });
        return LoaderService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var loaderDefaultState = [];
    /**
     * @param {?=} state
     * @param {?=} action
     * @return {?}
     */
    function loaderReducer(state, action) {
        if (state === void 0) {
            state = loaderDefaultState;
        }
        /** @type {?} */
        var loaders;
        /** @type {?} */
        var indexOnArray;
        switch (action.type) {
            case LoaderTypes.AddLoaderItem:
                loaders = state.slice(0);
                indexOnArray = loaders.findIndex(function (element) {
                    return element.page === action.payload.page && element.name === action.payload.name;
                });
                if (indexOnArray === -1) {
                    loaders.push(action.payload);
                }
                return loaders;
            case LoaderTypes.RemoveLoaderItem:
                loaders = state.slice(0);
                indexOnArray = loaders.findIndex(function (element) {
                    return element.page === action.payload.page && element.name === action.payload.name;
                });
                loaders.splice(indexOnArray, 1);
                return loaders;
            default:
                return state;
        }
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    exports.LoaderModule = LoaderModule;
    exports.LoaderService = LoaderService;
    exports.LoaderTypes = LoaderTypes;
    exports.AddLoaderItem = AddLoaderItem;
    exports.RemoveLoaderItem = RemoveLoaderItem;
    exports.loaderReducer = loaderReducer;
    exports.loaderDefaultState = loaderDefaultState;
    exports.ɵa = LoaderComponent;

    Object.defineProperty(exports, '__esModule', { value: true });

})));

//# sourceMappingURL=sofisa-loader.umd.js.map