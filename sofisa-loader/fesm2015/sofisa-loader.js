import { CommonModule } from '@angular/common';
import { Component, Input, NgModule, Injectable, Inject, defineInjectable, inject } from '@angular/core';
import { Store, select } from '@ngrx/store';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class LoaderComponent {
    constructor() {
        this.show = false;
    }
}
LoaderComponent.decorators = [
    { type: Component, args: [{
                selector: 'so-loader',
                template: "<div class=\"so-loader clearfix\" [hidden]=\"!show\">\n\t<div class=\"so-loader--content clearfix\">\n\t\t<img src=\"assets/images/comet-spinner.gif\" />\n\t\t\n\t\t<span>Carregando...</span>\n\t</div>\n</div>",
                styles: [".so-loader{width:100%;height:100%;position:fixed;top:0;left:0;z-index:9000;background:rgba(255,255,255,.8)}.so-loader--content{display:table;position:absolute;top:50%;left:50%;-webkit-transform:translate(-50%,-50%);transform:translate(-50%,-50%);text-align:center}.so-loader span{font-family:Roboto;font-weight:500;color:#333;text-transform:uppercase;text-align:center;display:block;margin-top:20px}"]
            }] }
];
LoaderComponent.propDecorators = {
    show: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class LoaderModule {
}
LoaderModule.decorators = [
    { type: NgModule, args: [{
                declarations: [LoaderComponent],
                imports: [
                    CommonModule
                ],
                exports: [LoaderComponent]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
const LoaderTypes = {
    AddLoaderItem: '[AppComponent] AddLoaderItem',
    RemoveLoaderItem: '[AppComponent] RemoveLoaderItem',
};
class AddLoaderItem {
    /**
     * @param {?} payload
     */
    constructor(payload) {
        this.payload = payload;
        this.type = LoaderTypes.AddLoaderItem;
    }
}
class RemoveLoaderItem {
    /**
     * @param {?} payload
     */
    constructor(payload) {
        this.payload = payload;
        this.type = LoaderTypes.RemoveLoaderItem;
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class LoaderService {
    /**
     * @param {?} store
     */
    constructor(store) {
        this.store = store;
        this.store$ = store.pipe(select('loader'));
        this.store$.subscribe((state) => {
            this.isActive = Boolean(state.length);
        });
    }
    /**
     * @param {?} payload
     * @return {?}
     */
    addAnItemToLoader(payload) {
        this.store.dispatch(new AddLoaderItem(payload));
    }
    /**
     * @param {?} payload
     * @return {?}
     */
    removeAnItemToLoader(payload) {
        this.store.dispatch(new RemoveLoaderItem(payload));
    }
}
LoaderService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
LoaderService.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [Store,] }] }
];
/** @nocollapse */ LoaderService.ngInjectableDef = defineInjectable({ factory: function LoaderService_Factory() { return new LoaderService(inject(Store)); }, token: LoaderService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const loaderDefaultState = [];
/**
 * @param {?=} state
 * @param {?=} action
 * @return {?}
 */
function loaderReducer(state = loaderDefaultState, action) {
    /** @type {?} */
    let loaders;
    /** @type {?} */
    let indexOnArray;
    switch (action.type) {
        case LoaderTypes.AddLoaderItem:
            loaders = state.slice(0);
            indexOnArray = loaders.findIndex((element) => {
                return element.page === action.payload.page && element.name === action.payload.name;
            });
            if (indexOnArray === -1) {
                loaders.push(action.payload);
            }
            return loaders;
        case LoaderTypes.RemoveLoaderItem:
            loaders = state.slice(0);
            indexOnArray = loaders.findIndex((element) => {
                return element.page === action.payload.page && element.name === action.payload.name;
            });
            loaders.splice(indexOnArray, 1);
            return loaders;
        default:
            return state;
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { LoaderModule, LoaderService, LoaderTypes, AddLoaderItem, RemoveLoaderItem, loaderReducer, loaderDefaultState, LoaderComponent as ɵa };

//# sourceMappingURL=sofisa-loader.js.map