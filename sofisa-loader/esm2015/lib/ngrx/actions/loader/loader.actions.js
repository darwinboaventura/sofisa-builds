/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
const LoaderTypes = {
    AddLoaderItem: '[AppComponent] AddLoaderItem',
    RemoveLoaderItem: '[AppComponent] RemoveLoaderItem',
};
export { LoaderTypes };
export class AddLoaderItem {
    /**
     * @param {?} payload
     */
    constructor(payload) {
        this.payload = payload;
        this.type = LoaderTypes.AddLoaderItem;
    }
}
if (false) {
    /** @type {?} */
    AddLoaderItem.prototype.type;
    /** @type {?} */
    AddLoaderItem.prototype.payload;
}
export class RemoveLoaderItem {
    /**
     * @param {?} payload
     */
    constructor(payload) {
        this.payload = payload;
        this.type = LoaderTypes.RemoveLoaderItem;
    }
}
if (false) {
    /** @type {?} */
    RemoveLoaderItem.prototype.type;
    /** @type {?} */
    RemoveLoaderItem.prototype.payload;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9hZGVyLmFjdGlvbnMuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9zb2Zpc2EtbG9hZGVyLyIsInNvdXJjZXMiOlsibGliL25ncngvYWN0aW9ucy9sb2FkZXIvbG9hZGVyLmFjdGlvbnMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0lBR0MsZUFBZ0IsOEJBQThCO0lBQzlDLGtCQUFtQixpQ0FBaUM7OztBQUdyRCxNQUFNLE9BQU8sYUFBYTs7OztJQUd6QixZQUFtQixPQUF1QztRQUF2QyxZQUFPLEdBQVAsT0FBTyxDQUFnQztRQUZqRCxTQUFJLEdBQUcsV0FBVyxDQUFDLGFBQWEsQ0FBQztJQUVtQixDQUFDO0NBQzlEOzs7SUFIQSw2QkFBMEM7O0lBRTlCLGdDQUE4Qzs7QUFHM0QsTUFBTSxPQUFPLGdCQUFnQjs7OztJQUc1QixZQUFtQixPQUF1QztRQUF2QyxZQUFPLEdBQVAsT0FBTyxDQUFnQztRQUZqRCxTQUFJLEdBQUcsV0FBVyxDQUFDLGdCQUFnQixDQUFDO0lBRWdCLENBQUM7Q0FDOUQ7OztJQUhBLGdDQUE2Qzs7SUFFakMsbUNBQThDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtBY3Rpb259IGZyb20gJ0BuZ3J4L3N0b3JlJztcclxuXHJcbmV4cG9ydCBlbnVtIExvYWRlclR5cGVzIHtcclxuXHRBZGRMb2FkZXJJdGVtID0gJ1tBcHBDb21wb25lbnRdIEFkZExvYWRlckl0ZW0nLFxyXG5cdFJlbW92ZUxvYWRlckl0ZW0gPSAnW0FwcENvbXBvbmVudF0gUmVtb3ZlTG9hZGVySXRlbSdcclxufVxyXG5cclxuZXhwb3J0IGNsYXNzIEFkZExvYWRlckl0ZW0gaW1wbGVtZW50cyBBY3Rpb24ge1xyXG5cdHJlYWRvbmx5IHR5cGUgPSBMb2FkZXJUeXBlcy5BZGRMb2FkZXJJdGVtO1xyXG5cdFxyXG5cdGNvbnN0cnVjdG9yKHB1YmxpYyBwYXlsb2FkOiB7IHBhZ2U6IHN0cmluZywgbmFtZTogc3RyaW5nIH0pIHt9XHJcbn1cclxuXHJcbmV4cG9ydCBjbGFzcyBSZW1vdmVMb2FkZXJJdGVtIGltcGxlbWVudHMgQWN0aW9uIHtcclxuXHRyZWFkb25seSB0eXBlID0gTG9hZGVyVHlwZXMuUmVtb3ZlTG9hZGVySXRlbTtcclxuXHRcclxuXHRjb25zdHJ1Y3RvcihwdWJsaWMgcGF5bG9hZDogeyBwYWdlOiBzdHJpbmcsIG5hbWU6IHN0cmluZyB9KSB7fVxyXG59XHJcblxyXG4iXX0=