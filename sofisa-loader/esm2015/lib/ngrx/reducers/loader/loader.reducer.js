/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { LoaderTypes } from '../../actions/loader/loader.actions';
/** @type {?} */
export const loaderDefaultState = [];
/**
 * @param {?=} state
 * @param {?=} action
 * @return {?}
 */
export function loaderReducer(state = loaderDefaultState, action) {
    /** @type {?} */
    let loaders;
    /** @type {?} */
    let indexOnArray;
    switch (action.type) {
        case LoaderTypes.AddLoaderItem:
            loaders = state.slice(0);
            indexOnArray = loaders.findIndex((element) => {
                return element.page === action.payload.page && element.name === action.payload.name;
            });
            if (indexOnArray === -1) {
                loaders.push(action.payload);
            }
            return loaders;
        case LoaderTypes.RemoveLoaderItem:
            loaders = state.slice(0);
            indexOnArray = loaders.findIndex((element) => {
                return element.page === action.payload.page && element.name === action.payload.name;
            });
            loaders.splice(indexOnArray, 1);
            return loaders;
        default:
            return state;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9hZGVyLnJlZHVjZXIuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9zb2Zpc2EtbG9hZGVyLyIsInNvdXJjZXMiOlsibGliL25ncngvcmVkdWNlcnMvbG9hZGVyL2xvYWRlci5yZWR1Y2VyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsV0FBVyxFQUFDLE1BQU0scUNBQXFDLENBQUM7O0FBRWhFLE1BQU0sT0FBTyxrQkFBa0IsR0FBRyxFQUFFOzs7Ozs7QUFFcEMsTUFBTSxVQUFVLGFBQWEsQ0FBQyxLQUFLLEdBQUcsa0JBQWtCLEVBQUUsTUFBVzs7UUFDaEUsT0FBTzs7UUFDUCxZQUFZO0lBRWhCLFFBQVEsTUFBTSxDQUFDLElBQUksRUFBRTtRQUNwQixLQUFLLFdBQVcsQ0FBQyxhQUFhO1lBQzdCLE9BQU8sR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3pCLFlBQVksR0FBRyxPQUFPLENBQUMsU0FBUyxDQUFDLENBQUMsT0FBTyxFQUFFLEVBQUU7Z0JBQzVDLE9BQU8sT0FBTyxDQUFDLElBQUksS0FBSyxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksSUFBSSxPQUFPLENBQUMsSUFBSSxLQUFLLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDO1lBQ3JGLENBQUMsQ0FBQyxDQUFDO1lBRUgsSUFBSSxZQUFZLEtBQUssQ0FBQyxDQUFDLEVBQUU7Z0JBQ3hCLE9BQU8sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2FBQzdCO1lBRUQsT0FBTyxPQUFPLENBQUM7UUFDaEIsS0FBSyxXQUFXLENBQUMsZ0JBQWdCO1lBQ2hDLE9BQU8sR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3pCLFlBQVksR0FBRyxPQUFPLENBQUMsU0FBUyxDQUFDLENBQUMsT0FBTyxFQUFFLEVBQUU7Z0JBQzVDLE9BQU8sT0FBTyxDQUFDLElBQUksS0FBSyxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksSUFBSSxPQUFPLENBQUMsSUFBSSxLQUFLLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDO1lBQ3JGLENBQUMsQ0FBQyxDQUFDO1lBRUgsT0FBTyxDQUFDLE1BQU0sQ0FBQyxZQUFZLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFFaEMsT0FBTyxPQUFPLENBQUM7UUFDaEI7WUFDQyxPQUFPLEtBQUssQ0FBQztLQUNkO0FBQ0YsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7TG9hZGVyVHlwZXN9IGZyb20gJy4uLy4uL2FjdGlvbnMvbG9hZGVyL2xvYWRlci5hY3Rpb25zJztcclxuXHJcbmV4cG9ydCBjb25zdCBsb2FkZXJEZWZhdWx0U3RhdGUgPSBbXTtcclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBsb2FkZXJSZWR1Y2VyKHN0YXRlID0gbG9hZGVyRGVmYXVsdFN0YXRlLCBhY3Rpb246IGFueSkge1xyXG5cdGxldCBsb2FkZXJzO1xyXG5cdGxldCBpbmRleE9uQXJyYXk7XHJcblxyXG5cdHN3aXRjaCAoYWN0aW9uLnR5cGUpIHtcclxuXHRcdGNhc2UgTG9hZGVyVHlwZXMuQWRkTG9hZGVySXRlbTpcclxuXHRcdFx0bG9hZGVycyA9IHN0YXRlLnNsaWNlKDApO1xyXG5cdFx0XHRpbmRleE9uQXJyYXkgPSBsb2FkZXJzLmZpbmRJbmRleCgoZWxlbWVudCkgPT4ge1xyXG5cdFx0XHRcdHJldHVybiBlbGVtZW50LnBhZ2UgPT09IGFjdGlvbi5wYXlsb2FkLnBhZ2UgJiYgZWxlbWVudC5uYW1lID09PSBhY3Rpb24ucGF5bG9hZC5uYW1lO1xyXG5cdFx0XHR9KTtcclxuXHJcblx0XHRcdGlmIChpbmRleE9uQXJyYXkgPT09IC0xKSB7XHJcblx0XHRcdFx0bG9hZGVycy5wdXNoKGFjdGlvbi5wYXlsb2FkKTtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0cmV0dXJuIGxvYWRlcnM7XHJcblx0XHRjYXNlIExvYWRlclR5cGVzLlJlbW92ZUxvYWRlckl0ZW06XHJcblx0XHRcdGxvYWRlcnMgPSBzdGF0ZS5zbGljZSgwKTtcclxuXHRcdFx0aW5kZXhPbkFycmF5ID0gbG9hZGVycy5maW5kSW5kZXgoKGVsZW1lbnQpID0+IHtcclxuXHRcdFx0XHRyZXR1cm4gZWxlbWVudC5wYWdlID09PSBhY3Rpb24ucGF5bG9hZC5wYWdlICYmIGVsZW1lbnQubmFtZSA9PT0gYWN0aW9uLnBheWxvYWQubmFtZTtcclxuXHRcdFx0fSk7XHJcblxyXG5cdFx0XHRsb2FkZXJzLnNwbGljZShpbmRleE9uQXJyYXksIDEpO1xyXG5cclxuXHRcdFx0cmV0dXJuIGxvYWRlcnM7XHJcblx0XHRkZWZhdWx0OlxyXG5cdFx0XHRyZXR1cm4gc3RhdGU7XHJcblx0fVxyXG59XHJcblxyXG5cclxuIl19