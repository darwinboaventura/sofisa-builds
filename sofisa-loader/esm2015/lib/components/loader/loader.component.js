/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
export class LoaderComponent {
    constructor() {
        this.show = false;
    }
}
LoaderComponent.decorators = [
    { type: Component, args: [{
                selector: 'so-loader',
                template: "<div class=\"so-loader clearfix\" [hidden]=\"!show\">\n\t<div class=\"so-loader--content clearfix\">\n\t\t<img src=\"assets/images/comet-spinner.gif\" />\n\t\t\n\t\t<span>Carregando...</span>\n\t</div>\n</div>",
                styles: [".so-loader{width:100%;height:100%;position:fixed;top:0;left:0;z-index:9000;background:rgba(255,255,255,.8)}.so-loader--content{display:table;position:absolute;top:50%;left:50%;-webkit-transform:translate(-50%,-50%);transform:translate(-50%,-50%);text-align:center}.so-loader span{font-family:Roboto;font-weight:500;color:#333;text-transform:uppercase;text-align:center;display:block;margin-top:20px}"]
            }] }
];
LoaderComponent.propDecorators = {
    show: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    LoaderComponent.prototype.show;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9hZGVyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3NvZmlzYS1sb2FkZXIvIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9sb2FkZXIvbG9hZGVyLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFDLFNBQVMsRUFBRSxLQUFLLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFRL0MsTUFBTSxPQUFPLGVBQWU7SUFONUI7UUFPVSxTQUFJLEdBQVksS0FBSyxDQUFDO0lBQ2hDLENBQUM7OztZQVJBLFNBQVMsU0FBQztnQkFDVixRQUFRLEVBQUUsV0FBVztnQkFDckIsNk5BQXNDOzthQUV0Qzs7O21CQUdDLEtBQUs7Ozs7SUFBTiwrQkFBK0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgSW5wdXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcblx0c2VsZWN0b3I6ICdzby1sb2FkZXInLFxuXHR0ZW1wbGF0ZVVybDogJy4vbG9hZGVyLmNvbXBvbmVudC5odG1sJyxcblx0c3R5bGVVcmxzOiBbJy4vbG9hZGVyLmNvbXBvbmVudC5zY3NzJ11cbn0pXG5cbmV4cG9ydCBjbGFzcyBMb2FkZXJDb21wb25lbnQge1xuXHRASW5wdXQoKSBzaG93OiBCb29sZWFuID0gZmFsc2U7XG59XG4iXX0=