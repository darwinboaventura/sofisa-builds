import { CommonModule } from '@angular/common';
import { Component, Input, NgModule, Injectable, Inject, defineInjectable, inject } from '@angular/core';
import { Store, select } from '@ngrx/store';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var LoaderComponent = /** @class */ (function () {
    function LoaderComponent() {
        this.show = false;
    }
    LoaderComponent.decorators = [
        { type: Component, args: [{
                    selector: 'so-loader',
                    template: "<div class=\"so-loader clearfix\" [hidden]=\"!show\">\n\t<div class=\"so-loader--content clearfix\">\n\t\t<img src=\"assets/images/comet-spinner.gif\" />\n\t\t\n\t\t<span>Carregando...</span>\n\t</div>\n</div>",
                    styles: [".so-loader{width:100%;height:100%;position:fixed;top:0;left:0;z-index:9000;background:rgba(255,255,255,.8)}.so-loader--content{display:table;position:absolute;top:50%;left:50%;-webkit-transform:translate(-50%,-50%);transform:translate(-50%,-50%);text-align:center}.so-loader span{font-family:Roboto;font-weight:500;color:#333;text-transform:uppercase;text-align:center;display:block;margin-top:20px}"]
                }] }
    ];
    LoaderComponent.propDecorators = {
        show: [{ type: Input }]
    };
    return LoaderComponent;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var LoaderModule = /** @class */ (function () {
    function LoaderModule() {
    }
    LoaderModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [LoaderComponent],
                    imports: [
                        CommonModule
                    ],
                    exports: [LoaderComponent]
                },] }
    ];
    return LoaderModule;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
var LoaderTypes = {
    AddLoaderItem: '[AppComponent] AddLoaderItem',
    RemoveLoaderItem: '[AppComponent] RemoveLoaderItem',
};
var AddLoaderItem = /** @class */ (function () {
    function AddLoaderItem(payload) {
        this.payload = payload;
        this.type = LoaderTypes.AddLoaderItem;
    }
    return AddLoaderItem;
}());
var RemoveLoaderItem = /** @class */ (function () {
    function RemoveLoaderItem(payload) {
        this.payload = payload;
        this.type = LoaderTypes.RemoveLoaderItem;
    }
    return RemoveLoaderItem;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var LoaderService = /** @class */ (function () {
    function LoaderService(store) {
        var _this = this;
        this.store = store;
        this.store$ = store.pipe(select('loader'));
        this.store$.subscribe(function (state) {
            _this.isActive = Boolean(state.length);
        });
    }
    /**
     * @param {?} payload
     * @return {?}
     */
    LoaderService.prototype.addAnItemToLoader = /**
     * @param {?} payload
     * @return {?}
     */
    function (payload) {
        this.store.dispatch(new AddLoaderItem(payload));
    };
    /**
     * @param {?} payload
     * @return {?}
     */
    LoaderService.prototype.removeAnItemToLoader = /**
     * @param {?} payload
     * @return {?}
     */
    function (payload) {
        this.store.dispatch(new RemoveLoaderItem(payload));
    };
    LoaderService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    LoaderService.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [Store,] }] }
    ]; };
    /** @nocollapse */ LoaderService.ngInjectableDef = defineInjectable({ factory: function LoaderService_Factory() { return new LoaderService(inject(Store)); }, token: LoaderService, providedIn: "root" });
    return LoaderService;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
var loaderDefaultState = [];
/**
 * @param {?=} state
 * @param {?=} action
 * @return {?}
 */
function loaderReducer(state, action) {
    if (state === void 0) { state = loaderDefaultState; }
    /** @type {?} */
    var loaders;
    /** @type {?} */
    var indexOnArray;
    switch (action.type) {
        case LoaderTypes.AddLoaderItem:
            loaders = state.slice(0);
            indexOnArray = loaders.findIndex(function (element) {
                return element.page === action.payload.page && element.name === action.payload.name;
            });
            if (indexOnArray === -1) {
                loaders.push(action.payload);
            }
            return loaders;
        case LoaderTypes.RemoveLoaderItem:
            loaders = state.slice(0);
            indexOnArray = loaders.findIndex(function (element) {
                return element.page === action.payload.page && element.name === action.payload.name;
            });
            loaders.splice(indexOnArray, 1);
            return loaders;
        default:
            return state;
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { LoaderModule, LoaderService, LoaderTypes, AddLoaderItem, RemoveLoaderItem, loaderReducer, loaderDefaultState, LoaderComponent as ɵa };

//# sourceMappingURL=sofisa-loader.js.map