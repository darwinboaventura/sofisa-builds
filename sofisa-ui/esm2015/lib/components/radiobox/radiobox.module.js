/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RadioboxComponent } from './radiobox.component';
export class RadioboxModule {
}
RadioboxModule.decorators = [
    { type: NgModule, args: [{
                declarations: [RadioboxComponent],
                imports: [
                    CommonModule
                ],
                exports: [RadioboxComponent]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmFkaW9ib3gubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vc29maXNhLXVpLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvcmFkaW9ib3gvcmFkaW9ib3gubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsUUFBUSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ3ZDLE9BQU8sRUFBQyxZQUFZLEVBQUMsTUFBTSxpQkFBaUIsQ0FBQztBQUM3QyxPQUFPLEVBQUMsaUJBQWlCLEVBQUMsTUFBTSxzQkFBc0IsQ0FBQztBQVV2RCxNQUFNLE9BQU8sY0FBYzs7O1lBUjFCLFFBQVEsU0FBQztnQkFDVCxZQUFZLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQztnQkFDakMsT0FBTyxFQUFFO29CQUNSLFlBQVk7aUJBQ1o7Z0JBQ0QsT0FBTyxFQUFFLENBQUMsaUJBQWlCLENBQUM7YUFDNUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge05nTW9kdWxlfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7Q29tbW9uTW9kdWxlfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHtSYWRpb2JveENvbXBvbmVudH0gZnJvbSAnLi9yYWRpb2JveC5jb21wb25lbnQnO1xuXG5ATmdNb2R1bGUoe1xuXHRkZWNsYXJhdGlvbnM6IFtSYWRpb2JveENvbXBvbmVudF0sXG5cdGltcG9ydHM6IFtcblx0XHRDb21tb25Nb2R1bGVcblx0XSxcblx0ZXhwb3J0czogW1JhZGlvYm94Q29tcG9uZW50XVxufSlcblxuZXhwb3J0IGNsYXNzIFJhZGlvYm94TW9kdWxlIHt9XG4iXX0=