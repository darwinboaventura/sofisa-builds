/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, EventEmitter, Input, Output } from '@angular/core';
export class TextareaComponent {
    constructor() {
        this.label = '';
        this.value = '';
        this.hasChanged = new EventEmitter();
    }
    /**
     * @param {?} e
     * @return {?}
     */
    handleChange(e) {
        this.hasChanged.emit(e.target.value);
    }
}
TextareaComponent.decorators = [
    { type: Component, args: [{
                selector: 'so-textarea',
                template: "<fieldset class=\"so-input clearfix\">\n\t<label class=\"so-input--label\">{{label}}</label>\n\t\n\t<textarea class=\"so-input--textarea\" (change)=\"handleChange($event)\">{{value}}</textarea>\n</fieldset>",
                styles: [".so-input--field,.so-input--textarea,.so-shadow--1{box-shadow:0 2px 4px 0 rgba(0,0,0,.1)}.so-shadow--2{box-shadow:0 5px 8px 0 rgba(0,0,0,.1)}.so-shadow--3{box-shadow:0 9px 10px 0 rgba(0,0,0,.1)}.so-shadow--4{box-shadow:0 20px 30px 0 rgba(0,0,0,.1)}.so-shadow--5{box-shadow:0 30px 30px 0 rgba(0,0,0,.1)}.so-shadow--6{box-shadow:0 50px 50px 0 rgba(0,0,0,.1)}.so-input{width:100%;position:relative}.so-input--field,.so-input--label{font-size:14px;color:#333}.so-input--label{display:block;margin-bottom:12px}.so-input--field,.so-input--textarea{width:100%;border:0;padding:16px;background:#fff}.so-input--field[disabled],.so-input--textarea[disabled]{color:#999;background:#ebebeb}.so-input--textarea{height:250px}.so-input--icon,.so-input--param{width:50px;height:53px;position:absolute;bottom:0;background:#e5e5e5;font-size:14px;color:#333}.so-input--icon.left,.so-input--param.left{left:0}.so-input--icon.right,.so-input--param.right{right:0}.so-input--icon--center,.so-input--param--center{position:absolute;top:50%;left:50%;-webkit-transform:translate(-50%,-50%);transform:translate(-50%,-50%)}.so-input--icon{background:0 0}.so-input--message{font-size:14px;position:absolute;bottom:-35px;left:0}.so-input.with-params .so-input--field{padding-left:70px;padding-right:70px}.so-input.error-input,.so-input.success-input,.so-input.warning-input{margin-bottom:65.3px}.so-input.without-bg .so-input--field[disabled]{border-bottom-color:#ebebeb;background:0 0}.so-input.error-input.without-bg .so-input--field{border-bottom:2px solid #e24c4c}.so-input.error-input.with-params .so-input--param{color:#ff6b6b;border-bottom:2px solid #e24c4c;background:#f9d3cf}.so-input.error-input .so-input--field{border-bottom:2px solid #e24c4c}.so-input.error-input .so-input--message{color:#e24c4c}.so-input.success-input.without-bg .so-input--field{border-bottom:2px solid #4ce2a7}.so-input.success-input.with-params .so-input--param{color:#4ce2a7;border-bottom:2px solid #4ce2a7;background:#cbf3dc}.so-input.success-input .so-input--field{border-bottom:2px solid #4ce2a7}.so-input.success-input .so-input--message{color:#4ce2a7}.so-input.warning-input.without-bg .so-input--field{border-bottom:2px solid #f1a153}.so-input.warning-input.with-params .so-input--param{color:#ffaa31;border-bottom:2px solid #f1a153;background:#fce7c4}.so-input.warning-input .so-input--field{border-bottom:2px solid #f1a153}.so-input.warning-input .so-input--message{color:#f1a153}.so-input.without-bg .so-input--field{color:#666;border-bottom:2px solid #666;box-shadow:none;background:0 0}.so-input.without-bg .so-input--field.is-active,.so-input.without-bg .so-input--field:active{border-bottom-color:#2d4ef5}.so-input.without-bg .so-input--param{background:0 0}.so-input.with-params.without-bg .so-input--param{background:0 0!important}"]
            }] }
];
TextareaComponent.propDecorators = {
    label: [{ type: Input }],
    value: [{ type: Input }],
    hasChanged: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    TextareaComponent.prototype.label;
    /** @type {?} */
    TextareaComponent.prototype.value;
    /** @type {?} */
    TextareaComponent.prototype.hasChanged;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGV4dGFyZWEuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vc29maXNhLXVpLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvdGV4dGFyZWEvdGV4dGFyZWEuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsU0FBUyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBUXJFLE1BQU0sT0FBTyxpQkFBaUI7SUFOOUI7UUFPVSxVQUFLLEdBQVcsRUFBRSxDQUFDO1FBQ25CLFVBQUssR0FBVyxFQUFFLENBQUM7UUFFbEIsZUFBVSxHQUFzQixJQUFJLFlBQVksRUFBRSxDQUFDO0lBSzlELENBQUM7Ozs7O0lBSEEsWUFBWSxDQUFDLENBQUM7UUFDYixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3RDLENBQUM7OztZQWRELFNBQVMsU0FBQztnQkFDVixRQUFRLEVBQUUsYUFBYTtnQkFDdkIsME5BQXdDOzthQUV4Qzs7O29CQUdDLEtBQUs7b0JBQ0wsS0FBSzt5QkFFTCxNQUFNOzs7O0lBSFAsa0NBQTRCOztJQUM1QixrQ0FBNEI7O0lBRTVCLHVDQUE2RCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBFdmVudEVtaXR0ZXIsIElucHV0LCBPdXRwdXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcblx0c2VsZWN0b3I6ICdzby10ZXh0YXJlYScsXG5cdHRlbXBsYXRlVXJsOiAnLi90ZXh0YXJlYS5jb21wb25lbnQuaHRtbCcsXG5cdHN0eWxlVXJsczogWycuL3RleHRhcmVhLmNvbXBvbmVudC5zY3NzJ11cbn0pXG5cbmV4cG9ydCBjbGFzcyBUZXh0YXJlYUNvbXBvbmVudCB7XG5cdEBJbnB1dCgpIGxhYmVsOiBTdHJpbmcgPSAnJztcblx0QElucHV0KCkgdmFsdWU6IFN0cmluZyA9ICcnO1xuXHRcblx0QE91dHB1dCgpIGhhc0NoYW5nZWQ6IEV2ZW50RW1pdHRlcjxhbnk+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuXHRcblx0aGFuZGxlQ2hhbmdlKGUpIHtcblx0XHR0aGlzLmhhc0NoYW5nZWQuZW1pdChlLnRhcmdldC52YWx1ZSk7XG5cdH1cbn1cbiJdfQ==