/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
export class TabsComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() { }
}
TabsComponent.decorators = [
    { type: Component, args: [{
                selector: 'so-tabs',
                template: "<div class=\"so-tabs clearfix\">\n\t<div class=\"so-tabs--head clearfix\">\n\t\t<div class=\"so-tabs--head--item is-active clearfix\">Active</div>\n\t\t<div class=\"so-tabs--head--item clearfix\">Hover</div>\n\t\t<div class=\"so-tabs--head--item clearfix\">Default</div>\n\t</div>\n\n\t<div class=\"so-tabs--main clearfix\">\n\t\t<h3>This feels like a diary</h3>\n\n\t\t<p>With supporting text below as a natural lead-in to additional content.</p>\n\t</div>\n</div>",
                styles: [".so-shadow--1,.so-tabs--main{box-shadow:0 2px 4px 0 rgba(0,0,0,.1)}.so-shadow--2{box-shadow:0 5px 8px 0 rgba(0,0,0,.1)}.so-shadow--3{box-shadow:0 9px 10px 0 rgba(0,0,0,.1)}.so-shadow--4{box-shadow:0 20px 30px 0 rgba(0,0,0,.1)}.so-shadow--5{box-shadow:0 30px 30px 0 rgba(0,0,0,.1)}.so-shadow--6{box-shadow:0 50px 50px 0 rgba(0,0,0,.1)}.so-tabs{width:100%}.so-tabs--head{background-color:#fafafa}.so-tabs--head--item{font-family:Nunito;font-weight:700;font-size:14px;color:#666;text-transform:uppercase;float:left;padding:16px 20px;cursor:pointer}.so-tabs--head--item.is-active{color:#022047;box-shadow:2px 0 4px 0 rgba(0,0,0,.1);border-radius:5px 5px 0 0;background-color:#fff}.so-tabs--head--item:hover{color:#2d4ef5}.so-tabs--main{position:relative;padding:26px 20px;border-radius:3px 3px 0;background-color:#fff}"]
            }] }
];
/** @nocollapse */
TabsComponent.ctorParameters = () => [];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFicy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9zb2Zpc2EtdWkvIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy90YWJzL3RhYnMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsU0FBUyxFQUFTLE1BQU0sZUFBZSxDQUFDO0FBUWhELE1BQU0sT0FBTyxhQUFhO0lBQ3pCLGdCQUFlLENBQUM7Ozs7SUFFaEIsUUFBUSxLQUFJLENBQUM7OztZQVRiLFNBQVMsU0FBQztnQkFDVixRQUFRLEVBQUUsU0FBUztnQkFDbkIsNmRBQW9DOzthQUVwQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBPbkluaXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcblx0c2VsZWN0b3I6ICdzby10YWJzJyxcblx0dGVtcGxhdGVVcmw6ICcuL3RhYnMuY29tcG9uZW50Lmh0bWwnLFxuXHRzdHlsZVVybHM6IFsnLi90YWJzLmNvbXBvbmVudC5zY3NzJ11cbn0pXG5cbmV4cG9ydCBjbGFzcyBUYWJzQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblx0Y29uc3RydWN0b3IoKSB7fVxuXG5cdG5nT25Jbml0KCkge31cbn1cbiJdfQ==