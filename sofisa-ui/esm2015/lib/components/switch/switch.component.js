/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, EventEmitter, Input, Output } from '@angular/core';
export class SwitchComponent {
    constructor() {
        this.isActive = false;
        this.hasChange = new EventEmitter();
    }
    /**
     * @return {?}
     */
    ngOnInit() { }
    /**
     * @return {?}
     */
    toggleSwitch() {
        this.isActive = !this.isActive;
        this.hasChange.emit(this.isActive);
    }
}
SwitchComponent.decorators = [
    { type: Component, args: [{
                selector: 'so-switch',
                template: "<div class=\"so-switch\" [ngClass]=\"{ 'is-active': isActive}\" (click)=\"toggleSwitch()\"></div>",
                styles: [".so-shadow--1,.so-switch{box-shadow:0 2px 4px 0 rgba(0,0,0,.1)}.so-shadow--2{box-shadow:0 5px 8px 0 rgba(0,0,0,.1)}.so-shadow--3{box-shadow:0 9px 10px 0 rgba(0,0,0,.1)}.so-shadow--4{box-shadow:0 20px 30px 0 rgba(0,0,0,.1)}.so-shadow--5{box-shadow:0 30px 30px 0 rgba(0,0,0,.1)}.so-shadow--6{box-shadow:0 50px 50px 0 rgba(0,0,0,.1)}.so-switch{width:58px;height:28px;border-radius:22px;background:#fff;position:relative;cursor:pointer}.so-switch:after{width:20px;height:20px;border-radius:50%;content:'';display:table;transition:.3s ease-in-out;background:#e5e5e5;position:absolute;top:50%;left:4px;-webkit-transform:translateY(-50%);transform:translateY(-50%)}.so-switch.is-active:after{left:unset;right:4px;background:#0cb598}"]
            }] }
];
/** @nocollapse */
SwitchComponent.ctorParameters = () => [];
SwitchComponent.propDecorators = {
    isActive: [{ type: Input }],
    hasChange: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    SwitchComponent.prototype.isActive;
    /** @type {?} */
    SwitchComponent.prototype.hasChange;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3dpdGNoLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3NvZmlzYS11aS8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL3N3aXRjaC9zd2l0Y2guY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsU0FBUyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQVUsTUFBTSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBUTdFLE1BQU0sT0FBTyxlQUFlO0lBSTNCO1FBSFMsYUFBUSxHQUFZLEtBQUssQ0FBQztRQUN6QixjQUFTLEdBQTBCLElBQUksWUFBWSxFQUFFLENBQUM7SUFFakQsQ0FBQzs7OztJQUVoQixRQUFRLEtBQUksQ0FBQzs7OztJQUViLFlBQVk7UUFDWCxJQUFJLENBQUMsUUFBUSxHQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUUvQixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDcEMsQ0FBQzs7O1lBbEJELFNBQVMsU0FBQztnQkFDVixRQUFRLEVBQUUsV0FBVztnQkFDckIsNkdBQXNDOzthQUV0Qzs7Ozs7dUJBR0MsS0FBSzt3QkFDTCxNQUFNOzs7O0lBRFAsbUNBQW1DOztJQUNuQyxvQ0FBZ0UiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgRXZlbnRFbWl0dGVyLCBJbnB1dCwgT25Jbml0LCBPdXRwdXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcblx0c2VsZWN0b3I6ICdzby1zd2l0Y2gnLFxuXHR0ZW1wbGF0ZVVybDogJy4vc3dpdGNoLmNvbXBvbmVudC5odG1sJyxcblx0c3R5bGVVcmxzOiBbJy4vc3dpdGNoLmNvbXBvbmVudC5zY3NzJ11cbn0pXG5cbmV4cG9ydCBjbGFzcyBTd2l0Y2hDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXHRASW5wdXQoKSBpc0FjdGl2ZTogQm9vbGVhbiA9IGZhbHNlO1xuXHRAT3V0cHV0KCkgaGFzQ2hhbmdlOiBFdmVudEVtaXR0ZXI8Qm9vbGVhbj4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG5cdFxuXHRjb25zdHJ1Y3RvcigpIHt9XG5cdFxuXHRuZ09uSW5pdCgpIHt9XG5cdFxuXHR0b2dnbGVTd2l0Y2goKSB7XG5cdFx0dGhpcy5pc0FjdGl2ZSA9ICF0aGlzLmlzQWN0aXZlO1xuXHRcdFxuXHRcdHRoaXMuaGFzQ2hhbmdlLmVtaXQodGhpcy5pc0FjdGl2ZSk7XG5cdH1cbn1cbiJdfQ==