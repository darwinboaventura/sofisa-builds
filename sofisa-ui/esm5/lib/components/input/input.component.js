/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, EventEmitter, Input, Output } from '@angular/core';
var InputComponent = /** @class */ (function () {
    function InputComponent() {
        this.label = '';
        this.value = '';
        this.placeholder = '';
        this.inputType = 'text';
        this.inputConfig = {
            type: '',
            disabled: false,
            validation: {
                type: '',
                message: ''
            },
            params: {
                left: '',
                right: ''
            }
        };
        this.hasChanged = new EventEmitter();
    }
    /**
     * @param {?} e
     * @return {?}
     */
    InputComponent.prototype.handleChange = /**
     * @param {?} e
     * @return {?}
     */
    function (e) {
        this.hasChanged.emit(e.target.value);
    };
    InputComponent.decorators = [
        { type: Component, args: [{
                    selector: 'so-input',
                    template: "<fieldset class=\"so-input clearfix\" [ngClass]=\"{\n\t'with-params': inputConfig.validation && inputConfig.validation.type && inputConfig.validation.type.includes('with-params'),\n\t'without-bg': inputConfig.validation && inputConfig.validation.type && inputConfig.validation.type.includes('without-bg'),\n\t'error-input': inputConfig.validation && inputConfig.validation.type && inputConfig.validation.type.includes('error'),\n\t'warning-input': inputConfig.validation && inputConfig.validation.type && inputConfig.validation.type.includes('warning'),\n\t'success-input': inputConfig.validation && inputConfig.validation.type && inputConfig.validation.type.includes('success')\n}\">\n\t<label class=\"so-input--label\">{{label}}</label>\n\t\n\t<input [type]=\"inputType\" [value]=\"value\" class=\"so-input--field\" [placeholder]=\"placeholder\" [disabled]=\"inputConfig.disabled\" (change)=\"handleChange($event)\" [ngClass]=\"{\n\t'is-active': inputConfig.validation && inputConfig.validation.type && inputConfig.validation.type.includes('is-active')\n}\"/>\n\t\n\t<div *ngIf=\"(inputConfig && inputConfig.params) && ( inputConfig.params.left || inputConfig.params.right)\">\n\t\t<div class=\"so-input--param left\" *ngIf=\"inputConfig && inputConfig.params && inputConfig.params.left\">\n\t\t\t<span class=\"so-input--param--center\">{{inputConfig.params.left}}</span>\n\t\t</div>\n\t\t\n\t\t<div class=\"so-input--param right\" *ngIf=\"inputConfig && inputConfig.params && inputConfig.params.right\">\n\t\t\t<span class=\"so-input--param--center\">{{inputConfig.params.right}}</span>\n\t\t</div>\n\t</div>\n\t\n\t<div *ngIf=\"inputConfig.validation && inputConfig.validation.message\">\n\t\t<div class=\"so-input--icon right\" *ngIf=\"inputConfig.validation.type && inputConfig.validation.type === 'error'\">\n\t\t\t<span class=\"so-input--param--center\">\n\t\t\t\t<img src=\"assets/images/icon-close.png\" alt=\"Fechar\">\n\t\t\t</span>\n\t\t</div>\n\t\t\n\t\t<div class=\"so-input--icon right\" *ngIf=\"inputConfig.validation.type && inputConfig.validation.type === 'warning'\">\n\t\t\t<span class=\"so-input--param--center\">\n\t\t\t\t<img src=\"assets/images/icon-alert.png\" alt=\"Aten\u00E7\u00E3o\">\n\t\t\t</span>\n\t\t</div>\n\t\t\n\t\t<div class=\"so-input--icon right\" *ngIf=\"inputConfig.validation.type && inputConfig.validation.type === 'success'\">\n\t\t\t<span class=\"so-input--param--center\">\n\t\t\t\t<img src=\"assets/images/icon-success-green.png\" alt=\"OK\">\n\t\t\t</span>\n\t\t</div>\n\t\t\n\t\t<div class=\"so-input--message\">{{inputConfig.validation.message}}</div>\n\t</div>\n</fieldset>",
                    styles: [".so-input--field,.so-input--textarea,.so-shadow--1{box-shadow:0 2px 4px 0 rgba(0,0,0,.1)}.so-shadow--2{box-shadow:0 5px 8px 0 rgba(0,0,0,.1)}.so-shadow--3{box-shadow:0 9px 10px 0 rgba(0,0,0,.1)}.so-shadow--4{box-shadow:0 20px 30px 0 rgba(0,0,0,.1)}.so-shadow--5{box-shadow:0 30px 30px 0 rgba(0,0,0,.1)}.so-shadow--6{box-shadow:0 50px 50px 0 rgba(0,0,0,.1)}.so-input{width:100%;position:relative}.so-input--field,.so-input--label{font-size:14px;color:#333}.so-input--label{display:block;margin-bottom:12px}.so-input--field,.so-input--textarea{width:100%;border:0;padding:16px;background:#fff}.so-input--field[disabled],.so-input--textarea[disabled]{color:#999;background:#ebebeb}.so-input--textarea{height:250px}.so-input--icon,.so-input--param{width:50px;height:53px;position:absolute;bottom:0;background:#e5e5e5;font-size:14px;color:#333}.so-input--icon.left,.so-input--param.left{left:0}.so-input--icon.right,.so-input--param.right{right:0}.so-input--icon--center,.so-input--param--center{position:absolute;top:50%;left:50%;-webkit-transform:translate(-50%,-50%);transform:translate(-50%,-50%)}.so-input--icon{background:0 0}.so-input--message{font-size:14px;position:absolute;bottom:-35px;left:0}.so-input.with-params .so-input--field{padding-left:70px;padding-right:70px}.so-input.error-input,.so-input.success-input,.so-input.warning-input{margin-bottom:65.3px}.so-input.without-bg .so-input--field[disabled]{border-bottom-color:#ebebeb;background:0 0}.so-input.error-input.without-bg .so-input--field{border-bottom:2px solid #e24c4c}.so-input.error-input.with-params .so-input--param{color:#ff6b6b;border-bottom:2px solid #e24c4c;background:#f9d3cf}.so-input.error-input .so-input--field{border-bottom:2px solid #e24c4c}.so-input.error-input .so-input--message{color:#e24c4c}.so-input.success-input.without-bg .so-input--field{border-bottom:2px solid #4ce2a7}.so-input.success-input.with-params .so-input--param{color:#4ce2a7;border-bottom:2px solid #4ce2a7;background:#cbf3dc}.so-input.success-input .so-input--field{border-bottom:2px solid #4ce2a7}.so-input.success-input .so-input--message{color:#4ce2a7}.so-input.warning-input.without-bg .so-input--field{border-bottom:2px solid #f1a153}.so-input.warning-input.with-params .so-input--param{color:#ffaa31;border-bottom:2px solid #f1a153;background:#fce7c4}.so-input.warning-input .so-input--field{border-bottom:2px solid #f1a153}.so-input.warning-input .so-input--message{color:#f1a153}.so-input.without-bg .so-input--field{color:#666;border-bottom:2px solid #666;box-shadow:none;background:0 0}.so-input.without-bg .so-input--field.is-active,.so-input.without-bg .so-input--field:active{border-bottom-color:#2d4ef5}.so-input.without-bg .so-input--param{background:0 0}.so-input.with-params.without-bg .so-input--param{background:0 0!important}"]
                }] }
    ];
    InputComponent.propDecorators = {
        label: [{ type: Input }],
        value: [{ type: Input }],
        placeholder: [{ type: Input }],
        inputType: [{ type: Input }],
        inputConfig: [{ type: Input }],
        hasChanged: [{ type: Output }]
    };
    return InputComponent;
}());
export { InputComponent };
if (false) {
    /** @type {?} */
    InputComponent.prototype.label;
    /** @type {?} */
    InputComponent.prototype.value;
    /** @type {?} */
    InputComponent.prototype.placeholder;
    /** @type {?} */
    InputComponent.prototype.inputType;
    /** @type {?} */
    InputComponent.prototype.inputConfig;
    /** @type {?} */
    InputComponent.prototype.hasChanged;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5wdXQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vc29maXNhLXVpLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvaW5wdXQvaW5wdXQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsU0FBUyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBRXJFO0lBQUE7UUFPVSxVQUFLLEdBQVcsRUFBRSxDQUFDO1FBQ25CLFVBQUssR0FBVyxFQUFFLENBQUM7UUFDbkIsZ0JBQVcsR0FBVyxFQUFFLENBQUM7UUFDekIsY0FBUyxHQUFXLE1BQU0sQ0FBQztRQUMzQixnQkFBVyxHQUFRO1lBQzNCLElBQUksRUFBRSxFQUFFO1lBQ1IsUUFBUSxFQUFFLEtBQUs7WUFDZixVQUFVLEVBQUU7Z0JBQ1gsSUFBSSxFQUFFLEVBQUU7Z0JBQ1IsT0FBTyxFQUFFLEVBQUU7YUFDWDtZQUNELE1BQU0sRUFBRTtnQkFDUCxJQUFJLEVBQUUsRUFBRTtnQkFDUixLQUFLLEVBQUUsRUFBRTthQUNUO1NBQ0QsQ0FBQztRQUVRLGVBQVUsR0FBeUIsSUFBSSxZQUFZLEVBQUUsQ0FBQztJQUtqRSxDQUFDOzs7OztJQUhBLHFDQUFZOzs7O0lBQVosVUFBYSxDQUFDO1FBQ2IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUN0QyxDQUFDOztnQkE1QkQsU0FBUyxTQUFDO29CQUNWLFFBQVEsRUFBRSxVQUFVO29CQUNwQiw2a0ZBQXFDOztpQkFFckM7Ozt3QkFHQyxLQUFLO3dCQUNMLEtBQUs7OEJBQ0wsS0FBSzs0QkFDTCxLQUFLOzhCQUNMLEtBQUs7NkJBYUwsTUFBTTs7SUFLUixxQkFBQztDQUFBLEFBN0JELElBNkJDO1NBdkJZLGNBQWM7OztJQUMxQiwrQkFBNEI7O0lBQzVCLCtCQUE0Qjs7SUFDNUIscUNBQWtDOztJQUNsQyxtQ0FBb0M7O0lBQ3BDLHFDQVdFOztJQUVGLG9DQUFnRSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBFdmVudEVtaXR0ZXIsIElucHV0LCBPdXRwdXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcblx0c2VsZWN0b3I6ICdzby1pbnB1dCcsXG5cdHRlbXBsYXRlVXJsOiAnLi9pbnB1dC5jb21wb25lbnQuaHRtbCcsXG5cdHN0eWxlVXJsczogWycuL2lucHV0LmNvbXBvbmVudC5zY3NzJ11cbn0pXG5cbmV4cG9ydCBjbGFzcyBJbnB1dENvbXBvbmVudCB7XG5cdEBJbnB1dCgpIGxhYmVsOiBTdHJpbmcgPSAnJztcblx0QElucHV0KCkgdmFsdWU6IFN0cmluZyA9ICcnO1xuXHRASW5wdXQoKSBwbGFjZWhvbGRlcjogU3RyaW5nID0gJyc7XG5cdEBJbnB1dCgpIGlucHV0VHlwZTogU3RyaW5nID0gJ3RleHQnO1xuXHRASW5wdXQoKSBpbnB1dENvbmZpZzogYW55ID0ge1xuXHRcdHR5cGU6ICcnLFxuXHRcdGRpc2FibGVkOiBmYWxzZSxcblx0XHR2YWxpZGF0aW9uOiB7XG5cdFx0XHR0eXBlOiAnJyxcblx0XHRcdG1lc3NhZ2U6ICcnXG5cdFx0fSxcblx0XHRwYXJhbXM6IHtcblx0XHRcdGxlZnQ6ICcnLFxuXHRcdFx0cmlnaHQ6ICcnXG5cdFx0fVxuXHR9O1xuXHRcblx0QE91dHB1dCgpIGhhc0NoYW5nZWQ6IEV2ZW50RW1pdHRlcjxzdHJpbmc+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuXHRcblx0aGFuZGxlQ2hhbmdlKGUpIHtcblx0XHR0aGlzLmhhc0NoYW5nZWQuZW1pdChlLnRhcmdldC52YWx1ZSk7XG5cdH1cbn1cbiJdfQ==