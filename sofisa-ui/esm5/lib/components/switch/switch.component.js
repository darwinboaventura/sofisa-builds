/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, EventEmitter, Input, Output } from '@angular/core';
var SwitchComponent = /** @class */ (function () {
    function SwitchComponent() {
        this.isActive = false;
        this.hasChange = new EventEmitter();
    }
    /**
     * @return {?}
     */
    SwitchComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () { };
    /**
     * @return {?}
     */
    SwitchComponent.prototype.toggleSwitch = /**
     * @return {?}
     */
    function () {
        this.isActive = !this.isActive;
        this.hasChange.emit(this.isActive);
    };
    SwitchComponent.decorators = [
        { type: Component, args: [{
                    selector: 'so-switch',
                    template: "<div class=\"so-switch\" [ngClass]=\"{ 'is-active': isActive}\" (click)=\"toggleSwitch()\"></div>",
                    styles: [".so-shadow--1,.so-switch{box-shadow:0 2px 4px 0 rgba(0,0,0,.1)}.so-shadow--2{box-shadow:0 5px 8px 0 rgba(0,0,0,.1)}.so-shadow--3{box-shadow:0 9px 10px 0 rgba(0,0,0,.1)}.so-shadow--4{box-shadow:0 20px 30px 0 rgba(0,0,0,.1)}.so-shadow--5{box-shadow:0 30px 30px 0 rgba(0,0,0,.1)}.so-shadow--6{box-shadow:0 50px 50px 0 rgba(0,0,0,.1)}.so-switch{width:58px;height:28px;border-radius:22px;background:#fff;position:relative;cursor:pointer}.so-switch:after{width:20px;height:20px;border-radius:50%;content:'';display:table;transition:.3s ease-in-out;background:#e5e5e5;position:absolute;top:50%;left:4px;-webkit-transform:translateY(-50%);transform:translateY(-50%)}.so-switch.is-active:after{left:unset;right:4px;background:#0cb598}"]
                }] }
    ];
    /** @nocollapse */
    SwitchComponent.ctorParameters = function () { return []; };
    SwitchComponent.propDecorators = {
        isActive: [{ type: Input }],
        hasChange: [{ type: Output }]
    };
    return SwitchComponent;
}());
export { SwitchComponent };
if (false) {
    /** @type {?} */
    SwitchComponent.prototype.isActive;
    /** @type {?} */
    SwitchComponent.prototype.hasChange;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3dpdGNoLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3NvZmlzYS11aS8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL3N3aXRjaC9zd2l0Y2guY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsU0FBUyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQVUsTUFBTSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBRTdFO0lBVUM7UUFIUyxhQUFRLEdBQVksS0FBSyxDQUFDO1FBQ3pCLGNBQVMsR0FBMEIsSUFBSSxZQUFZLEVBQUUsQ0FBQztJQUVqRCxDQUFDOzs7O0lBRWhCLGtDQUFROzs7SUFBUixjQUFZLENBQUM7Ozs7SUFFYixzQ0FBWTs7O0lBQVo7UUFDQyxJQUFJLENBQUMsUUFBUSxHQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUUvQixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDcEMsQ0FBQzs7Z0JBbEJELFNBQVMsU0FBQztvQkFDVixRQUFRLEVBQUUsV0FBVztvQkFDckIsNkdBQXNDOztpQkFFdEM7Ozs7OzJCQUdDLEtBQUs7NEJBQ0wsTUFBTTs7SUFXUixzQkFBQztDQUFBLEFBbkJELElBbUJDO1NBYlksZUFBZTs7O0lBQzNCLG1DQUFtQzs7SUFDbkMsb0NBQWdFIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIEV2ZW50RW1pdHRlciwgSW5wdXQsIE9uSW5pdCwgT3V0cHV0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQENvbXBvbmVudCh7XG5cdHNlbGVjdG9yOiAnc28tc3dpdGNoJyxcblx0dGVtcGxhdGVVcmw6ICcuL3N3aXRjaC5jb21wb25lbnQuaHRtbCcsXG5cdHN0eWxlVXJsczogWycuL3N3aXRjaC5jb21wb25lbnQuc2NzcyddXG59KVxuXG5leHBvcnQgY2xhc3MgU3dpdGNoQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblx0QElucHV0KCkgaXNBY3RpdmU6IEJvb2xlYW4gPSBmYWxzZTtcblx0QE91dHB1dCgpIGhhc0NoYW5nZTogRXZlbnRFbWl0dGVyPEJvb2xlYW4+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuXHRcblx0Y29uc3RydWN0b3IoKSB7fVxuXHRcblx0bmdPbkluaXQoKSB7fVxuXHRcblx0dG9nZ2xlU3dpdGNoKCkge1xuXHRcdHRoaXMuaXNBY3RpdmUgPSAhdGhpcy5pc0FjdGl2ZTtcblx0XHRcblx0XHR0aGlzLmhhc0NoYW5nZS5lbWl0KHRoaXMuaXNBY3RpdmUpO1xuXHR9XG59XG4iXX0=