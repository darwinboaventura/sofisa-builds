/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { CheckboxComponent } from '../checkbox/checkbox.component';
var RadioboxComponent = /** @class */ (function (_super) {
    tslib_1.__extends(RadioboxComponent, _super);
    function RadioboxComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    RadioboxComponent.decorators = [
        { type: Component, args: [{
                    selector: 'so-radiobox',
                    template: "<fieldset class=\"so-radiobox clearfix\">\n\t<label class=\"so-radiobox--label\">{{label}}</label>\n\t\n\t<input type=\"radio\" class=\"so-radiobox--input\" [value]=\"value\" [name]=\"name\" [checked]=\"checked\" [disabled]=\"disabled\" (change)=\"handleChange($event)\">\n\t\n\t<span class=\"so-radiobox--checkmark\"></span>\n</fieldset>",
                    styles: [".so-checkbox,.so-radiobox{position:relative;cursor:pointer;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.so-checkbox--checkmark:after,.so-checkbox--input,.so-checkbox--label,.so-radiobox--checkmark:after,.so-radiobox--input,.so-radiobox--label{position:absolute}.so-checkbox--label,.so-radiobox--label{font-family:Roboto;font-weight:400;font-size:14px;color:#333;left:35px;position:absolute;top:50%;-webkit-transform:translateY(-50%);transform:translateY(-50%)}.so-checkbox--checkmark,.so-checkbox--input,.so-radiobox--checkmark,.so-radiobox--input{width:21px;height:21px}.so-checkbox--input,.so-radiobox--input{opacity:0;cursor:pointer;z-index:1000;border:0}.so-checkbox--checkmark,.so-radiobox--checkmark{top:0;left:0;display:table;position:relative;border:2px solid #e5e5e5;border-radius:4px}.so-checkbox--checkmark:after,.so-radiobox--checkmark:after{width:7px;height:12px;border:solid #fff;border-width:0 2px 2px 0;-webkit-transform:translate(-50%,-65%) rotate(45deg);transform:translate(-50%,-65%) rotate(45deg);top:50%;left:50%;content:'';display:block}input:checked~.so-checkbox--checkmark,input:checked~.so-radiobox--checkmark,input[checked]~.so-checkbox--checkmark,input[checked]~.so-radiobox--checkmark{border-color:#0cb598;background:#0cb598}input:disabled~.so-checkbox--checkmark,input:disabled~.so-radiobox--checkmark,input[disabled]~.so-checkbox--checkmark,input[disabled]~.so-radiobox--checkmark{border-color:#e5e5e5;background:#e5e5e5;cursor:default}input:disabled~.so-checkbox--checkmark:after,input:disabled~.so-radiobox--checkmark:after,input[disabled]~.so-checkbox--checkmark:after,input[disabled]~.so-radiobox--checkmark:after{cursor:default;display:none}.so-radiobox--checkmark{border-radius:50%;background:0 0}.so-radiobox--checkmark:after{display:none}input:checked~.so-radiobox--checkmark,input[checked]~.so-radiobox--checkmark{border-color:#0cb598;background:0 0}input:checked~.so-radiobox--checkmark:after,input[checked]~.so-radiobox--checkmark:after{width:9.55px;height:9.55px;display:block;border-radius:50%;-webkit-transform:translate(-50%,-50%);transform:translate(-50%,-50%);border:0;background:#0cb598}"]
                }] }
    ];
    return RadioboxComponent;
}(CheckboxComponent));
export { RadioboxComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmFkaW9ib3guY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vc29maXNhLXVpLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvcmFkaW9ib3gvcmFkaW9ib3guY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFDLFNBQVMsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUN4QyxPQUFPLEVBQUMsaUJBQWlCLEVBQUMsTUFBTSxnQ0FBZ0MsQ0FBQztBQUVqRTtJQU11Qyw2Q0FBaUI7SUFOeEQ7O0lBTTBELENBQUM7O2dCQU4xRCxTQUFTLFNBQUM7b0JBQ1YsUUFBUSxFQUFFLGFBQWE7b0JBQ3ZCLDhWQUF3Qzs7aUJBRXhDOztJQUV5RCx3QkFBQztDQUFBLEFBTjNELENBTXVDLGlCQUFpQixHQUFHO1NBQTlDLGlCQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50fSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7Q2hlY2tib3hDb21wb25lbnR9IGZyb20gJy4uL2NoZWNrYm94L2NoZWNrYm94LmNvbXBvbmVudCc7XG5cbkBDb21wb25lbnQoe1xuXHRzZWxlY3RvcjogJ3NvLXJhZGlvYm94Jyxcblx0dGVtcGxhdGVVcmw6ICcuL3JhZGlvYm94LmNvbXBvbmVudC5odG1sJyxcblx0c3R5bGVVcmxzOiBbJy4vcmFkaW9ib3guY29tcG9uZW50LnNjc3MnXVxufSlcblxuZXhwb3J0IGNsYXNzIFJhZGlvYm94Q29tcG9uZW50IGV4dGVuZHMgQ2hlY2tib3hDb21wb25lbnQge31cbiJdfQ==