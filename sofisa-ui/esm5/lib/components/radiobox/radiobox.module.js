/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RadioboxComponent } from './radiobox.component';
var RadioboxModule = /** @class */ (function () {
    function RadioboxModule() {
    }
    RadioboxModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [RadioboxComponent],
                    imports: [
                        CommonModule
                    ],
                    exports: [RadioboxComponent]
                },] }
    ];
    return RadioboxModule;
}());
export { RadioboxModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmFkaW9ib3gubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vc29maXNhLXVpLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvcmFkaW9ib3gvcmFkaW9ib3gubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsUUFBUSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ3ZDLE9BQU8sRUFBQyxZQUFZLEVBQUMsTUFBTSxpQkFBaUIsQ0FBQztBQUM3QyxPQUFPLEVBQUMsaUJBQWlCLEVBQUMsTUFBTSxzQkFBc0IsQ0FBQztBQUV2RDtJQUFBO0lBUTZCLENBQUM7O2dCQVI3QixRQUFRLFNBQUM7b0JBQ1QsWUFBWSxFQUFFLENBQUMsaUJBQWlCLENBQUM7b0JBQ2pDLE9BQU8sRUFBRTt3QkFDUixZQUFZO3FCQUNaO29CQUNELE9BQU8sRUFBRSxDQUFDLGlCQUFpQixDQUFDO2lCQUM1Qjs7SUFFNEIscUJBQUM7Q0FBQSxBQVI5QixJQVE4QjtTQUFqQixjQUFjIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtOZ01vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0NvbW1vbk1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7UmFkaW9ib3hDb21wb25lbnR9IGZyb20gJy4vcmFkaW9ib3guY29tcG9uZW50JztcblxuQE5nTW9kdWxlKHtcblx0ZGVjbGFyYXRpb25zOiBbUmFkaW9ib3hDb21wb25lbnRdLFxuXHRpbXBvcnRzOiBbXG5cdFx0Q29tbW9uTW9kdWxlXG5cdF0sXG5cdGV4cG9ydHM6IFtSYWRpb2JveENvbXBvbmVudF1cbn0pXG5cbmV4cG9ydCBjbGFzcyBSYWRpb2JveE1vZHVsZSB7fVxuIl19