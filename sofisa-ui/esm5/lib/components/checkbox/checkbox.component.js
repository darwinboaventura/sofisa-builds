/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, EventEmitter, Input, Output } from '@angular/core';
var CheckboxComponent = /** @class */ (function () {
    function CheckboxComponent() {
        this.label = '';
        this.name = '';
        this.value = '';
        this.checked = false;
        this.disabled = false;
        this.hasChange = new EventEmitter();
    }
    /**
     * @param {?} e
     * @return {?}
     */
    CheckboxComponent.prototype.handleChange = /**
     * @param {?} e
     * @return {?}
     */
    function (e) {
        this.hasChange.emit(e.target.value);
    };
    CheckboxComponent.decorators = [
        { type: Component, args: [{
                    selector: 'so-checkbox',
                    template: "<fieldset class=\"so-checkbox clearfix\">\n\t<label class=\"so-checkbox--label\">{{label}}</label>\n\t\n\t<input type=\"checkbox\" class=\"so-checkbox--input\" [value]=\"value\" [name]=\"name\" [checked]=\"checked\" [disabled]=\"disabled\" (change)=\"handleChange($event)\">\n\t\n\t<span class=\"so-checkbox--checkmark\"></span>\n</fieldset>",
                    styles: [".so-checkbox,.so-radiobox{position:relative;cursor:pointer;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.so-checkbox--checkmark:after,.so-checkbox--input,.so-checkbox--label,.so-radiobox--checkmark:after,.so-radiobox--input,.so-radiobox--label{position:absolute}.so-checkbox--label,.so-radiobox--label{font-family:Roboto;font-weight:400;font-size:14px;color:#333;left:35px;position:absolute;top:50%;-webkit-transform:translateY(-50%);transform:translateY(-50%)}.so-checkbox--checkmark,.so-checkbox--input,.so-radiobox--checkmark,.so-radiobox--input{width:21px;height:21px}.so-checkbox--input,.so-radiobox--input{opacity:0;cursor:pointer;z-index:1000;border:0}.so-checkbox--checkmark,.so-radiobox--checkmark{top:0;left:0;display:table;position:relative;border:2px solid #e5e5e5;border-radius:4px}.so-checkbox--checkmark:after,.so-radiobox--checkmark:after{width:7px;height:12px;border:solid #fff;border-width:0 2px 2px 0;-webkit-transform:translate(-50%,-65%) rotate(45deg);transform:translate(-50%,-65%) rotate(45deg);top:50%;left:50%;content:'';display:block}input:checked~.so-checkbox--checkmark,input:checked~.so-radiobox--checkmark,input[checked]~.so-checkbox--checkmark,input[checked]~.so-radiobox--checkmark{border-color:#0cb598;background:#0cb598}input:disabled~.so-checkbox--checkmark,input:disabled~.so-radiobox--checkmark,input[disabled]~.so-checkbox--checkmark,input[disabled]~.so-radiobox--checkmark{border-color:#e5e5e5;background:#e5e5e5;cursor:default}input:disabled~.so-checkbox--checkmark:after,input:disabled~.so-radiobox--checkmark:after,input[disabled]~.so-checkbox--checkmark:after,input[disabled]~.so-radiobox--checkmark:after{cursor:default;display:none}"]
                }] }
    ];
    CheckboxComponent.propDecorators = {
        label: [{ type: Input }],
        name: [{ type: Input }],
        value: [{ type: Input }],
        checked: [{ type: Input }],
        disabled: [{ type: Input }],
        hasChange: [{ type: Output }]
    };
    return CheckboxComponent;
}());
export { CheckboxComponent };
if (false) {
    /** @type {?} */
    CheckboxComponent.prototype.label;
    /** @type {?} */
    CheckboxComponent.prototype.name;
    /** @type {?} */
    CheckboxComponent.prototype.value;
    /** @type {?} */
    CheckboxComponent.prototype.checked;
    /** @type {?} */
    CheckboxComponent.prototype.disabled;
    /** @type {?} */
    CheckboxComponent.prototype.hasChange;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2hlY2tib3guY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vc29maXNhLXVpLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvY2hlY2tib3gvY2hlY2tib3guY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsU0FBUyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBRXJFO0lBQUE7UUFPVSxVQUFLLEdBQVcsRUFBRSxDQUFDO1FBQ25CLFNBQUksR0FBVyxFQUFFLENBQUM7UUFDbEIsVUFBSyxHQUFXLEVBQUUsQ0FBQztRQUNuQixZQUFPLEdBQVksS0FBSyxDQUFDO1FBQ3pCLGFBQVEsR0FBWSxLQUFLLENBQUM7UUFFekIsY0FBUyxHQUF5QixJQUFJLFlBQVksRUFBRSxDQUFDO0lBS2hFLENBQUM7Ozs7O0lBSEEsd0NBQVk7Ozs7SUFBWixVQUFhLENBQUM7UUFDYixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3JDLENBQUM7O2dCQWpCRCxTQUFTLFNBQUM7b0JBQ1YsUUFBUSxFQUFFLGFBQWE7b0JBQ3ZCLGlXQUF3Qzs7aUJBRXhDOzs7d0JBR0MsS0FBSzt1QkFDTCxLQUFLO3dCQUNMLEtBQUs7MEJBQ0wsS0FBSzsyQkFDTCxLQUFLOzRCQUVMLE1BQU07O0lBS1Isd0JBQUM7Q0FBQSxBQWxCRCxJQWtCQztTQVpZLGlCQUFpQjs7O0lBQzdCLGtDQUE0Qjs7SUFDNUIsaUNBQTJCOztJQUMzQixrQ0FBNEI7O0lBQzVCLG9DQUFrQzs7SUFDbEMscUNBQW1DOztJQUVuQyxzQ0FBK0QiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgRXZlbnRFbWl0dGVyLCBJbnB1dCwgT3V0cHV0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQENvbXBvbmVudCh7XG5cdHNlbGVjdG9yOiAnc28tY2hlY2tib3gnLFxuXHR0ZW1wbGF0ZVVybDogJy4vY2hlY2tib3guY29tcG9uZW50Lmh0bWwnLFxuXHRzdHlsZVVybHM6IFsnLi9jaGVja2JveC5jb21wb25lbnQuc2NzcyddXG59KVxuXG5leHBvcnQgY2xhc3MgQ2hlY2tib3hDb21wb25lbnQge1xuXHRASW5wdXQoKSBsYWJlbDogU3RyaW5nID0gJyc7XG5cdEBJbnB1dCgpIG5hbWU6IFN0cmluZyA9ICcnO1xuXHRASW5wdXQoKSB2YWx1ZTogU3RyaW5nID0gJyc7XG5cdEBJbnB1dCgpIGNoZWNrZWQ6IEJvb2xlYW4gPSBmYWxzZTtcblx0QElucHV0KCkgZGlzYWJsZWQ6IEJvb2xlYW4gPSBmYWxzZTtcblx0XG5cdEBPdXRwdXQoKSBoYXNDaGFuZ2U6IEV2ZW50RW1pdHRlcjxzdHJpbmc+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuXHRcblx0aGFuZGxlQ2hhbmdlKGUpIHtcblx0XHR0aGlzLmhhc0NoYW5nZS5lbWl0KGUudGFyZ2V0LnZhbHVlKTtcblx0fVxufVxuIl19