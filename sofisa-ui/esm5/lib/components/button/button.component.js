/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, EventEmitter, Input, Output } from '@angular/core';
var ButtonComponent = /** @class */ (function () {
    function ButtonComponent() {
        this.type = 'primary';
        this.hasClicked = new EventEmitter();
    }
    /**
     * @param {?} e
     * @return {?}
     */
    ButtonComponent.prototype.handleClick = /**
     * @param {?} e
     * @return {?}
     */
    function (e) {
        e.preventDefault();
        this.hasClicked.emit(true);
    };
    ButtonComponent.decorators = [
        { type: Component, args: [{
                    selector: 'so-button',
                    template: "<a href=\"#\" class=\"btn\" (click)=\"handleClick($event)\" [ngClass]=\"{\n\t'btn-primary': type === 'primary',\n\t'btn-info': type === 'info',\n\t'btn-success': type === 'success',\n\t'btn-error': type === 'error',\n\t'btn-alert': type === 'alert',\n\t'btn-outline-primary': type === 'outline-primary',\n\t'btn-outline-info': type === 'outline-info',\n\t'btn-outline-success': type === 'outline-success',\n\t'btn-outline-error': type === 'outline-error',\n\t'btn-outline-alert': type === 'outline-notification',\n\t'btn-outline-disabled': type === 'outline-disabled',\n\t'btn-primary-hover': type === 'primary-hover',\n\t'btn-info-hover': type === 'info-hover',\n\t'btn-success-hover': type === 'success-hover',\n\t'btn-error-hover': type === 'error-hover',\n\t'btn-alert-hover': type === 'notification-hover',\n\t'btn-primary-click': type === 'primary-click',\n\t'btn-info-click': type === 'info-click',\n\t'btn-success-click': type === 'success-click',\n\t'btn-error-click': type === 'error-click',\n\t'btn-alert-click': type === 'notification-click'\n}\">{{text}}</a>",
                    styles: [".btn{font-size:14px;color:#fff;text-decoration:none;text-align:center;padding:16px 30px;border-radius:50px;display:table;transition:.3s ease-in-out}.btn-disabled{background-color:#e5e5e5}.btn-primary{background-color:#0cb598}.btn-primary-hover,.btn-primary:hover{background-color:#0fe5c0}.btn-primary-click,.btn-primary:active{background-color:#0a957d}.btn-info{background-color:#2d4ef5}.btn-info-hover,.btn-info:hover{background-color:#798ef8}.btn-info-click,.btn-info:active{background-color:#1d329c}.btn-success{background-color:#4ce2a7}.btn-success-hover,.btn-success:hover{background-color:#9defcf}.btn-success-click,.btn-success:active{background-color:#31906b}.btn-error{background-color:#e24c4c}.btn-error-hover,.btn-error:hover{background-color:#e97c7c}.btn-error-click,.btn-error:active{background-color:#a53838}.btn-alert{background-color:#f1a153}.btn-alert-hover,.btn-alert:hover{background-color:#f7cba1}.btn-alert-click,.btn-alert:active{background-color:#b0763d}.btn-outline-primary{color:#0cb598;border:2px solid #0cb598}.btn-outline-info{color:#2d4ef5;border:2px solid #2d4ef5}.btn-outline-success{color:#4ce2a7;border:2px solid #4ce2a7}.btn-outline-error{color:#e24c4c;border:2px solid #e24c4c}.btn-outline-alert{color:#f1a153;border:2px solid #f1a153}.btn-outline-disabled{color:#dfdfdf;border:2px solid #dfdfdf}"]
                }] }
    ];
    ButtonComponent.propDecorators = {
        type: [{ type: Input }],
        text: [{ type: Input }],
        hasClicked: [{ type: Output }]
    };
    return ButtonComponent;
}());
export { ButtonComponent };
if (false) {
    /** @type {?} */
    ButtonComponent.prototype.type;
    /** @type {?} */
    ButtonComponent.prototype.text;
    /** @type {?} */
    ButtonComponent.prototype.hasClicked;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnV0dG9uLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3NvZmlzYS11aS8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2J1dHRvbi9idXR0b24uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsU0FBUyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBRXJFO0lBQUE7UUFPVSxTQUFJLEdBQVcsU0FBUyxDQUFDO1FBRXhCLGVBQVUsR0FBc0IsSUFBSSxZQUFZLEVBQUUsQ0FBQztJQU85RCxDQUFDOzs7OztJQUxBLHFDQUFXOzs7O0lBQVgsVUFBWSxDQUFDO1FBQ1osQ0FBQyxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBRW5CLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzVCLENBQUM7O2dCQWZELFNBQVMsU0FBQztvQkFDVixRQUFRLEVBQUUsV0FBVztvQkFDckIsMmpDQUFzQzs7aUJBRXRDOzs7dUJBR0MsS0FBSzt1QkFDTCxLQUFLOzZCQUNMLE1BQU07O0lBT1Isc0JBQUM7Q0FBQSxBQWhCRCxJQWdCQztTQVZZLGVBQWU7OztJQUMzQiwrQkFBa0M7O0lBQ2xDLCtCQUFzQjs7SUFDdEIscUNBQTZEIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIEV2ZW50RW1pdHRlciwgSW5wdXQsIE91dHB1dH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuXHRzZWxlY3RvcjogJ3NvLWJ1dHRvbicsXG5cdHRlbXBsYXRlVXJsOiAnLi9idXR0b24uY29tcG9uZW50Lmh0bWwnLFxuXHRzdHlsZVVybHM6IFsnLi9idXR0b24uY29tcG9uZW50LnNjc3MnXVxufSlcblxuZXhwb3J0IGNsYXNzIEJ1dHRvbkNvbXBvbmVudCB7XG5cdEBJbnB1dCgpIHR5cGU6IFN0cmluZyA9ICdwcmltYXJ5Jztcblx0QElucHV0KCkgdGV4dDogU3RyaW5nO1xuXHRAT3V0cHV0KCkgaGFzQ2xpY2tlZDogRXZlbnRFbWl0dGVyPGFueT4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG5cdFxuXHRoYW5kbGVDbGljayhlKSB7XG5cdFx0ZS5wcmV2ZW50RGVmYXVsdCgpO1xuXHRcdFxuXHRcdHRoaXMuaGFzQ2xpY2tlZC5lbWl0KHRydWUpO1xuXHR9XG59XG4iXX0=