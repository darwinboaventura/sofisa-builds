/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TextareaComponent } from './textarea.component';
var TextareaModule = /** @class */ (function () {
    function TextareaModule() {
    }
    TextareaModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [TextareaComponent],
                    imports: [
                        CommonModule
                    ],
                    exports: [TextareaComponent]
                },] }
    ];
    return TextareaModule;
}());
export { TextareaModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGV4dGFyZWEubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vc29maXNhLXVpLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvdGV4dGFyZWEvdGV4dGFyZWEubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsUUFBUSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ3ZDLE9BQU8sRUFBQyxZQUFZLEVBQUMsTUFBTSxpQkFBaUIsQ0FBQztBQUM3QyxPQUFPLEVBQUMsaUJBQWlCLEVBQUMsTUFBTSxzQkFBc0IsQ0FBQztBQUV2RDtJQUFBO0lBTzhCLENBQUM7O2dCQVA5QixRQUFRLFNBQUM7b0JBQ1IsWUFBWSxFQUFFLENBQUMsaUJBQWlCLENBQUM7b0JBQ2pDLE9BQU8sRUFBRTt3QkFDUCxZQUFZO3FCQUNiO29CQUNELE9BQU8sRUFBRSxDQUFDLGlCQUFpQixDQUFDO2lCQUM3Qjs7SUFDNkIscUJBQUM7Q0FBQSxBQVAvQixJQU8rQjtTQUFsQixjQUFjIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtOZ01vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0NvbW1vbk1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7VGV4dGFyZWFDb21wb25lbnR9IGZyb20gJy4vdGV4dGFyZWEuY29tcG9uZW50JztcblxuQE5nTW9kdWxlKHtcbiAgZGVjbGFyYXRpb25zOiBbVGV4dGFyZWFDb21wb25lbnRdLFxuICBpbXBvcnRzOiBbXG4gICAgQ29tbW9uTW9kdWxlXG4gIF0sXG4gIGV4cG9ydHM6IFtUZXh0YXJlYUNvbXBvbmVudF1cbn0pXG5leHBvcnQgY2xhc3MgVGV4dGFyZWFNb2R1bGUgeyB9XG4iXX0=