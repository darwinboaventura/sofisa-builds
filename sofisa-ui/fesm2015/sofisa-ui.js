import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, Output, NgModule } from '@angular/core';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class ButtonComponent {
    constructor() {
        this.type = 'primary';
        this.hasClicked = new EventEmitter();
    }
    /**
     * @param {?} e
     * @return {?}
     */
    handleClick(e) {
        e.preventDefault();
        this.hasClicked.emit(true);
    }
}
ButtonComponent.decorators = [
    { type: Component, args: [{
                selector: 'so-button',
                template: "<a href=\"#\" class=\"btn\" (click)=\"handleClick($event)\" [ngClass]=\"{\n\t'btn-primary': type === 'primary',\n\t'btn-info': type === 'info',\n\t'btn-success': type === 'success',\n\t'btn-error': type === 'error',\n\t'btn-alert': type === 'alert',\n\t'btn-outline-primary': type === 'outline-primary',\n\t'btn-outline-info': type === 'outline-info',\n\t'btn-outline-success': type === 'outline-success',\n\t'btn-outline-error': type === 'outline-error',\n\t'btn-outline-alert': type === 'outline-notification',\n\t'btn-outline-disabled': type === 'outline-disabled',\n\t'btn-primary-hover': type === 'primary-hover',\n\t'btn-info-hover': type === 'info-hover',\n\t'btn-success-hover': type === 'success-hover',\n\t'btn-error-hover': type === 'error-hover',\n\t'btn-alert-hover': type === 'notification-hover',\n\t'btn-primary-click': type === 'primary-click',\n\t'btn-info-click': type === 'info-click',\n\t'btn-success-click': type === 'success-click',\n\t'btn-error-click': type === 'error-click',\n\t'btn-alert-click': type === 'notification-click'\n}\">{{text}}</a>",
                styles: [".btn{font-size:14px;color:#fff;text-decoration:none;text-align:center;padding:16px 30px;border-radius:50px;display:table;transition:.3s ease-in-out}.btn-disabled{background-color:#e5e5e5}.btn-primary{background-color:#0cb598}.btn-primary-hover,.btn-primary:hover{background-color:#0fe5c0}.btn-primary-click,.btn-primary:active{background-color:#0a957d}.btn-info{background-color:#2d4ef5}.btn-info-hover,.btn-info:hover{background-color:#798ef8}.btn-info-click,.btn-info:active{background-color:#1d329c}.btn-success{background-color:#4ce2a7}.btn-success-hover,.btn-success:hover{background-color:#9defcf}.btn-success-click,.btn-success:active{background-color:#31906b}.btn-error{background-color:#e24c4c}.btn-error-hover,.btn-error:hover{background-color:#e97c7c}.btn-error-click,.btn-error:active{background-color:#a53838}.btn-alert{background-color:#f1a153}.btn-alert-hover,.btn-alert:hover{background-color:#f7cba1}.btn-alert-click,.btn-alert:active{background-color:#b0763d}.btn-outline-primary{color:#0cb598;border:2px solid #0cb598}.btn-outline-info{color:#2d4ef5;border:2px solid #2d4ef5}.btn-outline-success{color:#4ce2a7;border:2px solid #4ce2a7}.btn-outline-error{color:#e24c4c;border:2px solid #e24c4c}.btn-outline-alert{color:#f1a153;border:2px solid #f1a153}.btn-outline-disabled{color:#dfdfdf;border:2px solid #dfdfdf}"]
            }] }
];
ButtonComponent.propDecorators = {
    type: [{ type: Input }],
    text: [{ type: Input }],
    hasClicked: [{ type: Output }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class ButtonModule {
}
ButtonModule.decorators = [
    { type: NgModule, args: [{
                declarations: [ButtonComponent],
                imports: [
                    CommonModule
                ],
                exports: [ButtonComponent]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class InputComponent {
    constructor() {
        this.label = '';
        this.value = '';
        this.placeholder = '';
        this.inputType = 'text';
        this.inputConfig = {
            type: '',
            disabled: false,
            validation: {
                type: '',
                message: ''
            },
            params: {
                left: '',
                right: ''
            }
        };
        this.hasChanged = new EventEmitter();
    }
    /**
     * @param {?} e
     * @return {?}
     */
    handleChange(e) {
        this.hasChanged.emit(e.target.value);
    }
}
InputComponent.decorators = [
    { type: Component, args: [{
                selector: 'so-input',
                template: "<fieldset class=\"so-input clearfix\" [ngClass]=\"{\n\t'with-params': inputConfig.validation && inputConfig.validation.type && inputConfig.validation.type.includes('with-params'),\n\t'without-bg': inputConfig.validation && inputConfig.validation.type && inputConfig.validation.type.includes('without-bg'),\n\t'error-input': inputConfig.validation && inputConfig.validation.type && inputConfig.validation.type.includes('error'),\n\t'warning-input': inputConfig.validation && inputConfig.validation.type && inputConfig.validation.type.includes('warning'),\n\t'success-input': inputConfig.validation && inputConfig.validation.type && inputConfig.validation.type.includes('success')\n}\">\n\t<label class=\"so-input--label\">{{label}}</label>\n\t\n\t<input [type]=\"inputType\" [value]=\"value\" class=\"so-input--field\" [placeholder]=\"placeholder\" [disabled]=\"inputConfig.disabled\" (change)=\"handleChange($event)\" [ngClass]=\"{\n\t'is-active': inputConfig.validation && inputConfig.validation.type && inputConfig.validation.type.includes('is-active')\n}\"/>\n\t\n\t<div *ngIf=\"(inputConfig && inputConfig.params) && ( inputConfig.params.left || inputConfig.params.right)\">\n\t\t<div class=\"so-input--param left\" *ngIf=\"inputConfig && inputConfig.params && inputConfig.params.left\">\n\t\t\t<span class=\"so-input--param--center\">{{inputConfig.params.left}}</span>\n\t\t</div>\n\t\t\n\t\t<div class=\"so-input--param right\" *ngIf=\"inputConfig && inputConfig.params && inputConfig.params.right\">\n\t\t\t<span class=\"so-input--param--center\">{{inputConfig.params.right}}</span>\n\t\t</div>\n\t</div>\n\t\n\t<div *ngIf=\"inputConfig.validation && inputConfig.validation.message\">\n\t\t<div class=\"so-input--icon right\" *ngIf=\"inputConfig.validation.type && inputConfig.validation.type === 'error'\">\n\t\t\t<span class=\"so-input--param--center\">\n\t\t\t\t<img src=\"assets/images/icon-close.png\" alt=\"Fechar\">\n\t\t\t</span>\n\t\t</div>\n\t\t\n\t\t<div class=\"so-input--icon right\" *ngIf=\"inputConfig.validation.type && inputConfig.validation.type === 'warning'\">\n\t\t\t<span class=\"so-input--param--center\">\n\t\t\t\t<img src=\"assets/images/icon-alert.png\" alt=\"Aten\u00E7\u00E3o\">\n\t\t\t</span>\n\t\t</div>\n\t\t\n\t\t<div class=\"so-input--icon right\" *ngIf=\"inputConfig.validation.type && inputConfig.validation.type === 'success'\">\n\t\t\t<span class=\"so-input--param--center\">\n\t\t\t\t<img src=\"assets/images/icon-success-green.png\" alt=\"OK\">\n\t\t\t</span>\n\t\t</div>\n\t\t\n\t\t<div class=\"so-input--message\">{{inputConfig.validation.message}}</div>\n\t</div>\n</fieldset>",
                styles: [".so-input--field,.so-input--textarea,.so-shadow--1{box-shadow:0 2px 4px 0 rgba(0,0,0,.1)}.so-shadow--2{box-shadow:0 5px 8px 0 rgba(0,0,0,.1)}.so-shadow--3{box-shadow:0 9px 10px 0 rgba(0,0,0,.1)}.so-shadow--4{box-shadow:0 20px 30px 0 rgba(0,0,0,.1)}.so-shadow--5{box-shadow:0 30px 30px 0 rgba(0,0,0,.1)}.so-shadow--6{box-shadow:0 50px 50px 0 rgba(0,0,0,.1)}.so-input{width:100%;position:relative}.so-input--field,.so-input--label{font-size:14px;color:#333}.so-input--label{display:block;margin-bottom:12px}.so-input--field,.so-input--textarea{width:100%;border:0;padding:16px;background:#fff}.so-input--field[disabled],.so-input--textarea[disabled]{color:#999;background:#ebebeb}.so-input--textarea{height:250px}.so-input--icon,.so-input--param{width:50px;height:53px;position:absolute;bottom:0;background:#e5e5e5;font-size:14px;color:#333}.so-input--icon.left,.so-input--param.left{left:0}.so-input--icon.right,.so-input--param.right{right:0}.so-input--icon--center,.so-input--param--center{position:absolute;top:50%;left:50%;-webkit-transform:translate(-50%,-50%);transform:translate(-50%,-50%)}.so-input--icon{background:0 0}.so-input--message{font-size:14px;position:absolute;bottom:-35px;left:0}.so-input.with-params .so-input--field{padding-left:70px;padding-right:70px}.so-input.error-input,.so-input.success-input,.so-input.warning-input{margin-bottom:65.3px}.so-input.without-bg .so-input--field[disabled]{border-bottom-color:#ebebeb;background:0 0}.so-input.error-input.without-bg .so-input--field{border-bottom:2px solid #e24c4c}.so-input.error-input.with-params .so-input--param{color:#ff6b6b;border-bottom:2px solid #e24c4c;background:#f9d3cf}.so-input.error-input .so-input--field{border-bottom:2px solid #e24c4c}.so-input.error-input .so-input--message{color:#e24c4c}.so-input.success-input.without-bg .so-input--field{border-bottom:2px solid #4ce2a7}.so-input.success-input.with-params .so-input--param{color:#4ce2a7;border-bottom:2px solid #4ce2a7;background:#cbf3dc}.so-input.success-input .so-input--field{border-bottom:2px solid #4ce2a7}.so-input.success-input .so-input--message{color:#4ce2a7}.so-input.warning-input.without-bg .so-input--field{border-bottom:2px solid #f1a153}.so-input.warning-input.with-params .so-input--param{color:#ffaa31;border-bottom:2px solid #f1a153;background:#fce7c4}.so-input.warning-input .so-input--field{border-bottom:2px solid #f1a153}.so-input.warning-input .so-input--message{color:#f1a153}.so-input.without-bg .so-input--field{color:#666;border-bottom:2px solid #666;box-shadow:none;background:0 0}.so-input.without-bg .so-input--field.is-active,.so-input.without-bg .so-input--field:active{border-bottom-color:#2d4ef5}.so-input.without-bg .so-input--param{background:0 0}.so-input.with-params.without-bg .so-input--param{background:0 0!important}"]
            }] }
];
InputComponent.propDecorators = {
    label: [{ type: Input }],
    value: [{ type: Input }],
    placeholder: [{ type: Input }],
    inputType: [{ type: Input }],
    inputConfig: [{ type: Input }],
    hasChanged: [{ type: Output }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class InputModule {
}
InputModule.decorators = [
    { type: NgModule, args: [{
                declarations: [InputComponent],
                imports: [
                    CommonModule,
                    ReactiveFormsModule
                ],
                exports: [InputComponent]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class TextareaComponent {
    constructor() {
        this.label = '';
        this.value = '';
        this.hasChanged = new EventEmitter();
    }
    /**
     * @param {?} e
     * @return {?}
     */
    handleChange(e) {
        this.hasChanged.emit(e.target.value);
    }
}
TextareaComponent.decorators = [
    { type: Component, args: [{
                selector: 'so-textarea',
                template: "<fieldset class=\"so-input clearfix\">\n\t<label class=\"so-input--label\">{{label}}</label>\n\t\n\t<textarea class=\"so-input--textarea\" (change)=\"handleChange($event)\">{{value}}</textarea>\n</fieldset>",
                styles: [".so-input--field,.so-input--textarea,.so-shadow--1{box-shadow:0 2px 4px 0 rgba(0,0,0,.1)}.so-shadow--2{box-shadow:0 5px 8px 0 rgba(0,0,0,.1)}.so-shadow--3{box-shadow:0 9px 10px 0 rgba(0,0,0,.1)}.so-shadow--4{box-shadow:0 20px 30px 0 rgba(0,0,0,.1)}.so-shadow--5{box-shadow:0 30px 30px 0 rgba(0,0,0,.1)}.so-shadow--6{box-shadow:0 50px 50px 0 rgba(0,0,0,.1)}.so-input{width:100%;position:relative}.so-input--field,.so-input--label{font-size:14px;color:#333}.so-input--label{display:block;margin-bottom:12px}.so-input--field,.so-input--textarea{width:100%;border:0;padding:16px;background:#fff}.so-input--field[disabled],.so-input--textarea[disabled]{color:#999;background:#ebebeb}.so-input--textarea{height:250px}.so-input--icon,.so-input--param{width:50px;height:53px;position:absolute;bottom:0;background:#e5e5e5;font-size:14px;color:#333}.so-input--icon.left,.so-input--param.left{left:0}.so-input--icon.right,.so-input--param.right{right:0}.so-input--icon--center,.so-input--param--center{position:absolute;top:50%;left:50%;-webkit-transform:translate(-50%,-50%);transform:translate(-50%,-50%)}.so-input--icon{background:0 0}.so-input--message{font-size:14px;position:absolute;bottom:-35px;left:0}.so-input.with-params .so-input--field{padding-left:70px;padding-right:70px}.so-input.error-input,.so-input.success-input,.so-input.warning-input{margin-bottom:65.3px}.so-input.without-bg .so-input--field[disabled]{border-bottom-color:#ebebeb;background:0 0}.so-input.error-input.without-bg .so-input--field{border-bottom:2px solid #e24c4c}.so-input.error-input.with-params .so-input--param{color:#ff6b6b;border-bottom:2px solid #e24c4c;background:#f9d3cf}.so-input.error-input .so-input--field{border-bottom:2px solid #e24c4c}.so-input.error-input .so-input--message{color:#e24c4c}.so-input.success-input.without-bg .so-input--field{border-bottom:2px solid #4ce2a7}.so-input.success-input.with-params .so-input--param{color:#4ce2a7;border-bottom:2px solid #4ce2a7;background:#cbf3dc}.so-input.success-input .so-input--field{border-bottom:2px solid #4ce2a7}.so-input.success-input .so-input--message{color:#4ce2a7}.so-input.warning-input.without-bg .so-input--field{border-bottom:2px solid #f1a153}.so-input.warning-input.with-params .so-input--param{color:#ffaa31;border-bottom:2px solid #f1a153;background:#fce7c4}.so-input.warning-input .so-input--field{border-bottom:2px solid #f1a153}.so-input.warning-input .so-input--message{color:#f1a153}.so-input.without-bg .so-input--field{color:#666;border-bottom:2px solid #666;box-shadow:none;background:0 0}.so-input.without-bg .so-input--field.is-active,.so-input.without-bg .so-input--field:active{border-bottom-color:#2d4ef5}.so-input.without-bg .so-input--param{background:0 0}.so-input.with-params.without-bg .so-input--param{background:0 0!important}"]
            }] }
];
TextareaComponent.propDecorators = {
    label: [{ type: Input }],
    value: [{ type: Input }],
    hasChanged: [{ type: Output }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class TextareaModule {
}
TextareaModule.decorators = [
    { type: NgModule, args: [{
                declarations: [TextareaComponent],
                imports: [
                    CommonModule
                ],
                exports: [TextareaComponent]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class CheckboxComponent {
    constructor() {
        this.label = '';
        this.name = '';
        this.value = '';
        this.checked = false;
        this.disabled = false;
        this.hasChange = new EventEmitter();
    }
    /**
     * @param {?} e
     * @return {?}
     */
    handleChange(e) {
        this.hasChange.emit(e.target.value);
    }
}
CheckboxComponent.decorators = [
    { type: Component, args: [{
                selector: 'so-checkbox',
                template: "<fieldset class=\"so-checkbox clearfix\">\n\t<label class=\"so-checkbox--label\">{{label}}</label>\n\t\n\t<input type=\"checkbox\" class=\"so-checkbox--input\" [value]=\"value\" [name]=\"name\" [checked]=\"checked\" [disabled]=\"disabled\" (change)=\"handleChange($event)\">\n\t\n\t<span class=\"so-checkbox--checkmark\"></span>\n</fieldset>",
                styles: [".so-checkbox,.so-radiobox{position:relative;cursor:pointer;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.so-checkbox--checkmark:after,.so-checkbox--input,.so-checkbox--label,.so-radiobox--checkmark:after,.so-radiobox--input,.so-radiobox--label{position:absolute}.so-checkbox--label,.so-radiobox--label{font-family:Roboto;font-weight:400;font-size:14px;color:#333;left:35px;position:absolute;top:50%;-webkit-transform:translateY(-50%);transform:translateY(-50%)}.so-checkbox--checkmark,.so-checkbox--input,.so-radiobox--checkmark,.so-radiobox--input{width:21px;height:21px}.so-checkbox--input,.so-radiobox--input{opacity:0;cursor:pointer;z-index:1000;border:0}.so-checkbox--checkmark,.so-radiobox--checkmark{top:0;left:0;display:table;position:relative;border:2px solid #e5e5e5;border-radius:4px}.so-checkbox--checkmark:after,.so-radiobox--checkmark:after{width:7px;height:12px;border:solid #fff;border-width:0 2px 2px 0;-webkit-transform:translate(-50%,-65%) rotate(45deg);transform:translate(-50%,-65%) rotate(45deg);top:50%;left:50%;content:'';display:block}input:checked~.so-checkbox--checkmark,input:checked~.so-radiobox--checkmark,input[checked]~.so-checkbox--checkmark,input[checked]~.so-radiobox--checkmark{border-color:#0cb598;background:#0cb598}input:disabled~.so-checkbox--checkmark,input:disabled~.so-radiobox--checkmark,input[disabled]~.so-checkbox--checkmark,input[disabled]~.so-radiobox--checkmark{border-color:#e5e5e5;background:#e5e5e5;cursor:default}input:disabled~.so-checkbox--checkmark:after,input:disabled~.so-radiobox--checkmark:after,input[disabled]~.so-checkbox--checkmark:after,input[disabled]~.so-radiobox--checkmark:after{cursor:default;display:none}"]
            }] }
];
CheckboxComponent.propDecorators = {
    label: [{ type: Input }],
    name: [{ type: Input }],
    value: [{ type: Input }],
    checked: [{ type: Input }],
    disabled: [{ type: Input }],
    hasChange: [{ type: Output }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class CheckboxModule {
}
CheckboxModule.decorators = [
    { type: NgModule, args: [{
                declarations: [CheckboxComponent],
                imports: [
                    CommonModule
                ],
                exports: [CheckboxComponent]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class RadioboxComponent extends CheckboxComponent {
}
RadioboxComponent.decorators = [
    { type: Component, args: [{
                selector: 'so-radiobox',
                template: "<fieldset class=\"so-radiobox clearfix\">\n\t<label class=\"so-radiobox--label\">{{label}}</label>\n\t\n\t<input type=\"radio\" class=\"so-radiobox--input\" [value]=\"value\" [name]=\"name\" [checked]=\"checked\" [disabled]=\"disabled\" (change)=\"handleChange($event)\">\n\t\n\t<span class=\"so-radiobox--checkmark\"></span>\n</fieldset>",
                styles: [".so-checkbox,.so-radiobox{position:relative;cursor:pointer;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.so-checkbox--checkmark:after,.so-checkbox--input,.so-checkbox--label,.so-radiobox--checkmark:after,.so-radiobox--input,.so-radiobox--label{position:absolute}.so-checkbox--label,.so-radiobox--label{font-family:Roboto;font-weight:400;font-size:14px;color:#333;left:35px;position:absolute;top:50%;-webkit-transform:translateY(-50%);transform:translateY(-50%)}.so-checkbox--checkmark,.so-checkbox--input,.so-radiobox--checkmark,.so-radiobox--input{width:21px;height:21px}.so-checkbox--input,.so-radiobox--input{opacity:0;cursor:pointer;z-index:1000;border:0}.so-checkbox--checkmark,.so-radiobox--checkmark{top:0;left:0;display:table;position:relative;border:2px solid #e5e5e5;border-radius:4px}.so-checkbox--checkmark:after,.so-radiobox--checkmark:after{width:7px;height:12px;border:solid #fff;border-width:0 2px 2px 0;-webkit-transform:translate(-50%,-65%) rotate(45deg);transform:translate(-50%,-65%) rotate(45deg);top:50%;left:50%;content:'';display:block}input:checked~.so-checkbox--checkmark,input:checked~.so-radiobox--checkmark,input[checked]~.so-checkbox--checkmark,input[checked]~.so-radiobox--checkmark{border-color:#0cb598;background:#0cb598}input:disabled~.so-checkbox--checkmark,input:disabled~.so-radiobox--checkmark,input[disabled]~.so-checkbox--checkmark,input[disabled]~.so-radiobox--checkmark{border-color:#e5e5e5;background:#e5e5e5;cursor:default}input:disabled~.so-checkbox--checkmark:after,input:disabled~.so-radiobox--checkmark:after,input[disabled]~.so-checkbox--checkmark:after,input[disabled]~.so-radiobox--checkmark:after{cursor:default;display:none}.so-radiobox--checkmark{border-radius:50%;background:0 0}.so-radiobox--checkmark:after{display:none}input:checked~.so-radiobox--checkmark,input[checked]~.so-radiobox--checkmark{border-color:#0cb598;background:0 0}input:checked~.so-radiobox--checkmark:after,input[checked]~.so-radiobox--checkmark:after{width:9.55px;height:9.55px;display:block;border-radius:50%;-webkit-transform:translate(-50%,-50%);transform:translate(-50%,-50%);border:0;background:#0cb598}"]
            }] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class RadioboxModule {
}
RadioboxModule.decorators = [
    { type: NgModule, args: [{
                declarations: [RadioboxComponent],
                imports: [
                    CommonModule
                ],
                exports: [RadioboxComponent]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class SwitchComponent {
    constructor() {
        this.isActive = false;
        this.hasChange = new EventEmitter();
    }
    /**
     * @return {?}
     */
    ngOnInit() { }
    /**
     * @return {?}
     */
    toggleSwitch() {
        this.isActive = !this.isActive;
        this.hasChange.emit(this.isActive);
    }
}
SwitchComponent.decorators = [
    { type: Component, args: [{
                selector: 'so-switch',
                template: "<div class=\"so-switch\" [ngClass]=\"{ 'is-active': isActive}\" (click)=\"toggleSwitch()\"></div>",
                styles: [".so-shadow--1,.so-switch{box-shadow:0 2px 4px 0 rgba(0,0,0,.1)}.so-shadow--2{box-shadow:0 5px 8px 0 rgba(0,0,0,.1)}.so-shadow--3{box-shadow:0 9px 10px 0 rgba(0,0,0,.1)}.so-shadow--4{box-shadow:0 20px 30px 0 rgba(0,0,0,.1)}.so-shadow--5{box-shadow:0 30px 30px 0 rgba(0,0,0,.1)}.so-shadow--6{box-shadow:0 50px 50px 0 rgba(0,0,0,.1)}.so-switch{width:58px;height:28px;border-radius:22px;background:#fff;position:relative;cursor:pointer}.so-switch:after{width:20px;height:20px;border-radius:50%;content:'';display:table;transition:.3s ease-in-out;background:#e5e5e5;position:absolute;top:50%;left:4px;-webkit-transform:translateY(-50%);transform:translateY(-50%)}.so-switch.is-active:after{left:unset;right:4px;background:#0cb598}"]
            }] }
];
/** @nocollapse */
SwitchComponent.ctorParameters = () => [];
SwitchComponent.propDecorators = {
    isActive: [{ type: Input }],
    hasChange: [{ type: Output }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class SwitchModule {
}
SwitchModule.decorators = [
    { type: NgModule, args: [{
                declarations: [SwitchComponent],
                imports: [
                    CommonModule
                ],
                exports: [SwitchComponent]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class TabsComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() { }
}
TabsComponent.decorators = [
    { type: Component, args: [{
                selector: 'so-tabs',
                template: "<div class=\"so-tabs clearfix\">\n\t<div class=\"so-tabs--head clearfix\">\n\t\t<div class=\"so-tabs--head--item is-active clearfix\">Active</div>\n\t\t<div class=\"so-tabs--head--item clearfix\">Hover</div>\n\t\t<div class=\"so-tabs--head--item clearfix\">Default</div>\n\t</div>\n\n\t<div class=\"so-tabs--main clearfix\">\n\t\t<h3>This feels like a diary</h3>\n\n\t\t<p>With supporting text below as a natural lead-in to additional content.</p>\n\t</div>\n</div>",
                styles: [".so-shadow--1,.so-tabs--main{box-shadow:0 2px 4px 0 rgba(0,0,0,.1)}.so-shadow--2{box-shadow:0 5px 8px 0 rgba(0,0,0,.1)}.so-shadow--3{box-shadow:0 9px 10px 0 rgba(0,0,0,.1)}.so-shadow--4{box-shadow:0 20px 30px 0 rgba(0,0,0,.1)}.so-shadow--5{box-shadow:0 30px 30px 0 rgba(0,0,0,.1)}.so-shadow--6{box-shadow:0 50px 50px 0 rgba(0,0,0,.1)}.so-tabs{width:100%}.so-tabs--head{background-color:#fafafa}.so-tabs--head--item{font-family:Nunito;font-weight:700;font-size:14px;color:#666;text-transform:uppercase;float:left;padding:16px 20px;cursor:pointer}.so-tabs--head--item.is-active{color:#022047;box-shadow:2px 0 4px 0 rgba(0,0,0,.1);border-radius:5px 5px 0 0;background-color:#fff}.so-tabs--head--item:hover{color:#2d4ef5}.so-tabs--main{position:relative;padding:26px 20px;border-radius:3px 3px 0;background-color:#fff}"]
            }] }
];
/** @nocollapse */
TabsComponent.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class TabsModule {
}
TabsModule.decorators = [
    { type: NgModule, args: [{
                declarations: [TabsComponent],
                imports: [
                    CommonModule
                ],
                exports: [TabsComponent]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { ButtonModule, InputModule, TextareaModule, CheckboxModule, RadioboxModule, SwitchModule, TabsModule, ButtonComponent as ɵa, CheckboxComponent as ɵd, InputComponent as ɵb, RadioboxComponent as ɵe, SwitchComponent as ɵf, TabsComponent as ɵg, TextareaComponent as ɵc };

//# sourceMappingURL=sofisa-ui.js.map