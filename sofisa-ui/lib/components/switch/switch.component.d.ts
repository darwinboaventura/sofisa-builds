import { EventEmitter, OnInit } from '@angular/core';
export declare class SwitchComponent implements OnInit {
    isActive: Boolean;
    hasChange: EventEmitter<Boolean>;
    constructor();
    ngOnInit(): void;
    toggleSwitch(): void;
}
