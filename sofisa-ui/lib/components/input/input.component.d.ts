import { EventEmitter } from '@angular/core';
export declare class InputComponent {
    label: String;
    value: String;
    placeholder: String;
    inputType: String;
    inputConfig: any;
    hasChanged: EventEmitter<string>;
    handleChange(e: any): void;
}
