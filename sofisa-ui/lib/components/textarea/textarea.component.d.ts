import { EventEmitter } from '@angular/core';
export declare class TextareaComponent {
    label: String;
    value: String;
    hasChanged: EventEmitter<any>;
    handleChange(e: any): void;
}
