import { EventEmitter } from '@angular/core';
export declare class CheckboxComponent {
    label: String;
    name: String;
    value: String;
    checked: Boolean;
    disabled: Boolean;
    hasChange: EventEmitter<string>;
    handleChange(e: any): void;
}
