import { EventEmitter } from '@angular/core';
export declare class ButtonComponent {
    type: String;
    text: String;
    hasClicked: EventEmitter<any>;
    handleClick(e: any): void;
}
