import { HttpClient } from '@angular/common/http';
export declare class BpmService {
    http: HttpClient;
    apiUri: String;
    constructor(http: HttpClient);
    claimTaskById(id: any, userId: any): import("rxjs/internal/Observable").Observable<Object>;
    completeTaskById(id: any, values: any): import("rxjs/internal/Observable").Observable<Object>;
    getTaskById(id: any): import("rxjs/internal/Observable").Observable<Object>;
    getTaskByIdVariables(id: any): import("rxjs/internal/Observable").Observable<Object>;
    getTasks(): import("rxjs/internal/Observable").Observable<Object>;
    getTaskDiagram(id: any): import("rxjs/internal/Observable").Observable<Object>;
    startABpm(value: any): import("rxjs/internal/Observable").Observable<Object>;
}
