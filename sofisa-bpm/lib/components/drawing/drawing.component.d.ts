import { BpmService } from '../../services/bpm/bpm.service';
import { OnInit } from '@angular/core';
export declare class DrawingComponent implements OnInit {
    bpmService: BpmService;
    width: any;
    height: any;
    diagram: any;
    viewer: any;
    constructor(bpmService: BpmService);
    ngOnInit(): void;
}
