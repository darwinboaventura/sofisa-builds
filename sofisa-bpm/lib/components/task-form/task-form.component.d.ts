import { Store } from '@ngrx/store';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { LoaderService } from 'sofisa-loader';
import { NotificationService } from 'sofisa-notification';
import { EventEmitter, OnChanges, OnInit } from '@angular/core';
export declare class TaskFormComponent implements OnInit, OnChanges {
    store: Store<{
        pages: any;
    }>;
    loaderService: LoaderService;
    notificationService: NotificationService;
    handleFieldChange: any;
    store$: Observable<any>;
    props: {
        user: any;
        task: any;
        page: string;
        form: {
            formGroup: FormGroup;
            validations: any;
            actions: any;
        };
    };
    submission: EventEmitter<FormGroup>;
    claim: EventEmitter<{
        taskId: any;
        userId: any;
    }>;
    constructor(store: Store<{
        pages: any;
    }>, loaderService: LoaderService, notificationService: NotificationService);
    ngOnInit(): void;
    ngOnChanges(): void;
    handleClaim(): void;
    handleSubmission(): void;
}
