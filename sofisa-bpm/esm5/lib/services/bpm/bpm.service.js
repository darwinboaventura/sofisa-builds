/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
var BpmService = /** @class */ (function () {
    function BpmService(http) {
        this.http = http;
        // TODO: Mover para arquivos de configurações
        this.apiUri = '//sofisa-lab-gateway.eastus2.cloudapp.azure.com';
    }
    /**
     * @param {?} id
     * @param {?} userId
     * @return {?}
     */
    BpmService.prototype.claimTaskById = /**
     * @param {?} id
     * @param {?} userId
     * @return {?}
     */
    function (id, userId) {
        return this.http.post(this.apiUri + "/api/bpm-api/bpm-pj-digital/task/" + id + "/claim", {
            userId: userId
        });
    };
    /**
     * @param {?} id
     * @param {?} values
     * @return {?}
     */
    BpmService.prototype.completeTaskById = /**
     * @param {?} id
     * @param {?} values
     * @return {?}
     */
    function (id, values) {
        return this.http.post(this.apiUri + "/api/bpm-api/bpm-pj-digital/task/" + id + "/complete", values);
    };
    /**
     * @param {?} id
     * @return {?}
     */
    BpmService.prototype.getTaskById = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
        return this.http.get(this.apiUri + "/api/bpm-api/bpm-pj-digital/task/" + id);
    };
    /**
     * @param {?} id
     * @return {?}
     */
    BpmService.prototype.getTaskByIdVariables = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
        return this.http.get(this.apiUri + "/api/bpm-api/bpm-pj-digital/task/" + id + "/variables");
    };
    /**
     * @return {?}
     */
    BpmService.prototype.getTasks = /**
     * @return {?}
     */
    function () {
        return this.http.get('${this.apiUri}/api/bpm-api/bpm-pj-digital/task');
    };
    /**
     * @param {?} id
     * @return {?}
     */
    BpmService.prototype.getTaskDiagram = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
        return this.http.get(this.apiUri + "/api/bpm-api/bpm-pj-digital/engine/default/process-definition/" + id + "/xml");
    };
    /**
     * @param {?} value
     * @return {?}
     */
    BpmService.prototype.startABpm = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        return this.http.post('${this.apiUri}/api/bpm-api/bpm-pj-digital/process-definition/key/' +
            'Workflow_PJ_Digital/start', value);
    };
    BpmService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    BpmService.ctorParameters = function () { return [
        { type: HttpClient }
    ]; };
    /** @nocollapse */ BpmService.ngInjectableDef = i0.defineInjectable({ factory: function BpmService_Factory() { return new BpmService(i0.inject(i1.HttpClient)); }, token: BpmService, providedIn: "root" });
    return BpmService;
}());
export { BpmService };
if (false) {
    /** @type {?} */
    BpmService.prototype.apiUri;
    /** @type {?} */
    BpmService.prototype.http;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnBtLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9zb2Zpc2EtYnBtLyIsInNvdXJjZXMiOlsibGliL3NlcnZpY2VzL2JwbS9icG0uc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFDLFVBQVUsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUMsVUFBVSxFQUFDLE1BQU0sc0JBQXNCLENBQUM7OztBQUVoRDtJQVFDLG9CQUFtQixJQUFnQjtRQUFoQixTQUFJLEdBQUosSUFBSSxDQUFZOztRQUZuQyxXQUFNLEdBQVcsaURBQWlELENBQUM7SUFFN0IsQ0FBQzs7Ozs7O0lBRXZDLGtDQUFhOzs7OztJQUFiLFVBQWMsRUFBTyxFQUFFLE1BQVc7UUFDakMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBSSxJQUFJLENBQUMsTUFBTSx5Q0FBb0MsRUFBRSxXQUFRLEVBQUU7WUFDbkYsTUFBTSxFQUFFLE1BQU07U0FDZCxDQUFDLENBQUM7SUFDSixDQUFDOzs7Ozs7SUFFRCxxQ0FBZ0I7Ozs7O0lBQWhCLFVBQWlCLEVBQU8sRUFBRSxNQUFXO1FBQ3BDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUksSUFBSSxDQUFDLE1BQU0seUNBQW9DLEVBQUUsY0FBVyxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ2hHLENBQUM7Ozs7O0lBRUQsZ0NBQVc7Ozs7SUFBWCxVQUFZLEVBQU87UUFDbEIsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBSSxJQUFJLENBQUMsTUFBTSx5Q0FBb0MsRUFBSSxDQUFDLENBQUM7SUFDOUUsQ0FBQzs7Ozs7SUFFRCx5Q0FBb0I7Ozs7SUFBcEIsVUFBcUIsRUFBTztRQUMzQixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFJLElBQUksQ0FBQyxNQUFNLHlDQUFvQyxFQUFFLGVBQVksQ0FBQyxDQUFDO0lBQ3hGLENBQUM7Ozs7SUFFRCw2QkFBUTs7O0lBQVI7UUFDQyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLGdEQUFnRCxDQUFDLENBQUM7SUFDeEUsQ0FBQzs7Ozs7SUFFRCxtQ0FBYzs7OztJQUFkLFVBQWUsRUFBTztRQUNyQixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFJLElBQUksQ0FBQyxNQUFNLHNFQUFpRSxFQUFFLFNBQU0sQ0FBQyxDQUFDO0lBQy9HLENBQUM7Ozs7O0lBRUQsOEJBQVM7Ozs7SUFBVCxVQUFVLEtBQVU7UUFDbkIsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxtRUFBbUU7WUFDeEYsMkJBQTJCLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDdEMsQ0FBQzs7Z0JBdkNELFVBQVUsU0FBQztvQkFDWCxVQUFVLEVBQUUsTUFBTTtpQkFDbEI7Ozs7Z0JBSk8sVUFBVTs7O3FCQURsQjtDQTJDQyxBQXhDRCxJQXdDQztTQXBDWSxVQUFVOzs7SUFFdEIsNEJBQW1FOztJQUV2RCwwQkFBdUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0luamVjdGFibGV9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtIdHRwQ2xpZW50fSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5cbkBJbmplY3RhYmxlKHtcblx0cHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuXG5leHBvcnQgY2xhc3MgQnBtU2VydmljZSB7XG5cdC8vIFRPRE86IE1vdmVyIHBhcmEgYXJxdWl2b3MgZGUgY29uZmlndXJhw6fDtWVzXG5cdGFwaVVyaTogU3RyaW5nID0gJy8vc29maXNhLWxhYi1nYXRld2F5LmVhc3R1czIuY2xvdWRhcHAuYXp1cmUuY29tJztcblxuXHRjb25zdHJ1Y3RvcihwdWJsaWMgaHR0cDogSHR0cENsaWVudCkge31cblx0XG5cdGNsYWltVGFza0J5SWQoaWQ6IGFueSwgdXNlcklkOiBhbnkpIHtcblx0XHRyZXR1cm4gdGhpcy5odHRwLnBvc3QoYCR7dGhpcy5hcGlVcml9L2FwaS9icG0tYXBpL2JwbS1wai1kaWdpdGFsL3Rhc2svJHtpZH0vY2xhaW1gLCB7XG5cdFx0XHR1c2VySWQ6IHVzZXJJZFxuXHRcdH0pO1xuXHR9XG5cdFxuXHRjb21wbGV0ZVRhc2tCeUlkKGlkOiBhbnksIHZhbHVlczogYW55KSB7XG5cdFx0cmV0dXJuIHRoaXMuaHR0cC5wb3N0KGAke3RoaXMuYXBpVXJpfS9hcGkvYnBtLWFwaS9icG0tcGotZGlnaXRhbC90YXNrLyR7aWR9L2NvbXBsZXRlYCwgdmFsdWVzKTtcblx0fVxuXHRcblx0Z2V0VGFza0J5SWQoaWQ6IGFueSkge1xuXHRcdHJldHVybiB0aGlzLmh0dHAuZ2V0KGAke3RoaXMuYXBpVXJpfS9hcGkvYnBtLWFwaS9icG0tcGotZGlnaXRhbC90YXNrLyR7aWR9YCk7XG5cdH1cblx0XG5cdGdldFRhc2tCeUlkVmFyaWFibGVzKGlkOiBhbnkpIHtcblx0XHRyZXR1cm4gdGhpcy5odHRwLmdldChgJHt0aGlzLmFwaVVyaX0vYXBpL2JwbS1hcGkvYnBtLXBqLWRpZ2l0YWwvdGFzay8ke2lkfS92YXJpYWJsZXNgKTtcblx0fVxuXHRcblx0Z2V0VGFza3MoKSB7XG5cdFx0cmV0dXJuIHRoaXMuaHR0cC5nZXQoJyR7dGhpcy5hcGlVcml9L2FwaS9icG0tYXBpL2JwbS1wai1kaWdpdGFsL3Rhc2snKTtcblx0fVxuXHRcblx0Z2V0VGFza0RpYWdyYW0oaWQ6IGFueSkge1xuXHRcdHJldHVybiB0aGlzLmh0dHAuZ2V0KGAke3RoaXMuYXBpVXJpfS9hcGkvYnBtLWFwaS9icG0tcGotZGlnaXRhbC9lbmdpbmUvZGVmYXVsdC9wcm9jZXNzLWRlZmluaXRpb24vJHtpZH0veG1sYCk7XG5cdH1cblx0XG5cdHN0YXJ0QUJwbSh2YWx1ZTogYW55KSB7XG5cdFx0cmV0dXJuIHRoaXMuaHR0cC5wb3N0KCcke3RoaXMuYXBpVXJpfS9hcGkvYnBtLWFwaS9icG0tcGotZGlnaXRhbC9wcm9jZXNzLWRlZmluaXRpb24va2V5LycgK1xuXHRcdFx0J1dvcmtmbG93X1BKX0RpZ2l0YWwvc3RhcnQnLCB2YWx1ZSk7XG5cdH1cbn1cbiJdfQ==