/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DrawingComponent } from './drawing.component';
var DrawingModule = /** @class */ (function () {
    function DrawingModule() {
    }
    DrawingModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [DrawingComponent],
                    imports: [
                        CommonModule
                    ],
                    exports: [DrawingComponent]
                },] }
    ];
    return DrawingModule;
}());
export { DrawingModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHJhd2luZy5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9zb2Zpc2EtYnBtLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvZHJhd2luZy9kcmF3aW5nLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFDLFFBQVEsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUN2QyxPQUFPLEVBQUMsWUFBWSxFQUFDLE1BQU0saUJBQWlCLENBQUM7QUFDN0MsT0FBTyxFQUFDLGdCQUFnQixFQUFDLE1BQU0scUJBQXFCLENBQUM7QUFFckQ7SUFBQTtJQVE0QixDQUFDOztnQkFSNUIsUUFBUSxTQUFDO29CQUNULFlBQVksRUFBRSxDQUFDLGdCQUFnQixDQUFDO29CQUNoQyxPQUFPLEVBQUU7d0JBQ1IsWUFBWTtxQkFDWjtvQkFDRCxPQUFPLEVBQUUsQ0FBQyxnQkFBZ0IsQ0FBQztpQkFDM0I7O0lBRTJCLG9CQUFDO0NBQUEsQUFSN0IsSUFRNkI7U0FBaEIsYUFBYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7TmdNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtDb21tb25Nb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQge0RyYXdpbmdDb21wb25lbnR9IGZyb20gJy4vZHJhd2luZy5jb21wb25lbnQnO1xuXG5ATmdNb2R1bGUoe1xuXHRkZWNsYXJhdGlvbnM6IFtEcmF3aW5nQ29tcG9uZW50XSxcblx0aW1wb3J0czogW1xuXHRcdENvbW1vbk1vZHVsZVxuXHRdLFxuXHRleHBvcnRzOiBbRHJhd2luZ0NvbXBvbmVudF1cbn0pXG5cbmV4cG9ydCBjbGFzcyBEcmF3aW5nTW9kdWxlIHt9XG4iXX0=