/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import BpmnJS from 'bpmn-js';
import { BpmService } from '../../services/bpm/bpm.service';
import { Component, Input } from '@angular/core';
var DrawingComponent = /** @class */ (function () {
    function DrawingComponent(bpmService) {
        this.bpmService = bpmService;
    }
    /**
     * @return {?}
     */
    DrawingComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this.viewer = new BpmnJS({
            container: '#viewer',
            width: this.width,
            height: this.height
        });
        this.viewer.importXML(this.diagram, function (err) {
            if (err) {
                console.log('XML error:', err);
            }
        });
    };
    DrawingComponent.decorators = [
        { type: Component, args: [{
                    selector: 'so-drawing',
                    template: "<div id=\"viewer\"></div>",
                    styles: ["#container .bjs-powered-by{display:none!important}"]
                }] }
    ];
    /** @nocollapse */
    DrawingComponent.ctorParameters = function () { return [
        { type: BpmService }
    ]; };
    DrawingComponent.propDecorators = {
        width: [{ type: Input }],
        height: [{ type: Input }],
        diagram: [{ type: Input }]
    };
    return DrawingComponent;
}());
export { DrawingComponent };
if (false) {
    /** @type {?} */
    DrawingComponent.prototype.width;
    /** @type {?} */
    DrawingComponent.prototype.height;
    /** @type {?} */
    DrawingComponent.prototype.diagram;
    /** @type {?} */
    DrawingComponent.prototype.viewer;
    /** @type {?} */
    DrawingComponent.prototype.bpmService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHJhd2luZy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9zb2Zpc2EtYnBtLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvZHJhd2luZy9kcmF3aW5nLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxNQUFNLE1BQU0sU0FBUyxDQUFDO0FBQzdCLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSxnQ0FBZ0MsQ0FBQztBQUMxRCxPQUFPLEVBQUMsU0FBUyxFQUFFLEtBQUssRUFBUyxNQUFNLGVBQWUsQ0FBQztBQUV2RDtJQWFDLDBCQUFtQixVQUFzQjtRQUF0QixlQUFVLEdBQVYsVUFBVSxDQUFZO0lBQUcsQ0FBQzs7OztJQUU3QyxtQ0FBUTs7O0lBQVI7UUFDQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksTUFBTSxDQUFDO1lBQ3hCLFNBQVMsRUFBRSxTQUFTO1lBQ3BCLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSztZQUNqQixNQUFNLEVBQUUsSUFBSSxDQUFDLE1BQU07U0FDbkIsQ0FBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxVQUFDLEdBQVE7WUFDNUMsSUFBSSxHQUFHLEVBQUU7Z0JBQ1IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxZQUFZLEVBQUUsR0FBRyxDQUFDLENBQUM7YUFDL0I7UUFDRixDQUFDLENBQUMsQ0FBQztJQUNKLENBQUM7O2dCQTNCRCxTQUFTLFNBQUM7b0JBQ1YsUUFBUSxFQUFFLFlBQVk7b0JBQ3RCLHFDQUF1Qzs7aUJBRXZDOzs7O2dCQVBPLFVBQVU7Ozt3QkFVaEIsS0FBSzt5QkFDTCxLQUFLOzBCQUNMLEtBQUs7O0lBbUJQLHVCQUFDO0NBQUEsQUE1QkQsSUE0QkM7U0F0QlksZ0JBQWdCOzs7SUFDNUIsaUNBQW9COztJQUNwQixrQ0FBcUI7O0lBQ3JCLG1DQUFzQjs7SUFFdEIsa0NBQW1COztJQUVQLHNDQUE2QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBCcG1uSlMgZnJvbSAnYnBtbi1qcyc7XG5pbXBvcnQge0JwbVNlcnZpY2V9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2JwbS9icG0uc2VydmljZSc7XG5pbXBvcnQge0NvbXBvbmVudCwgSW5wdXQsIE9uSW5pdH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuXHRzZWxlY3RvcjogJ3NvLWRyYXdpbmcnLFxuXHR0ZW1wbGF0ZVVybDogJy4vZHJhd2luZy5jb21wb25lbnQuaHRtbCcsXG5cdHN0eWxlVXJsczogWycuL2RyYXdpbmcuY29tcG9uZW50LnNjc3MnXVxufSlcblxuZXhwb3J0IGNsYXNzIERyYXdpbmdDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXHRASW5wdXQoKSB3aWR0aDogYW55O1xuXHRASW5wdXQoKSBoZWlnaHQ6IGFueTtcblx0QElucHV0KCkgZGlhZ3JhbTogYW55O1xuXHRcblx0cHVibGljIHZpZXdlcjogYW55O1xuXHRcblx0Y29uc3RydWN0b3IocHVibGljIGJwbVNlcnZpY2U6IEJwbVNlcnZpY2UpIHt9XG5cdFxuXHRuZ09uSW5pdCgpIHtcblx0XHR0aGlzLnZpZXdlciA9IG5ldyBCcG1uSlMoe1xuXHRcdFx0Y29udGFpbmVyOiAnI3ZpZXdlcicsXG5cdFx0XHR3aWR0aDogdGhpcy53aWR0aCxcblx0XHRcdGhlaWdodDogdGhpcy5oZWlnaHRcblx0XHR9KTtcblx0XHRcblx0XHR0aGlzLnZpZXdlci5pbXBvcnRYTUwodGhpcy5kaWFncmFtLCAoZXJyOiBhbnkpID0+IHtcblx0XHRcdGlmIChlcnIpIHtcblx0XHRcdFx0Y29uc29sZS5sb2coJ1hNTCBlcnJvcjonLCBlcnIpO1xuXHRcdFx0fVxuXHRcdH0pO1xuXHR9XG59XG4iXX0=