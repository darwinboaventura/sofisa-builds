/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TaskFormComponent } from './task-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { InputModule, ButtonModule } from 'sofisa-ui';
var TaskFormModule = /** @class */ (function () {
    function TaskFormModule() {
    }
    TaskFormModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [TaskFormComponent],
                    imports: [
                        CommonModule,
                        InputModule,
                        ButtonModule,
                        ReactiveFormsModule
                    ],
                    exports: [TaskFormComponent]
                },] }
    ];
    return TaskFormModule;
}());
export { TaskFormModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFzay1mb3JtLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3NvZmlzYS1icG0vIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy90YXNrLWZvcm0vdGFzay1mb3JtLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFDLFFBQVEsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUN2QyxPQUFPLEVBQUMsWUFBWSxFQUFDLE1BQU0saUJBQWlCLENBQUM7QUFDN0MsT0FBTyxFQUFDLGlCQUFpQixFQUFDLE1BQU0sdUJBQXVCLENBQUM7QUFDeEQsT0FBTyxFQUFDLG1CQUFtQixFQUFDLE1BQU0sZ0JBQWdCLENBQUM7QUFDbkQsT0FBTyxFQUFDLFdBQVcsRUFBRSxZQUFZLEVBQUMsTUFBTSxXQUFXLENBQUM7QUFFcEQ7SUFBQTtJQVc2QixDQUFDOztnQkFYN0IsUUFBUSxTQUFDO29CQUNULFlBQVksRUFBRSxDQUFDLGlCQUFpQixDQUFDO29CQUNqQyxPQUFPLEVBQUU7d0JBQ1IsWUFBWTt3QkFDWixXQUFXO3dCQUNYLFlBQVk7d0JBQ1osbUJBQW1CO3FCQUNuQjtvQkFDRCxPQUFPLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQztpQkFDNUI7O0lBRTRCLHFCQUFDO0NBQUEsQUFYOUIsSUFXOEI7U0FBakIsY0FBYyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7TmdNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtDb21tb25Nb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQge1Rhc2tGb3JtQ29tcG9uZW50fSBmcm9tICcuL3Rhc2stZm9ybS5jb21wb25lbnQnO1xuaW1wb3J0IHtSZWFjdGl2ZUZvcm1zTW9kdWxlfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQge0lucHV0TW9kdWxlLCBCdXR0b25Nb2R1bGV9IGZyb20gJ3NvZmlzYS11aSc7XG5cbkBOZ01vZHVsZSh7XG5cdGRlY2xhcmF0aW9uczogW1Rhc2tGb3JtQ29tcG9uZW50XSxcblx0aW1wb3J0czogW1xuXHRcdENvbW1vbk1vZHVsZSxcblx0XHRJbnB1dE1vZHVsZSxcblx0XHRCdXR0b25Nb2R1bGUsXG5cdFx0UmVhY3RpdmVGb3Jtc01vZHVsZVxuXHRdLFxuXHRleHBvcnRzOiBbVGFza0Zvcm1Db21wb25lbnRdXG59KVxuXG5leHBvcnQgY2xhc3MgVGFza0Zvcm1Nb2R1bGUge31cbiJdfQ==