/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { select, Store } from '@ngrx/store';
import { LoaderService } from 'sofisa-loader';
import { NotificationService } from 'sofisa-notification';
import { UpdateActionsOnTaskForm, formatDateToApiFormat, handleFieldChange } from 'sofisa-core';
import { Component, EventEmitter, Input, Output } from '@angular/core';
var TaskFormComponent = /** @class */ (function () {
    function TaskFormComponent(store, loaderService, notificationService) {
        this.store = store;
        this.loaderService = loaderService;
        this.notificationService = notificationService;
        this.handleFieldChange = handleFieldChange;
        this.submission = new EventEmitter();
        this.claim = new EventEmitter();
        this.store$ = this.store.pipe(select('pages'));
    }
    /**
     * @return {?}
     */
    TaskFormComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.store$.subscribe(function (store) {
            _this.props.form.validations = store.pages[_this.props.page].form.validations;
            _this.props.form.actions = store.pages[_this.props.page].form.actions;
        });
    };
    /**
     * @return {?}
     */
    TaskFormComponent.prototype.ngOnChanges = /**
     * @return {?}
     */
    function () {
        this.props.form.formGroup.updateValueAndValidity();
    };
    /**
     * @return {?}
     */
    TaskFormComponent.prototype.handleClaim = /**
     * @return {?}
     */
    function () {
        this.claim.emit({
            taskId: this.props.task.id,
            userId: this.props.user.id,
        });
    };
    /**
     * @return {?}
     */
    TaskFormComponent.prototype.handleSubmission = /**
     * @return {?}
     */
    function () {
        this.store.dispatch(new UpdateActionsOnTaskForm({
            page: this.props.page,
            actionName: 'submitted',
            actionValue: true
        }));
        if (this.props.page === 'tasklist/editar') {
            if (this.props.task.type === 'credito') {
                this.props.form.formGroup.get('variables.aprovadoT3.value').setValue('sim');
            }
            else if (this.props.task.type === 'comercial') {
                this.props.form.formGroup.get('variables.aprovadoT4.value').setValue('sim');
            }
        }
        if (this.props.form.formGroup.valid) {
            this.props.form.formGroup.get('variables.dataVisita.value').setValue(formatDateToApiFormat(this.props.form.formGroup.value.variables.dataVisita.value));
            this.submission.emit(this.props.form.formGroup);
        }
    };
    TaskFormComponent.decorators = [
        { type: Component, args: [{
                    selector: 'so-task-form',
                    template: "<form class=\"so-pg-task-novo--form clearfix\" [formGroup]=\"props.form.formGroup\">\n\t<so-input label=\"Cliente\" placeholder=\"Digite aqui o nome do cliente\" [value]=\"props.form.formGroup.get('variables.cliente.value').value\" [inputConfig]=\"{ validation: { type: props.form.validations.cliente.type, message: props.form.validations.cliente.message} }\" (hasChanged)=\"handleFieldChange(props.form.formGroup, store, props.page, $event, 'cliente')\"></so-input>\n\t\n\t<so-input label=\"CNPJ\" placeholder=\"Digite aqui seu CNPJ\" [value]=\"props.form.formGroup.get('variables.cnpj.value').value\" [inputConfig]=\"{ validation: { type: props.form.validations.cnpj.type, message: props.form.validations.cnpj.message} }\" (hasChanged)=\"handleFieldChange(props.form.formGroup, store, props.page, $event, 'cnpj')\"></so-input>\n\t\n\t<so-input label=\"Data da visita\" inputType=\"date\" placeholder=\"Qual a data da visita?\" [value]=\"props.form.formGroup.get('variables.dataVisita.value').value\" [inputConfig]=\"{ validation: { type: props.form.validations.dataVisita.type, message: props.form.validations.dataVisita.message} }\" (hasChanged)=\"handleFieldChange(props.form.formGroup, store, props.page, $event, 'dataVisita')\"></so-input>\n\t\n\t<so-button text=\"Criar task\" (hasClicked)=\"handleSubmission()\" *ngIf=\"props.page === 'tasklist/novo'\"></so-button>\n\t\n\t<so-button text=\"Reivindicar\" *ngIf=\"props.page === 'tasklist/editar' && !props.task.claimed\" (hasClicked)=\"handleClaim()\"></so-button>\n\t\n\t<so-button text=\"Aprovar\" *ngIf=\"props.page === 'tasklist/editar' && props.task.claimed\" (hasClicked)=\"handleSubmission()\"></so-button>\n</form>",
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    TaskFormComponent.ctorParameters = function () { return [
        { type: Store },
        { type: LoaderService },
        { type: NotificationService }
    ]; };
    TaskFormComponent.propDecorators = {
        props: [{ type: Input }],
        submission: [{ type: Output }],
        claim: [{ type: Output }]
    };
    return TaskFormComponent;
}());
export { TaskFormComponent };
if (false) {
    /** @type {?} */
    TaskFormComponent.prototype.handleFieldChange;
    /** @type {?} */
    TaskFormComponent.prototype.store$;
    /** @type {?} */
    TaskFormComponent.prototype.props;
    /** @type {?} */
    TaskFormComponent.prototype.submission;
    /** @type {?} */
    TaskFormComponent.prototype.claim;
    /** @type {?} */
    TaskFormComponent.prototype.store;
    /** @type {?} */
    TaskFormComponent.prototype.loaderService;
    /** @type {?} */
    TaskFormComponent.prototype.notificationService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFzay1mb3JtLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3NvZmlzYS1icG0vIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy90YXNrLWZvcm0vdGFzay1mb3JtLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFDLE1BQU0sRUFBRSxLQUFLLEVBQUMsTUFBTSxhQUFhLENBQUM7QUFHMUMsT0FBTyxFQUFDLGFBQWEsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUM1QyxPQUFPLEVBQUMsbUJBQW1CLEVBQUMsTUFBTSxxQkFBcUIsQ0FBQztBQUN4RCxPQUFPLEVBQUMsdUJBQXVCLEVBQUUscUJBQXFCLEVBQUUsaUJBQWlCLEVBQUMsTUFBTSxhQUFhLENBQUM7QUFDOUYsT0FBTyxFQUFDLFNBQVMsRUFBRSxZQUFZLEVBQUUsS0FBSyxFQUFxQixNQUFNLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFFeEY7SUFzQkMsMkJBQW1CLEtBQTBCLEVBQVMsYUFBNEIsRUFBUyxtQkFBd0M7UUFBaEgsVUFBSyxHQUFMLEtBQUssQ0FBcUI7UUFBUyxrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQUFTLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUFmbkksc0JBQWlCLEdBQVEsaUJBQWlCLENBQUM7UUFZakMsZUFBVSxHQUE0QixJQUFJLFlBQVksRUFBRSxDQUFDO1FBQ3pELFVBQUssR0FBOEMsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUcvRSxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO0lBQ2hELENBQUM7Ozs7SUFFRCxvQ0FBUTs7O0lBQVI7UUFBQSxpQkFLQztRQUpBLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLFVBQUMsS0FBVTtZQUNoQyxLQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxLQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUM7WUFDNUUsS0FBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsS0FBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDO1FBQ3JFLENBQUMsQ0FBQyxDQUFDO0lBQ0osQ0FBQzs7OztJQUVELHVDQUFXOzs7SUFBWDtRQUNDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO0lBQ3BELENBQUM7Ozs7SUFFRCx1Q0FBVzs7O0lBQVg7UUFDQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQztZQUNmLE1BQU0sRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQzFCLE1BQU0sRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFFO1NBQzFCLENBQUMsQ0FBQztJQUNKLENBQUM7Ozs7SUFFRCw0Q0FBZ0I7OztJQUFoQjtRQUNDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksdUJBQXVCLENBQUM7WUFDL0MsSUFBSSxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSTtZQUNyQixVQUFVLEVBQUUsV0FBVztZQUN2QixXQUFXLEVBQUUsSUFBSTtTQUNqQixDQUFDLENBQUMsQ0FBQztRQUVKLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLEtBQUssaUJBQWlCLEVBQUU7WUFDMUMsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLEtBQUssU0FBUyxFQUFFO2dCQUN2QyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLDRCQUE0QixDQUFDLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQzVFO2lCQUFNLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxLQUFLLFdBQVcsRUFBRTtnQkFDaEQsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyw0QkFBNEIsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUM1RTtTQUNEO1FBRUQsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFO1lBQ3BDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsNEJBQTRCLENBQUMsQ0FBQyxRQUFRLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFFeEosSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7U0FDaEQ7SUFDRixDQUFDOztnQkFoRUQsU0FBUyxTQUFDO29CQUNWLFFBQVEsRUFBRSxjQUFjO29CQUN4Qiw0cERBQXlDOztpQkFFekM7Ozs7Z0JBWmUsS0FBSztnQkFHYixhQUFhO2dCQUNiLG1CQUFtQjs7O3dCQWF6QixLQUFLOzZCQVVMLE1BQU07d0JBQ04sTUFBTTs7SUE2Q1Isd0JBQUM7Q0FBQSxBQWpFRCxJQWlFQztTQTNEWSxpQkFBaUI7OztJQUM3Qiw4Q0FBMkM7O0lBQzNDLG1DQUF3Qjs7SUFDeEIsa0NBU0U7O0lBQ0YsdUNBQW1FOztJQUNuRSxrQ0FBZ0Y7O0lBRXBFLGtDQUFpQzs7SUFBRSwwQ0FBbUM7O0lBQUUsZ0RBQStDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtzZWxlY3QsIFN0b3JlfSBmcm9tICdAbmdyeC9zdG9yZSc7XG5pbXBvcnQge0Zvcm1Hcm91cH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuaW1wb3J0IHtPYnNlcnZhYmxlfSBmcm9tICdyeGpzJztcbmltcG9ydCB7TG9hZGVyU2VydmljZX0gZnJvbSAnc29maXNhLWxvYWRlcic7XG5pbXBvcnQge05vdGlmaWNhdGlvblNlcnZpY2V9IGZyb20gJ3NvZmlzYS1ub3RpZmljYXRpb24nO1xuaW1wb3J0IHtVcGRhdGVBY3Rpb25zT25UYXNrRm9ybSwgZm9ybWF0RGF0ZVRvQXBpRm9ybWF0LCBoYW5kbGVGaWVsZENoYW5nZX0gZnJvbSAnc29maXNhLWNvcmUnO1xuaW1wb3J0IHtDb21wb25lbnQsIEV2ZW50RW1pdHRlciwgSW5wdXQsIE9uQ2hhbmdlcywgT25Jbml0LCBPdXRwdXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcblx0c2VsZWN0b3I6ICdzby10YXNrLWZvcm0nLFxuXHR0ZW1wbGF0ZVVybDogJy4vdGFzay1mb3JtLmNvbXBvbmVudC5odG1sJyxcblx0c3R5bGVVcmxzOiBbJy4vdGFzay1mb3JtLmNvbXBvbmVudC5zY3NzJ11cbn0pXG5cbmV4cG9ydCBjbGFzcyBUYXNrRm9ybUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25DaGFuZ2VzIHtcblx0aGFuZGxlRmllbGRDaGFuZ2U6IGFueSA9IGhhbmRsZUZpZWxkQ2hhbmdlO1xuXHRzdG9yZSQ6IE9ic2VydmFibGU8YW55Pjtcblx0QElucHV0KCkgcHJvcHM6IHtcblx0XHR1c2VyOiBhbnksXG5cdFx0dGFzazogYW55LFxuXHRcdHBhZ2U6IHN0cmluZyxcblx0XHRmb3JtOiB7XG5cdFx0XHRmb3JtR3JvdXA6IEZvcm1Hcm91cCxcblx0XHRcdHZhbGlkYXRpb25zOiBhbnksXG5cdFx0XHRhY3Rpb25zOiBhbnlcblx0XHR9XG5cdH07XG5cdEBPdXRwdXQoKSBzdWJtaXNzaW9uOiBFdmVudEVtaXR0ZXI8Rm9ybUdyb3VwPiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcblx0QE91dHB1dCgpIGNsYWltOiBFdmVudEVtaXR0ZXI8eyB0YXNrSWQ6IGFueSwgdXNlcklkOiBhbnl9PiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcblx0XG5cdGNvbnN0cnVjdG9yKHB1YmxpYyBzdG9yZTogU3RvcmU8e3BhZ2VzOiBhbnl9PiwgcHVibGljIGxvYWRlclNlcnZpY2U6IExvYWRlclNlcnZpY2UsIHB1YmxpYyBub3RpZmljYXRpb25TZXJ2aWNlOiBOb3RpZmljYXRpb25TZXJ2aWNlKSB7XG5cdFx0dGhpcy5zdG9yZSQgPSB0aGlzLnN0b3JlLnBpcGUoc2VsZWN0KCdwYWdlcycpKTtcblx0fVxuXHRcblx0bmdPbkluaXQoKSB7XG5cdFx0dGhpcy5zdG9yZSQuc3Vic2NyaWJlKChzdG9yZTogYW55KSA9PiB7XG5cdFx0XHR0aGlzLnByb3BzLmZvcm0udmFsaWRhdGlvbnMgPSBzdG9yZS5wYWdlc1t0aGlzLnByb3BzLnBhZ2VdLmZvcm0udmFsaWRhdGlvbnM7XG5cdFx0XHR0aGlzLnByb3BzLmZvcm0uYWN0aW9ucyA9IHN0b3JlLnBhZ2VzW3RoaXMucHJvcHMucGFnZV0uZm9ybS5hY3Rpb25zO1xuXHRcdH0pO1xuXHR9XG5cdFxuXHRuZ09uQ2hhbmdlcygpIHtcblx0XHR0aGlzLnByb3BzLmZvcm0uZm9ybUdyb3VwLnVwZGF0ZVZhbHVlQW5kVmFsaWRpdHkoKTtcblx0fVxuXHRcblx0aGFuZGxlQ2xhaW0oKSB7XG5cdFx0dGhpcy5jbGFpbS5lbWl0KHtcblx0XHRcdHRhc2tJZDogdGhpcy5wcm9wcy50YXNrLmlkLFxuXHRcdFx0dXNlcklkOiB0aGlzLnByb3BzLnVzZXIuaWQsXG5cdFx0fSk7XG5cdH1cblx0XG5cdGhhbmRsZVN1Ym1pc3Npb24oKSB7XG5cdFx0dGhpcy5zdG9yZS5kaXNwYXRjaChuZXcgVXBkYXRlQWN0aW9uc09uVGFza0Zvcm0oe1xuXHRcdFx0cGFnZTogdGhpcy5wcm9wcy5wYWdlLFxuXHRcdFx0YWN0aW9uTmFtZTogJ3N1Ym1pdHRlZCcsXG5cdFx0XHRhY3Rpb25WYWx1ZTogdHJ1ZVxuXHRcdH0pKTtcblx0XHRcblx0XHRpZiAodGhpcy5wcm9wcy5wYWdlID09PSAndGFza2xpc3QvZWRpdGFyJykge1xuXHRcdFx0aWYgKHRoaXMucHJvcHMudGFzay50eXBlID09PSAnY3JlZGl0bycpIHtcblx0XHRcdFx0dGhpcy5wcm9wcy5mb3JtLmZvcm1Hcm91cC5nZXQoJ3ZhcmlhYmxlcy5hcHJvdmFkb1QzLnZhbHVlJykuc2V0VmFsdWUoJ3NpbScpO1xuXHRcdFx0fSBlbHNlIGlmICh0aGlzLnByb3BzLnRhc2sudHlwZSA9PT0gJ2NvbWVyY2lhbCcpIHtcblx0XHRcdFx0dGhpcy5wcm9wcy5mb3JtLmZvcm1Hcm91cC5nZXQoJ3ZhcmlhYmxlcy5hcHJvdmFkb1Q0LnZhbHVlJykuc2V0VmFsdWUoJ3NpbScpO1xuXHRcdFx0fVxuXHRcdH1cblx0XHRcblx0XHRpZiAodGhpcy5wcm9wcy5mb3JtLmZvcm1Hcm91cC52YWxpZCkge1xuXHRcdFx0dGhpcy5wcm9wcy5mb3JtLmZvcm1Hcm91cC5nZXQoJ3ZhcmlhYmxlcy5kYXRhVmlzaXRhLnZhbHVlJykuc2V0VmFsdWUoZm9ybWF0RGF0ZVRvQXBpRm9ybWF0KHRoaXMucHJvcHMuZm9ybS5mb3JtR3JvdXAudmFsdWUudmFyaWFibGVzLmRhdGFWaXNpdGEudmFsdWUpKTtcblx0XHRcdFxuXHRcdFx0dGhpcy5zdWJtaXNzaW9uLmVtaXQodGhpcy5wcm9wcy5mb3JtLmZvcm1Hcm91cCk7XG5cdFx0fVxuXHR9XG59XG4iXX0=