/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TasklistComponent } from './tasklist.component';
var TasklistModule = /** @class */ (function () {
    function TasklistModule() {
    }
    TasklistModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [TasklistComponent],
                    imports: [
                        CommonModule,
                        RouterModule
                    ],
                    exports: [TasklistComponent]
                },] }
    ];
    return TasklistModule;
}());
export { TasklistModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFza2xpc3QubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vc29maXNhLWJwbS8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL3Rhc2tsaXN0L3Rhc2tsaXN0Lm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFDLFFBQVEsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUN2QyxPQUFPLEVBQUMsWUFBWSxFQUFDLE1BQU0saUJBQWlCLENBQUM7QUFDN0MsT0FBTyxFQUFDLFlBQVksRUFBQyxNQUFNLGlCQUFpQixDQUFDO0FBQzdDLE9BQU8sRUFBQyxpQkFBaUIsRUFBQyxNQUFNLHNCQUFzQixDQUFDO0FBRXZEO0lBQUE7SUFTNkIsQ0FBQzs7Z0JBVDdCLFFBQVEsU0FBQztvQkFDVCxZQUFZLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQztvQkFDakMsT0FBTyxFQUFFO3dCQUNSLFlBQVk7d0JBQ1osWUFBWTtxQkFDWjtvQkFDRCxPQUFPLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQztpQkFDNUI7O0lBRTRCLHFCQUFDO0NBQUEsQUFUOUIsSUFTOEI7U0FBakIsY0FBYyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7TmdNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtDb21tb25Nb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQge1JvdXRlck1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7VGFza2xpc3RDb21wb25lbnR9IGZyb20gJy4vdGFza2xpc3QuY29tcG9uZW50JztcblxuQE5nTW9kdWxlKHtcblx0ZGVjbGFyYXRpb25zOiBbVGFza2xpc3RDb21wb25lbnRdLFxuXHRpbXBvcnRzOiBbXG5cdFx0Q29tbW9uTW9kdWxlLFxuXHRcdFJvdXRlck1vZHVsZVxuXHRdLFxuXHRleHBvcnRzOiBbVGFza2xpc3RDb21wb25lbnRdXG59KVxuXG5leHBvcnQgY2xhc3MgVGFza2xpc3RNb2R1bGUge31cbiJdfQ==