/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
var TasklistComponent = /** @class */ (function () {
    function TasklistComponent() {
    }
    /**
     * @return {?}
     */
    TasklistComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () { };
    TasklistComponent.decorators = [
        { type: Component, args: [{
                    selector: 'so-tasklist',
                    template: "<div class=\"so-tasklist clearfix\" *ngIf=\"props.tasks\">\n\t<div class=\"so-tasklist--head clearfix\">\n\t\t<div class=\"so-tasklist--column clearfix\">Nome</div>\n\t\t<div class=\"so-tasklist--column clearfix\">Data de cria\u00E7\u00E3o</div>\n\t\t<div class=\"so-tasklist--column clearfix\">Clamado por</div>\n\t\t<div class=\"so-tasklist--column clearfix\">A\u00E7\u00F5es</div>\n\t</div>\n\t\n\t<div class=\"so-tasklist--body clearfix\">\n\t\t<div class=\"so-tasklist--row clearfix\" *ngFor=\"let task of props.tasks\">\n\t\t\t<div class=\"so-tasklist--column clearfix\">{{task.name}}</div>\n\t\t\t<div class=\"so-tasklist--column clearfix\">{{task.created | date:'dd/MM/y'}}</div>\n\t\t\t<div class=\"so-tasklist--column clearfix\" *ngIf=\"task.assignee\">{{task.assignee}}</div>\n\t\t\t<div class=\"so-tasklist--column clearfix\" *ngIf=\"!task.assignee\">Ningu\u00E9m</div>\n\t\t\t<div class=\"so-tasklist--column clearfix\">\n\t\t\t\t<a [routerLink]=\"[ '/tasklist/editar', task.id ]\" class=\"btn btn-edit\">Editar</a>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>",
                    styles: [".so-tasklist{width:100%}.so-tasklist--head,.so-tasklist--row{display:flex;flex-direction:row;flex-wrap:wrap;align-items:stretch}.so-tasklist--head .so-tasklist--column,.so-tasklist--row .so-tasklist--column{width:25%}.so-tasklist--head{padding:0 27px}.so-tasklist--head .so-tasklist--column{font-family:Roboto;font-size:12px;color:#90a4ae;letter-spacing:.5px;line-height:14px;text-transform:uppercase}.so-tasklist--body{margin-top:12px;box-shadow:0 4px 9px 0 rgba(0,0,0,.1)}.so-tasklist--body .so-tasklist--row{padding:24px 27px;border:1px solid #e9eff4;background:#fff}.so-tasklist--body .so-tasklist--column{font-family:Roboto;font-size:14px;font-weight:500;color:#31393c;letter-spacing:-.07px;line-height:16px}.so-tasklist--body .so-tasklist--column .btn{font-family:Roboto;font-weight:500;font-size:12px;color:#1dec3a;letter-spacing:.06px;line-height:14px;padding:2px 10px;display:table;border-radius:2px;text-decoration:none;text-transform:uppercase;background-color:rgba(29,236,58,.1)}"]
                }] }
    ];
    /** @nocollapse */
    TasklistComponent.ctorParameters = function () { return []; };
    TasklistComponent.propDecorators = {
        props: [{ type: Input }]
    };
    return TasklistComponent;
}());
export { TasklistComponent };
if (false) {
    /** @type {?} */
    TasklistComponent.prototype.props;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFza2xpc3QuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vc29maXNhLWJwbS8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL3Rhc2tsaXN0L3Rhc2tsaXN0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFDLFNBQVMsRUFBRSxLQUFLLEVBQVMsTUFBTSxlQUFlLENBQUM7QUFFdkQ7SUFXQztJQUFlLENBQUM7Ozs7SUFFaEIsb0NBQVE7OztJQUFSLGNBQVksQ0FBQzs7Z0JBYmIsU0FBUyxTQUFDO29CQUNWLFFBQVEsRUFBRSxhQUFhO29CQUN2Qiw0akNBQXdDOztpQkFFeEM7Ozs7O3dCQUdDLEtBQUs7O0lBT1Asd0JBQUM7Q0FBQSxBQWRELElBY0M7U0FSWSxpQkFBaUI7OztJQUM3QixrQ0FFRSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBJbnB1dCwgT25Jbml0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQENvbXBvbmVudCh7XG5cdHNlbGVjdG9yOiAnc28tdGFza2xpc3QnLFxuXHR0ZW1wbGF0ZVVybDogJy4vdGFza2xpc3QuY29tcG9uZW50Lmh0bWwnLFxuXHRzdHlsZVVybHM6IFsnLi90YXNrbGlzdC5jb21wb25lbnQuc2NzcyddXG59KVxuXG5leHBvcnQgY2xhc3MgVGFza2xpc3RDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXHRASW5wdXQoKSBwcm9wczoge1xuXHRcdHRhc2tzOiBbXVxuXHR9O1xuXHRcblx0Y29uc3RydWN0b3IoKSB7fVxuXHRcblx0bmdPbkluaXQoKSB7fVxufVxuIl19