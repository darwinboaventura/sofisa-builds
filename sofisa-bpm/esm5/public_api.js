/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
// Componentes
export { TasklistModule } from './lib/components/tasklist/tasklist.module';
export { TaskFormModule } from './lib/components/task-form/task-form.module';
export { DrawingModule } from './lib/components/drawing/drawing.module';
// Services
export { BpmService } from './lib/services/bpm/bpm.service';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHVibGljX2FwaS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3NvZmlzYS1icG0vIiwic291cmNlcyI6WyJwdWJsaWNfYXBpLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQ0EsK0JBQWMsMkNBQTJDLENBQUM7QUFDMUQsK0JBQWMsNkNBQTZDLENBQUM7QUFDNUQsOEJBQWMseUNBQXlDLENBQUM7O0FBR3hELDJCQUFjLGdDQUFnQyxDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiLy8gQ29tcG9uZW50ZXNcbmV4cG9ydCAqIGZyb20gJy4vbGliL2NvbXBvbmVudHMvdGFza2xpc3QvdGFza2xpc3QubW9kdWxlJztcbmV4cG9ydCAqIGZyb20gJy4vbGliL2NvbXBvbmVudHMvdGFzay1mb3JtL3Rhc2stZm9ybS5tb2R1bGUnO1xuZXhwb3J0ICogZnJvbSAnLi9saWIvY29tcG9uZW50cy9kcmF3aW5nL2RyYXdpbmcubW9kdWxlJztcblxuLy8gU2VydmljZXNcbmV4cG9ydCAqIGZyb20gJy4vbGliL3NlcnZpY2VzL2JwbS9icG0uc2VydmljZSc7XG4iXX0=