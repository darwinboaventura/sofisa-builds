/**
 * Generated bundle index. Do not edit.
 */
export * from './public_api';
export { DrawingComponent as ɵc } from './lib/components/drawing/drawing.component';
export { TaskFormComponent as ɵb } from './lib/components/task-form/task-form.component';
export { TasklistComponent as ɵa } from './lib/components/tasklist/tasklist.component';
