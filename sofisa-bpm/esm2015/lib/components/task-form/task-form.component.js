/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { select, Store } from '@ngrx/store';
import { LoaderService } from 'sofisa-loader';
import { NotificationService } from 'sofisa-notification';
import { UpdateActionsOnTaskForm, formatDateToApiFormat, handleFieldChange } from 'sofisa-core';
import { Component, EventEmitter, Input, Output } from '@angular/core';
export class TaskFormComponent {
    /**
     * @param {?} store
     * @param {?} loaderService
     * @param {?} notificationService
     */
    constructor(store, loaderService, notificationService) {
        this.store = store;
        this.loaderService = loaderService;
        this.notificationService = notificationService;
        this.handleFieldChange = handleFieldChange;
        this.submission = new EventEmitter();
        this.claim = new EventEmitter();
        this.store$ = this.store.pipe(select('pages'));
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.store$.subscribe((store) => {
            this.props.form.validations = store.pages[this.props.page].form.validations;
            this.props.form.actions = store.pages[this.props.page].form.actions;
        });
    }
    /**
     * @return {?}
     */
    ngOnChanges() {
        this.props.form.formGroup.updateValueAndValidity();
    }
    /**
     * @return {?}
     */
    handleClaim() {
        this.claim.emit({
            taskId: this.props.task.id,
            userId: this.props.user.id,
        });
    }
    /**
     * @return {?}
     */
    handleSubmission() {
        this.store.dispatch(new UpdateActionsOnTaskForm({
            page: this.props.page,
            actionName: 'submitted',
            actionValue: true
        }));
        if (this.props.page === 'tasklist/editar') {
            if (this.props.task.type === 'credito') {
                this.props.form.formGroup.get('variables.aprovadoT3.value').setValue('sim');
            }
            else if (this.props.task.type === 'comercial') {
                this.props.form.formGroup.get('variables.aprovadoT4.value').setValue('sim');
            }
        }
        if (this.props.form.formGroup.valid) {
            this.props.form.formGroup.get('variables.dataVisita.value').setValue(formatDateToApiFormat(this.props.form.formGroup.value.variables.dataVisita.value));
            this.submission.emit(this.props.form.formGroup);
        }
    }
}
TaskFormComponent.decorators = [
    { type: Component, args: [{
                selector: 'so-task-form',
                template: "<form class=\"so-pg-task-novo--form clearfix\" [formGroup]=\"props.form.formGroup\">\n\t<so-input label=\"Cliente\" placeholder=\"Digite aqui o nome do cliente\" [value]=\"props.form.formGroup.get('variables.cliente.value').value\" [inputConfig]=\"{ validation: { type: props.form.validations.cliente.type, message: props.form.validations.cliente.message} }\" (hasChanged)=\"handleFieldChange(props.form.formGroup, store, props.page, $event, 'cliente')\"></so-input>\n\t\n\t<so-input label=\"CNPJ\" placeholder=\"Digite aqui seu CNPJ\" [value]=\"props.form.formGroup.get('variables.cnpj.value').value\" [inputConfig]=\"{ validation: { type: props.form.validations.cnpj.type, message: props.form.validations.cnpj.message} }\" (hasChanged)=\"handleFieldChange(props.form.formGroup, store, props.page, $event, 'cnpj')\"></so-input>\n\t\n\t<so-input label=\"Data da visita\" inputType=\"date\" placeholder=\"Qual a data da visita?\" [value]=\"props.form.formGroup.get('variables.dataVisita.value').value\" [inputConfig]=\"{ validation: { type: props.form.validations.dataVisita.type, message: props.form.validations.dataVisita.message} }\" (hasChanged)=\"handleFieldChange(props.form.formGroup, store, props.page, $event, 'dataVisita')\"></so-input>\n\t\n\t<so-button text=\"Criar task\" (hasClicked)=\"handleSubmission()\" *ngIf=\"props.page === 'tasklist/novo'\"></so-button>\n\t\n\t<so-button text=\"Reivindicar\" *ngIf=\"props.page === 'tasklist/editar' && !props.task.claimed\" (hasClicked)=\"handleClaim()\"></so-button>\n\t\n\t<so-button text=\"Aprovar\" *ngIf=\"props.page === 'tasklist/editar' && props.task.claimed\" (hasClicked)=\"handleSubmission()\"></so-button>\n</form>",
                styles: [""]
            }] }
];
/** @nocollapse */
TaskFormComponent.ctorParameters = () => [
    { type: Store },
    { type: LoaderService },
    { type: NotificationService }
];
TaskFormComponent.propDecorators = {
    props: [{ type: Input }],
    submission: [{ type: Output }],
    claim: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    TaskFormComponent.prototype.handleFieldChange;
    /** @type {?} */
    TaskFormComponent.prototype.store$;
    /** @type {?} */
    TaskFormComponent.prototype.props;
    /** @type {?} */
    TaskFormComponent.prototype.submission;
    /** @type {?} */
    TaskFormComponent.prototype.claim;
    /** @type {?} */
    TaskFormComponent.prototype.store;
    /** @type {?} */
    TaskFormComponent.prototype.loaderService;
    /** @type {?} */
    TaskFormComponent.prototype.notificationService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFzay1mb3JtLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3NvZmlzYS1icG0vIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy90YXNrLWZvcm0vdGFzay1mb3JtLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFDLE1BQU0sRUFBRSxLQUFLLEVBQUMsTUFBTSxhQUFhLENBQUM7QUFHMUMsT0FBTyxFQUFDLGFBQWEsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUM1QyxPQUFPLEVBQUMsbUJBQW1CLEVBQUMsTUFBTSxxQkFBcUIsQ0FBQztBQUN4RCxPQUFPLEVBQUMsdUJBQXVCLEVBQUUscUJBQXFCLEVBQUUsaUJBQWlCLEVBQUMsTUFBTSxhQUFhLENBQUM7QUFDOUYsT0FBTyxFQUFDLFNBQVMsRUFBRSxZQUFZLEVBQUUsS0FBSyxFQUFxQixNQUFNLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFReEYsTUFBTSxPQUFPLGlCQUFpQjs7Ozs7O0lBZ0I3QixZQUFtQixLQUEwQixFQUFTLGFBQTRCLEVBQVMsbUJBQXdDO1FBQWhILFVBQUssR0FBTCxLQUFLLENBQXFCO1FBQVMsa0JBQWEsR0FBYixhQUFhLENBQWU7UUFBUyx3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO1FBZm5JLHNCQUFpQixHQUFRLGlCQUFpQixDQUFDO1FBWWpDLGVBQVUsR0FBNEIsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUN6RCxVQUFLLEdBQThDLElBQUksWUFBWSxFQUFFLENBQUM7UUFHL0UsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztJQUNoRCxDQUFDOzs7O0lBRUQsUUFBUTtRQUNQLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUMsS0FBVSxFQUFFLEVBQUU7WUFDcEMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDO1lBQzVFLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQztRQUNyRSxDQUFDLENBQUMsQ0FBQztJQUNKLENBQUM7Ozs7SUFFRCxXQUFXO1FBQ1YsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLHNCQUFzQixFQUFFLENBQUM7SUFDcEQsQ0FBQzs7OztJQUVELFdBQVc7UUFDVixJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQztZQUNmLE1BQU0sRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQzFCLE1BQU0sRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFFO1NBQzFCLENBQUMsQ0FBQztJQUNKLENBQUM7Ozs7SUFFRCxnQkFBZ0I7UUFDZixJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxJQUFJLHVCQUF1QixDQUFDO1lBQy9DLElBQUksRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUk7WUFDckIsVUFBVSxFQUFFLFdBQVc7WUFDdkIsV0FBVyxFQUFFLElBQUk7U0FDakIsQ0FBQyxDQUFDLENBQUM7UUFFSixJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLGlCQUFpQixFQUFFO1lBQzFDLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxLQUFLLFNBQVMsRUFBRTtnQkFDdkMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyw0QkFBNEIsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUM1RTtpQkFBTSxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksS0FBSyxXQUFXLEVBQUU7Z0JBQ2hELElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsNEJBQTRCLENBQUMsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDNUU7U0FDRDtRQUVELElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRTtZQUNwQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLDRCQUE0QixDQUFDLENBQUMsUUFBUSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1lBRXhKLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1NBQ2hEO0lBQ0YsQ0FBQzs7O1lBaEVELFNBQVMsU0FBQztnQkFDVixRQUFRLEVBQUUsY0FBYztnQkFDeEIsNHBEQUF5Qzs7YUFFekM7Ozs7WUFaZSxLQUFLO1lBR2IsYUFBYTtZQUNiLG1CQUFtQjs7O29CQWF6QixLQUFLO3lCQVVMLE1BQU07b0JBQ04sTUFBTTs7OztJQWJQLDhDQUEyQzs7SUFDM0MsbUNBQXdCOztJQUN4QixrQ0FTRTs7SUFDRix1Q0FBbUU7O0lBQ25FLGtDQUFnRjs7SUFFcEUsa0NBQWlDOztJQUFFLDBDQUFtQzs7SUFBRSxnREFBK0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge3NlbGVjdCwgU3RvcmV9IGZyb20gJ0BuZ3J4L3N0b3JlJztcbmltcG9ydCB7Rm9ybUdyb3VwfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQge09ic2VydmFibGV9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHtMb2FkZXJTZXJ2aWNlfSBmcm9tICdzb2Zpc2EtbG9hZGVyJztcbmltcG9ydCB7Tm90aWZpY2F0aW9uU2VydmljZX0gZnJvbSAnc29maXNhLW5vdGlmaWNhdGlvbic7XG5pbXBvcnQge1VwZGF0ZUFjdGlvbnNPblRhc2tGb3JtLCBmb3JtYXREYXRlVG9BcGlGb3JtYXQsIGhhbmRsZUZpZWxkQ2hhbmdlfSBmcm9tICdzb2Zpc2EtY29yZSc7XG5pbXBvcnQge0NvbXBvbmVudCwgRXZlbnRFbWl0dGVyLCBJbnB1dCwgT25DaGFuZ2VzLCBPbkluaXQsIE91dHB1dH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuXHRzZWxlY3RvcjogJ3NvLXRhc2stZm9ybScsXG5cdHRlbXBsYXRlVXJsOiAnLi90YXNrLWZvcm0uY29tcG9uZW50Lmh0bWwnLFxuXHRzdHlsZVVybHM6IFsnLi90YXNrLWZvcm0uY29tcG9uZW50LnNjc3MnXVxufSlcblxuZXhwb3J0IGNsYXNzIFRhc2tGb3JtQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkNoYW5nZXMge1xuXHRoYW5kbGVGaWVsZENoYW5nZTogYW55ID0gaGFuZGxlRmllbGRDaGFuZ2U7XG5cdHN0b3JlJDogT2JzZXJ2YWJsZTxhbnk+O1xuXHRASW5wdXQoKSBwcm9wczoge1xuXHRcdHVzZXI6IGFueSxcblx0XHR0YXNrOiBhbnksXG5cdFx0cGFnZTogc3RyaW5nLFxuXHRcdGZvcm06IHtcblx0XHRcdGZvcm1Hcm91cDogRm9ybUdyb3VwLFxuXHRcdFx0dmFsaWRhdGlvbnM6IGFueSxcblx0XHRcdGFjdGlvbnM6IGFueVxuXHRcdH1cblx0fTtcblx0QE91dHB1dCgpIHN1Ym1pc3Npb246IEV2ZW50RW1pdHRlcjxGb3JtR3JvdXA+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuXHRAT3V0cHV0KCkgY2xhaW06IEV2ZW50RW1pdHRlcjx7IHRhc2tJZDogYW55LCB1c2VySWQ6IGFueX0+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuXHRcblx0Y29uc3RydWN0b3IocHVibGljIHN0b3JlOiBTdG9yZTx7cGFnZXM6IGFueX0+LCBwdWJsaWMgbG9hZGVyU2VydmljZTogTG9hZGVyU2VydmljZSwgcHVibGljIG5vdGlmaWNhdGlvblNlcnZpY2U6IE5vdGlmaWNhdGlvblNlcnZpY2UpIHtcblx0XHR0aGlzLnN0b3JlJCA9IHRoaXMuc3RvcmUucGlwZShzZWxlY3QoJ3BhZ2VzJykpO1xuXHR9XG5cdFxuXHRuZ09uSW5pdCgpIHtcblx0XHR0aGlzLnN0b3JlJC5zdWJzY3JpYmUoKHN0b3JlOiBhbnkpID0+IHtcblx0XHRcdHRoaXMucHJvcHMuZm9ybS52YWxpZGF0aW9ucyA9IHN0b3JlLnBhZ2VzW3RoaXMucHJvcHMucGFnZV0uZm9ybS52YWxpZGF0aW9ucztcblx0XHRcdHRoaXMucHJvcHMuZm9ybS5hY3Rpb25zID0gc3RvcmUucGFnZXNbdGhpcy5wcm9wcy5wYWdlXS5mb3JtLmFjdGlvbnM7XG5cdFx0fSk7XG5cdH1cblx0XG5cdG5nT25DaGFuZ2VzKCkge1xuXHRcdHRoaXMucHJvcHMuZm9ybS5mb3JtR3JvdXAudXBkYXRlVmFsdWVBbmRWYWxpZGl0eSgpO1xuXHR9XG5cdFxuXHRoYW5kbGVDbGFpbSgpIHtcblx0XHR0aGlzLmNsYWltLmVtaXQoe1xuXHRcdFx0dGFza0lkOiB0aGlzLnByb3BzLnRhc2suaWQsXG5cdFx0XHR1c2VySWQ6IHRoaXMucHJvcHMudXNlci5pZCxcblx0XHR9KTtcblx0fVxuXHRcblx0aGFuZGxlU3VibWlzc2lvbigpIHtcblx0XHR0aGlzLnN0b3JlLmRpc3BhdGNoKG5ldyBVcGRhdGVBY3Rpb25zT25UYXNrRm9ybSh7XG5cdFx0XHRwYWdlOiB0aGlzLnByb3BzLnBhZ2UsXG5cdFx0XHRhY3Rpb25OYW1lOiAnc3VibWl0dGVkJyxcblx0XHRcdGFjdGlvblZhbHVlOiB0cnVlXG5cdFx0fSkpO1xuXHRcdFxuXHRcdGlmICh0aGlzLnByb3BzLnBhZ2UgPT09ICd0YXNrbGlzdC9lZGl0YXInKSB7XG5cdFx0XHRpZiAodGhpcy5wcm9wcy50YXNrLnR5cGUgPT09ICdjcmVkaXRvJykge1xuXHRcdFx0XHR0aGlzLnByb3BzLmZvcm0uZm9ybUdyb3VwLmdldCgndmFyaWFibGVzLmFwcm92YWRvVDMudmFsdWUnKS5zZXRWYWx1ZSgnc2ltJyk7XG5cdFx0XHR9IGVsc2UgaWYgKHRoaXMucHJvcHMudGFzay50eXBlID09PSAnY29tZXJjaWFsJykge1xuXHRcdFx0XHR0aGlzLnByb3BzLmZvcm0uZm9ybUdyb3VwLmdldCgndmFyaWFibGVzLmFwcm92YWRvVDQudmFsdWUnKS5zZXRWYWx1ZSgnc2ltJyk7XG5cdFx0XHR9XG5cdFx0fVxuXHRcdFxuXHRcdGlmICh0aGlzLnByb3BzLmZvcm0uZm9ybUdyb3VwLnZhbGlkKSB7XG5cdFx0XHR0aGlzLnByb3BzLmZvcm0uZm9ybUdyb3VwLmdldCgndmFyaWFibGVzLmRhdGFWaXNpdGEudmFsdWUnKS5zZXRWYWx1ZShmb3JtYXREYXRlVG9BcGlGb3JtYXQodGhpcy5wcm9wcy5mb3JtLmZvcm1Hcm91cC52YWx1ZS52YXJpYWJsZXMuZGF0YVZpc2l0YS52YWx1ZSkpO1xuXHRcdFx0XG5cdFx0XHR0aGlzLnN1Ym1pc3Npb24uZW1pdCh0aGlzLnByb3BzLmZvcm0uZm9ybUdyb3VwKTtcblx0XHR9XG5cdH1cbn1cbiJdfQ==