/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TaskFormComponent } from './task-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { InputModule, ButtonModule } from 'sofisa-ui';
export class TaskFormModule {
}
TaskFormModule.decorators = [
    { type: NgModule, args: [{
                declarations: [TaskFormComponent],
                imports: [
                    CommonModule,
                    InputModule,
                    ButtonModule,
                    ReactiveFormsModule
                ],
                exports: [TaskFormComponent]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFzay1mb3JtLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3NvZmlzYS1icG0vIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy90YXNrLWZvcm0vdGFzay1mb3JtLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFDLFFBQVEsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUN2QyxPQUFPLEVBQUMsWUFBWSxFQUFDLE1BQU0saUJBQWlCLENBQUM7QUFDN0MsT0FBTyxFQUFDLGlCQUFpQixFQUFDLE1BQU0sdUJBQXVCLENBQUM7QUFDeEQsT0FBTyxFQUFDLG1CQUFtQixFQUFDLE1BQU0sZ0JBQWdCLENBQUM7QUFDbkQsT0FBTyxFQUFDLFdBQVcsRUFBRSxZQUFZLEVBQUMsTUFBTSxXQUFXLENBQUM7QUFhcEQsTUFBTSxPQUFPLGNBQWM7OztZQVgxQixRQUFRLFNBQUM7Z0JBQ1QsWUFBWSxFQUFFLENBQUMsaUJBQWlCLENBQUM7Z0JBQ2pDLE9BQU8sRUFBRTtvQkFDUixZQUFZO29CQUNaLFdBQVc7b0JBQ1gsWUFBWTtvQkFDWixtQkFBbUI7aUJBQ25CO2dCQUNELE9BQU8sRUFBRSxDQUFDLGlCQUFpQixDQUFDO2FBQzVCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtOZ01vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0NvbW1vbk1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7VGFza0Zvcm1Db21wb25lbnR9IGZyb20gJy4vdGFzay1mb3JtLmNvbXBvbmVudCc7XG5pbXBvcnQge1JlYWN0aXZlRm9ybXNNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCB7SW5wdXRNb2R1bGUsIEJ1dHRvbk1vZHVsZX0gZnJvbSAnc29maXNhLXVpJztcblxuQE5nTW9kdWxlKHtcblx0ZGVjbGFyYXRpb25zOiBbVGFza0Zvcm1Db21wb25lbnRdLFxuXHRpbXBvcnRzOiBbXG5cdFx0Q29tbW9uTW9kdWxlLFxuXHRcdElucHV0TW9kdWxlLFxuXHRcdEJ1dHRvbk1vZHVsZSxcblx0XHRSZWFjdGl2ZUZvcm1zTW9kdWxlXG5cdF0sXG5cdGV4cG9ydHM6IFtUYXNrRm9ybUNvbXBvbmVudF1cbn0pXG5cbmV4cG9ydCBjbGFzcyBUYXNrRm9ybU1vZHVsZSB7fVxuIl19