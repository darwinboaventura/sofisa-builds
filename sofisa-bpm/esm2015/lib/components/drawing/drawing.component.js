/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import BpmnJS from 'bpmn-js';
import { BpmService } from '../../services/bpm/bpm.service';
import { Component, Input } from '@angular/core';
export class DrawingComponent {
    /**
     * @param {?} bpmService
     */
    constructor(bpmService) {
        this.bpmService = bpmService;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.viewer = new BpmnJS({
            container: '#viewer',
            width: this.width,
            height: this.height
        });
        this.viewer.importXML(this.diagram, (err) => {
            if (err) {
                console.log('XML error:', err);
            }
        });
    }
}
DrawingComponent.decorators = [
    { type: Component, args: [{
                selector: 'so-drawing',
                template: "<div id=\"viewer\"></div>",
                styles: ["#container .bjs-powered-by{display:none!important}"]
            }] }
];
/** @nocollapse */
DrawingComponent.ctorParameters = () => [
    { type: BpmService }
];
DrawingComponent.propDecorators = {
    width: [{ type: Input }],
    height: [{ type: Input }],
    diagram: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    DrawingComponent.prototype.width;
    /** @type {?} */
    DrawingComponent.prototype.height;
    /** @type {?} */
    DrawingComponent.prototype.diagram;
    /** @type {?} */
    DrawingComponent.prototype.viewer;
    /** @type {?} */
    DrawingComponent.prototype.bpmService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHJhd2luZy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9zb2Zpc2EtYnBtLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvZHJhd2luZy9kcmF3aW5nLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxNQUFNLE1BQU0sU0FBUyxDQUFDO0FBQzdCLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSxnQ0FBZ0MsQ0FBQztBQUMxRCxPQUFPLEVBQUMsU0FBUyxFQUFFLEtBQUssRUFBUyxNQUFNLGVBQWUsQ0FBQztBQVF2RCxNQUFNLE9BQU8sZ0JBQWdCOzs7O0lBTzVCLFlBQW1CLFVBQXNCO1FBQXRCLGVBQVUsR0FBVixVQUFVLENBQVk7SUFBRyxDQUFDOzs7O0lBRTdDLFFBQVE7UUFDUCxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksTUFBTSxDQUFDO1lBQ3hCLFNBQVMsRUFBRSxTQUFTO1lBQ3BCLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSztZQUNqQixNQUFNLEVBQUUsSUFBSSxDQUFDLE1BQU07U0FDbkIsQ0FBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDLEdBQVEsRUFBRSxFQUFFO1lBQ2hELElBQUksR0FBRyxFQUFFO2dCQUNSLE9BQU8sQ0FBQyxHQUFHLENBQUMsWUFBWSxFQUFFLEdBQUcsQ0FBQyxDQUFDO2FBQy9CO1FBQ0YsQ0FBQyxDQUFDLENBQUM7SUFDSixDQUFDOzs7WUEzQkQsU0FBUyxTQUFDO2dCQUNWLFFBQVEsRUFBRSxZQUFZO2dCQUN0QixxQ0FBdUM7O2FBRXZDOzs7O1lBUE8sVUFBVTs7O29CQVVoQixLQUFLO3FCQUNMLEtBQUs7c0JBQ0wsS0FBSzs7OztJQUZOLGlDQUFvQjs7SUFDcEIsa0NBQXFCOztJQUNyQixtQ0FBc0I7O0lBRXRCLGtDQUFtQjs7SUFFUCxzQ0FBNkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgQnBtbkpTIGZyb20gJ2JwbW4tanMnO1xuaW1wb3J0IHtCcG1TZXJ2aWNlfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9icG0vYnBtLnNlcnZpY2UnO1xuaW1wb3J0IHtDb21wb25lbnQsIElucHV0LCBPbkluaXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcblx0c2VsZWN0b3I6ICdzby1kcmF3aW5nJyxcblx0dGVtcGxhdGVVcmw6ICcuL2RyYXdpbmcuY29tcG9uZW50Lmh0bWwnLFxuXHRzdHlsZVVybHM6IFsnLi9kcmF3aW5nLmNvbXBvbmVudC5zY3NzJ11cbn0pXG5cbmV4cG9ydCBjbGFzcyBEcmF3aW5nQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblx0QElucHV0KCkgd2lkdGg6IGFueTtcblx0QElucHV0KCkgaGVpZ2h0OiBhbnk7XG5cdEBJbnB1dCgpIGRpYWdyYW06IGFueTtcblx0XG5cdHB1YmxpYyB2aWV3ZXI6IGFueTtcblx0XG5cdGNvbnN0cnVjdG9yKHB1YmxpYyBicG1TZXJ2aWNlOiBCcG1TZXJ2aWNlKSB7fVxuXHRcblx0bmdPbkluaXQoKSB7XG5cdFx0dGhpcy52aWV3ZXIgPSBuZXcgQnBtbkpTKHtcblx0XHRcdGNvbnRhaW5lcjogJyN2aWV3ZXInLFxuXHRcdFx0d2lkdGg6IHRoaXMud2lkdGgsXG5cdFx0XHRoZWlnaHQ6IHRoaXMuaGVpZ2h0XG5cdFx0fSk7XG5cdFx0XG5cdFx0dGhpcy52aWV3ZXIuaW1wb3J0WE1MKHRoaXMuZGlhZ3JhbSwgKGVycjogYW55KSA9PiB7XG5cdFx0XHRpZiAoZXJyKSB7XG5cdFx0XHRcdGNvbnNvbGUubG9nKCdYTUwgZXJyb3I6JywgZXJyKTtcblx0XHRcdH1cblx0XHR9KTtcblx0fVxufVxuIl19