/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TasklistComponent } from './tasklist.component';
export class TasklistModule {
}
TasklistModule.decorators = [
    { type: NgModule, args: [{
                declarations: [TasklistComponent],
                imports: [
                    CommonModule,
                    RouterModule
                ],
                exports: [TasklistComponent]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFza2xpc3QubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vc29maXNhLWJwbS8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL3Rhc2tsaXN0L3Rhc2tsaXN0Lm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFDLFFBQVEsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUN2QyxPQUFPLEVBQUMsWUFBWSxFQUFDLE1BQU0saUJBQWlCLENBQUM7QUFDN0MsT0FBTyxFQUFDLFlBQVksRUFBQyxNQUFNLGlCQUFpQixDQUFDO0FBQzdDLE9BQU8sRUFBQyxpQkFBaUIsRUFBQyxNQUFNLHNCQUFzQixDQUFDO0FBV3ZELE1BQU0sT0FBTyxjQUFjOzs7WUFUMUIsUUFBUSxTQUFDO2dCQUNULFlBQVksRUFBRSxDQUFDLGlCQUFpQixDQUFDO2dCQUNqQyxPQUFPLEVBQUU7b0JBQ1IsWUFBWTtvQkFDWixZQUFZO2lCQUNaO2dCQUNELE9BQU8sRUFBRSxDQUFDLGlCQUFpQixDQUFDO2FBQzVCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtOZ01vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0NvbW1vbk1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7Um91dGVyTW9kdWxlfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuaW1wb3J0IHtUYXNrbGlzdENvbXBvbmVudH0gZnJvbSAnLi90YXNrbGlzdC5jb21wb25lbnQnO1xuXG5ATmdNb2R1bGUoe1xuXHRkZWNsYXJhdGlvbnM6IFtUYXNrbGlzdENvbXBvbmVudF0sXG5cdGltcG9ydHM6IFtcblx0XHRDb21tb25Nb2R1bGUsXG5cdFx0Um91dGVyTW9kdWxlXG5cdF0sXG5cdGV4cG9ydHM6IFtUYXNrbGlzdENvbXBvbmVudF1cbn0pXG5cbmV4cG9ydCBjbGFzcyBUYXNrbGlzdE1vZHVsZSB7fVxuIl19