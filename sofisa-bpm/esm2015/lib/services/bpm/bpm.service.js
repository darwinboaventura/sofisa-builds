/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
export class BpmService {
    /**
     * @param {?} http
     */
    constructor(http) {
        this.http = http;
        // TODO: Mover para arquivos de configurações
        this.apiUri = '//sofisa-lab-gateway.eastus2.cloudapp.azure.com';
    }
    /**
     * @param {?} id
     * @param {?} userId
     * @return {?}
     */
    claimTaskById(id, userId) {
        return this.http.post(`${this.apiUri}/api/bpm-api/bpm-pj-digital/task/${id}/claim`, {
            userId: userId
        });
    }
    /**
     * @param {?} id
     * @param {?} values
     * @return {?}
     */
    completeTaskById(id, values) {
        return this.http.post(`${this.apiUri}/api/bpm-api/bpm-pj-digital/task/${id}/complete`, values);
    }
    /**
     * @param {?} id
     * @return {?}
     */
    getTaskById(id) {
        return this.http.get(`${this.apiUri}/api/bpm-api/bpm-pj-digital/task/${id}`);
    }
    /**
     * @param {?} id
     * @return {?}
     */
    getTaskByIdVariables(id) {
        return this.http.get(`${this.apiUri}/api/bpm-api/bpm-pj-digital/task/${id}/variables`);
    }
    /**
     * @return {?}
     */
    getTasks() {
        return this.http.get('${this.apiUri}/api/bpm-api/bpm-pj-digital/task');
    }
    /**
     * @param {?} id
     * @return {?}
     */
    getTaskDiagram(id) {
        return this.http.get(`${this.apiUri}/api/bpm-api/bpm-pj-digital/engine/default/process-definition/${id}/xml`);
    }
    /**
     * @param {?} value
     * @return {?}
     */
    startABpm(value) {
        return this.http.post('${this.apiUri}/api/bpm-api/bpm-pj-digital/process-definition/key/' +
            'Workflow_PJ_Digital/start', value);
    }
}
BpmService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
BpmService.ctorParameters = () => [
    { type: HttpClient }
];
/** @nocollapse */ BpmService.ngInjectableDef = i0.defineInjectable({ factory: function BpmService_Factory() { return new BpmService(i0.inject(i1.HttpClient)); }, token: BpmService, providedIn: "root" });
if (false) {
    /** @type {?} */
    BpmService.prototype.apiUri;
    /** @type {?} */
    BpmService.prototype.http;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnBtLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9zb2Zpc2EtYnBtLyIsInNvdXJjZXMiOlsibGliL3NlcnZpY2VzL2JwbS9icG0uc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFDLFVBQVUsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUMsVUFBVSxFQUFDLE1BQU0sc0JBQXNCLENBQUM7OztBQU1oRCxNQUFNLE9BQU8sVUFBVTs7OztJQUl0QixZQUFtQixJQUFnQjtRQUFoQixTQUFJLEdBQUosSUFBSSxDQUFZOztRQUZuQyxXQUFNLEdBQVcsaURBQWlELENBQUM7SUFFN0IsQ0FBQzs7Ozs7O0lBRXZDLGFBQWEsQ0FBQyxFQUFPLEVBQUUsTUFBVztRQUNqQyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sb0NBQW9DLEVBQUUsUUFBUSxFQUFFO1lBQ25GLE1BQU0sRUFBRSxNQUFNO1NBQ2QsQ0FBQyxDQUFDO0lBQ0osQ0FBQzs7Ozs7O0lBRUQsZ0JBQWdCLENBQUMsRUFBTyxFQUFFLE1BQVc7UUFDcEMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLG9DQUFvQyxFQUFFLFdBQVcsRUFBRSxNQUFNLENBQUMsQ0FBQztJQUNoRyxDQUFDOzs7OztJQUVELFdBQVcsQ0FBQyxFQUFPO1FBQ2xCLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxvQ0FBb0MsRUFBRSxFQUFFLENBQUMsQ0FBQztJQUM5RSxDQUFDOzs7OztJQUVELG9CQUFvQixDQUFDLEVBQU87UUFDM0IsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLG9DQUFvQyxFQUFFLFlBQVksQ0FBQyxDQUFDO0lBQ3hGLENBQUM7Ozs7SUFFRCxRQUFRO1FBQ1AsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxnREFBZ0QsQ0FBQyxDQUFDO0lBQ3hFLENBQUM7Ozs7O0lBRUQsY0FBYyxDQUFDLEVBQU87UUFDckIsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLGlFQUFpRSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQy9HLENBQUM7Ozs7O0lBRUQsU0FBUyxDQUFDLEtBQVU7UUFDbkIsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxtRUFBbUU7WUFDeEYsMkJBQTJCLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDdEMsQ0FBQzs7O1lBdkNELFVBQVUsU0FBQztnQkFDWCxVQUFVLEVBQUUsTUFBTTthQUNsQjs7OztZQUpPLFVBQVU7Ozs7O0lBUWpCLDRCQUFtRTs7SUFFdkQsMEJBQXVCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtJbmplY3RhYmxlfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7SHR0cENsaWVudH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuXG5ASW5qZWN0YWJsZSh7XG5cdHByb3ZpZGVkSW46ICdyb290J1xufSlcblxuZXhwb3J0IGNsYXNzIEJwbVNlcnZpY2Uge1xuXHQvLyBUT0RPOiBNb3ZlciBwYXJhIGFycXVpdm9zIGRlIGNvbmZpZ3VyYcOnw7Vlc1xuXHRhcGlVcmk6IFN0cmluZyA9ICcvL3NvZmlzYS1sYWItZ2F0ZXdheS5lYXN0dXMyLmNsb3VkYXBwLmF6dXJlLmNvbSc7XG5cblx0Y29uc3RydWN0b3IocHVibGljIGh0dHA6IEh0dHBDbGllbnQpIHt9XG5cdFxuXHRjbGFpbVRhc2tCeUlkKGlkOiBhbnksIHVzZXJJZDogYW55KSB7XG5cdFx0cmV0dXJuIHRoaXMuaHR0cC5wb3N0KGAke3RoaXMuYXBpVXJpfS9hcGkvYnBtLWFwaS9icG0tcGotZGlnaXRhbC90YXNrLyR7aWR9L2NsYWltYCwge1xuXHRcdFx0dXNlcklkOiB1c2VySWRcblx0XHR9KTtcblx0fVxuXHRcblx0Y29tcGxldGVUYXNrQnlJZChpZDogYW55LCB2YWx1ZXM6IGFueSkge1xuXHRcdHJldHVybiB0aGlzLmh0dHAucG9zdChgJHt0aGlzLmFwaVVyaX0vYXBpL2JwbS1hcGkvYnBtLXBqLWRpZ2l0YWwvdGFzay8ke2lkfS9jb21wbGV0ZWAsIHZhbHVlcyk7XG5cdH1cblx0XG5cdGdldFRhc2tCeUlkKGlkOiBhbnkpIHtcblx0XHRyZXR1cm4gdGhpcy5odHRwLmdldChgJHt0aGlzLmFwaVVyaX0vYXBpL2JwbS1hcGkvYnBtLXBqLWRpZ2l0YWwvdGFzay8ke2lkfWApO1xuXHR9XG5cdFxuXHRnZXRUYXNrQnlJZFZhcmlhYmxlcyhpZDogYW55KSB7XG5cdFx0cmV0dXJuIHRoaXMuaHR0cC5nZXQoYCR7dGhpcy5hcGlVcml9L2FwaS9icG0tYXBpL2JwbS1wai1kaWdpdGFsL3Rhc2svJHtpZH0vdmFyaWFibGVzYCk7XG5cdH1cblx0XG5cdGdldFRhc2tzKCkge1xuXHRcdHJldHVybiB0aGlzLmh0dHAuZ2V0KCcke3RoaXMuYXBpVXJpfS9hcGkvYnBtLWFwaS9icG0tcGotZGlnaXRhbC90YXNrJyk7XG5cdH1cblx0XG5cdGdldFRhc2tEaWFncmFtKGlkOiBhbnkpIHtcblx0XHRyZXR1cm4gdGhpcy5odHRwLmdldChgJHt0aGlzLmFwaVVyaX0vYXBpL2JwbS1hcGkvYnBtLXBqLWRpZ2l0YWwvZW5naW5lL2RlZmF1bHQvcHJvY2Vzcy1kZWZpbml0aW9uLyR7aWR9L3htbGApO1xuXHR9XG5cdFxuXHRzdGFydEFCcG0odmFsdWU6IGFueSkge1xuXHRcdHJldHVybiB0aGlzLmh0dHAucG9zdCgnJHt0aGlzLmFwaVVyaX0vYXBpL2JwbS1hcGkvYnBtLXBqLWRpZ2l0YWwvcHJvY2Vzcy1kZWZpbml0aW9uL2tleS8nICtcblx0XHRcdCdXb3JrZmxvd19QSl9EaWdpdGFsL3N0YXJ0JywgdmFsdWUpO1xuXHR9XG59XG4iXX0=