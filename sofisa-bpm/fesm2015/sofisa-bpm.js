import { RouterModule } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { LoaderService } from 'sofisa-loader';
import { NotificationService } from 'sofisa-notification';
import { UpdateActionsOnTaskForm, formatDateToApiFormat, handleFieldChange } from 'sofisa-core';
import { ReactiveFormsModule } from '@angular/forms';
import { InputModule, ButtonModule } from 'sofisa-ui';
import { CommonModule } from '@angular/common';
import BpmnJS from 'bpmn-js';
import { HttpClient } from '@angular/common/http';
import { Component, Input, NgModule, EventEmitter, Output, Injectable, defineInjectable, inject } from '@angular/core';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class TasklistComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() { }
}
TasklistComponent.decorators = [
    { type: Component, args: [{
                selector: 'so-tasklist',
                template: "<div class=\"so-tasklist clearfix\" *ngIf=\"props.tasks\">\n\t<div class=\"so-tasklist--head clearfix\">\n\t\t<div class=\"so-tasklist--column clearfix\">Nome</div>\n\t\t<div class=\"so-tasklist--column clearfix\">Data de cria\u00E7\u00E3o</div>\n\t\t<div class=\"so-tasklist--column clearfix\">Clamado por</div>\n\t\t<div class=\"so-tasklist--column clearfix\">A\u00E7\u00F5es</div>\n\t</div>\n\t\n\t<div class=\"so-tasklist--body clearfix\">\n\t\t<div class=\"so-tasklist--row clearfix\" *ngFor=\"let task of props.tasks\">\n\t\t\t<div class=\"so-tasklist--column clearfix\">{{task.name}}</div>\n\t\t\t<div class=\"so-tasklist--column clearfix\">{{task.created | date:'dd/MM/y'}}</div>\n\t\t\t<div class=\"so-tasklist--column clearfix\" *ngIf=\"task.assignee\">{{task.assignee}}</div>\n\t\t\t<div class=\"so-tasklist--column clearfix\" *ngIf=\"!task.assignee\">Ningu\u00E9m</div>\n\t\t\t<div class=\"so-tasklist--column clearfix\">\n\t\t\t\t<a [routerLink]=\"[ '/tasklist/editar', task.id ]\" class=\"btn btn-edit\">Editar</a>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>",
                styles: [".so-tasklist{width:100%}.so-tasklist--head,.so-tasklist--row{display:flex;flex-direction:row;flex-wrap:wrap;align-items:stretch}.so-tasklist--head .so-tasklist--column,.so-tasklist--row .so-tasklist--column{width:25%}.so-tasklist--head{padding:0 27px}.so-tasklist--head .so-tasklist--column{font-family:Roboto;font-size:12px;color:#90a4ae;letter-spacing:.5px;line-height:14px;text-transform:uppercase}.so-tasklist--body{margin-top:12px;box-shadow:0 4px 9px 0 rgba(0,0,0,.1)}.so-tasklist--body .so-tasklist--row{padding:24px 27px;border:1px solid #e9eff4;background:#fff}.so-tasklist--body .so-tasklist--column{font-family:Roboto;font-size:14px;font-weight:500;color:#31393c;letter-spacing:-.07px;line-height:16px}.so-tasklist--body .so-tasklist--column .btn{font-family:Roboto;font-weight:500;font-size:12px;color:#1dec3a;letter-spacing:.06px;line-height:14px;padding:2px 10px;display:table;border-radius:2px;text-decoration:none;text-transform:uppercase;background-color:rgba(29,236,58,.1)}"]
            }] }
];
/** @nocollapse */
TasklistComponent.ctorParameters = () => [];
TasklistComponent.propDecorators = {
    props: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class TasklistModule {
}
TasklistModule.decorators = [
    { type: NgModule, args: [{
                declarations: [TasklistComponent],
                imports: [
                    CommonModule,
                    RouterModule
                ],
                exports: [TasklistComponent]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class TaskFormComponent {
    /**
     * @param {?} store
     * @param {?} loaderService
     * @param {?} notificationService
     */
    constructor(store, loaderService, notificationService) {
        this.store = store;
        this.loaderService = loaderService;
        this.notificationService = notificationService;
        this.handleFieldChange = handleFieldChange;
        this.submission = new EventEmitter();
        this.claim = new EventEmitter();
        this.store$ = this.store.pipe(select('pages'));
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.store$.subscribe((store) => {
            this.props.form.validations = store.pages[this.props.page].form.validations;
            this.props.form.actions = store.pages[this.props.page].form.actions;
        });
    }
    /**
     * @return {?}
     */
    ngOnChanges() {
        this.props.form.formGroup.updateValueAndValidity();
    }
    /**
     * @return {?}
     */
    handleClaim() {
        this.claim.emit({
            taskId: this.props.task.id,
            userId: this.props.user.id,
        });
    }
    /**
     * @return {?}
     */
    handleSubmission() {
        this.store.dispatch(new UpdateActionsOnTaskForm({
            page: this.props.page,
            actionName: 'submitted',
            actionValue: true
        }));
        if (this.props.page === 'tasklist/editar') {
            if (this.props.task.type === 'credito') {
                this.props.form.formGroup.get('variables.aprovadoT3.value').setValue('sim');
            }
            else if (this.props.task.type === 'comercial') {
                this.props.form.formGroup.get('variables.aprovadoT4.value').setValue('sim');
            }
        }
        if (this.props.form.formGroup.valid) {
            this.props.form.formGroup.get('variables.dataVisita.value').setValue(formatDateToApiFormat(this.props.form.formGroup.value.variables.dataVisita.value));
            this.submission.emit(this.props.form.formGroup);
        }
    }
}
TaskFormComponent.decorators = [
    { type: Component, args: [{
                selector: 'so-task-form',
                template: "<form class=\"so-pg-task-novo--form clearfix\" [formGroup]=\"props.form.formGroup\">\n\t<so-input label=\"Cliente\" placeholder=\"Digite aqui o nome do cliente\" [value]=\"props.form.formGroup.get('variables.cliente.value').value\" [inputConfig]=\"{ validation: { type: props.form.validations.cliente.type, message: props.form.validations.cliente.message} }\" (hasChanged)=\"handleFieldChange(props.form.formGroup, store, props.page, $event, 'cliente')\"></so-input>\n\t\n\t<so-input label=\"CNPJ\" placeholder=\"Digite aqui seu CNPJ\" [value]=\"props.form.formGroup.get('variables.cnpj.value').value\" [inputConfig]=\"{ validation: { type: props.form.validations.cnpj.type, message: props.form.validations.cnpj.message} }\" (hasChanged)=\"handleFieldChange(props.form.formGroup, store, props.page, $event, 'cnpj')\"></so-input>\n\t\n\t<so-input label=\"Data da visita\" inputType=\"date\" placeholder=\"Qual a data da visita?\" [value]=\"props.form.formGroup.get('variables.dataVisita.value').value\" [inputConfig]=\"{ validation: { type: props.form.validations.dataVisita.type, message: props.form.validations.dataVisita.message} }\" (hasChanged)=\"handleFieldChange(props.form.formGroup, store, props.page, $event, 'dataVisita')\"></so-input>\n\t\n\t<so-button text=\"Criar task\" (hasClicked)=\"handleSubmission()\" *ngIf=\"props.page === 'tasklist/novo'\"></so-button>\n\t\n\t<so-button text=\"Reivindicar\" *ngIf=\"props.page === 'tasklist/editar' && !props.task.claimed\" (hasClicked)=\"handleClaim()\"></so-button>\n\t\n\t<so-button text=\"Aprovar\" *ngIf=\"props.page === 'tasklist/editar' && props.task.claimed\" (hasClicked)=\"handleSubmission()\"></so-button>\n</form>",
                styles: [""]
            }] }
];
/** @nocollapse */
TaskFormComponent.ctorParameters = () => [
    { type: Store },
    { type: LoaderService },
    { type: NotificationService }
];
TaskFormComponent.propDecorators = {
    props: [{ type: Input }],
    submission: [{ type: Output }],
    claim: [{ type: Output }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class TaskFormModule {
}
TaskFormModule.decorators = [
    { type: NgModule, args: [{
                declarations: [TaskFormComponent],
                imports: [
                    CommonModule,
                    InputModule,
                    ButtonModule,
                    ReactiveFormsModule
                ],
                exports: [TaskFormComponent]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BpmService {
    /**
     * @param {?} http
     */
    constructor(http) {
        this.http = http;
        // TODO: Mover para arquivos de configurações
        this.apiUri = '//sofisa-lab-gateway.eastus2.cloudapp.azure.com';
    }
    /**
     * @param {?} id
     * @param {?} userId
     * @return {?}
     */
    claimTaskById(id, userId) {
        return this.http.post(`${this.apiUri}/api/bpm-api/bpm-pj-digital/task/${id}/claim`, {
            userId: userId
        });
    }
    /**
     * @param {?} id
     * @param {?} values
     * @return {?}
     */
    completeTaskById(id, values) {
        return this.http.post(`${this.apiUri}/api/bpm-api/bpm-pj-digital/task/${id}/complete`, values);
    }
    /**
     * @param {?} id
     * @return {?}
     */
    getTaskById(id) {
        return this.http.get(`${this.apiUri}/api/bpm-api/bpm-pj-digital/task/${id}`);
    }
    /**
     * @param {?} id
     * @return {?}
     */
    getTaskByIdVariables(id) {
        return this.http.get(`${this.apiUri}/api/bpm-api/bpm-pj-digital/task/${id}/variables`);
    }
    /**
     * @return {?}
     */
    getTasks() {
        return this.http.get('${this.apiUri}/api/bpm-api/bpm-pj-digital/task');
    }
    /**
     * @param {?} id
     * @return {?}
     */
    getTaskDiagram(id) {
        return this.http.get(`${this.apiUri}/api/bpm-api/bpm-pj-digital/engine/default/process-definition/${id}/xml`);
    }
    /**
     * @param {?} value
     * @return {?}
     */
    startABpm(value) {
        return this.http.post('${this.apiUri}/api/bpm-api/bpm-pj-digital/process-definition/key/' +
            'Workflow_PJ_Digital/start', value);
    }
}
BpmService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
BpmService.ctorParameters = () => [
    { type: HttpClient }
];
/** @nocollapse */ BpmService.ngInjectableDef = defineInjectable({ factory: function BpmService_Factory() { return new BpmService(inject(HttpClient)); }, token: BpmService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class DrawingComponent {
    /**
     * @param {?} bpmService
     */
    constructor(bpmService) {
        this.bpmService = bpmService;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.viewer = new BpmnJS({
            container: '#viewer',
            width: this.width,
            height: this.height
        });
        this.viewer.importXML(this.diagram, (err) => {
            if (err) {
                console.log('XML error:', err);
            }
        });
    }
}
DrawingComponent.decorators = [
    { type: Component, args: [{
                selector: 'so-drawing',
                template: "<div id=\"viewer\"></div>",
                styles: ["#container .bjs-powered-by{display:none!important}"]
            }] }
];
/** @nocollapse */
DrawingComponent.ctorParameters = () => [
    { type: BpmService }
];
DrawingComponent.propDecorators = {
    width: [{ type: Input }],
    height: [{ type: Input }],
    diagram: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class DrawingModule {
}
DrawingModule.decorators = [
    { type: NgModule, args: [{
                declarations: [DrawingComponent],
                imports: [
                    CommonModule
                ],
                exports: [DrawingComponent]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { TasklistModule, TaskFormModule, DrawingModule, BpmService, DrawingComponent as ɵc, TaskFormComponent as ɵb, TasklistComponent as ɵa };

//# sourceMappingURL=sofisa-bpm.js.map