import { RouterModule } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { LoaderService } from 'sofisa-loader';
import { NotificationService } from 'sofisa-notification';
import { UpdateActionsOnTaskForm, formatDateToApiFormat, handleFieldChange } from 'sofisa-core';
import { ReactiveFormsModule } from '@angular/forms';
import { InputModule, ButtonModule } from 'sofisa-ui';
import { CommonModule } from '@angular/common';
import BpmnJS from 'bpmn-js';
import { HttpClient } from '@angular/common/http';
import { Component, Input, NgModule, EventEmitter, Output, Injectable, defineInjectable, inject } from '@angular/core';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var TasklistComponent = /** @class */ (function () {
    function TasklistComponent() {
    }
    /**
     * @return {?}
     */
    TasklistComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () { };
    TasklistComponent.decorators = [
        { type: Component, args: [{
                    selector: 'so-tasklist',
                    template: "<div class=\"so-tasklist clearfix\" *ngIf=\"props.tasks\">\n\t<div class=\"so-tasklist--head clearfix\">\n\t\t<div class=\"so-tasklist--column clearfix\">Nome</div>\n\t\t<div class=\"so-tasklist--column clearfix\">Data de cria\u00E7\u00E3o</div>\n\t\t<div class=\"so-tasklist--column clearfix\">Clamado por</div>\n\t\t<div class=\"so-tasklist--column clearfix\">A\u00E7\u00F5es</div>\n\t</div>\n\t\n\t<div class=\"so-tasklist--body clearfix\">\n\t\t<div class=\"so-tasklist--row clearfix\" *ngFor=\"let task of props.tasks\">\n\t\t\t<div class=\"so-tasklist--column clearfix\">{{task.name}}</div>\n\t\t\t<div class=\"so-tasklist--column clearfix\">{{task.created | date:'dd/MM/y'}}</div>\n\t\t\t<div class=\"so-tasklist--column clearfix\" *ngIf=\"task.assignee\">{{task.assignee}}</div>\n\t\t\t<div class=\"so-tasklist--column clearfix\" *ngIf=\"!task.assignee\">Ningu\u00E9m</div>\n\t\t\t<div class=\"so-tasklist--column clearfix\">\n\t\t\t\t<a [routerLink]=\"[ '/tasklist/editar', task.id ]\" class=\"btn btn-edit\">Editar</a>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>",
                    styles: [".so-tasklist{width:100%}.so-tasklist--head,.so-tasklist--row{display:flex;flex-direction:row;flex-wrap:wrap;align-items:stretch}.so-tasklist--head .so-tasklist--column,.so-tasklist--row .so-tasklist--column{width:25%}.so-tasklist--head{padding:0 27px}.so-tasklist--head .so-tasklist--column{font-family:Roboto;font-size:12px;color:#90a4ae;letter-spacing:.5px;line-height:14px;text-transform:uppercase}.so-tasklist--body{margin-top:12px;box-shadow:0 4px 9px 0 rgba(0,0,0,.1)}.so-tasklist--body .so-tasklist--row{padding:24px 27px;border:1px solid #e9eff4;background:#fff}.so-tasklist--body .so-tasklist--column{font-family:Roboto;font-size:14px;font-weight:500;color:#31393c;letter-spacing:-.07px;line-height:16px}.so-tasklist--body .so-tasklist--column .btn{font-family:Roboto;font-weight:500;font-size:12px;color:#1dec3a;letter-spacing:.06px;line-height:14px;padding:2px 10px;display:table;border-radius:2px;text-decoration:none;text-transform:uppercase;background-color:rgba(29,236,58,.1)}"]
                }] }
    ];
    /** @nocollapse */
    TasklistComponent.ctorParameters = function () { return []; };
    TasklistComponent.propDecorators = {
        props: [{ type: Input }]
    };
    return TasklistComponent;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var TasklistModule = /** @class */ (function () {
    function TasklistModule() {
    }
    TasklistModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [TasklistComponent],
                    imports: [
                        CommonModule,
                        RouterModule
                    ],
                    exports: [TasklistComponent]
                },] }
    ];
    return TasklistModule;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var TaskFormComponent = /** @class */ (function () {
    function TaskFormComponent(store, loaderService, notificationService) {
        this.store = store;
        this.loaderService = loaderService;
        this.notificationService = notificationService;
        this.handleFieldChange = handleFieldChange;
        this.submission = new EventEmitter();
        this.claim = new EventEmitter();
        this.store$ = this.store.pipe(select('pages'));
    }
    /**
     * @return {?}
     */
    TaskFormComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.store$.subscribe(function (store) {
            _this.props.form.validations = store.pages[_this.props.page].form.validations;
            _this.props.form.actions = store.pages[_this.props.page].form.actions;
        });
    };
    /**
     * @return {?}
     */
    TaskFormComponent.prototype.ngOnChanges = /**
     * @return {?}
     */
    function () {
        this.props.form.formGroup.updateValueAndValidity();
    };
    /**
     * @return {?}
     */
    TaskFormComponent.prototype.handleClaim = /**
     * @return {?}
     */
    function () {
        this.claim.emit({
            taskId: this.props.task.id,
            userId: this.props.user.id,
        });
    };
    /**
     * @return {?}
     */
    TaskFormComponent.prototype.handleSubmission = /**
     * @return {?}
     */
    function () {
        this.store.dispatch(new UpdateActionsOnTaskForm({
            page: this.props.page,
            actionName: 'submitted',
            actionValue: true
        }));
        if (this.props.page === 'tasklist/editar') {
            if (this.props.task.type === 'credito') {
                this.props.form.formGroup.get('variables.aprovadoT3.value').setValue('sim');
            }
            else if (this.props.task.type === 'comercial') {
                this.props.form.formGroup.get('variables.aprovadoT4.value').setValue('sim');
            }
        }
        if (this.props.form.formGroup.valid) {
            this.props.form.formGroup.get('variables.dataVisita.value').setValue(formatDateToApiFormat(this.props.form.formGroup.value.variables.dataVisita.value));
            this.submission.emit(this.props.form.formGroup);
        }
    };
    TaskFormComponent.decorators = [
        { type: Component, args: [{
                    selector: 'so-task-form',
                    template: "<form class=\"so-pg-task-novo--form clearfix\" [formGroup]=\"props.form.formGroup\">\n\t<so-input label=\"Cliente\" placeholder=\"Digite aqui o nome do cliente\" [value]=\"props.form.formGroup.get('variables.cliente.value').value\" [inputConfig]=\"{ validation: { type: props.form.validations.cliente.type, message: props.form.validations.cliente.message} }\" (hasChanged)=\"handleFieldChange(props.form.formGroup, store, props.page, $event, 'cliente')\"></so-input>\n\t\n\t<so-input label=\"CNPJ\" placeholder=\"Digite aqui seu CNPJ\" [value]=\"props.form.formGroup.get('variables.cnpj.value').value\" [inputConfig]=\"{ validation: { type: props.form.validations.cnpj.type, message: props.form.validations.cnpj.message} }\" (hasChanged)=\"handleFieldChange(props.form.formGroup, store, props.page, $event, 'cnpj')\"></so-input>\n\t\n\t<so-input label=\"Data da visita\" inputType=\"date\" placeholder=\"Qual a data da visita?\" [value]=\"props.form.formGroup.get('variables.dataVisita.value').value\" [inputConfig]=\"{ validation: { type: props.form.validations.dataVisita.type, message: props.form.validations.dataVisita.message} }\" (hasChanged)=\"handleFieldChange(props.form.formGroup, store, props.page, $event, 'dataVisita')\"></so-input>\n\t\n\t<so-button text=\"Criar task\" (hasClicked)=\"handleSubmission()\" *ngIf=\"props.page === 'tasklist/novo'\"></so-button>\n\t\n\t<so-button text=\"Reivindicar\" *ngIf=\"props.page === 'tasklist/editar' && !props.task.claimed\" (hasClicked)=\"handleClaim()\"></so-button>\n\t\n\t<so-button text=\"Aprovar\" *ngIf=\"props.page === 'tasklist/editar' && props.task.claimed\" (hasClicked)=\"handleSubmission()\"></so-button>\n</form>",
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    TaskFormComponent.ctorParameters = function () { return [
        { type: Store },
        { type: LoaderService },
        { type: NotificationService }
    ]; };
    TaskFormComponent.propDecorators = {
        props: [{ type: Input }],
        submission: [{ type: Output }],
        claim: [{ type: Output }]
    };
    return TaskFormComponent;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var TaskFormModule = /** @class */ (function () {
    function TaskFormModule() {
    }
    TaskFormModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [TaskFormComponent],
                    imports: [
                        CommonModule,
                        InputModule,
                        ButtonModule,
                        ReactiveFormsModule
                    ],
                    exports: [TaskFormComponent]
                },] }
    ];
    return TaskFormModule;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var BpmService = /** @class */ (function () {
    function BpmService(http) {
        this.http = http;
        // TODO: Mover para arquivos de configurações
        this.apiUri = '//sofisa-lab-gateway.eastus2.cloudapp.azure.com';
    }
    /**
     * @param {?} id
     * @param {?} userId
     * @return {?}
     */
    BpmService.prototype.claimTaskById = /**
     * @param {?} id
     * @param {?} userId
     * @return {?}
     */
    function (id, userId) {
        return this.http.post(this.apiUri + "/api/bpm-api/bpm-pj-digital/task/" + id + "/claim", {
            userId: userId
        });
    };
    /**
     * @param {?} id
     * @param {?} values
     * @return {?}
     */
    BpmService.prototype.completeTaskById = /**
     * @param {?} id
     * @param {?} values
     * @return {?}
     */
    function (id, values) {
        return this.http.post(this.apiUri + "/api/bpm-api/bpm-pj-digital/task/" + id + "/complete", values);
    };
    /**
     * @param {?} id
     * @return {?}
     */
    BpmService.prototype.getTaskById = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
        return this.http.get(this.apiUri + "/api/bpm-api/bpm-pj-digital/task/" + id);
    };
    /**
     * @param {?} id
     * @return {?}
     */
    BpmService.prototype.getTaskByIdVariables = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
        return this.http.get(this.apiUri + "/api/bpm-api/bpm-pj-digital/task/" + id + "/variables");
    };
    /**
     * @return {?}
     */
    BpmService.prototype.getTasks = /**
     * @return {?}
     */
    function () {
        return this.http.get('${this.apiUri}/api/bpm-api/bpm-pj-digital/task');
    };
    /**
     * @param {?} id
     * @return {?}
     */
    BpmService.prototype.getTaskDiagram = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
        return this.http.get(this.apiUri + "/api/bpm-api/bpm-pj-digital/engine/default/process-definition/" + id + "/xml");
    };
    /**
     * @param {?} value
     * @return {?}
     */
    BpmService.prototype.startABpm = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        return this.http.post('${this.apiUri}/api/bpm-api/bpm-pj-digital/process-definition/key/' +
            'Workflow_PJ_Digital/start', value);
    };
    BpmService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    BpmService.ctorParameters = function () { return [
        { type: HttpClient }
    ]; };
    /** @nocollapse */ BpmService.ngInjectableDef = defineInjectable({ factory: function BpmService_Factory() { return new BpmService(inject(HttpClient)); }, token: BpmService, providedIn: "root" });
    return BpmService;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var DrawingComponent = /** @class */ (function () {
    function DrawingComponent(bpmService) {
        this.bpmService = bpmService;
    }
    /**
     * @return {?}
     */
    DrawingComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this.viewer = new BpmnJS({
            container: '#viewer',
            width: this.width,
            height: this.height
        });
        this.viewer.importXML(this.diagram, function (err) {
            if (err) {
                console.log('XML error:', err);
            }
        });
    };
    DrawingComponent.decorators = [
        { type: Component, args: [{
                    selector: 'so-drawing',
                    template: "<div id=\"viewer\"></div>",
                    styles: ["#container .bjs-powered-by{display:none!important}"]
                }] }
    ];
    /** @nocollapse */
    DrawingComponent.ctorParameters = function () { return [
        { type: BpmService }
    ]; };
    DrawingComponent.propDecorators = {
        width: [{ type: Input }],
        height: [{ type: Input }],
        diagram: [{ type: Input }]
    };
    return DrawingComponent;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var DrawingModule = /** @class */ (function () {
    function DrawingModule() {
    }
    DrawingModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [DrawingComponent],
                    imports: [
                        CommonModule
                    ],
                    exports: [DrawingComponent]
                },] }
    ];
    return DrawingModule;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { TasklistModule, TaskFormModule, DrawingModule, BpmService, DrawingComponent as ɵc, TaskFormComponent as ɵb, TasklistComponent as ɵa };

//# sourceMappingURL=sofisa-bpm.js.map