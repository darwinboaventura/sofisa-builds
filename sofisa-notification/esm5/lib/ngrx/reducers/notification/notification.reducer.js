/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NotificationTypes } from '../../actions/notifications/notifications.actions';
/** @type {?} */
export var notificationDefaultState = {
    'type': '',
    'message': '',
    'displaying': false
};
/**
 * @param {?=} state
 * @param {?=} action
 * @return {?}
 */
export function notificationReducer(state, action) {
    if (state === void 0) { state = notificationDefaultState; }
    switch (action.type) {
        case NotificationTypes.AddANotification:
            return Object.assign({}, action.payload, {
                displaying: true
            });
        case NotificationTypes.CleanNotification:
            return notificationDefaultState;
        default:
            return state;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9uLnJlZHVjZXIuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9zb2Zpc2Etbm90aWZpY2F0aW9uLyIsInNvdXJjZXMiOlsibGliL25ncngvcmVkdWNlcnMvbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5yZWR1Y2VyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsaUJBQWlCLEVBQUMsTUFBTSxtREFBbUQsQ0FBQzs7QUFFcEYsTUFBTSxLQUFPLHdCQUF3QixHQUFHO0lBQ3ZDLE1BQU0sRUFBRSxFQUFFO0lBQ1YsU0FBUyxFQUFFLEVBQUU7SUFDYixZQUFZLEVBQUUsS0FBSztDQUNuQjs7Ozs7O0FBRUQsTUFBTSxVQUFVLG1CQUFtQixDQUFDLEtBQWdDLEVBQUUsTUFBVztJQUE3QyxzQkFBQSxFQUFBLGdDQUFnQztJQUNuRSxRQUFRLE1BQU0sQ0FBQyxJQUFJLEVBQUU7UUFDcEIsS0FBSyxpQkFBaUIsQ0FBQyxnQkFBZ0I7WUFDdEMsT0FBTyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxNQUFNLENBQUMsT0FBTyxFQUFFO2dCQUN4QyxVQUFVLEVBQUUsSUFBSTthQUNoQixDQUFDLENBQUM7UUFDSixLQUFLLGlCQUFpQixDQUFDLGlCQUFpQjtZQUN2QyxPQUFPLHdCQUF3QixDQUFDO1FBQ2pDO1lBQ0MsT0FBTyxLQUFLLENBQUM7S0FDZDtBQUNGLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge05vdGlmaWNhdGlvblR5cGVzfSBmcm9tICcuLi8uLi9hY3Rpb25zL25vdGlmaWNhdGlvbnMvbm90aWZpY2F0aW9ucy5hY3Rpb25zJztcclxuXHJcbmV4cG9ydCBjb25zdCBub3RpZmljYXRpb25EZWZhdWx0U3RhdGUgPSB7XHJcblx0J3R5cGUnOiAnJyxcclxuXHQnbWVzc2FnZSc6ICcnLFxyXG5cdCdkaXNwbGF5aW5nJzogZmFsc2VcclxufTtcclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBub3RpZmljYXRpb25SZWR1Y2VyKHN0YXRlID0gbm90aWZpY2F0aW9uRGVmYXVsdFN0YXRlLCBhY3Rpb246IGFueSkge1xyXG5cdHN3aXRjaCAoYWN0aW9uLnR5cGUpIHtcclxuXHRcdGNhc2UgTm90aWZpY2F0aW9uVHlwZXMuQWRkQU5vdGlmaWNhdGlvbjpcclxuXHRcdFx0cmV0dXJuIE9iamVjdC5hc3NpZ24oe30sIGFjdGlvbi5wYXlsb2FkLCB7XHJcblx0XHRcdFx0ZGlzcGxheWluZzogdHJ1ZVxyXG5cdFx0XHR9KTtcclxuXHRcdGNhc2UgTm90aWZpY2F0aW9uVHlwZXMuQ2xlYW5Ob3RpZmljYXRpb246XHJcblx0XHRcdHJldHVybiBub3RpZmljYXRpb25EZWZhdWx0U3RhdGU7XHJcblx0XHRkZWZhdWx0OlxyXG5cdFx0XHRyZXR1cm4gc3RhdGU7XHJcblx0fVxyXG59XHJcblxyXG5cclxuIl19