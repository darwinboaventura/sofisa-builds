/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
var NotificationTypes = {
    AddANotification: '[AppComponent] AddANotification',
    CleanNotification: '[AppComponent] CleanNotification',
};
export { NotificationTypes };
var AddANotification = /** @class */ (function () {
    function AddANotification(payload) {
        this.payload = payload;
        this.type = NotificationTypes.AddANotification;
    }
    return AddANotification;
}());
export { AddANotification };
if (false) {
    /** @type {?} */
    AddANotification.prototype.type;
    /** @type {?} */
    AddANotification.prototype.payload;
}
var CleanNotification = /** @class */ (function () {
    function CleanNotification() {
        this.type = NotificationTypes.CleanNotification;
    }
    return CleanNotification;
}());
export { CleanNotification };
if (false) {
    /** @type {?} */
    CleanNotification.prototype.type;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9ucy5hY3Rpb25zLmpzIiwic291cmNlUm9vdCI6Im5nOi8vc29maXNhLW5vdGlmaWNhdGlvbi8iLCJzb3VyY2VzIjpbImxpYi9uZ3J4L2FjdGlvbnMvbm90aWZpY2F0aW9ucy9ub3RpZmljYXRpb25zLmFjdGlvbnMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0lBR0Msa0JBQW1CLGlDQUFpQztJQUNwRCxtQkFBb0Isa0NBQWtDOzs7QUFHdkQ7SUFHQywwQkFBbUIsT0FBbUM7UUFBbkMsWUFBTyxHQUFQLE9BQU8sQ0FBNEI7UUFGN0MsU0FBSSxHQUFHLGlCQUFpQixDQUFDLGdCQUFnQixDQUFDO0lBRU0sQ0FBQztJQUMzRCx1QkFBQztBQUFELENBQUMsQUFKRCxJQUlDOzs7O0lBSEEsZ0NBQW1EOztJQUV2QyxtQ0FBMEM7O0FBR3ZEO0lBQUE7UUFDVSxTQUFJLEdBQUcsaUJBQWlCLENBQUMsaUJBQWlCLENBQUM7SUFDckQsQ0FBQztJQUFELHdCQUFDO0FBQUQsQ0FBQyxBQUZELElBRUM7Ozs7SUFEQSxpQ0FBb0QiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0FjdGlvbn0gZnJvbSAnQG5ncngvc3RvcmUnO1xyXG5cclxuZXhwb3J0IGVudW0gTm90aWZpY2F0aW9uVHlwZXMge1xyXG5cdEFkZEFOb3RpZmljYXRpb24gPSAnW0FwcENvbXBvbmVudF0gQWRkQU5vdGlmaWNhdGlvbicsXHJcblx0Q2xlYW5Ob3RpZmljYXRpb24gPSAnW0FwcENvbXBvbmVudF0gQ2xlYW5Ob3RpZmljYXRpb24nXHJcbn1cclxuXHJcbmV4cG9ydCBjbGFzcyBBZGRBTm90aWZpY2F0aW9uIGltcGxlbWVudHMgQWN0aW9uIHtcclxuXHRyZWFkb25seSB0eXBlID0gTm90aWZpY2F0aW9uVHlwZXMuQWRkQU5vdGlmaWNhdGlvbjtcclxuXHRcclxuXHRjb25zdHJ1Y3RvcihwdWJsaWMgcGF5bG9hZDogeyB0eXBlOiBhbnksIG1lc3NhZ2U6IGFueX0pIHt9XHJcbn1cclxuXHJcbmV4cG9ydCBjbGFzcyBDbGVhbk5vdGlmaWNhdGlvbiBpbXBsZW1lbnRzIEFjdGlvbiB7XHJcblx0cmVhZG9ubHkgdHlwZSA9IE5vdGlmaWNhdGlvblR5cGVzLkNsZWFuTm90aWZpY2F0aW9uO1xyXG59XHJcblxyXG4iXX0=