/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Store, select } from '@ngrx/store';
import { Injectable, Inject } from '@angular/core';
import { CleanNotification, AddANotification } from '../../ngrx/actions/notifications/notifications.actions';
import * as i0 from "@angular/core";
import * as i1 from "@ngrx/store";
var NotificationService = /** @class */ (function () {
    function NotificationService(store) {
        var _this = this;
        this.store = store;
        this.state = {
            type: '',
            message: '',
            displaying: false
        };
        this.store$ = store.pipe(select('notification'));
        this.store$.subscribe(function (state) {
            _this.state = state;
        });
    }
    /**
     * @return {?}
     */
    NotificationService.prototype.cleanMessages = /**
     * @return {?}
     */
    function () {
        this.store.dispatch(new CleanNotification());
    };
    /**
     * @param {?} notification
     * @return {?}
     */
    NotificationService.prototype.addMessage = /**
     * @param {?} notification
     * @return {?}
     */
    function (notification) {
        this.store.dispatch(new AddANotification(notification));
    };
    NotificationService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    NotificationService.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [Store,] }] }
    ]; };
    /** @nocollapse */ NotificationService.ngInjectableDef = i0.defineInjectable({ factory: function NotificationService_Factory() { return new NotificationService(i0.inject(i1.Store)); }, token: NotificationService, providedIn: "root" });
    return NotificationService;
}());
export { NotificationService };
if (false) {
    /** @type {?} */
    NotificationService.prototype.state;
    /** @type {?} */
    NotificationService.prototype.store$;
    /** @type {?} */
    NotificationService.prototype.store;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9uLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9zb2Zpc2Etbm90aWZpY2F0aW9uLyIsInNvdXJjZXMiOlsibGliL3NlcnZpY2VzL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24uc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQ0EsT0FBTyxFQUFDLEtBQUssRUFBRSxNQUFNLEVBQUMsTUFBTSxhQUFhLENBQUM7QUFDMUMsT0FBTyxFQUFDLFVBQVUsRUFBRSxNQUFNLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDakQsT0FBTyxFQUFDLGlCQUFpQixFQUFFLGdCQUFnQixFQUFDLE1BQU0sd0RBQXdELENBQUM7OztBQUUzRztJQWFDLDZCQUFrQyxLQUFLO1FBQXZDLGlCQU1DO1FBTmlDLFVBQUssR0FBTCxLQUFLLENBQUE7UUFSaEMsVUFBSyxHQUFRO1lBQ25CLElBQUksRUFBRSxFQUFFO1lBQ1IsT0FBTyxFQUFFLEVBQUU7WUFDWCxVQUFVLEVBQUUsS0FBSztTQUNqQixDQUFDO1FBS0QsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO1FBRWpELElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLFVBQUMsS0FBVTtZQUNoQyxLQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztRQUNwQixDQUFDLENBQUMsQ0FBQztJQUNKLENBQUM7Ozs7SUFFRCwyQ0FBYTs7O0lBQWI7UUFDQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxJQUFJLGlCQUFpQixFQUFFLENBQUMsQ0FBQztJQUM5QyxDQUFDOzs7OztJQUVELHdDQUFVOzs7O0lBQVYsVUFBVyxZQUErQztRQUN6RCxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxJQUFJLGdCQUFnQixDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7SUFDekQsQ0FBQzs7Z0JBM0JELFVBQVUsU0FBQztvQkFDWCxVQUFVLEVBQUUsTUFBTTtpQkFDbEI7Ozs7Z0RBV2EsTUFBTSxTQUFDLEtBQUs7Ozs4QkFsQjFCO0NBaUNDLEFBNUJELElBNEJDO1NBeEJZLG1CQUFtQjs7O0lBQy9CLG9DQUlFOztJQUVGLHFDQUF3Qjs7SUFFWixvQ0FBMkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge09ic2VydmFibGV9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHtTdG9yZSwgc2VsZWN0fSBmcm9tICdAbmdyeC9zdG9yZSc7XG5pbXBvcnQge0luamVjdGFibGUsIEluamVjdH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0NsZWFuTm90aWZpY2F0aW9uLCBBZGRBTm90aWZpY2F0aW9ufSBmcm9tICcuLi8uLi9uZ3J4L2FjdGlvbnMvbm90aWZpY2F0aW9ucy9ub3RpZmljYXRpb25zLmFjdGlvbnMnO1xuXG5ASW5qZWN0YWJsZSh7XG5cdHByb3ZpZGVkSW46ICdyb290J1xufSlcblxuZXhwb3J0IGNsYXNzIE5vdGlmaWNhdGlvblNlcnZpY2Uge1xuXHRwdWJsaWMgc3RhdGU6IGFueSA9IHtcblx0XHR0eXBlOiAnJyxcblx0XHRtZXNzYWdlOiAnJyxcblx0XHRkaXNwbGF5aW5nOiBmYWxzZVxuXHR9O1xuXG5cdHN0b3JlJDogT2JzZXJ2YWJsZTxhbnk+O1xuXG5cdGNvbnN0cnVjdG9yKEBJbmplY3QoU3RvcmUpIHB1YmxpYyBzdG9yZSkge1xuXHRcdHRoaXMuc3RvcmUkID0gc3RvcmUucGlwZShzZWxlY3QoJ25vdGlmaWNhdGlvbicpKTtcblxuXHRcdHRoaXMuc3RvcmUkLnN1YnNjcmliZSgoc3RhdGU6IGFueSkgPT4ge1xuXHRcdFx0dGhpcy5zdGF0ZSA9IHN0YXRlO1xuXHRcdH0pO1xuXHR9XG5cdFxuXHRjbGVhbk1lc3NhZ2VzKCkge1xuXHRcdHRoaXMuc3RvcmUuZGlzcGF0Y2gobmV3IENsZWFuTm90aWZpY2F0aW9uKCkpO1xuXHR9XG5cdFxuXHRhZGRNZXNzYWdlKG5vdGlmaWNhdGlvbjogeyB0eXBlOiBzdHJpbmcsIG1lc3NhZ2U6IHN0cmluZyB9KSB7XG5cdFx0dGhpcy5zdG9yZS5kaXNwYXRjaChuZXcgQWRkQU5vdGlmaWNhdGlvbihub3RpZmljYXRpb24pKTtcblx0fVxufVxuIl19