/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificationComponent } from './notification.component';
var NotificationModule = /** @class */ (function () {
    function NotificationModule() {
    }
    NotificationModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [NotificationComponent],
                    imports: [
                        CommonModule
                    ],
                    exports: [NotificationComponent]
                },] }
    ];
    return NotificationModule;
}());
export { NotificationModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9uLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3NvZmlzYS1ub3RpZmljYXRpb24vIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFDLFFBQVEsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUN2QyxPQUFPLEVBQUMsWUFBWSxFQUFDLE1BQU0saUJBQWlCLENBQUM7QUFDN0MsT0FBTyxFQUFDLHFCQUFxQixFQUFDLE1BQU0sMEJBQTBCLENBQUM7QUFFL0Q7SUFBQTtJQVFpQyxDQUFDOztnQkFSakMsUUFBUSxTQUFDO29CQUNULFlBQVksRUFBRSxDQUFDLHFCQUFxQixDQUFDO29CQUNyQyxPQUFPLEVBQUU7d0JBQ1IsWUFBWTtxQkFDWjtvQkFDRCxPQUFPLEVBQUUsQ0FBQyxxQkFBcUIsQ0FBQztpQkFDaEM7O0lBRWdDLHlCQUFDO0NBQUEsQUFSbEMsSUFRa0M7U0FBckIsa0JBQWtCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtOZ01vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0NvbW1vbk1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7Tm90aWZpY2F0aW9uQ29tcG9uZW50fSBmcm9tICcuL25vdGlmaWNhdGlvbi5jb21wb25lbnQnO1xuXG5ATmdNb2R1bGUoe1xuXHRkZWNsYXJhdGlvbnM6IFtOb3RpZmljYXRpb25Db21wb25lbnRdLFxuXHRpbXBvcnRzOiBbXG5cdFx0Q29tbW9uTW9kdWxlXG5cdF0sXG5cdGV4cG9ydHM6IFtOb3RpZmljYXRpb25Db21wb25lbnRdXG59KVxuXG5leHBvcnQgY2xhc3MgTm90aWZpY2F0aW9uTW9kdWxlIHt9XG4iXX0=