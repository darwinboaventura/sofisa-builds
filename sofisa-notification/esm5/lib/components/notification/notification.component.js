/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
import { NotificationService } from '../../services/notification/notification.service';
var NotificationComponent = /** @class */ (function () {
    function NotificationComponent(notificationService) {
        this.notificationService = notificationService;
        this.type = 'primary';
        this.message = '';
        this.displaying = true;
    }
    /**
     * @return {?}
     */
    NotificationComponent.prototype.closeAlert = /**
     * @return {?}
     */
    function () {
        this.notificationService.cleanMessages();
    };
    NotificationComponent.decorators = [
        { type: Component, args: [{
                    selector: 'so-notification',
                    template: "<div class=\"so-notification clearfix\" *ngIf=\"displaying\" [ngClass]=\"{ 'so-notification--success': type === 'success', 'so-notification--warning': type === 'warning', 'so-notification--error': type === 'error'}\">\n\t<div class=\"so-notification--icon clearfix\">\n\t\t<i class=\"fas fa-thumbs-up\" *ngIf=\"type !== 'warning' || type !== 'error'\"></i>\n\t\t\n\t\t<i class=\"fas fa-exclamation\" *ngIf=\"type === 'warning' || type === 'error'\"></i>\n\t</div>\n\t\n\t<div class=\"so-notification--message clearfix\">\n\t\t{{message}}\n\t</div>\n\t\n\t<div class=\"so-notification--close clearfix\" (click)=\"closeAlert()\">\n\t\t<i class=\"fas fa-times\"></i>\n\t</div>\n</div>",
                    styles: [".so-notification,.so-shadow--1{box-shadow:0 2px 4px 0 rgba(0,0,0,.1)}.so-shadow--2{box-shadow:0 5px 8px 0 rgba(0,0,0,.1)}.so-shadow--3{box-shadow:0 9px 10px 0 rgba(0,0,0,.1)}.so-shadow--4{box-shadow:0 20px 30px 0 rgba(0,0,0,.1)}.so-shadow--5{box-shadow:0 30px 30px 0 rgba(0,0,0,.1)}.so-shadow--6{box-shadow:0 50px 50px 0 rgba(0,0,0,.1)}.so-notification{width:546px;height:50px;position:fixed;top:25px;right:0;z-index:1000;background-color:#fff}.so-notification--icon i,.so-notification--message{position:absolute;top:50%;left:50%;-webkit-transform:translate(-50%,-50%);transform:translate(-50%,-50%)}.so-notification--icon{width:50px;height:100%;position:relative;background-color:#2d4ef5}.so-notification--icon i{color:#fff}.so-notification--message{width:75%;display:table;font-family:Nunito;font-size:16px;color:#2d4ef5;line-height:24px}.so-notification--close{position:absolute;top:50%;right:15px;-webkit-transform:translateY(-50%);transform:translateY(-50%);cursor:pointer}.so-notification--success .so-notification--icon{background-color:#4ce2a7}.so-notification--success .so-notification--message{color:#4ce2a7}.so-notification--warning .so-notification--icon{background-color:#f1a153}.so-notification--warning .so-notification--message{color:#f1a153}.so-notification--error .so-notification--icon{background-color:#e24c4c}.so-notification--error .so-notification--message{color:#e24c4c}"]
                }] }
    ];
    /** @nocollapse */
    NotificationComponent.ctorParameters = function () { return [
        { type: NotificationService }
    ]; };
    NotificationComponent.propDecorators = {
        type: [{ type: Input }],
        message: [{ type: Input }],
        displaying: [{ type: Input }]
    };
    return NotificationComponent;
}());
export { NotificationComponent };
if (false) {
    /** @type {?} */
    NotificationComponent.prototype.type;
    /** @type {?} */
    NotificationComponent.prototype.message;
    /** @type {?} */
    NotificationComponent.prototype.displaying;
    /** @type {?} */
    NotificationComponent.prototype.notificationService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9uLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3NvZmlzYS1ub3RpZmljYXRpb24vIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFDLFNBQVMsRUFBRSxLQUFLLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDL0MsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sa0RBQWtELENBQUM7QUFFdkY7SUFXQywrQkFBbUIsbUJBQXdDO1FBQXhDLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUFKbEQsU0FBSSxHQUFXLFNBQVMsQ0FBQztRQUN6QixZQUFPLEdBQVcsRUFBRSxDQUFDO1FBQ3JCLGVBQVUsR0FBWSxJQUFJLENBQUM7SUFFMEIsQ0FBQzs7OztJQUUvRCwwQ0FBVTs7O0lBQVY7UUFDQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsYUFBYSxFQUFFLENBQUM7SUFDMUMsQ0FBQzs7Z0JBZkQsU0FBUyxTQUFDO29CQUNWLFFBQVEsRUFBRSxpQkFBaUI7b0JBQzNCLHFyQkFBNEM7O2lCQUU1Qzs7OztnQkFOUSxtQkFBbUI7Ozt1QkFTMUIsS0FBSzswQkFDTCxLQUFLOzZCQUNMLEtBQUs7O0lBT1AsNEJBQUM7Q0FBQSxBQWhCRCxJQWdCQztTQVZZLHFCQUFxQjs7O0lBQ2pDLHFDQUFrQzs7SUFDbEMsd0NBQThCOztJQUM5QiwyQ0FBb0M7O0lBRXhCLG9EQUErQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBJbnB1dH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5zZXJ2aWNlJztcblxuQENvbXBvbmVudCh7XG5cdHNlbGVjdG9yOiAnc28tbm90aWZpY2F0aW9uJyxcblx0dGVtcGxhdGVVcmw6ICcuL25vdGlmaWNhdGlvbi5jb21wb25lbnQuaHRtbCcsXG5cdHN0eWxlVXJsczogWycuL25vdGlmaWNhdGlvbi5jb21wb25lbnQuc2NzcyddXG59KVxuXG5leHBvcnQgY2xhc3MgTm90aWZpY2F0aW9uQ29tcG9uZW50IHtcblx0QElucHV0KCkgdHlwZTogU3RyaW5nID0gJ3ByaW1hcnknO1xuXHRASW5wdXQoKSBtZXNzYWdlOiBTdHJpbmcgPSAnJztcblx0QElucHV0KCkgZGlzcGxheWluZzogQm9vbGVhbiA9IHRydWU7XG5cdFxuXHRjb25zdHJ1Y3RvcihwdWJsaWMgbm90aWZpY2F0aW9uU2VydmljZTogTm90aWZpY2F0aW9uU2VydmljZSkge31cblx0XG5cdGNsb3NlQWxlcnQoKSB7XG5cdFx0dGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmNsZWFuTWVzc2FnZXMoKTtcblx0fVxufVxuIl19