import { Observable } from 'rxjs';
export declare class NotificationService {
    store: any;
    state: any;
    store$: Observable<any>;
    constructor(store: any);
    cleanMessages(): void;
    addMessage(notification: {
        type: string;
        message: string;
    }): void;
}
