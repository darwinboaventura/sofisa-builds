import { NotificationService } from '../../services/notification/notification.service';
export declare class NotificationComponent {
    notificationService: NotificationService;
    type: String;
    message: String;
    displaying: Boolean;
    constructor(notificationService: NotificationService);
    closeAlert(): void;
}
