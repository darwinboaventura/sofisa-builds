import { Action } from '@ngrx/store';
export declare enum NotificationTypes {
    AddANotification = "[AppComponent] AddANotification",
    CleanNotification = "[AppComponent] CleanNotification"
}
export declare class AddANotification implements Action {
    payload: {
        type: any;
        message: any;
    };
    readonly type = NotificationTypes.AddANotification;
    constructor(payload: {
        type: any;
        message: any;
    });
}
export declare class CleanNotification implements Action {
    readonly type = NotificationTypes.CleanNotification;
}
