export declare const notificationDefaultState: {
    'type': string;
    'message': string;
    'displaying': boolean;
};
export declare function notificationReducer(state: {
    'type': string;
    'message': string;
    'displaying': boolean;
}, action: any): any;
