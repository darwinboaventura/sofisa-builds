/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
// Services
export { NotificationService } from './lib/services/notification/notification.service';
// Components
export { NotificationModule } from './lib/components/notification/notification.module';
// Actions
export { NotificationTypes, AddANotification, CleanNotification } from './lib/ngrx/actions/notifications/notifications.actions';
// Reducers
export { notificationReducer, notificationDefaultState } from './lib/ngrx/reducers/notification/notification.reducer';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHVibGljX2FwaS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3NvZmlzYS1ub3RpZmljYXRpb24vIiwic291cmNlcyI6WyJwdWJsaWNfYXBpLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQ0Esb0NBQWMsa0RBQWtELENBQUM7O0FBR2pFLG1DQUFjLG1EQUFtRCxDQUFDOztBQUdsRSx1RUFBYyx3REFBd0QsQ0FBQzs7QUFHdkUsOERBQWMsdURBQXVELENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBTZXJ2aWNlc1xuZXhwb3J0ICogZnJvbSAnLi9saWIvc2VydmljZXMvbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5zZXJ2aWNlJztcblxuLy8gQ29tcG9uZW50c1xuZXhwb3J0ICogZnJvbSAnLi9saWIvY29tcG9uZW50cy9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLm1vZHVsZSc7XG5cbi8vIEFjdGlvbnNcbmV4cG9ydCAqIGZyb20gJy4vbGliL25ncngvYWN0aW9ucy9ub3RpZmljYXRpb25zL25vdGlmaWNhdGlvbnMuYWN0aW9ucyc7XG5cbi8vIFJlZHVjZXJzXG5leHBvcnQgKiBmcm9tICcuL2xpYi9uZ3J4L3JlZHVjZXJzL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24ucmVkdWNlcic7XG4iXX0=