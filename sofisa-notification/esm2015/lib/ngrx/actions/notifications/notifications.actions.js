/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
const NotificationTypes = {
    AddANotification: '[AppComponent] AddANotification',
    CleanNotification: '[AppComponent] CleanNotification',
};
export { NotificationTypes };
export class AddANotification {
    /**
     * @param {?} payload
     */
    constructor(payload) {
        this.payload = payload;
        this.type = NotificationTypes.AddANotification;
    }
}
if (false) {
    /** @type {?} */
    AddANotification.prototype.type;
    /** @type {?} */
    AddANotification.prototype.payload;
}
export class CleanNotification {
    constructor() {
        this.type = NotificationTypes.CleanNotification;
    }
}
if (false) {
    /** @type {?} */
    CleanNotification.prototype.type;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9ucy5hY3Rpb25zLmpzIiwic291cmNlUm9vdCI6Im5nOi8vc29maXNhLW5vdGlmaWNhdGlvbi8iLCJzb3VyY2VzIjpbImxpYi9uZ3J4L2FjdGlvbnMvbm90aWZpY2F0aW9ucy9ub3RpZmljYXRpb25zLmFjdGlvbnMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0lBR0Msa0JBQW1CLGlDQUFpQztJQUNwRCxtQkFBb0Isa0NBQWtDOzs7QUFHdkQsTUFBTSxPQUFPLGdCQUFnQjs7OztJQUc1QixZQUFtQixPQUFtQztRQUFuQyxZQUFPLEdBQVAsT0FBTyxDQUE0QjtRQUY3QyxTQUFJLEdBQUcsaUJBQWlCLENBQUMsZ0JBQWdCLENBQUM7SUFFTSxDQUFDO0NBQzFEOzs7SUFIQSxnQ0FBbUQ7O0lBRXZDLG1DQUEwQzs7QUFHdkQsTUFBTSxPQUFPLGlCQUFpQjtJQUE5QjtRQUNVLFNBQUksR0FBRyxpQkFBaUIsQ0FBQyxpQkFBaUIsQ0FBQztJQUNyRCxDQUFDO0NBQUE7OztJQURBLGlDQUFvRCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7QWN0aW9ufSBmcm9tICdAbmdyeC9zdG9yZSc7XHJcblxyXG5leHBvcnQgZW51bSBOb3RpZmljYXRpb25UeXBlcyB7XHJcblx0QWRkQU5vdGlmaWNhdGlvbiA9ICdbQXBwQ29tcG9uZW50XSBBZGRBTm90aWZpY2F0aW9uJyxcclxuXHRDbGVhbk5vdGlmaWNhdGlvbiA9ICdbQXBwQ29tcG9uZW50XSBDbGVhbk5vdGlmaWNhdGlvbidcclxufVxyXG5cclxuZXhwb3J0IGNsYXNzIEFkZEFOb3RpZmljYXRpb24gaW1wbGVtZW50cyBBY3Rpb24ge1xyXG5cdHJlYWRvbmx5IHR5cGUgPSBOb3RpZmljYXRpb25UeXBlcy5BZGRBTm90aWZpY2F0aW9uO1xyXG5cdFxyXG5cdGNvbnN0cnVjdG9yKHB1YmxpYyBwYXlsb2FkOiB7IHR5cGU6IGFueSwgbWVzc2FnZTogYW55fSkge31cclxufVxyXG5cclxuZXhwb3J0IGNsYXNzIENsZWFuTm90aWZpY2F0aW9uIGltcGxlbWVudHMgQWN0aW9uIHtcclxuXHRyZWFkb25seSB0eXBlID0gTm90aWZpY2F0aW9uVHlwZXMuQ2xlYW5Ob3RpZmljYXRpb247XHJcbn1cclxuXHJcbiJdfQ==