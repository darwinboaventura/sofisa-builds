/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NotificationTypes } from '../../actions/notifications/notifications.actions';
/** @type {?} */
export const notificationDefaultState = {
    'type': '',
    'message': '',
    'displaying': false
};
/**
 * @param {?=} state
 * @param {?=} action
 * @return {?}
 */
export function notificationReducer(state = notificationDefaultState, action) {
    switch (action.type) {
        case NotificationTypes.AddANotification:
            return Object.assign({}, action.payload, {
                displaying: true
            });
        case NotificationTypes.CleanNotification:
            return notificationDefaultState;
        default:
            return state;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9uLnJlZHVjZXIuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9zb2Zpc2Etbm90aWZpY2F0aW9uLyIsInNvdXJjZXMiOlsibGliL25ncngvcmVkdWNlcnMvbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5yZWR1Y2VyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsaUJBQWlCLEVBQUMsTUFBTSxtREFBbUQsQ0FBQzs7QUFFcEYsTUFBTSxPQUFPLHdCQUF3QixHQUFHO0lBQ3ZDLE1BQU0sRUFBRSxFQUFFO0lBQ1YsU0FBUyxFQUFFLEVBQUU7SUFDYixZQUFZLEVBQUUsS0FBSztDQUNuQjs7Ozs7O0FBRUQsTUFBTSxVQUFVLG1CQUFtQixDQUFDLEtBQUssR0FBRyx3QkFBd0IsRUFBRSxNQUFXO0lBQ2hGLFFBQVEsTUFBTSxDQUFDLElBQUksRUFBRTtRQUNwQixLQUFLLGlCQUFpQixDQUFDLGdCQUFnQjtZQUN0QyxPQUFPLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLE1BQU0sQ0FBQyxPQUFPLEVBQUU7Z0JBQ3hDLFVBQVUsRUFBRSxJQUFJO2FBQ2hCLENBQUMsQ0FBQztRQUNKLEtBQUssaUJBQWlCLENBQUMsaUJBQWlCO1lBQ3ZDLE9BQU8sd0JBQXdCLENBQUM7UUFDakM7WUFDQyxPQUFPLEtBQUssQ0FBQztLQUNkO0FBQ0YsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Tm90aWZpY2F0aW9uVHlwZXN9IGZyb20gJy4uLy4uL2FjdGlvbnMvbm90aWZpY2F0aW9ucy9ub3RpZmljYXRpb25zLmFjdGlvbnMnO1xyXG5cclxuZXhwb3J0IGNvbnN0IG5vdGlmaWNhdGlvbkRlZmF1bHRTdGF0ZSA9IHtcclxuXHQndHlwZSc6ICcnLFxyXG5cdCdtZXNzYWdlJzogJycsXHJcblx0J2Rpc3BsYXlpbmcnOiBmYWxzZVxyXG59O1xyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIG5vdGlmaWNhdGlvblJlZHVjZXIoc3RhdGUgPSBub3RpZmljYXRpb25EZWZhdWx0U3RhdGUsIGFjdGlvbjogYW55KSB7XHJcblx0c3dpdGNoIChhY3Rpb24udHlwZSkge1xyXG5cdFx0Y2FzZSBOb3RpZmljYXRpb25UeXBlcy5BZGRBTm90aWZpY2F0aW9uOlxyXG5cdFx0XHRyZXR1cm4gT2JqZWN0LmFzc2lnbih7fSwgYWN0aW9uLnBheWxvYWQsIHtcclxuXHRcdFx0XHRkaXNwbGF5aW5nOiB0cnVlXHJcblx0XHRcdH0pO1xyXG5cdFx0Y2FzZSBOb3RpZmljYXRpb25UeXBlcy5DbGVhbk5vdGlmaWNhdGlvbjpcclxuXHRcdFx0cmV0dXJuIG5vdGlmaWNhdGlvbkRlZmF1bHRTdGF0ZTtcclxuXHRcdGRlZmF1bHQ6XHJcblx0XHRcdHJldHVybiBzdGF0ZTtcclxuXHR9XHJcbn1cclxuXHJcblxyXG4iXX0=