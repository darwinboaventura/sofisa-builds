/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Store, select } from '@ngrx/store';
import { Injectable, Inject } from '@angular/core';
import { CleanNotification, AddANotification } from '../../ngrx/actions/notifications/notifications.actions';
import * as i0 from "@angular/core";
import * as i1 from "@ngrx/store";
export class NotificationService {
    /**
     * @param {?} store
     */
    constructor(store) {
        this.store = store;
        this.state = {
            type: '',
            message: '',
            displaying: false
        };
        this.store$ = store.pipe(select('notification'));
        this.store$.subscribe((state) => {
            this.state = state;
        });
    }
    /**
     * @return {?}
     */
    cleanMessages() {
        this.store.dispatch(new CleanNotification());
    }
    /**
     * @param {?} notification
     * @return {?}
     */
    addMessage(notification) {
        this.store.dispatch(new AddANotification(notification));
    }
}
NotificationService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
NotificationService.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [Store,] }] }
];
/** @nocollapse */ NotificationService.ngInjectableDef = i0.defineInjectable({ factory: function NotificationService_Factory() { return new NotificationService(i0.inject(i1.Store)); }, token: NotificationService, providedIn: "root" });
if (false) {
    /** @type {?} */
    NotificationService.prototype.state;
    /** @type {?} */
    NotificationService.prototype.store$;
    /** @type {?} */
    NotificationService.prototype.store;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9uLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9zb2Zpc2Etbm90aWZpY2F0aW9uLyIsInNvdXJjZXMiOlsibGliL3NlcnZpY2VzL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24uc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQ0EsT0FBTyxFQUFDLEtBQUssRUFBRSxNQUFNLEVBQUMsTUFBTSxhQUFhLENBQUM7QUFDMUMsT0FBTyxFQUFDLFVBQVUsRUFBRSxNQUFNLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDakQsT0FBTyxFQUFDLGlCQUFpQixFQUFFLGdCQUFnQixFQUFDLE1BQU0sd0RBQXdELENBQUM7OztBQU0zRyxNQUFNLE9BQU8sbUJBQW1COzs7O0lBUy9CLFlBQWtDLEtBQUs7UUFBTCxVQUFLLEdBQUwsS0FBSyxDQUFBO1FBUmhDLFVBQUssR0FBUTtZQUNuQixJQUFJLEVBQUUsRUFBRTtZQUNSLE9BQU8sRUFBRSxFQUFFO1lBQ1gsVUFBVSxFQUFFLEtBQUs7U0FDakIsQ0FBQztRQUtELElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztRQUVqRCxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEtBQVUsRUFBRSxFQUFFO1lBQ3BDLElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO1FBQ3BCLENBQUMsQ0FBQyxDQUFDO0lBQ0osQ0FBQzs7OztJQUVELGFBQWE7UUFDWixJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxJQUFJLGlCQUFpQixFQUFFLENBQUMsQ0FBQztJQUM5QyxDQUFDOzs7OztJQUVELFVBQVUsQ0FBQyxZQUErQztRQUN6RCxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxJQUFJLGdCQUFnQixDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7SUFDekQsQ0FBQzs7O1lBM0JELFVBQVUsU0FBQztnQkFDWCxVQUFVLEVBQUUsTUFBTTthQUNsQjs7Ozs0Q0FXYSxNQUFNLFNBQUMsS0FBSzs7Ozs7SUFSekIsb0NBSUU7O0lBRUYscUNBQXdCOztJQUVaLG9DQUEyQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7T2JzZXJ2YWJsZX0gZnJvbSAncnhqcyc7XG5pbXBvcnQge1N0b3JlLCBzZWxlY3R9IGZyb20gJ0BuZ3J4L3N0b3JlJztcbmltcG9ydCB7SW5qZWN0YWJsZSwgSW5qZWN0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7Q2xlYW5Ob3RpZmljYXRpb24sIEFkZEFOb3RpZmljYXRpb259IGZyb20gJy4uLy4uL25ncngvYWN0aW9ucy9ub3RpZmljYXRpb25zL25vdGlmaWNhdGlvbnMuYWN0aW9ucyc7XG5cbkBJbmplY3RhYmxlKHtcblx0cHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuXG5leHBvcnQgY2xhc3MgTm90aWZpY2F0aW9uU2VydmljZSB7XG5cdHB1YmxpYyBzdGF0ZTogYW55ID0ge1xuXHRcdHR5cGU6ICcnLFxuXHRcdG1lc3NhZ2U6ICcnLFxuXHRcdGRpc3BsYXlpbmc6IGZhbHNlXG5cdH07XG5cblx0c3RvcmUkOiBPYnNlcnZhYmxlPGFueT47XG5cblx0Y29uc3RydWN0b3IoQEluamVjdChTdG9yZSkgcHVibGljIHN0b3JlKSB7XG5cdFx0dGhpcy5zdG9yZSQgPSBzdG9yZS5waXBlKHNlbGVjdCgnbm90aWZpY2F0aW9uJykpO1xuXG5cdFx0dGhpcy5zdG9yZSQuc3Vic2NyaWJlKChzdGF0ZTogYW55KSA9PiB7XG5cdFx0XHR0aGlzLnN0YXRlID0gc3RhdGU7XG5cdFx0fSk7XG5cdH1cblx0XG5cdGNsZWFuTWVzc2FnZXMoKSB7XG5cdFx0dGhpcy5zdG9yZS5kaXNwYXRjaChuZXcgQ2xlYW5Ob3RpZmljYXRpb24oKSk7XG5cdH1cblx0XG5cdGFkZE1lc3NhZ2Uobm90aWZpY2F0aW9uOiB7IHR5cGU6IHN0cmluZywgbWVzc2FnZTogc3RyaW5nIH0pIHtcblx0XHR0aGlzLnN0b3JlLmRpc3BhdGNoKG5ldyBBZGRBTm90aWZpY2F0aW9uKG5vdGlmaWNhdGlvbikpO1xuXHR9XG59XG4iXX0=