/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
import { NotificationService } from '../../services/notification/notification.service';
export class NotificationComponent {
    /**
     * @param {?} notificationService
     */
    constructor(notificationService) {
        this.notificationService = notificationService;
        this.type = 'primary';
        this.message = '';
        this.displaying = true;
    }
    /**
     * @return {?}
     */
    closeAlert() {
        this.notificationService.cleanMessages();
    }
}
NotificationComponent.decorators = [
    { type: Component, args: [{
                selector: 'so-notification',
                template: "<div class=\"so-notification clearfix\" *ngIf=\"displaying\" [ngClass]=\"{ 'so-notification--success': type === 'success', 'so-notification--warning': type === 'warning', 'so-notification--error': type === 'error'}\">\n\t<div class=\"so-notification--icon clearfix\">\n\t\t<i class=\"fas fa-thumbs-up\" *ngIf=\"type !== 'warning' || type !== 'error'\"></i>\n\t\t\n\t\t<i class=\"fas fa-exclamation\" *ngIf=\"type === 'warning' || type === 'error'\"></i>\n\t</div>\n\t\n\t<div class=\"so-notification--message clearfix\">\n\t\t{{message}}\n\t</div>\n\t\n\t<div class=\"so-notification--close clearfix\" (click)=\"closeAlert()\">\n\t\t<i class=\"fas fa-times\"></i>\n\t</div>\n</div>",
                styles: [".so-notification,.so-shadow--1{box-shadow:0 2px 4px 0 rgba(0,0,0,.1)}.so-shadow--2{box-shadow:0 5px 8px 0 rgba(0,0,0,.1)}.so-shadow--3{box-shadow:0 9px 10px 0 rgba(0,0,0,.1)}.so-shadow--4{box-shadow:0 20px 30px 0 rgba(0,0,0,.1)}.so-shadow--5{box-shadow:0 30px 30px 0 rgba(0,0,0,.1)}.so-shadow--6{box-shadow:0 50px 50px 0 rgba(0,0,0,.1)}.so-notification{width:546px;height:50px;position:fixed;top:25px;right:0;z-index:1000;background-color:#fff}.so-notification--icon i,.so-notification--message{position:absolute;top:50%;left:50%;-webkit-transform:translate(-50%,-50%);transform:translate(-50%,-50%)}.so-notification--icon{width:50px;height:100%;position:relative;background-color:#2d4ef5}.so-notification--icon i{color:#fff}.so-notification--message{width:75%;display:table;font-family:Nunito;font-size:16px;color:#2d4ef5;line-height:24px}.so-notification--close{position:absolute;top:50%;right:15px;-webkit-transform:translateY(-50%);transform:translateY(-50%);cursor:pointer}.so-notification--success .so-notification--icon{background-color:#4ce2a7}.so-notification--success .so-notification--message{color:#4ce2a7}.so-notification--warning .so-notification--icon{background-color:#f1a153}.so-notification--warning .so-notification--message{color:#f1a153}.so-notification--error .so-notification--icon{background-color:#e24c4c}.so-notification--error .so-notification--message{color:#e24c4c}"]
            }] }
];
/** @nocollapse */
NotificationComponent.ctorParameters = () => [
    { type: NotificationService }
];
NotificationComponent.propDecorators = {
    type: [{ type: Input }],
    message: [{ type: Input }],
    displaying: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    NotificationComponent.prototype.type;
    /** @type {?} */
    NotificationComponent.prototype.message;
    /** @type {?} */
    NotificationComponent.prototype.displaying;
    /** @type {?} */
    NotificationComponent.prototype.notificationService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9uLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3NvZmlzYS1ub3RpZmljYXRpb24vIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFDLFNBQVMsRUFBRSxLQUFLLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDL0MsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sa0RBQWtELENBQUM7QUFRdkYsTUFBTSxPQUFPLHFCQUFxQjs7OztJQUtqQyxZQUFtQixtQkFBd0M7UUFBeEMsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUpsRCxTQUFJLEdBQVcsU0FBUyxDQUFDO1FBQ3pCLFlBQU8sR0FBVyxFQUFFLENBQUM7UUFDckIsZUFBVSxHQUFZLElBQUksQ0FBQztJQUUwQixDQUFDOzs7O0lBRS9ELFVBQVU7UUFDVCxJQUFJLENBQUMsbUJBQW1CLENBQUMsYUFBYSxFQUFFLENBQUM7SUFDMUMsQ0FBQzs7O1lBZkQsU0FBUyxTQUFDO2dCQUNWLFFBQVEsRUFBRSxpQkFBaUI7Z0JBQzNCLHFyQkFBNEM7O2FBRTVDOzs7O1lBTlEsbUJBQW1COzs7bUJBUzFCLEtBQUs7c0JBQ0wsS0FBSzt5QkFDTCxLQUFLOzs7O0lBRk4scUNBQWtDOztJQUNsQyx3Q0FBOEI7O0lBQzlCLDJDQUFvQzs7SUFFeEIsb0RBQStDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIElucHV0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcblx0c2VsZWN0b3I6ICdzby1ub3RpZmljYXRpb24nLFxuXHR0ZW1wbGF0ZVVybDogJy4vbm90aWZpY2F0aW9uLmNvbXBvbmVudC5odG1sJyxcblx0c3R5bGVVcmxzOiBbJy4vbm90aWZpY2F0aW9uLmNvbXBvbmVudC5zY3NzJ11cbn0pXG5cbmV4cG9ydCBjbGFzcyBOb3RpZmljYXRpb25Db21wb25lbnQge1xuXHRASW5wdXQoKSB0eXBlOiBTdHJpbmcgPSAncHJpbWFyeSc7XG5cdEBJbnB1dCgpIG1lc3NhZ2U6IFN0cmluZyA9ICcnO1xuXHRASW5wdXQoKSBkaXNwbGF5aW5nOiBCb29sZWFuID0gdHJ1ZTtcblx0XG5cdGNvbnN0cnVjdG9yKHB1YmxpYyBub3RpZmljYXRpb25TZXJ2aWNlOiBOb3RpZmljYXRpb25TZXJ2aWNlKSB7fVxuXHRcblx0Y2xvc2VBbGVydCgpIHtcblx0XHR0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuY2xlYW5NZXNzYWdlcygpO1xuXHR9XG59XG4iXX0=