(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@ngrx/store'), require('@angular/common'), require('@angular/core')) :
    typeof define === 'function' && define.amd ? define('sofisa-notification', ['exports', '@ngrx/store', '@angular/common', '@angular/core'], factory) :
    (factory((global['sofisa-notification'] = {}),global.i1,global.ng.common,global.ng.core));
}(this, (function (exports,i1,common,i0) { 'use strict';

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @enum {string} */
    var NotificationTypes = {
        AddANotification: '[AppComponent] AddANotification',
        CleanNotification: '[AppComponent] CleanNotification',
    };
    var AddANotification = /** @class */ (function () {
        function AddANotification(payload) {
            this.payload = payload;
            this.type = NotificationTypes.AddANotification;
        }
        return AddANotification;
    }());
    var CleanNotification = /** @class */ (function () {
        function CleanNotification() {
            this.type = NotificationTypes.CleanNotification;
        }
        return CleanNotification;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var NotificationService = /** @class */ (function () {
        function NotificationService(store) {
            var _this = this;
            this.store = store;
            this.state = {
                type: '',
                message: '',
                displaying: false
            };
            this.store$ = store.pipe(i1.select('notification'));
            this.store$.subscribe(function (state) {
                _this.state = state;
            });
        }
        /**
         * @return {?}
         */
        NotificationService.prototype.cleanMessages = /**
         * @return {?}
         */
            function () {
                this.store.dispatch(new CleanNotification());
            };
        /**
         * @param {?} notification
         * @return {?}
         */
        NotificationService.prototype.addMessage = /**
         * @param {?} notification
         * @return {?}
         */
            function (notification) {
                this.store.dispatch(new AddANotification(notification));
            };
        NotificationService.decorators = [
            { type: i0.Injectable, args: [{
                        providedIn: 'root'
                    },] }
        ];
        /** @nocollapse */
        NotificationService.ctorParameters = function () {
            return [
                { type: undefined, decorators: [{ type: i0.Inject, args: [i1.Store,] }] }
            ];
        };
        /** @nocollapse */ NotificationService.ngInjectableDef = i0.defineInjectable({ factory: function NotificationService_Factory() { return new NotificationService(i0.inject(i1.Store)); }, token: NotificationService, providedIn: "root" });
        return NotificationService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var NotificationComponent = /** @class */ (function () {
        function NotificationComponent(notificationService) {
            this.notificationService = notificationService;
            this.type = 'primary';
            this.message = '';
            this.displaying = true;
        }
        /**
         * @return {?}
         */
        NotificationComponent.prototype.closeAlert = /**
         * @return {?}
         */
            function () {
                this.notificationService.cleanMessages();
            };
        NotificationComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'so-notification',
                        template: "<div class=\"so-notification clearfix\" *ngIf=\"displaying\" [ngClass]=\"{ 'so-notification--success': type === 'success', 'so-notification--warning': type === 'warning', 'so-notification--error': type === 'error'}\">\n\t<div class=\"so-notification--icon clearfix\">\n\t\t<i class=\"fas fa-thumbs-up\" *ngIf=\"type !== 'warning' || type !== 'error'\"></i>\n\t\t\n\t\t<i class=\"fas fa-exclamation\" *ngIf=\"type === 'warning' || type === 'error'\"></i>\n\t</div>\n\t\n\t<div class=\"so-notification--message clearfix\">\n\t\t{{message}}\n\t</div>\n\t\n\t<div class=\"so-notification--close clearfix\" (click)=\"closeAlert()\">\n\t\t<i class=\"fas fa-times\"></i>\n\t</div>\n</div>",
                        styles: [".so-notification,.so-shadow--1{box-shadow:0 2px 4px 0 rgba(0,0,0,.1)}.so-shadow--2{box-shadow:0 5px 8px 0 rgba(0,0,0,.1)}.so-shadow--3{box-shadow:0 9px 10px 0 rgba(0,0,0,.1)}.so-shadow--4{box-shadow:0 20px 30px 0 rgba(0,0,0,.1)}.so-shadow--5{box-shadow:0 30px 30px 0 rgba(0,0,0,.1)}.so-shadow--6{box-shadow:0 50px 50px 0 rgba(0,0,0,.1)}.so-notification{width:546px;height:50px;position:fixed;top:25px;right:0;z-index:1000;background-color:#fff}.so-notification--icon i,.so-notification--message{position:absolute;top:50%;left:50%;-webkit-transform:translate(-50%,-50%);transform:translate(-50%,-50%)}.so-notification--icon{width:50px;height:100%;position:relative;background-color:#2d4ef5}.so-notification--icon i{color:#fff}.so-notification--message{width:75%;display:table;font-family:Nunito;font-size:16px;color:#2d4ef5;line-height:24px}.so-notification--close{position:absolute;top:50%;right:15px;-webkit-transform:translateY(-50%);transform:translateY(-50%);cursor:pointer}.so-notification--success .so-notification--icon{background-color:#4ce2a7}.so-notification--success .so-notification--message{color:#4ce2a7}.so-notification--warning .so-notification--icon{background-color:#f1a153}.so-notification--warning .so-notification--message{color:#f1a153}.so-notification--error .so-notification--icon{background-color:#e24c4c}.so-notification--error .so-notification--message{color:#e24c4c}"]
                    }] }
        ];
        /** @nocollapse */
        NotificationComponent.ctorParameters = function () {
            return [
                { type: NotificationService }
            ];
        };
        NotificationComponent.propDecorators = {
            type: [{ type: i0.Input }],
            message: [{ type: i0.Input }],
            displaying: [{ type: i0.Input }]
        };
        return NotificationComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var NotificationModule = /** @class */ (function () {
        function NotificationModule() {
        }
        NotificationModule.decorators = [
            { type: i0.NgModule, args: [{
                        declarations: [NotificationComponent],
                        imports: [
                            common.CommonModule
                        ],
                        exports: [NotificationComponent]
                    },] }
        ];
        return NotificationModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var notificationDefaultState = {
        'type': '',
        'message': '',
        'displaying': false
    };
    /**
     * @param {?=} state
     * @param {?=} action
     * @return {?}
     */
    function notificationReducer(state, action) {
        if (state === void 0) {
            state = notificationDefaultState;
        }
        switch (action.type) {
            case NotificationTypes.AddANotification:
                return Object.assign({}, action.payload, {
                    displaying: true
                });
            case NotificationTypes.CleanNotification:
                return notificationDefaultState;
            default:
                return state;
        }
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    exports.NotificationService = NotificationService;
    exports.NotificationModule = NotificationModule;
    exports.NotificationTypes = NotificationTypes;
    exports.AddANotification = AddANotification;
    exports.CleanNotification = CleanNotification;
    exports.notificationReducer = notificationReducer;
    exports.notificationDefaultState = notificationDefaultState;
    exports.ɵa = NotificationComponent;

    Object.defineProperty(exports, '__esModule', { value: true });

})));

//# sourceMappingURL=sofisa-notification.umd.js.map